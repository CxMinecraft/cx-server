﻿using System;
using CxServer.Utils;

namespace CxServer.Inventories
{
    /// <summary>
    /// Inventory is place where are ItemStacks stored.
    /// </summary>
    public class Inventory
    {

        #region Constructors

        /// <summary>
        /// Create a new instance of <see cref="CxServer.Inventories.Inventory"/>.
        /// </summary>
        /// <param name="size">Size of this inventory.</param>
        /// <param name="items">All items in this inventory or <see langword="null"/> for empty.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">Thrown when size does not match lenght of items array</exception>
        public Inventory(int size, params ItemStack[] items)
        {
            if (items == null || items.Length == 0)
                items = new ItemStack[size];
            else if (items.Length > size)
                throw new ArgumentOutOfRangeException("items > size");
            else if (items.Length < size)
                Array.Resize<ItemStack>(ref items, size);

            this.Size = size;
            this._items = items;
        }

        #endregion

        #region Variables

        /// <summary>
        /// Total size of this inventory.
        /// </summary>
        public int Size
        {
            get;
        }

        private readonly ItemStack[] _items;

        /// <summary>
        /// All items in this inventory.<br>
        /// Using and modifying this array is safe (this is only clone).
        /// </summary>
        public ItemStack[] AllItems
        {
            get
            {
                return (ItemStack[])_items.Clone();
            }
        }

        #endregion

        #region Add

        /// <summary>
        /// Add item to inventory.
        /// </summary>
        /// <returns>What was unable to fit inventory.</returns>
        /// <param name="item">Item to add.</param>
        public virtual ItemStack Add(ItemStack item)
        {
            throw new NotImplementedException("Inventory - Add");
        }

        /// <summary>
        /// Add item to inventory in specified range.
        /// </summary>
        /// <returns>What was unable to fit inventory.</returns>
        /// <param name="item">Item to add.</param>
        /// <param name="start">On which position start looking.</param>
        public virtual ItemStack Add(ItemStack item, int start) => Add(item, start, Size - start);

        /// <summary>
        /// Add item to inventory in specified range.
        /// </summary>
        /// <returns>What was unable to fit inventory.</returns>
        /// <param name="item">Item to add.</param>
        /// <param name="start">On which position start looking.</param>
        /// <param name="length">Number of slots to try.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">Thrown when start+length is bigger then intentory size.</exception>
        public virtual ItemStack Add(ItemStack item, int start, int length)
        {
            if (start + length > Size)
                throw new ArgumentOutOfRangeException("length");

            throw new NotImplementedException("Inventory - Add");
        }

        #endregion

        #region Utils

        /// <summary>
        /// Get or set item in this inventory.
        /// </summary>
        public ItemStack this[int id]
        {
            get
            {
                return _items[id];
            }
            set
            {
                if (ItemStackChange == null)
                    _items[id] = value;
                else
                {
                    ItemStack old = _items[id];
                    _items[id] = value;
                    ItemStackChange(id, old, value);
                }
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Called when item in this inventory is changed.
        /// </summary>
        public event ItemStackChangeHandler ItemStackChange;

        /// <summary>
        /// Item stack change handler for <see cref="CxServer.Inventories.Inventory.ItemStackChange"/>.
        /// </summary>
        public delegate void ItemStackChangeHandler(int slot, ItemStack oldItem, ItemStack newItem);

        #endregion
    }
}
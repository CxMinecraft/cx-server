﻿using System;

namespace CxServer.Inventories.Utils
{
    /// <summary>
    /// Inventory for humanoid entities (armor + main hand + off hand).
    /// </summary>
    public class HumanoidInventory : UtilInventory
    {

        #region Constructors

        /// <summary>
        /// Create new instance of <see cref="CxServer.Inventories.Utils.ArmorInventory"/>.
        /// </summary>
        /// <param name="inventory">Parent inventory.</param>
        /// <param name="startIndexArmor">Start index of <see cref="CxServer.Inventories.Utils.ArmorInventory"/> in it's Parent <see cref="CxServer.Inventories.Inventory"/>.</param>
        /// <param name="indexMainHand">Index of <see cref="CxServer.ItemStack"/> in main hand in it's Parent <see cref="CxServer.Inventories.Inventory"/>.</param>
        /// <param name="indexOffHand">Index of <see cref="CxServer.ItemStack"/> in off hand in it's Parent <see cref="CxServer.Inventories.Inventory"/>.</param>
        public HumanoidInventory(Inventory inventory, int startIndexArmor, int indexMainHand, int indexOffHand) : base(inventory, startIndexArmor, 6)
        {
            this.MainHandIndex = indexMainHand;
            this.OffHandIndex = indexOffHand;
        }

        public override ItemStack[] AllItems
        {
            get
            {
                return base.AllItems;
            }
        }

        public override ItemStack this[int id]
        {
            get
            {
                return base[id];
            }
            set
            {
                base[id] = value;
            }
        }

        #endregion

        #region Indexes

        public int MainHandIndex
        {
            get;
        }

        public int OffHandIndex
        {
            get;
        }

        #endregion

        #region Shortcuts

        /// <summary>
        /// Entity's head (helmet).
        /// </summary>
        public ItemStack Head => ParentInventory[StartIndex + 0];
        /// <summary>
        /// Entity's chest (chestplate).
        /// </summary>
        public ItemStack Chest => ParentInventory[StartIndex + 1];
        /// <summary>
        /// Entity's legs (leggings).
        /// </summary>
        public ItemStack Legs => ParentInventory[StartIndex + 2];
        /// <summary>
        /// Entity's feet (boots).
        /// </summary>
        public ItemStack Feet => ParentInventory[StartIndex + 3];

        #endregion


    }
}
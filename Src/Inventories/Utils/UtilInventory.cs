﻿using System;
using CxServer.Utils;
using Utils.Extensions;

namespace CxServer.Inventories.Utils
{
    /// <summary>
    /// Util inventory.<br>
    /// Use this inventory (or it's childs) to make your Inventory easier to use.
    /// </summary>
    public class UtilInventory
    {

        #region Constructors

        /// <summary>
        /// Create a new instance of <see cref="CxServer.Inventories.Utils.UtilInventory"/>.
        /// </summary>
        /// <param name="inventory">Parent inventory.</param>
        /// <param name="startIndex">Start index of this <see cref="CxServer.Inventories.Utils.UtilInventory"/> in it's Parent <see cref="CxServer.Inventories.Inventory"/>.</param>
        /// <param name="size">Size of this <see cref="CxServer.Inventories.Utils.UtilInventory"/>.</param>
        public UtilInventory(Inventory inventory, int startIndex, int size)
        {
            this.ParentInventory = inventory;
            this.StartIndex = startIndex;
            this.Size = size;
        }

        #endregion

        #region Variables

        /// <summary>
        /// Parent <see cref="CxServer.Inventories.Inventory"/>.
        /// </summary>
        public Inventory ParentInventory
        {
            get;
        }

        /// <summary>
        /// Start index of this <see cref="CxServer.Inventories.Utils.UtilInventory"/> in it's Parent <see cref="CxServer.Inventories.Inventory"/>.
        /// </summary>
        public int StartIndex
        {
            get;
        }

        /// <summary>
        /// Size of this <see cref="CxServer.Inventories.Utils.UtilInventory"/>.
        /// </summary>
        public int Size
        {
            get;
        }

        #endregion

        #region Utils

        /// <summary>
        /// All items in this <see cref="CxServer.Inventories.Utils.UtilInventory"/>.
        /// </summary>
        public virtual ItemStack[] AllItems
        {
            get
            {
                return ParentInventory.AllItems.SubArray(StartIndex, Size);
            }
        }

        /// <summary>
        /// Specified item in this <see cref="CxServer.Inventories.Utils.UtilInventory"/>.
        /// </summary>
        /// <exception cref="System.ArgumentOutOfRangeException">Thrown when ID is not in this <see cref="CxServer.Inventories.Utils.UtilInventory"/>.</exception>
        public virtual ItemStack this[int id]
        {
            get
            {
                if (id < 0 || id >= Size)
                    throw new ArgumentOutOfRangeException("id");
                return ParentInventory[id + StartIndex];
            }
            set
            {
                if (id < 0 || id >= Size)
                    throw new ArgumentOutOfRangeException("id");
                ParentInventory[id + StartIndex] = value;
            }
        }

        #endregion
    }
}
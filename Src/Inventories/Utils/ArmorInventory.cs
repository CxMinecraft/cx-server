﻿using System;

namespace CxServer.Inventories.Utils
{
    /// <summary>
    /// Inventory for armors (4 items = helmet, chestplate, leggings and boots).
    /// </summary>
    public class ArmorInventory : UtilInventory
    {

        #region Constructors

        /// <summary>
        /// Create new instance of <see cref="CxServer.Inventories.Utils.ArmorInventory"/>.
        /// </summary>
        /// <param name="inventory">Parent inventory.</param>
        /// <param name="startIndex">Start index of this <see cref="CxServer.Inventories.Utils.UtilInventory"/> in it's Parent <see cref="CxServer.Inventories.Inventory"/>.</param>
        public ArmorInventory(Inventory inventory, int startIndex) : base(inventory, startIndex, 4)
        {
        }

        #endregion

        #region Shortcuts

        /// <summary>
        /// Entity's head (helmet).
        /// </summary>
        public ItemStack Head => ParentInventory[StartIndex + 0];
        /// <summary>
        /// Entity's chest (chestplate).
        /// </summary>
        public ItemStack Chest => ParentInventory[StartIndex + 1];
        /// <summary>
        /// Entity's legs (leggings).
        /// </summary>
        public ItemStack Legs => ParentInventory[StartIndex + 2];
        /// <summary>
        /// Entity's feet (boots).
        /// </summary>
        public ItemStack Feet => ParentInventory[StartIndex + 3];

        #endregion
    }
}
﻿using System;

namespace CxServer.Inventories.Utils
{
    /// <summary>
    /// Util Inventory which contains <b>n</b> number of lines.<br>
    /// This is default type. It is used as Hotbar, Player's personal storage, Chest...
    /// </summary>
    public class InventoryLines : UtilInventory
    {

        #region Constructors

        /// <summary>
        /// Crate new instance of <see cref="CxServer.Inventories.Utils.InventoryLines"/>.
        /// </summary>
        /// <param name="inventory">Parent inventory.</param>
        /// <param name="startIndex">Start index of this <see cref="CxServer.Inventories.Utils.UtilInventory"/> in it's Parent <see cref="CxServer.Inventories.Inventory"/>.</param>
        /// <param name="linesCount">Number of lines. Each line contains 9 slots.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">Thrown when this <see cref="CxServer.Inventories.Utils.UtilInventory"/> cannot fit Parent Inventory</exception>
        public InventoryLines(Inventory inventory, int startIndex, int linesCount) : base(inventory, startIndex, linesCount * 9)
        {
            if (inventory.Size > startIndex + linesCount * 9)
                throw new ArgumentOutOfRangeException("linesCount");

            this.LinesCount = linesCount;
        }

        #endregion

        #region Variables

        /// <summary>
        /// Number of lines.<br>
        /// Each line contains 9 slots.
        /// </summary>
        public int LinesCount
        {
            get;
        }

        #endregion

    }
}
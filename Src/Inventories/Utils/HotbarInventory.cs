﻿using System;
using CxServer.Utils;

namespace CxServer.Inventories.Utils
{
    /// <summary>
    /// Inventory for Player's hotbar (9 easy-to-access items).
    /// </summary>
    public sealed class HotbarInventory : InventoryLines
    {

        #region Constructors

        /// <summary>
        /// Create new instance of <see cref="CxServer.Inventories.Utils.HotbarInventory"/>.
        /// </summary>
        /// <param name="inventory">Parent inventory.</param>
        /// <param name="startIndex">Start index of this <see cref="CxServer.Inventories.Utils.UtilInventory"/> in it's Parent <see cref="CxServer.Inventories.Inventory"/>.</param>
        /// <param name="selectedIndex">Player's selected slot index.</param>
        /// <exception cref="System.ArgumentOutOfRangeException">Thrown when selected index is over 8 or under 0.</exception>
        public HotbarInventory(Inventory inventory, int startIndex, int selectedIndex) : base(inventory, startIndex, 1)
        {
            if (selectedIndex < 0 || selectedIndex > 8)
                throw new ArgumentOutOfRangeException("selectedIndex");

            this._selectedIndex = selectedIndex;
        }

        #endregion

        #region Selected

        private int _selectedIndex = 0;
        /// <summary>
        /// Player's selected slot index.
        /// </summary>
        /// <exception cref="System.ArgumentOutOfRangeException">Thrown when trying to set selected index over 8 or under 0.</exception>
        public int SelectedIndex
        {
            get
            {
                return _selectedIndex;
            }
            set
            {
                if (value < 0 || value > 8)
                    throw new ArgumentOutOfRangeException("value");

                if (value == _selectedIndex)
                    return;

                if (HeldSlotChange == null)
                    _selectedIndex = value;
                else
                {
                    int old = _selectedIndex;
                    _selectedIndex = value;
                    HeldSlotChange(old, value);
                }
            }
        }

        /// <summary>
        /// Current Selected item.
        /// </summary>
        public ItemStack SelectedItem
        {
            get
            {
                return ParentInventory[StartIndex + SelectedIndex];
            }
            set
            {
                ParentInventory[StartIndex + SelectedIndex] = value;
            }
        }

        #endregion

        #region Events

        /// <summary>
        /// Called when held slot index is changed.
        /// </summary>
        public event HeldSlotChangeHandler HeldSlotChange;

        /// <summary>
        /// Held slot index change handler for <see cref="CxServer.Inventories.Utils.HotbarInventory.HeldSlotChange"/>.
        /// </summary>
        public delegate void HeldSlotChangeHandler(int oldSlot, int newSlot);

        #endregion
    }
}
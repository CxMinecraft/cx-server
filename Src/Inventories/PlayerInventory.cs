﻿using System;
using CxServer.Inventories.Utils;

namespace CxServer.Inventories
{
    /// <summary>
    /// Player inventory.
    /// </summary>
    public class PlayerInventory : Inventory
    {
        //41 = 3*9 + 9 + 1 + 4

        #region Constructors

        /// <summary>
        /// Create new instance of <see cref="CxServer.Inventories.PlayerInventory"/>.
        /// </summary>
        /// <param name="selectedIndex">Selected index on hotbar (current item slot).</param>
        /// <param name="content">All items in this inventory or <see langword="null"/> for empty.</param>
        public PlayerInventory(int selectedIndex, params ItemStack[] content) : base(41, content)
        {
            // 0 - 3 (4)
            this._armorInventory = new ArmorInventory(this, 0);

            // 4 - 30 (3*9)
            _storage = new InventoryLines(this, 4, 3);

            // 31 - 39 (9)
            this._hotbarInventory = new HotbarInventory(this, 31, selectedIndex);

            // 40 (1)
            //SecondHand
        }

        #endregion

        #region Hands

        /// <summary>
        /// Player's main hand.
        /// </summary>
        public ItemStack MainHand => _hotbarInventory.SelectedItem;
        /// <summary>
        /// Player's second hand (offhand).
        /// </summary>
        public ItemStack SecondHand => this[40];

        #endregion

        #region Inventory Utils

        private readonly ArmorInventory _armorInventory;
        /// <summary>
        /// Player's armor.
        /// </summary>
        public ArmorInventory ArmorInventory
        {
            get
            {
                return _armorInventory;
            }
        }

        private readonly HotbarInventory _hotbarInventory;
        /// <summary>
        /// Player's hotbar (9 easily accessable items).
        /// </summary>
        public HotbarInventory Hotbar
        {
            get
            {
                return _hotbarInventory;
            }
        }

        private readonly InventoryLines _storage;
        /// <summary>
        /// Player's private storage (3 lines of 9 items each).
        /// </summary>
        public InventoryLines Storage
        {
            get
            {
                return _storage;
            }
        }

        #endregion

    }
}
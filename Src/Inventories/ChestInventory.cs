﻿using System;

namespace CxServer.Inventories
{
    public class ChestInventory : Inventory
    {
        public ChestInventory(int lines, params ItemStack[] items) : base(lines * 9, items)
        {
            this.Lines = lines;
        }

        public int Lines
        {
            get;
        }

        public ItemStack this[int x, int y] => this[y * 9 + x];
    }
}
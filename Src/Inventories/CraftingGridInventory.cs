﻿using System;
using CxServer.Utils;
using Utils.Extensions;

namespace CxServer.Inventories
{
    /// <summary>
    /// Inventory for Crafting based on Grid.
    /// </summary>
    public class CraftingGridInventory : Inventory
    {

        #region Constructors

        /// <summary>
        /// Create new instance of <see cref="CxServer.Inventories.CraftingGridInventory"/>.
        /// </summary>
        /// <param name="width">Width of the Crafting Grid.</param>
        /// <param name="height">Height of the Crafting Grid.</param>
        /// <param name="content">Content of grid including result of <see langword="null"/> for empty.</param>
        public CraftingGridInventory(int width, int height, params ItemStack[] content) : base(width * height + 1, content)
        {
            this.GridWidth = width;
            this.GridHeight = height;
            this.GridSize = width * height;

            base.ItemStackChange += (slot, oldItem, newItem) =>
            {
                if (slot != 0)
                {
                    //TODO recalculate result
                }
            };
        }

        #endregion

        #region Grid

        /// <summary>
        /// First dimension of the Crafting Grid.
        /// </summary>
        public int GridWidth
        {
            get;
        }
        /// <summary>
        /// Second dimension of the Crafting Grid.
        /// </summary>
        public int GridHeight
        {
            get;
        }
        /// <summary>
        /// Total space in the Crafting Grid.
        /// </summary>
        public int GridSize
        {
            get;
        }

        /// <summary>
        /// Items in the Crafting Grid.
        /// </summary>
        public ItemStack[,] ItemGrid
        {
            get
            {
                return AllItems.SubArray(1, GridSize).Cut(GridWidth);
            }
        }

        #endregion

        /// <summary>
        /// Result of crafting.
        /// </summary>
        public ItemStack Result => this[0];
    }
}
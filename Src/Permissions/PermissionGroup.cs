﻿using System;
using System.Collections.Generic;
using System.Linq;
using Utils.Configurations;
using System.Text.RegularExpressions;
using CxServer.Worlds;

namespace CxServer
{
    public class PermissionGroup
    {

        #region Constructors

        private PermissionGroup(string name)
        {
            this.Name = name;
        }

        #endregion

        #region Variables

        public string Name
        {
            get;
            private set;
        }

        #endregion

        #region Permissions

        public bool this[string perm] => this["*", perm];

        public bool this[string world, string perm]
        {
            get
            {
                if (permissionsInWorld.ContainsKey(world))
                {
                    Dictionary<string, bool> p = permissionsInWorld[world];
                    return p.ContainsKey(perm) ? p[perm] : this[perm];
                }
                else
                    return this[perm];
            }
            set
            {
                if (permissionsInWorld.ContainsKey(world))
                {
                    Dictionary<string, bool> p = permissionsInWorld[world];
                    p[perm] = value;
                }
                else
                {
                    Dictionary<string, bool> p = new Dictionary<string, bool>();
                    p[perm] = value;
                    permissionsInWorld[world] = p;
                }
            }
        }

        public void RemovePermission(string perm) => RemovePermission("*", perm);

        public void RemovePermission(World world, string perm) => RemovePermission(world.Name, perm);

        public void RemovePermission(string world, string perm)
        {
            if (world != "*" && !Regex.IsMatch(world, "[A-Za-z0-9]*"))
                throw new ArgumentException("Invalid world name", "world");
            if (!permissionsInWorld.ContainsKey(world))
                return;
            Dictionary<string, bool> perms = permissionsInWorld[world];
            if (!perms.ContainsKey(perm))
                return;
            perms.Remove(perm);
            //TODO set dictionary?
        }

        private Dictionary<string, Dictionary<string, bool>> permissionsInWorld = new Dictionary<string, Dictionary<string, bool>>();

        #endregion


        public Configuration Saved
        {
            get
            {
                Configuration config = new Configuration();

                foreach (KeyValuePair<string, Dictionary<string, bool>> kvp1 in permissionsInWorld)
                {
                    Configuration wc = config.CreateConfiguration(kvp1.Key);

                    foreach (KeyValuePair<string, bool> kvp2 in kvp1.Value)
                        wc.Set(kvp2.Key, kvp2.Value);
                }

                return config;
            }
        }

        #region Static

        public static PermissionGroup getPermissionGroup(string group)
        {
            var pg = _groups.FirstOrDefault((PermissionGroup arg) => arg.Name.Equals(group, StringComparison.InvariantCultureIgnoreCase));
            if (pg == null)
            {
                pg = new PermissionGroup(group);
                _groups.Add(pg);
            }
            return pg;
        }

        internal static List<PermissionGroup> _groups = new List<PermissionGroup>();

        #endregion

    }
}
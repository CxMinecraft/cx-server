﻿using System;
using System.Collections.Generic;

namespace CxServer
{
    public class PermissionTrie
    {
        public List<string> Keys
        {
            get
            {
                List<string> keys = new List<string>();

                foreach (var kvp in _dictionary)
                {
                    keys.Add(kvp.Key);
                    foreach (string s in kvp.Value.Keys)
                        keys.Add(string.Format("{0}.{1}", kvp.Key, s));
                }

                return keys;
            }
        }

        private readonly Dictionary<string, PermissionTrie> _dictionary = new Dictionary<string, PermissionTrie>();

        public bool this[string path]
        {
            get
            {
                int index = path.IndexOf('.');
                if (index == -1)
                    return _dictionary.ContainsKey(path);
                else
                {
                    PermissionTrie trie;
                    return _dictionary.TryGetValue(path.Substring(0, index), out trie) && trie[path.Substring(index + 1)];
                }
            }
            set
            {
                int index = path.IndexOf('.');

                if (index == -1)
                {
                    if (value)
                    {
                        if (!_dictionary.ContainsKey(path))
                            _dictionary[path] = new PermissionTrie();
                    }
                    else
                    {
                        if (_dictionary.ContainsKey(path))
                            _dictionary.Remove(path);
                    }
                }
                else
                {
                    PermissionTrie trie;
                    if (_dictionary.TryGetValue(path.Substring(0, index), out trie))
                        trie[path.Substring(index + 1)] = value;
                }
            }
        }
    }
}
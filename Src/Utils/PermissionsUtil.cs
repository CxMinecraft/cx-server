﻿using System;
using CxServer.Commands;

namespace CxServer
{
    public static class PermissionsUtil
    {
        public static bool HasAnyPermission(this ICommandSender sender, params string[] permissions)
        {
            foreach (string perm in permissions)
                if (sender.HasPermission(perm))
                    return true;
            return false;
        }

        public static bool HasAllPermission(this ICommandSender sender, params string[] permissions)
        {
            foreach (string perm in permissions)
                if (!sender.HasPermission(perm))
                    return false;
            return true;
        }
    }
}
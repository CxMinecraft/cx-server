﻿using System;
using System.Text;
using CxServer.NBT;
using CxServer.Mojang;
using Utils;
using Utils.Extensions;

namespace CxServer.Utils
{
    public static class ItemStackUtil
    {

        #region Functions

        public static ItemStack GetHeadByUUID(UUID uuid)
        {
            var profile = MojangAPI.GetProfile(uuid);
            if (profile == null)
                return uuid.GetHashCode() % 2 == 0 ? Steve : Alex;
            return GetHeadByTexture(profile.SkinUrl);
        }

        public static ItemStack GetHeadByUUID(string name, UUID uuid)
        {
            return new ItemStack(GetHeadByUUID(uuid), string.Format("{{display:{{Name:\"{0}\"}} }}", name));
        }

        public static ItemStack GetHeadByName(string name) => GetHeadByUUID(name, MojangAPI.GetUUID(name));

        //http://textures.minecraft.net/texture/<texture>
        public static ItemStack GetHeadByTexture(string texture) => GetHeadByBase64(Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Format("{{\"textures\":{{\"SKIN\":{{\"url\":\"http://textures.minecraft.net/texture/{0}\"}}}}}}", texture))));

        public static ItemStack GetHeadByBase64(string base64)
        {
            return new ItemStack("skull", 1, 3, string.Format("{{SkullOwner:{{Id:\"{0}\", Properties:{{textures:[{{Value:{1} }}]}} }} }}", UUID.GenerateUUID5(base64).ToString(true), base64));
        }


        //http://textures.minecraft.net/texture/<texture>
        public static ItemStack GetHeadByTexture(string name, string texture) => GetHeadByBase64(name, Convert.ToBase64String(Encoding.UTF8.GetBytes(string.Format("{{\"textures\":{{\"SKIN\":{{\"url\":\"http://textures.minecraft.net/texture/{0}\"}}}}}}", texture))));

        public static ItemStack GetHeadByBase64(string name, string base64)
        {
            return new ItemStack("skull", 1, 3, string.Format("{{display:{{Name:\"{0}\"}}, SkullOwner:{{Id:\"{0}\", Properties:{{textures:[{{Value:{1} }}]}} }} }}", UUID.GenerateUUID5(base64).ToString(true), base64));
        }

        #endregion

        #region Players

        public static ItemStack[] MinecraftSkins
        {
            get
            {
                return new[] {
                    Steve,
                    Alex,
                    Herobrine
                };
            }
        }

        public static ItemStack Alex
        {
            get
            {
                return GetHeadByTexture("Alex", "63b098967340daac529293c24e04910509b208e7b94563c3ef31dec7b3750");
            }
        }

        public static ItemStack Steve
        {
            get
            {
                return GetHeadByTexture("Steve", "456eec1c2169c8c60a7ae436abcd2dc5417d56f8adef84f11343dc1188fe138");
            }
        }

        public static ItemStack Herobrine
        {
            get
            {
                return GetHeadByTexture("Herobrine", "98b7ca3c7d314a61abed8fc18d797fc30b6efc8445425c4e250997e52e6cb");
            }
        }

        public static ItemStack[] MojangCrew
        {
            get
            {
                return new[] {
                    Notch,
                    Jeb,
                    Dinnerbone,
                    Djinnibone,
                    Grumm,
                    ProfMobius,
                    Searge,
                    Searge_DP,
                    Binni,
                    Bopogamel,
                    Kappe,
                    MinecraftChick,
                    Marzenia,
                    Carnalizer,
                    Eldrone,
                    Xlson,
                    NeonMaster,
                    Zeeraw,
                    C418,
                    Vaht,
                    TheMogMiner,
                    Hideous,
                    Noirceuil,
                    EvilSeph,
                    FruktHamster,
                    ClayHero
                };
            }
        }

        /// <summary>
        ///Markus Persson
        ///Notch
        /// </summary>
        public static ItemStack Notch
        {
            get
            {
                return GetHeadByTexture("Notch (Mojang)", "a116e69a845e227f7ca1fdde8c357c8c821ebd4ba619382ea4a1f87d4ae94");
            }
        }

        /// <summary>
        ///Jens Bergensten
        ///jeb_
        /// </summary>
        public static ItemStack Jeb
        {
            get
            {
                return GetHeadByTexture("jeb_ (Mojang)", "a846b82963924cb13211122489263941d1403689f90151120d5234be4a73fb");
            }
        }

        /// <summary>
        ///Nathan Adams
        ///Dinnerbone
        /// </summary>
        public static ItemStack Dinnerbone
        {
            get
            {
                return GetHeadByTexture("Dinnerbone (Mojang)", "cd6be915b261643fd13621ee4e99c9e541a551d80272687a3b56183b981fb9a");
            }
        }

        /// <summary>
        ///Nathan Adams
        ///Djinnibone
        /// </summary>
        public static ItemStack Djinnibone
        {
            get
            {
                return Steve;//NO SKIN
            }
        }

        /// <summary>
        ///Erik Broes
        ///Grumm
        /// </summary>
        public static ItemStack Grumm
        {
            get
            {
                return GetHeadByTexture("Grumm (Mojang)", "b68673d86a46a84535cdf2d05f574917f87747e279fcf7ec1f00aced7d");
            }
        }

        /// <summary>
        ///Thomas Guimbretière
        ///ProfMobius
        /// </summary>
        public static ItemStack ProfMobius
        {
            get
            {
                return GetHeadByTexture("ProfMobius (Mojang)", "ac1cd1e0415afb965c13fca74b6b6c7cb825ec23122311b3cbb4ef93336563");
            }
        }

        /// <summary>
        ///Michael Stoyke
        ///Searge
        /// </summary>
        public static ItemStack Searge
        {
            get
            {
                return GetHeadByTexture("Searge (Mojang)", "117b303af07715b5ee38be1ded52077686fdc7ef978e39715838326298bb4");
            }
        }

        /// <summary>
        ///Michael Stoyke
        ///Searge_DP
        /// </summary>
        public static ItemStack Searge_DP
        {
            get
            {
                return GetHeadByTexture("Searge_DP (Mojang)", "21ab32a8bb47df792e2cbad9134f9c5d14e7ea436cbb375bdbbc81ab28d5");
            }
        }

        /// <summary>
        ///Brynjólfur Erlingsson
        ///Binni
        /// </summary>
        public static ItemStack Binni
        {
            get
            {
                return GetHeadByTexture("Bopogamel (Mojang)", "e4f41ac22d97eb0816b57abb2473152ed98fa1b8fd25a8e7da66f7f35fe3a6f");
            }
        }

        /// <summary>
        ///Owen Jones
        ///Bopogamel
        /// </summary>
        public static ItemStack Bopogamel
        {
            get
            {
                return GetHeadByTexture("Bopogamel (Mojang)", "73bfc092eb27dbb4f950af159c498d8127da1fc51c135921f65cee35659bfa2");
            }
        }

        /// <summary>
        ///Daniel Kaplan
        ///Kappe
        public static ItemStack Kappe
        {
            get
            {
                return GetHeadByTexture("Kappe (Mojang)", "8803fa3b5824bcbfc538bb4b09c1fed77f3c6335f88b5057e17f565a781c2");
            }
        }

        /// <summary>
        ///Lydia Winters
        ///MinecraftChick
        /// </summary>
        public static ItemStack MinecraftChick
        {
            get
            {
                return Alex;//No skin (only cape)
            }
        }

        /// <summary>
        ///Aleksandra Zając
        ///Marzenia
        /// </summary>
        public static ItemStack Marzenia
        {
            get
            {
                return GetHeadByTexture("Marzenia (Mojang)", "5e16b223b36edef6e25c26bcb8b9c3d5f49863713c8f3993acbb25ba148879d5");
            }
        }

        /// <summary>
        ///Henrik Pettersson
        ///carnalizer
        /// </summary>
        public static ItemStack Carnalizer
        {
            get
            {
                return GetHeadByTexture("carnalizer (Mojang)", "3f37bea23e3a979865770ad33f1798ab09eaab5e7c44faf1d8fb49552ca6");
            }
        }

        /// <summary>
        ///Jonatan Pöljö
        ///eldrone
        /// </summary>
        public static ItemStack Eldrone
        {
            get
            {
                return GetHeadByTexture("eldrone (Mojang)", "afc029d7f36330806bc518c3521c7dac4b5315209e5b19ac5d1d51d5bf898324");
            }
        }

        /// <summary>
        ///Leonard Gram
        ///xlson
        /// </summary>
        public static ItemStack Xlson
        {
            get
            {
                return GetHeadByTexture("xlson (Mojang)", "f2e1e85f8cf423f36fe8e13b7de39de83699c19d26e4d3ee36ec2fb9cbb31");
            }
        }

        /// <summary>
        ///David Marby
        ///NeonMaster
        /// </summary>
        public static ItemStack NeonMaster
        {
            get
            {
                return GetHeadByTexture("NeonMaster (Mojang)", "b5169cbd6458ae9704425c2b76398a962641113b7268bfcf188efafb5a6b4");
            }
        }

        /// <summary>
        ///Philip Vieira
        ///zeeraw
        /// </summary>
        public static ItemStack Zeeraw
        {
            get
            {
                return GetHeadByTexture("zeeraw (Mojang)", "3e1c9efecae6ab3c6ba82d9479972da73f266288f8f0381c5ad0e7302da3ff");
            }
        }

        /// <summary>
        ///Daniel Rosenfeld
        ///C418
        /// </summary>
        public static ItemStack C418
        {
            get
            {
                return GetHeadByTexture("C418 (Mojang)", "a620b82cf11e3c1371cc51eb9e312de72a6a62664494ed2cb7181b1bdfbc9278");
            }
        }

        /// <summary>
        ///Nathan Gilbert
        ///Vaht
        /// </summary>
        public static ItemStack Vaht
        {
            get
            {
                return GetHeadByTexture("Vaht (Mojang)", "394bf430f34c27adfb6839ad2c27ada47990ab9d65eb0828775062194");
            }
        }

        /// <summary>
        ///Ryan Holtz
        ///TheMogMiner
        /// </summary>
        public static ItemStack TheMogMiner
        {
            get
            {
                return GetHeadByTexture("TheMogMiner (Mojang)", "4b66add86ceee6bde7e6e08d983a7ba32f7941bcc28293324dc1d962b69d9");
            }
        }

        /// <summary>
        ///Andreas Jörgensen
        ///Hideous
        /// </summary>
        public static ItemStack Hideous
        {
            get
            {
                return GetHeadByTexture("Hideous (Mojang)", "62b142b787f1758d3f0dc11ccede0b58130d125e971f6e8d88baa45c397cbd7");
            }
        }

        /// <summary>
        ///Joe Liu
        ///Noirceuil
        /// </summary>
        public static ItemStack Noirceuil
        {
            get
            {
                return GetHeadByTexture("Noirceuil (Mojang)", "7a83cb2b13417f4d4d1357a4e753875151aef2b357f59cbe6e5820e0525998");
            }
        }

        /// <summary>
        ///Warren Loo
        ///EvilSeph
        /// </summary>
        public static ItemStack EvilSeph
        {
            get
            {
                return GetHeadByTexture("EvilSeph (Mojang)", "362f66ef371a7476e9f9662ab8bcbbe818aae48428d43f331e642e9ad2269d7");
            }
        }

        /// <summary>
        ///Jakob Porsér
        ///FruktHamster
        /// </summary>
        public static ItemStack FruktHamster
        {
            get
            {
                return GetHeadByTexture("FruktHamster (Mojang)", "85e0257b839d9530619c182294cfd2f3f18ef06a8377d721e338079c9a37fb7");
            }
        }

        /// <summary>
        ///Mike Till
        ///ClayHero
        /// </summary>
        public static ItemStack ClayHero
        {
            get
            {
                return GetHeadByTexture("ClayHero (Mojang)", "d8ceabe4dfb81f13575362e6ee17ae61d53589bc4c11125f1a3be8ef26ea59b8");
            }
        }

        #endregion

        #region Mobs

        public static ItemStack[] Mobs
        {
            get
            {
                return new[] {
                    Blaze,
                    CaveSpider,
                    Chicken,
                    Cow,
                    Creeper,
                    ElderGuardian,
                    Enderman,
                    Ghast,
                    Guardian,
                    Horse,
                    IronGolem,
                    MagmaCube,
                    MushroomCow,
                    Ocelot,
                    Penguin,
                    Pig,
                    Sheep,
                    Skeleton,
                    Slime,
                    Spider,
                    Squid,
                    Villager,
                    WitherSkeleton,
                    Zombie,
                    ZombiePigman
                };
            }
        }

        public static ItemStack Blaze
        {
            get
            {
                return GetHeadByTexture("Blaze (Mob)", "b78ef2e4cf2c41a2d14bfde9caff10219f5b1bf5b35a49eb51c6467882cb5f0");
            }
        }

        public static ItemStack CaveSpider
        {
            get
            {
                return GetHeadByTexture("Cave Spider (Mob)", "41645dfd77d09923107b3496e94eeb5c30329f97efc96ed76e226e98224");
            }
        }

        public static ItemStack Chicken
        {
            get
            {
                return GetHeadByTexture("Chicken (Mob)", "1638469a599ceef7207537603248a9ab11ff591fd378bea4735b346a7fae893");
            }
        }

        public static ItemStack Cow
        {
            get
            {
                return GetHeadByTexture("Cow (Mob)", "5d6c6eda942f7f5f71c3161c7306f4aed307d82895f9d2b07ab4525718edc5");
            }
        }

        public static ItemStack Creeper
        {
            get
            {
                return GetHeadByTexture("Creeper (Mob)", "295ef836389af993158aba27ff37b6567185f7a721ca90fdfeb937a7cb5747");
            }
        }

        public static ItemStack ElderGuardian
        {
            get
            {
                return GetHeadByTexture("Elder Guardian (Mob)", "1c797482a14bfcb877257cb2cff1b6e6a8b8413336ffb4c29a6139278b436b");
            }
        }

        public static ItemStack Enderman
        {
            get
            {
                return GetHeadByTexture("Enderman (Mob)", "7a59bb0a7a32965b3d90d8eafa899d1835f424509eadd4e6b709ada50b9cf");
            }
        }

        public static ItemStack Ghast
        {
            get
            {
                return GetHeadByTexture("Ghast (Mob)", "8b6a72138d69fbbd2fea3fa251cabd87152e4f1c97e5f986bf685571db3cc0");
            }
        }

        public static ItemStack Guardian
        {
            get
            {
                return GetHeadByTexture("Guardian (Mob)", "c25af966a326f9d98466a7bf8582ca4da6453de271b3bc9e59f57a99b63511c6");
            }
        }

        public static ItemStack Horse
        {
            get
            {
                return GetHeadByTexture("Horse (Mob)", "7bb4b288991efb8ca0743beccef31258b31d39f24951efb1c9c18a417ba48f9");
            }
        }

        public static ItemStack IronGolem
        {
            get
            {
                return GetHeadByTexture("Iron Golem (Mob)", "89091d79ea0f59ef7ef94d7bba6e5f17f2f7d4572c44f90f76c4819a714");
            }
        }

        public static ItemStack MagmaCube
        {
            get
            {
                return GetHeadByTexture("Magma Cube (Mob)", "38957d5023c937c4c41aa2412d43410bda23cf79a9f6ab36b76fef2d7c429");
            }
        }

        public static ItemStack MushroomCow
        {
            get
            {
                return GetHeadByTexture("Mushroom Cow (Mob)", "d0bc61b9757a7b83e03cd2507a2157913c2cf016e7c096a4d6cf1fe1b8db");
            }
        }

        public static ItemStack Ocelot
        {
            get
            {
                return GetHeadByTexture("Ocelot (Mob)", "5657cd5c2989ff97570fec4ddcdc6926a68a3393250c1be1f0b114a1db1");
            }
        }

        public static ItemStack Penguin
        {
            get
            {
                return GetHeadByTexture("Penguin (Mob)", "865964c5dc5a374ed9864290129b7eaecc3f4399f7516b2a38573aa752717ac");
            }
        }

        public static ItemStack Pig
        {
            get
            {
                return GetHeadByTexture("Pig (Mob)", "621668ef7cb79dd9c22ce3d1f3f4cb6e2559893b6df4a469514e667c16aa4");
            }
        }

        public static ItemStack Sheep
        {
            get
            {
                return GetHeadByTexture("Sheep (Mob)", "f31f9ccc6b3e32ecf13b8a11ac29cd33d18c95fc73db8a66c5d657ccb8be70");
            }
        }

        public static ItemStack Skeleton
        {
            get
            {
                return GetHeadByTexture("Skeleton (Mob)", "2e5be6a3c0159d2c1f3b1e4e1d8384b6f7ebac993d58b10b9f8989c78a232");
            }
        }

        public static ItemStack Slime
        {
            get
            {
                return GetHeadByTexture("Slime (Mob)", "16ad20fc2d579be250d3db659c832da2b478a73a698b7ea10d18c9162e4d9b5");
            }
        }

        public static ItemStack Spider
        {
            get
            {
                return GetHeadByTexture("Spider (Mob)", "cd541541daaff50896cd258bdbdd4cf80c3ba816735726078bfe393927e57f1");
            }
        }

        public static ItemStack Squid
        {
            get
            {
                return GetHeadByTexture("Squid (Mob)", "01433be242366af126da434b8735df1eb5b3cb2cede39145974e9c483607bac");
            }
        }

        public static ItemStack Villager
        {
            get
            {
                return GetHeadByTexture("Villager (Mob)", "822d8e751c8f2fd4c8942c44bdb2f5ca4d8ae8e575ed3eb34c18a86e93b");
            }
        }

        public static ItemStack WitherSkeleton
        {
            get
            {
                return GetHeadByTexture("Wither Skeleton (Mob)", "233b41fa79cd53a230e2db942863843183a70404533bbc01fab744769bcb");
            }
        }

        public static ItemStack Zombie
        {
            get
            {
                return GetHeadByTexture("Zombie (Mob)", "56fc854bb84cf4b7697297973e02b79bc10698460b51a639c60e5e417734e11");
            }
        }

        public static ItemStack ZombiePigman
        {
            get
            {
                return GetHeadByTexture("Zombie Pigman (Mob)", "74e9c6e98582ffd8ff8feb3322cd1849c43fb16b158abb11ca7b42eda7743eb");
            }
        }

        #endregion

        #region Helmets

        public static ItemStack[] HelmetIron
        {
            get
            {
                return new[] {
                    HelmetIron00,
                    HelmetIron01,
                    HelmetIron02,
                    HelmetIron03,
                    HelmetIron04,
                    HelmetIron05,
                    HelmetIron06,
                    HelmetIron07,
                    HelmetIron08,
                    HelmetIron09,
                    HelmetIron10,
                    HelmetIron11,
                    HelmetIron12
                };
            }
        }

        public static ItemStack HelmetIron00
        {
            get
            {
                return GetHeadByTexture("Iron Helmet 0 (Helmets)", "04d5f6a1d3cb9d368ac5ebc64653a7336f1bb921201ae17a5684e371c4d5");
            }
        }

        public static ItemStack HelmetIron01
        {
            get
            {
                return GetHeadByTexture("Iron Helmet 1 (Helmets)", "7c9d7263a13a38fa7d47738c2f6eaf083a3d4592b97e526cb111c86110ccc");
            }
        }

        public static ItemStack HelmetIron02
        {
            get
            {
                return GetHeadByTexture("Iron Helmet 2 (Helmets)", "fd117ced0d47e52999a7f646afd9f7a8e2f35cffb43b6cb6e9b1dad163d339");
            }
        }

        public static ItemStack HelmetIron03
        {
            get
            {
                return GetHeadByTexture("Iron Helmet 3 (Helmets)", "e0198eefd25eb297eea76ae3fdc1ee2a186bdc7f2a0b3624a431b626ba3caf");
            }
        }

        public static ItemStack HelmetIron04
        {
            get
            {
                return GetHeadByTexture("Iron Helmet 4 (Helmets)", "80ec9a3abaeae84cb42b89fadbfa973198b2b11ffe60ab301590d18992f63e");
            }
        }

        public static ItemStack HelmetIron05
        {
            get
            {
                return GetHeadByTexture("Iron Helmet 5 (Helmets)", "a07412151e2a3a5b33f7b298ed237fae39e6488d462fa5d558a2ebf747847");
            }
        }

        public static ItemStack HelmetIron06
        {
            get
            {
                return GetHeadByTexture("Iron Helmet 6 (Helmets)", "1b4bf7a7a45d8ddeb470c961737ce9621148fce2cfe81459c311775cdab9");
            }
        }

        public static ItemStack HelmetIron07
        {
            get
            {
                return GetHeadByTexture("Iron Helmet 7 (Helmets)", "4fd85a968334ffa874d63e8068c4d1835e29b8da17cf58d8de0a71338ecbf60");
            }
        }

        public static ItemStack HelmetIron08
        {
            get
            {
                return GetHeadByTexture("Iron Helmet 8 (Helmets)", "c4e71b684249469eaf1e22cd193814c5cfd2152525cb3646a32d423247238");
            }
        }

        public static ItemStack HelmetIron09
        {
            get
            {
                return GetHeadByTexture("Iron Helmet 9 (Helmets)", "4de8cb3a4926ed9e228f8bdcf9dc628419e394e9b146c523fdd5bb7e31ce4d");
            }
        }

        public static ItemStack HelmetIron10
        {
            get
            {
                return GetHeadByTexture("Iron Helmet 10 (Helmets)", "f7c17865c27934f8c8ec6c627e1fe2d99f783ec8ae414ca2d4fd3640a7f3c");
            }
        }

        public static ItemStack HelmetIron11
        {
            get
            {
                return GetHeadByTexture("Iron Helmet 11 (Helmets)", "7c3982cabb6251511a4dc13b6f5a32f842cf1a06e1ef5e332b8793db7f250");
            }
        }

        public static ItemStack HelmetIron12
        {
            get
            {
                return GetHeadByTexture("Iron Helmet 12 (Helmets)", "7d46793bdbce5cad5f37b124eaf1e3689bba18d59a8068567c74f4ff1a18");
            }
        }

        public static ItemStack[] HelmetGold
        {
            get
            {
                return new[] {
                    HelmetGold00,
                    HelmetGold01,
                    HelmetGold02,
                    HelmetGold03,
                    HelmetGold04,
                    HelmetGold05,
                    HelmetGold06,
                    HelmetGold07,
                    HelmetGold08
                };
            }
        }

        public static ItemStack HelmetGold00
        {
            get
            {
                return GetHeadByTexture("Golden Helmet 0 (Helmets)", "02e5ff7fdde26c1ac498141e60d08c7661fc3c0db96699b99ec2a8f3acab");
            }
        }

        public static ItemStack HelmetGold01
        {
            get
            {
                return GetHeadByTexture("Golden Helmet 1 (Helmets)", "6c8587d097fc3e6eab802c6b55c8bd72316bd68fd835281c427248c7a96f3c");
            }
        }

        public static ItemStack HelmetGold02
        {
            get
            {
                return GetHeadByTexture("Golden Helmet 2 (Helmets)", "cd4730ad52a9b997950e637e59542f66641aa2fcd99af3a937736f042642688");
            }
        }

        public static ItemStack HelmetGold03
        {
            get
            {
                return GetHeadByTexture("Golden Helmet 3 (Helmets)", "cead2978e6654f37d1303585f9f23c125d777a31ffd66e86f365ad8ccebf1");
            }
        }

        public static ItemStack HelmetGold04
        {
            get
            {
                return GetHeadByTexture("Golden Helmet 4 (Helmets)", "d3a88cdc3519e9d4be65fd171ee372c2ba15d0e6925cffb9d6531aa1799891c3");
            }
        }

        public static ItemStack HelmetGold05
        {
            get
            {
                return GetHeadByTexture("Golden Helmet 5 (Helmets)", "c925f939dc9def2518532d29cc5464a57ae339248e9a6ab2cecb7c78d379");
            }
        }

        public static ItemStack HelmetGold06
        {
            get
            {
                return GetHeadByTexture("Golden Helmet 6 (Helmets)", "14d217aa93f0b22d683a131defd54985ca87b69888fd89d34b2f87ce4b9c95f8");
            }
        }

        public static ItemStack HelmetGold07
        {
            get
            {
                return GetHeadByTexture("Golden Helmet 7 (Helmets)", "c3f25160043745619904fec8b67855adc5884713996bcc1f8c3ad2796dcc21");
            }
        }

        public static ItemStack HelmetGold08
        {
            get
            {
                return GetHeadByTexture("Golden Helmet 8 (Helmets)", "9d8eb267c8139bd69149069aab5eaeb7634434ab6fb9686f234471cccb9171");
            }
        }

        #endregion

        #region Blocks and Items

        public static ItemStack Apple
        {
            get
            {
                return GetHeadByTexture("Apple (Block/Item)", "35e2e0959712dcd3357cc3cea85f99b3fd8097855c754b9b171f963514225d");
            }
        }

        public static ItemStack Beacon
        {
            get
            {
                return GetHeadByTexture("Beacon (Block/Item)", "8735dd2b3e04e5b995b7298b3d4d95be3255289f2f3c316d3bfd13b0312b2f");
            }
        }

        public static ItemStack Bedrock
        {
            get
            {
                return GetHeadByTexture("Bedrock (Block/Item)", "36d1fabdf3e342671bd9f95f687fe263f439ddc2f1c9ea8ff15b13f1e7e48b9");
            }
        }

        public static ItemStack Cactus
        {
            get
            {
                return GetHeadByTexture("Cactus (Block/Item)", "38c9a730269ce1de3e9fa064afb370cbcd0766d729f3e29e4f320a433b098b5");
            }
        }

        public static ItemStack CactusRound
        {
            get
            {
                return GetHeadByTexture("Cactus (Round) (Block/Item)", "2f585b41ca5a1b4ac26f556760ed11307c94f8f8a1ade615bd12ce074f4793");
            }
        }

        public static ItemStack CactusRoundFlower
        {
            get
            {
                return GetHeadByTexture("Cactus - Flower (Round) (Block/Item)", "904f1a55943c594e7119e884c5da2a2bca8e7e6516a0649aa7e55658e0e9");
            }
        }

        public static ItemStack Cake
        {
            get
            {
                return GetHeadByTexture("Cake (Block/Item)", "f9136514f342e7c5208a1422506a866158ef84d2b249220139e8bf6032e193");
            }
        }

        public static ItemStack Chest
        {
            get
            {
                return GetHeadByTexture("Chest (Block/Item)", "6f68d509b5d1669b971dd1d4df2e47e19bcb1b33bf1a7ff1dda29bfc6f9ebf");
            }
        }

        public static ItemStack ChorusFlower
        {
            get
            {
                return GetHeadByTexture("Chorus Flower (Block/Item)", "92eb30a0be3d7c7e162c76e72a3c4cc44223567162a941752572ae6cceb96c86");
            }
        }

        public static ItemStack ChorusPlant
        {
            get
            {
                return GetHeadByTexture("Chorus Plant (Block/Item)", "bbc58d2893c25ac13cf9be8766d7398d4345ac80c2e625ba11bdceb24c5fd7");
            }
        }

        public static ItemStack Clay
        {
            get
            {
                return GetHeadByTexture("Clay (Block/Item)", "67826829eab5ad62f0c11d9faafdc9954364871160dd839e1ab5a3b213a33");
            }
        }

        public static ItemStack CoconutBrown
        {
            get
            {
                return GetHeadByTexture("Coconut - Brown (Block/Item)", "32c62fd8e474d09940604f82712a44abb249d63aff87f998374ca849ab17412");
            }
        }

        public static ItemStack CoconutGreen
        {
            get
            {
                return GetHeadByTexture("Coconut - Green (Block/Item)", "bf61259a7ed75dfc15f4328f69fa5d549ef1ba9c7aa85c53b8c76173fac3c69");
            }
        }

        public static ItemStack CommandBlock
        {
            get
            {
                return GetHeadByTexture("Command Block (Block/Item)", "8514d225b262d847c7e557b474327dcef758c2c5882e41ee6d8c5e9cd3bc914");
            }
        }

        public static ItemStack Egg
        {
            get
            {
                return GetHeadByTexture("Egg (Block/Item)", "b4e0385c56a3ae53369e7e265df5aa21e28d0c07ef357172cd1f61e5bf913ad");
            }
        }

        public static ItemStack EndBrick
        {
            get
            {
                return GetHeadByTexture("End Brick (Block/Item)", "66de65d1896812681105bb3c017fb3acd3f4dd63b13b18da14d59be984a63d7");
            }
        }

        public static ItemStack EndStone
        {
            get
            {
                return GetHeadByTexture("End Stone (Block/Item)", "19f21f5d883316fd65a9366f32a33013182e3381dec21c17c78355d9bf4f0");
            }
        }

        public static ItemStack EnderChest
        {
            get
            {
                return GetHeadByTexture("Ender Chest (Block/Item)", "a6cc486c2be1cb9dfcb2e53dd9a3e9a883bfadb27cb956f1896d602b4067");
            }
        }

        public static ItemStack EnderPearl
        {
            get
            {
                return GetHeadByTexture("Ender Pearl (Block/Item)", "5cb7c21cc43dc17678ee6f16591ffaab1f637c37f4f6bbd8cea497451d76db6d");
            }
        }

        public static ItemStack EyeOfEnder
        {
            get
            {
                return GetHeadByTexture("Eye Of Ender (Block/Item)", "daa8fc8de6417b48d48c80b443cf5326e3d9da4dbe9b25fcd49549d96168fc0");
            }
        }

        public static ItemStack Furnace
        {
            get
            {
                return GetHeadByTexture("Furnace - Empty (Block/Item)", "6bacee4bab9021804e99dfe411e17e15cfe7e052293a3497574dfdb626053f7");
            }
        }

        public static ItemStack FurnaceBurning
        {
            get
            {
                return GetHeadByTexture("Furnace - Burning (Block/Item)", "2347278ca72dc319b2249c951882983393dde0204afc6434bd326f3ab56b2781");
            }
        }

        public static ItemStack GrassBlock
        {
            get
            {
                return GetHeadByTexture("Grass Block - Green (Block/Item)", "c95d37993e594082678472bf9d86823413c250d4332a2c7d8c52de4976b362");
            }
        }

        public static ItemStack GrassBlockSnow
        {
            get
            {
                return GetHeadByTexture("Grass Block - Snow (Block/Item)", "43c52eae747cad5b4fd19b1a23b39a336b62ed422797a622d045f43e5d38");
            }
        }

        public static ItemStack Lava
        {
            get
            {
                return GetHeadByTexture("Lava (Block/Item)", "c078f3f24b1760f9d4abb0851eb8f6c7dfff8855708e6049d15dc02042ba8436");
            }
        }

        public static ItemStack Leaves1
        {
            get
            {
                return GetHeadByTexture("Leaves 1 (Block/Item)", "158337a5285a6458543c77aa8f90c276ae537ad16a4e9af8b45733e99f4cbd");
            }
        }

        public static ItemStack Leaves2
        {
            get
            {
                return GetHeadByTexture("Leaves 2 (Block/Item)", "83765e8ce11ae6f131c725c07f58f2d4132d984ad6636039a7a36a3c512721");
            }
        }

        public static ItemStack Leaves3
        {
            get
            {
                return GetHeadByTexture("Leaves 3 (Block/Item)", "ffc8641835dde6273adf24f60146a22e64ec4cfa1a130904d9497c21c8470c0");
            }
        }

        public static ItemStack LogOak
        {
            get
            {
                return GetHeadByTexture("Log - Oak (Block/Item)", "ffe5109c86fe6de8eb8afe32f0496a844dd655e4d4a39ef7b0fb61791848a5da");
            }
        }

        public static ItemStack Melon
        {
            get
            {
                return GetHeadByTexture("Melon (Block/Item)", "c3fed514c3e238ca7ac1c94b897ff6711b1dbe50174afc235c8f80d029");
            }
        }

        public static ItemStack MelonRound
        {
            get
            {
                return GetHeadByTexture("Melon (Round) (Block/Item)", "9636dee806ba47a2c40e95b57a12f37de6c2e677f2160132a07e24eeffa6");
            }
        }

        public static ItemStack MinecartChest
        {
            get
            {
                return GetHeadByTexture("Minecart - Chest (Block/Item)", "4ced34211fed4010a8c85724a27fa5fb205d67684b3da517b6821279c6b65d3f");
            }
        }

        public static ItemStack MinecartCommandblock
        {
            get
            {
                return GetHeadByTexture("Minecart - Commandblock (Block/Item)", "ba9053d2163d0f561145d33a513145d4ac1f8a458baa796be383e7525a05f45");
            }
        }

        public static ItemStack MinecartFurnace
        {
            get
            {
                return GetHeadByTexture("Minecart - Furnace (Block/Item)", "e079abbafb981c795a9a2f82bab3fbd9f166b8c0dbf9a1751d769beac667b6");
            }
        }

        public static ItemStack MinecartTNT
        {
            get
            {
                return GetHeadByTexture("Minecart - TNT (Block/Item)", "c4d7fc8e3a959ade7d9cf663f1e82db7975543e288ab8d11eb2541888213526");
            }
        }

        public static ItemStack MissingTexture
        {
            get
            {
                return GetHeadByTexture("Missing Texture (Block/Item)", "3d487a79a0117338feaddbd2f2ac4e896643f9bd9612ad865a84d41ed8f9f0");
            }
        }

        public static ItemStack MushroomRed
        {
            get
            {
                return GetHeadByTexture("Mushroom - Red (Block/Item)", "33bdcde0dc5d826a7b36f3c169bc1fed606cc59a3370aa9cb2ab23c6cb3433d");
            }
        }

        public static ItemStack MushroomBrown
        {
            get
            {
                return GetHeadByTexture("Mushroom - Brown (Block/Item)", "6ee1231daee6a1fcfc308d373bfea9cd24fc35b6f1f434973a9dec40521fa7");
            }
        }

        public static ItemStack Obsidian
        {
            get
            {
                return GetHeadByTexture("Obsidian (Block/Item)", "6ee1231daee6a1fcfc308d373bfea9cd24fc35b6f1f434973a9dec40521fa7");
            }
        }

        public static ItemStack PistonNormal
        {
            get
            {
                return GetHeadByTexture("Piston - Normal (Block/Item)", "12743a8bea752d6daf672dea367e1a5d398aecbe0c3c8483a90ec9ac4412a41");
            }
        }

        public static ItemStack PistonSticky
        {
            get
            {
                return GetHeadByTexture("Piston - Sticky (Block/Item)", "7ca4d218df9d32cd47d9c1d294877122be5919b418a6cc3d089162b133f2db");
            }
        }

        public static ItemStack Present1
        {
            get
            {
                return GetHeadByTexture("Present 1 (Block/Item)", "64abe81e6f4961e0f6bd82f2d4135b6b5fc845739e71cfe3b8943531d921e");
            }
        }

        public static ItemStack Present2
        {
            get
            {
                return GetHeadByTexture("Present 2 (Block/Item)", "bd7a9f6ed08dd217fdf09f4652bf6b7af621e1d5f8963605349da73998a443");
            }
        }

        public static ItemStack PrismarineBlock
        {
            get
            {
                return GetHeadByTexture("Prismarine Block (Block/Item)", "37cba233ffc457b3305228b25f35c02335611c9efb76698b5e94c0d541b5f4");
            }
        }

        public static ItemStack PrismarineDark
        {
            get
            {
                return GetHeadByTexture("Dark Prismarine (Block/Item)", "fd918598989549594446e83f33873891178da9db42f912e5272e1fb240312a");
            }
        }

        public static ItemStack Pumpkin
        {
            get
            {
                return GetHeadByTexture("Pumpkin (Block/Item)", "d7d7ad2dcb57dfa9f023dbb99b698fc53075c3e9d654506139a647ac907fddc5");
            }
        }

        public static ItemStack PumpkinRound
        {
            get
            {
                return GetHeadByTexture("Pumpkin - No Face (Round) (Block/Item)", "63979e9639a14c3d46ce46d714d423cb159091839a762cf939716f1511fd15");
            }
        }

        public static ItemStack PumpkinRoundFace
        {
            get
            {
                return GetHeadByTexture("Pumpkin - Face (Round) (Block/Item)", "ee28a03ed2eb90eaff1a119a5b554452701b97af47bff73ce710849c6b0");
            }
        }

        public static ItemStack PumpkinRoundJackOLantern
        {
            get
            {
                return GetHeadByTexture("Pumpkin - Jack'o'Latern (Round) (Block/Item)", "8db711ff52eedda59c434bb03169763d7c40b5b89127778feacd63aa94dfc");
            }
        }

        public static ItemStack PurpurBlock
        {
            get
            {
                return GetHeadByTexture("Purpur - Block (Block/Item)", "708f3832bc15fac41522ce6ed1d88f9543dd571f934073874ef0cb9bfccea");
            }
        }

        public static ItemStack PurpurPillar
        {
            get
            {
                return GetHeadByTexture("Purpur - Pillar (Block/Item)", "a452498bd9ba51a1424d96bf96be0cb103c9338defdf172ca13f5e24f5f9d8f");
            }
        }

        public static ItemStack SeaLantern
        {
            get
            {
                return GetHeadByTexture("Sea Lantern (Block/Item)", "824c6ff1714eb2c3b844d46d2e5ea2f26d273a33eaaa744abf645b060b47d7");
            }
        }

        public static ItemStack Slimeball
        {
            get
            {
                return GetHeadByTexture("Slimeball (Block/Item)", "4934a9f5ab1789a7d8dd96d32493cdacff577d8c81e7b23917dff2e32bd0bc10");
            }
        }

        public static ItemStack TNT
        {
            get
            {
                return GetHeadByTexture("TNT 1 (Block/Item)", "728a1815689d7194cf7db061b59f63106264b51387976a7fb74ab79b5641");
            }
        }

        public static ItemStack TNT2
        {
            get
            {
                return GetHeadByTexture("TNT 2 (Block/Item)", "eb994b41f07f87b328186acfcbdabc699d5b1847fabb2e49d5abc27865143a4e");
            }
        }

        public static ItemStack Water
        {
            get
            {
                return GetHeadByTexture("Water (Block/Item)", "5c7ecbfd6d33e873a1cf9a92f57f146152b52d9d7311694602671111a302f");
            }
        }

        #endregion

        #region Emoticon

        public static ItemStack[] Emoticons
        {
            get
            {
                return new[] {
                    EmoticonSmile,
                    EmoticonBigSmile,
                    EmoticonMustache,
                    EmoticonDerp,
                    EmoticonWink,
                    EmoticonBigGrin,
                    EmoticonCrying,
                    EmoticonDead,
                    EmoticonSurprised,
                    EmoticonCool,
                    EmoticonSad,
                    EmoticonKissy,
                    EmoticonEmbarrased,
                    EmoticonAngel,
                    EmoticonScared
                };
            }
        }

        public static ItemStack EmoticonSmile
        {
            get
            {
                return GetHeadByTexture("Smile (Emoticon)", "52e98165deef4ed621953921c1ef817dc638af71c1934a4287b69d7a31f6b8");
            }
        }

        public static ItemStack EmoticonBigSmile
        {
            get
            {
                return GetHeadByTexture("Big Smile (Emoticon)", "7ffaccf17879b17891fc5ef66472cc066a85bfa31b6d786c32afee4796068d");
            }
        }

        public static ItemStack EmoticonMustache
        {
            get
            {
                return GetHeadByTexture("Mustache (Emoticon)", "3636f2724aa6aa4de7ac46c19f3c845fb14847a518c8f7e03d792c82effb1");
            }
        }

        public static ItemStack EmoticonDerp
        {
            get
            {
                return GetHeadByTexture("Derp (Emoticon)", "3baabe724eae59c5d13f442c7dc5d2b1c6b70c2f83364a488ce5973ae80b4c3");
            }
        }

        public static ItemStack EmoticonWink
        {
            get
            {
                return GetHeadByTexture("Wink (Emoticon)", "f4ea2d6f939fefeff5d122e63dd26fa8a427df90b2928bc1fa89a8252a7e");
            }
        }

        public static ItemStack EmoticonBigGrin
        {
            get
            {
                return GetHeadByTexture("Big Grin (Emoticon)", "5059d59eb4e59c31eecf9ece2f9cf3934e45c0ec476fc86bfaef8ea913ea710");
            }
        }

        public static ItemStack EmoticonCrying
        {
            get
            {
                return GetHeadByTexture("Crying (Emoticon)", "1f1b875de49c587e3b4023ce24d472ff27583a1f054f37e73a1154b5b5498");
            }
        }

        public static ItemStack EmoticonDead
        {
            get
            {
                return GetHeadByTexture("Dead (Emoticon)", "b371e4e1cf6a1a36fdae27137fd9b8748e6169299925f9af2be301e54298c73");
            }
        }

        public static ItemStack EmoticonSurprised
        {
            get
            {
                return GetHeadByTexture("Surprised (Emoticon)", "bc2b9b9ae622bd68adff7180f8206ec4494abbfa130e94a584ec692e8984ab2");
            }
        }

        public static ItemStack EmoticonCool
        {
            get
            {
                return GetHeadByTexture("Cool (Emoticon)", "868f4cef949f32e33ec5ae845f9c56983cbe13375a4dec46e5bbfb7dcb6");
            }
        }

        public static ItemStack EmoticonSad
        {
            get
            {
                return GetHeadByTexture("Sad (Emoticon)", "14968ac5af3146826fa2b0d4dd114fda197f8b28f4750553f3f88836a21fac9");
            }
        }

        public static ItemStack EmoticonKissy
        {
            get
            {
                return GetHeadByTexture("Kissy (Emoticon)", "545bd18a2aaf469fad72e52cde6cfb02bfbaa5bfed2a8151277f779ebcdcec1");
            }
        }

        public static ItemStack EmoticonEmbarrased
        {
            get
            {
                return GetHeadByTexture("Embarrased (Emoticon)", "f720df911c052377065408db78a25c678f791eb944c063935ae86dbe51c71b");
            }
        }

        public static ItemStack EmoticonAngel
        {
            get
            {
                return GetHeadByTexture("Angel (Emoticon)", "3e1debc73231f8ed4b69d5c3ac1b1f18f3656a8988e23f2e1bdbc4e85f6d46a");
            }
        }

        public static ItemStack EmoticonScared
        {
            get
            {
                return GetHeadByTexture("Scared (Emoticon)", "636e26c44659e8148ed58aa79e4d60db595f426442116f81b5415c2446ed8");
            }
        }

        #endregion

        #region Color Egg

        public static ItemStack[] ColorEggs
        {
            get
            {
                return new[] {
                    BlackEgg,
                    RedEgg
                };
            }
        }

        public static ItemStack BlackEgg
        {
            get
            {
                return GetHeadByTexture("Black (Color) (Egg)", "c4db4adfa9bf48ff5d41707ae34ea78bd2371659fcd8cd8934749af4cce9b");
            }
        }

        public static ItemStack RedEgg
        {
            get
            {
                return GetHeadByTexture("Red (Color) (Egg)", "93a8b0e9a06f19931ea597ba76823b59404841d53bc481127be9f710c6b998");
            }
        }

        #endregion

        #region Spawn Egg

        public static ItemStack[] SpawnEggs
        {
            get
            {
                return new[] {
                    BatEgg,
                    BlazeEgg,
                    CaveSpiderEgg,
                    ChickenEgg,
                    CowEgg,
                    CreeperEgg,
                    EndermanEgg,
                    EndermiteEgg,
                    GhastEgg,
                    GuardianEgg,
                    HorseEgg,
                    MushroomCowEgg,
                    MagmaCubeEgg,
                    OcelotEgg,
                    PigEgg,
                    RabbitEgg,
                    SheepEgg,
                    SlimeEgg,
                    SpiderEgg,
                    SilverfishEgg,
                    SkeletonEgg,
                    SquidEgg,
                    VillagerEgg,
                    WitchEgg,
                    WolfEgg,
                    ZombieEgg,
                    ZombiePigmanEgg
                };
            }
        }

        public static ItemStack BatEgg
        {
            get
            {
                return GetHeadByTexture("Bat (Egg)", "93c8aa3fde295fa9f9c27f734bdbab11d33a2e43e855accd7465352377413b");
            }
        }

        public static ItemStack BlazeEgg
        {
            get
            {
                return GetHeadByTexture("Blaze (Egg)", "533acae6e075a578ccfc7dc2d5a15dbccfa8f59c609f9703889ef54c742c56");
            }
        }

        public static ItemStack CaveSpiderEgg
        {
            get
            {
                return GetHeadByTexture("Cave Spider (Egg)", "16617131250e578333a441fdf4a5b8c62163640a9d06cd67db89031d03accf6");
            }
        }

        public static ItemStack ChickenEgg
        {
            get
            {
                return GetHeadByTexture("Chicken (Egg)", "d429ff1d2015cb11398471bb2f895f7b4c3ccec201e4ad7a86ff24b744878c");
            }
        }

        public static ItemStack CowEgg
        {
            get
            {
                return GetHeadByTexture("Cow (Egg)", "9419f15ff54dae5d040f9b9d8eb2a8989e676710922a0ca164da613ca61e9");
            }
        }

        public static ItemStack CreeperEgg
        {
            get
            {
                return GetHeadByTexture("Creeper (Egg)", "87c63d9079b75f90979783cf07ca726f65e3024415ac622a7c906cd25082af");
            }
        }

        public static ItemStack EndermanEgg
        {
            get
            {
                return GetHeadByTexture("Enderman (Egg)", "37c0d010dd0e512ffea108d7c5fe69d576c31ec266c884b51ec0b28cc457");
            }
        }

        public static ItemStack EndermiteEgg
        {
            get
            {
                return GetHeadByTexture("Endermite (Egg)", "3beac501e97db1cc035287d068a8eb538e55ef802f5cca25683933a243136c");
            }
        }

        public static ItemStack GhastEgg
        {
            get
            {
                return GetHeadByTexture("Ghast (Egg)", "c442c228f099fdfc1c6b46dfc80b252d81f7fb1739deb16ee7a597c17f7c9");
            }
        }

        public static ItemStack GuardianEgg
        {
            get
            {
                return GetHeadByTexture("Guardian (Egg)", "fe119aaa4999648a75b978efafa97edab1cdca1ff1d8301ba61cdc2db1606e22");
            }
        }

        public static ItemStack HorseEgg
        {
            get
            {
                return GetHeadByTexture("Horse (Egg)", "5c6d5abbf68ccb2386bf16af25ac38d8b77bb0e043152461bd97f3f630dbb8bc");
            }
        }

        public static ItemStack MushroomCowEgg
        {
            get
            {
                return GetHeadByTexture("Mushroom Cow (Egg)", "41b3b02e36ae9850df25aa09c2ca5a17b9c6616ce53e0b141ad360b6c67c");
            }
        }

        public static ItemStack MagmaCubeEgg
        {
            get
            {
                return GetHeadByTexture("Magma Cube (Egg)", "1185657c38acdd8f95e1d2cd1115bb0f11139ad2b3ce442267e69706d916e");
            }
        }

        public static ItemStack OcelotEgg
        {
            get
            {
                return GetHeadByTexture("Ocelot (Egg)", "c579a743b66bd0b4d559898ed1b9857a49f1127d9d237bed3dc97bceb9379a5");
            }
        }

        public static ItemStack PigEgg
        {
            get
            {
                return GetHeadByTexture("Pig (Egg)", "527ad51dd773b72dca1c13e6f3547a83181aad91165282999bbdf13a3b3c9");
            }
        }

        public static ItemStack RabbitEgg
        {
            get
            {
                return GetHeadByTexture("Rabbit (Egg)", "63e06ed8809243e317393f6162679b2c1fe6911eda2d30cb99cfc82d347cb");
            }
        }

        public static ItemStack SheepEgg
        {
            get
            {
                return GetHeadByTexture("Sheep (Egg)", "ff481f77347fe59c083665c9efbb49071d493ea2027454aee87735d63bf3b");
            }
        }

        public static ItemStack SlimeEgg
        {
            get
            {
                return GetHeadByTexture("Slime (Egg)", "9330af17f8512ed3b49e78bca7ef2d83f2dc1e598a8cb542ecc3b6becee9f57");
            }
        }

        public static ItemStack SpiderEgg
        {
            get
            {
                return GetHeadByTexture("Spider (Egg)", "5d59aa78cb7e9b6ca6fee4121329059dd68afddc0c8b53a906b7953994e8a76");
            }
        }

        public static ItemStack SilverfishEgg
        {
            get
            {
                return GetHeadByTexture("Silverfish (Egg)", "d06310a8952b265c6e6bed4348239ddea8e5482c8c68be6fff981ba8056bf2e");
            }
        }

        public static ItemStack SkeletonEgg
        {
            get
            {
                return GetHeadByTexture("Skeleton (Egg)", "377055cadacbb0f8f35c1d18acc2ed86e0bcc6d73dda71e4c59f7ea28b7b27b6");
            }
        }

        public static ItemStack SquidEgg
        {
            get
            {
                return GetHeadByTexture("Squid (Egg)", "449088861fc1e14b605a5154d79fa7dd65e041a5c635d24744b3e152535");
            }
        }

        public static ItemStack VillagerEgg
        {
            get
            {
                return GetHeadByTexture("Villager (Egg)", "f3b36425cbfa48e4684c8e1a71477919e0a46b613511a8436c775b15f5615");
            }
        }

        public static ItemStack WitchEgg
        {
            get
            {
                return GetHeadByTexture("Witch (Egg)", "afbdceef773d959b49ddd9615f4269c176e23154d45752667428dc4e3fd4d");
            }
        }

        public static ItemStack WolfEgg
        {
            get
            {
                return GetHeadByTexture("Wolf (Egg)", "4399c973d6496d1d258492c28d4c95956ac3a253762bf15f7644af1f5728dd");
            }
        }

        public static ItemStack ZombieEgg
        {
            get
            {
                return GetHeadByTexture("Zombie (Egg)", "77f844bfea25429d45e1fcf96ef6654dfaaa6fc902dc1b6b68c0abc1343447");
            }
        }

        public static ItemStack ZombiePigmanEgg
        {
            get
            {
                return GetHeadByTexture("Zombie Pigman (Egg)", "5f7de9fafa8fcb77d5944e628b72042b9f7988de43e422983c78d3762d6d7");
            }
        }

        #endregion

        #region Decoration

        public static ItemStack[] Decorations
        {
            get
            {
                return new[] {
                    CreeperStone,
                    Globe,
                    RedLampDown,
                    RedLampSide,
                    TrafficLight,
                    TrafficLightGreen,
                    TrafficLightRed,
                    TrafficLightYellow
                };
            }
        }

        public static ItemStack CreeperStone
        {
            get
            {
                return GetHeadByTexture("Creeper Stone (Decoration)", "b7a6636d96d08f61939a60f57ee0d0299c0bd273b2bd30c1efe4f74e433a");
            }
        }

        public static ItemStack Globe
        {
            get
            {
                return GetHeadByTexture("Globe (Decoration)", "98daa1e3ed94ff3e33e1d4c6e43f024c47d78a57ba4d38e75e7c9264106");
            }
        }

        public static ItemStack RedLampDown
        {
            get
            {
                return GetHeadByTexture("Red Lamp - Down (Decoration)", "9f446a8f9284c62cf8d491fbdb338fd39ebbebe35e959c2fc4f786c67522efb");
            }
        }

        public static ItemStack RedLampSide
        {
            get
            {
                return GetHeadByTexture("Red Lamp - Side (Decoration)", "8e2c18ab35949bf9f9e7d6a69b885ccd8cc2efb9475946d7d3fb5c3fef61");
            }
        }

        public static ItemStack TrafficLight
        {
            get
            {
                return GetHeadByTexture("TrafficLight - Off (Decoration)", "f986b3f5ebf415a3c418781489bf55be5d5d275a5bafd1deaec2da63b0f0");
            }
        }

        public static ItemStack TrafficLightGreen
        {
            get
            {
                return GetHeadByTexture("TrafficLight - Green (Decoration)", "f45c9acea8da71b4f252cd4deb5943f49e7dbc0764274b25a6a6f5875baea3");
            }
        }

        public static ItemStack TrafficLightRed
        {
            get
            {
                return GetHeadByTexture("TrafficLight - Red (Decoration)", "ad5fcd31287d63e7826ea760a7ed154f685dfdc7f3465732a96e619b2e1347");
            }
        }

        public static ItemStack TrafficLightYellow
        {
            get
            {
                return GetHeadByTexture("TrafficLight - Yellow (Decoration)", "7440123e50d4b3d8a2a8b3581ef483fe3335ff57cfac15a21cbced2ac2cfe4");
            }
        }

        #endregion

        #region Dice

        #endregion

        #region Presents

        #endregion

        #region Pods

        #endregion

        #region Right x Wrong

        public static ItemStack BlackRight
        {
            get
            {
                return GetHeadByTexture("Right - Black (Right/Wrong)", "8a99342e2c73a9f3822628e796488234f258446f5a2d4d59dde4aa87db98");
            }
        }

        public static ItemStack BlackWrong
        {
            get
            {
                return GetHeadByTexture("Wrong - Black (Right/Wrong)", "16c60da414bf037159c8be8d09a8ecb919bf89a1a21501b5b2ea75963918b7b");
            }
        }

        public static ItemStack BlackQuestion
        {
            get
            {
                return GetHeadByTexture("Question - Black (Right/Wrong)", "638c53e66f28cf2c7fb1523c9e5de1ae0cf4d7a1faf553e752494a8d6d2e32");
            }
        }

        public static ItemStack[] BlackRightWrongQuestion
        {
            get
            {
                return new[] {
                    BlackRight,
                    BlackWrong,
                    BlackQuestion
                };
            }
        }

        #endregion

        #region Alphabets

        public const string Alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 :?!./+-=(),;\"%[]_#\\";

        public static ItemStack WoodenChar(char c)
        {
            c = c.ToUpper();
            int index = Alphabet.IndexOf(c);
            if (index == -1)
                return WoodenHeart;
            else
                return WoodenAlphabet[index];
        }

        public static ItemStack[] WoodenString(string s)
        {
            ItemStack[] stacks = new ItemStack[s.Length];
            for (int i = 0; i < s.Length; i++)
                stacks[i] = WoodenChar(s[i]);
            return stacks;
        }

        public static ItemStack WoodenHeart
        {
            get
            {
                return GetHeadByTexture("Heart - Wooden (Placeholder) (Char)", "336febeca7c488a6671dc071655dde2a1b65c3ccb20b6e8eaf9bfb08e64b80");
            }
        }

        public static ItemStack[] WoodenAlphabet
        {
            get
            {
                return new[] {
                    GetHeadByTexture("A - Wooden (Char)", "a67d813ae7ffe5be951a4f41f2aa619a5e3894e85ea5d4986f84949c63d7672e"),//A
					GetHeadByTexture("B - Wooden (Char)", "50c1b584f13987b466139285b2f3f28df6787123d0b32283d8794e3374e23"),//B
					GetHeadByTexture("C - Wooden (Char)", "abe983ec478024ec6fd046fcdfa4842676939551b47350447c77c13af18e6f"),//C
					GetHeadByTexture("D - Wooden (Char)", "3193dc0d4c5e80ff9a8a05d2fcfe269539cb3927190bac19da2fce61d71"),//D
					GetHeadByTexture("E - Wooden (Char)", "dbb2737ecbf910efe3b267db7d4b327f360abc732c77bd0e4eff1d510cdef"),//E
					GetHeadByTexture("F - Wooden (Char)", "b183bab50a3224024886f25251d24b6db93d73c2432559ff49e459b4cd6a"),//F
					GetHeadByTexture("G - Wooden (Char)", "1ca3f324beeefb6a0e2c5b3c46abc91ca91c14eba419fa4768ac3023dbb4b2"),//G
					GetHeadByTexture("H - Wooden (Char)", "31f3462a473549f1469f897f84a8d4119bc71d4a5d852e85c26b588a5c0c72f"),//H
					GetHeadByTexture("I - Wooden (Char)", "46178ad51fd52b19d0a3888710bd92068e933252aac6b13c76e7e6ea5d3226"),//I
					GetHeadByTexture("J - Wooden (Char)", "3a79db9923867e69c1dbf17151e6f4ad92ce681bcedd3977eebbc44c206f49"),//J
					GetHeadByTexture("K - Wooden (Char)", "9461b38c8e45782ada59d16132a4222c193778e7d70c4542c9536376f37be42"),//K
					GetHeadByTexture("L - Wooden (Char)", "319f50b432d868ae358e16f62ec26f35437aeb9492bce1356c9aa6bb19a386"),//L
					GetHeadByTexture("M - Wooden (Char)", "49c45a24aaabf49e217c15483204848a73582aba7fae10ee2c57bdb76482f"),//M
					GetHeadByTexture("N - Wooden (Char)", "35b8b3d8c77dfb8fbd2495c842eac94fffa6f593bf15a2574d854dff3928"),//N
					GetHeadByTexture("O - Wooden (Char)", "d11de1cadb2ade61149e5ded1bd885edf0df6259255b33b587a96f983b2a1"),//O
					GetHeadByTexture("P - Wooden (Char)", "a0a7989b5d6e621a121eedae6f476d35193c97c1a7cb8ecd43622a485dc2e912"),//P
					GetHeadByTexture("Q - Wooden (Char)", "43609f1faf81ed49c5894ac14c94ba52989fda4e1d2a52fd945a55ed719ed4"),//Q
					GetHeadByTexture("R - Wooden (Char)", "a5ced9931ace23afc351371379bf05c635ad186943bc136474e4e5156c4c37"),//R
					GetHeadByTexture("S - Wooden (Char)", "3e41c60572c533e93ca421228929e54d6c856529459249c25c32ba33a1b1517"),//S
					GetHeadByTexture("T - Wooden (Char)", "1562e8c1d66b21e459be9a24e5c027a34d269bdce4fbee2f7678d2d3ee4718"),//T
					GetHeadByTexture("U - Wooden (Char)", "607fbc339ff241ac3d6619bcb68253dfc3c98782baf3f1f4efdb954f9c26"),//U
					GetHeadByTexture("V - Wooden (Char)", "cc9a138638fedb534d79928876baba261c7a64ba79c424dcbafcc9bac7010b8"),//V
					GetHeadByTexture("W - Wooden (Char)", "269ad1a88ed2b074e1303a129f94e4b710cf3e5b4d995163567f68719c3d9792"),//W
					GetHeadByTexture("X - Wooden (Char)", "5a6787ba32564e7c2f3a0ce64498ecbb23b89845e5a66b5cec7736f729ed37"),//X
					GetHeadByTexture("Y - Wooden (Char)", "c52fb388e33212a2478b5e15a96f27aca6c62ac719e1e5f87a1cf0de7b15e918"),//Y
					GetHeadByTexture("Z - Wooden (Char)", "90582b9b5d97974b11461d63eced85f438a3eef5dc3279f9c47e1e38ea54ae8d"),//Z

					GetHeadByTexture("0 - Wooden (Char)", "0ebe7e5215169a699acc6cefa7b73fdb108db87bb6dae2849fbe24714b27"),//0
					GetHeadByTexture("1 - Wooden (Char)", "71bc2bcfb2bd3759e6b1e86fc7a79585e1127dd357fc202893f9de241bc9e530"),//1
					GetHeadByTexture("2 - Wooden (Char)", "4cd9eeee883468881d83848a46bf3012485c23f75753b8fbe8487341419847"),//2
					GetHeadByTexture("3 - Wooden (Char)", "1d4eae13933860a6df5e8e955693b95a8c3b15c36b8b587532ac0996bc37e5"),//3
					GetHeadByTexture("4 - Wooden (Char)", "d2e78fb22424232dc27b81fbcb47fd24c1acf76098753f2d9c28598287db5"),//4
					GetHeadByTexture("5 - Wooden (Char)", "6d57e3bc88a65730e31a14e3f41e038a5ecf0891a6c243643b8e5476ae2"),//5
					GetHeadByTexture("6 - Wooden (Char)", "334b36de7d679b8bbc725499adaef24dc518f5ae23e716981e1dcc6b2720ab"),//6
					GetHeadByTexture("7 - Wooden (Char)", "6db6eb25d1faabe30cf444dc633b5832475e38096b7e2402a3ec476dd7b9"),//7
					GetHeadByTexture("8 - Wooden (Char)", "59194973a3f17bda9978ed6273383997222774b454386c8319c04f1f4f74c2b5"),//8
					GetHeadByTexture("9 - Wooden (Char)", "e67caf7591b38e125a8017d58cfc6433bfaf84cd499d794f41d10bff2e5b840"),//9

					GetHeadByTexture("  - Wooden (Char)", "5db532b5cced46b4b535ece16eced7bbc5cac55594d61e8b8f8eac4299c9fc"),// 
					GetHeadByTexture(": - Wooden (Char)", "ccbee28e2c79db138f3977ba472dfae6b11a9bb82d5b3d7f25479338fff1fe92"),//:
					GetHeadByTexture("? - Wooden (Char)", "5163dafac1d91a8c91db576caac784336791a6e18d8f7f62778fc47bf146b6"),//?
					GetHeadByTexture("! - Wooden (Char)", "6a53bdd1545531c9ebb9c6f895bc576012f61820e6f489885988a7e8709a3f48"),//!
					GetHeadByTexture(". - Wooden (Char)", "733aa24916c88696ee71db7ac8cd306ad73096b5b6ffd868e1c384b1d62cfb3c"),//.
					GetHeadByTexture("/ - Wooden (Char)", "7f95d7c1bbf3afa285d8d96757bb5572259a3ae854f5389dc53207699d94fd8"),// /

					GetHeadByTexture("+ - Wooden (Char)", "3edd20be93520949e6ce789dc4f43efaeb28c717ee6bfcbbe02780142f716"),//+
					GetHeadByTexture("- - Wooden (Char)", "bd8a99db2c37ec71d7199cd52639981a7513ce9cca9626a3936f965b131193"),//-
					GetHeadByTexture("= - Wooden (Char)", "787689f833436aa711abbd45168856775a2b114656cdf4dc5a6c6f1afae520"),//=
					GetHeadByTexture("( - Wooden (Char)", "83b13ff8e7a3fab5ff1e98e8fd939cc097b6b45c85240d32450994f7671439d"),//(
					GetHeadByTexture(") - Wooden (Char)", "6176ef0558815b7f05c153cf4cdc69f9ba4ba1c10ea1a8749aeb8c2b977e"),//)
					GetHeadByTexture(", - Wooden (Char)", "7840b4e7a680434857427111bdaf64e7fba912ea5707182e98bc585ccb53"),//,
					GetHeadByTexture("; - Wooden (Char)", "971f4d2bffb594c158c6ce1ef9d982b6fd2458275e7f89655bb74ede768ded"),//;
					GetHeadByTexture("\" - Wooden (Char)", "4757e972c763149476bc556cfbad3614c1d49954f13f8893eb9d105845753f2e"),//"
					GetHeadByTexture("% - Wooden (Char)", "50851cf062548c436253c337a4112cfc985443a748d931cf201d1e84fc72b12c"),//%
					GetHeadByTexture("[ - Wooden (Char)", "2cc6bd13a61fcba4a3859ec06bb8c823739b6beacba8cfa0152e8c60db94"),//[
					GetHeadByTexture("] - Wooden (Char)", "a6507336e35066daa8e4e6e624f93f65e35ee974e9e8b64d2dd9904ecefda2"),//]
					GetHeadByTexture("_ - Wooden (Char)", "7966f891c1546aecbfcc3baedcfb67079d7f2a6a8b739ed5bac2bb3cf308d38"),//_
					GetHeadByTexture("# - Wooden (Char)", "9ae85f74f8e2c054b781a29fa9b25934ba63bb79f1de8a95b436d9bfdcaf4cd"),//#
					GetHeadByTexture("\\ - Wooden (Char)", "c362daa41a1254b2ed6e9fd3a015b4d7a419ba2920de10b478251f49aaca19")// \
				};
            }
        }

        public static ItemStack StoneChar(char c)
        {
            c = c.ToUpper();
            int index = Alphabet.IndexOf(c);
            if (index == -1)
                return StoneHeart;
            else
                return StoneAlphabet[index];
        }

        public static ItemStack[] StoneString(string s)
        {
            ItemStack[] stacks = new ItemStack[s.Length];
            for (int i = 0; i < s.Length; i++)
                stacks[i] = StoneChar(s[i]);
            return stacks;
        }

        public static ItemStack StoneHeart
        {
            get
            {
                return GetHeadByTexture("Heart - Stone (Placeholder) (Char)", "");
            }
        }

        public static ItemStack[] StoneAlphabet
        {
            get
            {
                return new[] {
                    GetHeadByTexture("A - Stone (Char)", "2ac58b1a3b53b9481e317a1ea4fc5eed6bafca7a25e741a32e4e3c2841278c"),//A
					GetHeadByTexture("B - Stone (Char)", "d4c711571e7e214ee78dfe4ee0e1263b92516e418de8fc8f3257ae0901431"),//B
					GetHeadByTexture("C - Stone (Char)", "fff5aabead6feafaaecf4422cdd7837cbb36b03c9841dd1b1d2d3edb7825e851"),//C
					GetHeadByTexture("D - Stone (Char)", "893e622b581975792f7c119ec6f40a4f16e552bb98776b0c7ae2bdfd4154fe7"),//D
					GetHeadByTexture("E - Stone (Char)", "a157d65b19921c760ff4910b3404455b9c2ee36afc202d8538baefec676953"),//E
					GetHeadByTexture("F - Stone (Char)", "c54cf261b2cd6ab54b0c624f8f6ff565a7b63e28e3b50c6dbfb52b5f0d7cf9f"),//F
					GetHeadByTexture("G - Stone (Char)", "d3c9f8a74ca01ba8c54de1edc82e1fc07a83923e66574b6ffe606919240c6"),//G
					GetHeadByTexture("H - Stone (Char)", "f8c58c509034617bf81ee0db9be0ba3e85ca15568163914c87669edb2fd7"),//H
					GetHeadByTexture("I - Stone (Char)", "4246323c9fb319326ee2bf3f5b63ec3d99df76a12439bf0b4c3ab32d13fd9"),//I
					GetHeadByTexture("J - Stone (Char)", "c58456cd9bb8a7e978591ae0cb26af1aadad4fa7a16725b295145e09bed8064"),//J
					GetHeadByTexture("K - Stone (Char)", "af49fb708369e7bc2944ad706963fb6ac6ce6d4c67081ddadecfe5da51"),//K
					GetHeadByTexture("L - Stone (Char)", "8c84f75416e853a74f6c70fc7e1093d53961879955b433bd8c7c6d5a6df"),//L
					GetHeadByTexture("M - Stone (Char)", "31fde91b19b9309913724fea9e85311271c67bcb78578d461bf65d9613074"),//M
					GetHeadByTexture("N - Stone (Char)", "1c7c972e6785d6b0aceb779abdd7702d98341c24c2a71e702930eca58055"),//N
					GetHeadByTexture("O - Stone (Char)", "8073bb44f9345f9bb31a679027e7939e461842a8c27486d7a6b842c39eb38c4e"),//O
					GetHeadByTexture("P - Stone (Char)", "64b231a8d55870cfb5a9f4e65db06dd7f8e34282f1416f95878b19acc34ac95"),//P
					GetHeadByTexture("Q - Stone (Char)", "ffedd6f9efdb156b86935699b2b4834df0f5d214513c01d38af3bd031cbcc92"),//Q
					GetHeadByTexture("R - Stone (Char)", "c03a1cd583cbbffde08f943e56ac3e3afafecaede834221a81e6db6c64667f7d"),//R
					GetHeadByTexture("S - Stone (Char)", "b6572e655725d78375a9817eb9ee8b37829ca1fea93b6095cc7aa19e5eac"),//S
					GetHeadByTexture("T - Stone (Char)", "708c9ef3a3751e254e2af1ad8b5d668ccf5c6ec3ea2641877cba575807d39"),//T
					GetHeadByTexture("U - Stone (Char)", "55a6e3ae5ae625923524838fac9fef5b42527f5027c9ca149e6c207792eb"),//U
					GetHeadByTexture("V - Stone (Char)", "975121f7d9c68da0e5b6a96ac615298b12b2ee5bd19989436ee647879da5b"),//V
					GetHeadByTexture("W - Stone (Char)", "67e165c3edc5541d4654c4728871e6908f613fc0ec46e823c96eac82ac62e62"),//W
					GetHeadByTexture("X - Stone (Char)", "1919d1594bf809db7b44b3782bf90a69f449a87ce5d18cb40eb653fdec2722"),//X
					GetHeadByTexture("Y - Stone (Char)", "e35424bb86305d7747604b13e924d74f1efe38906e4e458dd18dcc67b6ca48"),//Y
					GetHeadByTexture("Z - Stone (Char)", "4e91200df1cae51acc071f85c7f7f5b8449d39bb32f363b0aa51dbc85d133e"),//Z

					GetHeadByTexture("0 - Stone (Char)", "55a224807693978ed834355f9e5145f9c56ef68cf6f2c9e1734a46e246aae1"),//0
					GetHeadByTexture("1 - Stone (Char)", "31a9463fd3c433d5e1d9fec6d5d4b09a83a970b0b74dd546ce67a73348caab"),//1
					GetHeadByTexture("2 - Stone (Char)", "acb419d984d8796373c9646233c7a02664bd2ce3a1d3476dd9b1c5463b14ebe"),//2
					GetHeadByTexture("3 - Stone (Char)", "f8ebab57b7614bb22a117be43e848bcd14daecb50e8f5d0926e4864dff470"),//3
					GetHeadByTexture("4 - Stone (Char)", "62bfcfb489da867dce96e3c3c17a3db7c79cae8ac1f9a5a8c8ac95e4ba3"),//4
					GetHeadByTexture("5 - Stone (Char)", "ef4ecf110b0acee4af1da343fb136f1f2c216857dfda6961defdbee7b9528"),//5
					GetHeadByTexture("6 - Stone (Char)", "f331a6a6fcd6995b62088d353bfb68d9b89ae258325caf3f2886464f54a7329"),//6
					GetHeadByTexture("7 - Stone (Char)", "d4ba6ac07d422377a855793f36dea2ed240223f52fd1648181612ecd1a0cfd5"),//7
					GetHeadByTexture("8 - Stone (Char)", "c61a8a641437be9aea207253dd3f25440d954ea2b5866c552f386b29ac4d049"),//8
					GetHeadByTexture("9 - Stone (Char)", "a1928e1bfd86a9b79397c4cb4b65ef99af49b7d5f7957ad62c0c699a622cfbe"),//9

					GetHeadByTexture("  - Stone (Char)", "de9b8aae7f9cc76d625ccb8abc686f30d38f9e6c42533098b9ad577f91c333c"),// 
					GetHeadByTexture(": - Stone (Char)", "bd898c40e47c5d2d76924065360768065d624ee5b9ee0be9e12b98fb77c76"),//:
					GetHeadByTexture("? - Stone (Char)", "d23eaefbd581159384274cdbbd576ced82eb72423f2ea887124f9ed33a6872c"),//?
					GetHeadByTexture("! - Stone (Char)", "87d19aabfcfd99ffaba4214caef299516ce52e6d13bf2dda125985e481b72f9"),//!
					GetHeadByTexture(". - Stone (Char)", "6ff99ff279a2cf25deb4bd5b66c3576b824cc96c36781027af727ed3a4c1308e"),//.
					GetHeadByTexture("/ - Stone (Char)", "2d593f0945cbb85a8e0be7d9a526010ee774810f2bc428cd4a23e4d232eff8"),// /

					GetHeadByTexture("+ - Stone (Char)", ""),//+
					GetHeadByTexture("- - Stone (Char)", ""),//-
					GetHeadByTexture("= - Stone (Char)", ""),//=
					GetHeadByTexture("( - Stone (Char)", ""),//(
					GetHeadByTexture(") - Stone (Char)", ""),//)
					GetHeadByTexture(", - Stone (Char)", ""),//,
					GetHeadByTexture("; - Stone (Char)", ""),//;
					GetHeadByTexture("\" - Stone (Char)", ""),//"
					GetHeadByTexture("% - Stone (Char)", ""),//%
					GetHeadByTexture("[ - Stone (Char)", ""),//[
					GetHeadByTexture("] - Stone (Char)", ""),//]
					GetHeadByTexture("_ - Stone (Char)", ""),//_
					GetHeadByTexture("# - Stone (Char)", ""),//#
					GetHeadByTexture("\\ - Stone (Char)", "")// \
				};
            }
        }

        public static ItemStack IronChar(char c)
        {
            c = c.ToUpper();
            int index = Alphabet.IndexOf(c);
            if (index == -1)
                return IronHeart;
            else
                return IronAlphabet[index];
        }

        public static ItemStack[] IronString(string s)
        {
            ItemStack[] stacks = new ItemStack[s.Length];
            for (int i = 0; i < s.Length; i++)
                stacks[i] = IronChar(s[i]);
            return stacks;
        }

        public static ItemStack IronHeart
        {
            get
            {
                return GetHeadByTexture("Heart - Iron (Placeholder) (Char)", "");
            }
        }

        public static ItemStack[] IronAlphabet
        {
            get
            {
                return new[] {
                    GetHeadByTexture("A - Iron (Char)", ""),//A
					GetHeadByTexture("B - Iron (Char)", ""),//B
					GetHeadByTexture("C - Iron (Char)", ""),//C
					GetHeadByTexture("D - Iron (Char)", ""),//D
					GetHeadByTexture("E - Iron (Char)", ""),//E
					GetHeadByTexture("F - Iron (Char)", ""),//F
					GetHeadByTexture("G - Iron (Char)", ""),//G
					GetHeadByTexture("H - Iron (Char)", ""),//H
					GetHeadByTexture("I - Iron (Char)", ""),//I
					GetHeadByTexture("J - Iron (Char)", ""),//J
					GetHeadByTexture("K - Iron (Char)", ""),//K
					GetHeadByTexture("L - Iron (Char)", ""),//L
					GetHeadByTexture("M - Iron (Char)", ""),//M
					GetHeadByTexture("N - Iron (Char)", ""),//N
					GetHeadByTexture("O - Iron (Char)", ""),//O
					GetHeadByTexture("P - Iron (Char)", ""),//P
					GetHeadByTexture("Q - Iron (Char)", ""),//Q
					GetHeadByTexture("R - Iron (Char)", ""),//R
					GetHeadByTexture("S - Iron (Char)", ""),//S
					GetHeadByTexture("T - Iron (Char)", ""),//T
					GetHeadByTexture("U - Iron (Char)", ""),//U
					GetHeadByTexture("V - Iron (Char)", ""),//V
					GetHeadByTexture("W - Iron (Char)", ""),//W
					GetHeadByTexture("X - Iron (Char)", ""),//X
					GetHeadByTexture("Y - Iron (Char)", ""),//Y
					GetHeadByTexture("Z - Iron (Char)", ""),//Z

					GetHeadByTexture("0 - Iron (Char)", ""),//0
					GetHeadByTexture("1 - Iron (Char)", ""),//1
					GetHeadByTexture("2 - Iron (Char)", ""),//2
					GetHeadByTexture("3 - Iron (Char)", ""),//3
					GetHeadByTexture("4 - Iron (Char)", ""),//4
					GetHeadByTexture("5 - Iron (Char)", ""),//5
					GetHeadByTexture("6 - Iron (Char)", ""),//6
					GetHeadByTexture("7 - Iron (Char)", ""),//7
					GetHeadByTexture("8 - Iron (Char)", ""),//8
					GetHeadByTexture("9 - Iron (Char)", ""),//9

					GetHeadByTexture("  - Iron (Char)", ""),// 
					GetHeadByTexture(": - Iron (Char)", ""),//:
					GetHeadByTexture("? - Iron (Char)", ""),//?
					GetHeadByTexture("! - Iron (Char)", ""),//!
					GetHeadByTexture(". - Iron (Char)", ""),//.
					GetHeadByTexture("/ - Iron (Char)", ""),// /

					GetHeadByTexture("+ - Iron (Char)", ""),//+
					GetHeadByTexture("- - Iron (Char)", ""),//-
					GetHeadByTexture("= - Iron (Char)", ""),//=
					GetHeadByTexture("( - Iron (Char)", ""),//(
					GetHeadByTexture(") - Iron (Char)", ""),//)
					GetHeadByTexture(", - Iron (Char)", ""),//,
					GetHeadByTexture("; - Iron (Char)", ""),//;
					GetHeadByTexture("\" - Iron (Char)", ""),//"
					GetHeadByTexture("% - Iron (Char)", ""),//%
					GetHeadByTexture("[ - Iron (Char)", ""),//[
					GetHeadByTexture("] - Iron (Char)", ""),//]
					GetHeadByTexture("_ - Iron (Char)", ""),//_
					GetHeadByTexture("# - Iron (Char)", ""),//#
					GetHeadByTexture("\\ - Iron (Char)", "")// \
				};
            }
        }

        public static ItemStack DisplayChar(char c)
        {
            c = c.ToUpper();
            int index = Alphabet.IndexOf(c);
            if (index == -1)
                return IronHeart;
            else
                return IronAlphabet[index];
        }

        public static ItemStack[] DisplayString(string s)
        {
            ItemStack[] stacks = new ItemStack[s.Length];
            for (int i = 0; i < s.Length; i++)
                stacks[i] = DisplayChar(s[i]);
            return stacks;
        }

        public static ItemStack DisplayHeart
        {
            get
            {
                return GetHeadByTexture("Heart - Display (Placeholder) (Char)", "");
            }
        }

        public static ItemStack[] DisplayAlphabet
        {
            get
            {
                return new[] {
                    GetHeadByTexture("A - Display (Char)", "f748f213588dbf4415ce24fe66de3526816bf35df8e398f98efec2fb08956a3"),//A
					GetHeadByTexture("B - Display (Char)", "10dcdca2a45e3abf88d63c416faecea9ac285484bebedecbebb80779228417f"),//B
					GetHeadByTexture("C - Display (Char)", "3234103821e7cc45cff9f4cdabf15bb6788b82655ab036f2a0a1f9d47013be2f"),//C
					GetHeadByTexture("D - Display (Char)", "7725c32c336f75f79e660a6feeb375ecd61fc8bcc5e7a7decf4855c995890"),//D
					GetHeadByTexture("E - Display (Char)", "ed8115b1adbb39cdbcbcd034e9560de7dab4025bb4bc43c49b43d15dd8a77"),//E
					GetHeadByTexture("F - Display (Char)", "7bb75a3ea33fb6636f2bfafe897638bb2df912f26f5a24b699b50b31b547a"),//F
					GetHeadByTexture("G - Display (Char)", "7c423ab8df97bf1b9a9c1d0e471690cf677e4fca10b376c5e6c07692b55d14"),//G
					GetHeadByTexture("H - Display (Char)", "ee9ae3e7e7cbd739525189a7ac3c91d261fc29d2efedb702264d749aed3d92a"),//H
					GetHeadByTexture("I - Display (Char)", "e37889b98f351c2db12bf1ffa57d08cb6460c649c902135b133e26961b"),//I
					GetHeadByTexture("J - Display (Char)", "bbfa2f65e540f876cc999466d9f596ea1f9934f62583ccf4679abf6b18d44389"),//J
					GetHeadByTexture("K - Display (Char)", "ffdce233aeabf2915f3a6875d268228ff908236a563e1fdd14d59b931d499b"),//K
					GetHeadByTexture("L - Display (Char)", "baeaa9ef47caf6e44e59aae99bedd8949e14236230801f39ed2a2e29783"),//L
					GetHeadByTexture("M - Display (Char)", "5aa54d742492989f739deb7222a23bcfcf85e6c782fe2ce0cdeee0f3f2b0eb"),//M
					GetHeadByTexture("N - Display (Char)", "77d8f27f57ff3626a11d7bc5456683f2c892486c2d96e8e856d62cb79aa4a05b"),//N
					GetHeadByTexture("O - Display (Char)", "b899f4368f6236b99d5f479186ea6a39d1083ff207eaca71af10745c4e"),//O
					GetHeadByTexture("P - Display (Char)", "af2adce09524168e84ac6c8b7f29c9e8745712c73158b8d5a1c6b12df87cce0"),//P
					GetHeadByTexture("Q - Display (Char)", "43c1ac6dc0ebfbf4e3ad3e8e883556397d6cca77e2d5a9a437bbc62658086"),//Q
					GetHeadByTexture("R - Display (Char)", "5deca6f91150d18c9b68a14d63d382e9c42132eee43d6a786a115f255cfd7"),//R
					GetHeadByTexture("S - Display (Char)", "274d273dd7b38417151825f1fb2450c9468ac64c146f1bad24416edb19b5"),//S
					GetHeadByTexture("T - Display (Char)", "af66782045fabd495b97a886a266fda9f66a52be79acedabdea8710358e2285"),//T
					GetHeadByTexture("U - Display (Char)", "c5f5e4bb891e5b6d60135dd6721cc631c4764ac8a934207f31d9eeb2bc49a7a"),//U
					GetHeadByTexture("V - Display (Char)", "fd4b453118b7ae9ddcc79a82841d81ed3ab7fa87ff28eff41f48675fd4d"),//V
					GetHeadByTexture("W - Display (Char)", "c93e755cbb6f6c2da3bf169a2e75eff34151637d2391a41e861735eb9ea0675"),//W
					GetHeadByTexture("X - Display (Char)", "89ba3b834755de24da6a83ce1fb7f971f7abb205585ef823b2fc47abd5028"),//X
					GetHeadByTexture("Y - Display (Char)", "684b96f1c113665e39a3c28ea185b36dd5a75c7d650f2c944171dae99dfb"),//Y
					GetHeadByTexture("Z - Display (Char)", "dcdb80175169613280a8a06a1c751fb71b6689edc8ce3d8b9d96bd450cc81"),//Z

					GetHeadByTexture("0 - Display (Char)", ""),//0
					GetHeadByTexture("1 - Display (Char)", ""),//1
					GetHeadByTexture("2 - Display (Char)", ""),//2
					GetHeadByTexture("3 - Display (Char)", ""),//3
					GetHeadByTexture("4 - Display (Char)", ""),//4
					GetHeadByTexture("5 - Display (Char)", ""),//5
					GetHeadByTexture("6 - Display (Char)", ""),//6
					GetHeadByTexture("7 - Display (Char)", ""),//7
					GetHeadByTexture("8 - Display (Char)", ""),//8
					GetHeadByTexture("9 - Display (Char)", ""),//9

					GetHeadByTexture("  - Display (Char)", "fcaf4473372364426444e49676f6e3372438afa3a6eb04a38af8e412965c1b8"),// 
					GetHeadByTexture(": - Display (Char)", ""),//:
					GetHeadByTexture("? - Display (Char)", ""),//?
					GetHeadByTexture("! - Display (Char)", ""),//!
					GetHeadByTexture(". - Display (Char)", ""),//.
					GetHeadByTexture("/ - Display (Char)", ""),// /

					GetHeadByTexture("+ - Display (Char)", ""),//+
					GetHeadByTexture("- - Display (Char)", ""),//-
					GetHeadByTexture("= - Display (Char)", ""),//=
					GetHeadByTexture("( - Display (Char)", ""),//(
					GetHeadByTexture(") - Display (Char)", ""),//)
					GetHeadByTexture(", - Display (Char)", ""),//,
					GetHeadByTexture("; - Display (Char)", ""),//;
					GetHeadByTexture("\" - Display (Char)", ""),//"
					GetHeadByTexture("% - Display (Char)", ""),//%
					GetHeadByTexture("[ - Display (Char)", ""),//[
					GetHeadByTexture("] - Display (Char)", ""),//]
					GetHeadByTexture("_ - Display (Char)", ""),//_
					GetHeadByTexture("# - Display (Char)", ""),//#
					GetHeadByTexture("\\ - Display (Char)", "")// \
				};
            }
        }

        #endregion

        #region Arrows - Wooden

        public static ItemStack[] WoodenArrows
        {
            get
            {
                return new[] {
                    WoodenArrowUp,
                    WoodenArrowDown,
                    WoodenArrowLeft,
                    WoodenArrowRight
                };
            }
        }

        public static ItemStack WoodenArrowUp
        {
            get
            {
                return GetHeadByTexture("Up - Wooden (Arrow)", "d48b768c623432dfb259fb3c3978e98dec111f79dbd6cd88f21155374b70b3c");
            }
        }

        public static ItemStack WoodenArrowDown
        {
            get
            {
                return GetHeadByTexture("Down - Wooden (Arrow)", "2dadd755d08537352bf7a93e3bb7dd4d733121d39f2fb67073cd471f561194dd");
            }
        }

        public static ItemStack WoodenArrowLeft
        {
            get
            {
                return GetHeadByTexture("Left - Wooden (Arrow)", "3ebf907494a935e955bfcadab81beafb90fb9be49c7026ba97d798d5f1a23");
            }
        }

        public static ItemStack WoodenArrowRight
        {
            get
            {
                return GetHeadByTexture("Right - Wooden (Arrow)", "1b6f1a25b6bc199946472aedb370522584ff6f4e83221e5946bd2e41b5ca13b");
            }
        }

        #endregion

        #region Arrows - Stone

        public static ItemStack[] StoneArrows
        {
            get
            {
                return new[] {
                    StoneArrowUp,
                    StoneArrowDown,
                    StoneArrowLeft,
                    StoneArrowRight
                };
            }
        }

        public static ItemStack StoneArrowUp
        {
            get
            {
                return GetHeadByTexture("Up - Stone (Arrow)", "58fe251a40e4167d35d081c27869ac151af96b6bd16dd2834d5dc7235f47791d");
            }
        }

        public static ItemStack StoneArrowDown
        {
            get
            {
                return GetHeadByTexture("Down - Stone (Arrow)", "9b7ce683d0868aa4378aeb60caa5ea80596bcffdab6b5af2d12595837a84853");
            }
        }

        public static ItemStack StoneArrowLeft
        {
            get
            {
                return GetHeadByTexture("Left - Stone (Arrow)", "bb0f6e8af46ac6faf88914191ab66f261d6726a7999c637cf2e4159fe1fc477");
            }
        }

        public static ItemStack StoneArrowRight
        {
            get
            {
                return GetHeadByTexture("Right - Stone (Arrow)", "f2f3a2dfce0c3dab7ee10db385e5229f1a39534a8ba2646178e37c4fa93b");
            }
        }

        #endregion

        #region Arrows - Quartz

        public static ItemStack[] QuartzArrows
        {
            get
            {
                return new[] {
                    QuartzArrowUp,
                    QuartzArrowDown,
                    QuartzArrowLeft,
                    QuartzArrowRight
                };
            }
        }

        public static ItemStack QuartzArrowUp
        {
            get
            {
                return GetHeadByTexture("Up - Quartz (Arrow)", "5a8ef92efa15669df48332d118a2f705772a2a3fcd0fb82a61f277b849a2bd6");
            }
        }

        public static ItemStack QuartzArrowDown
        {
            get
            {
                return GetHeadByTexture("Down - Quartz (Arrow)", "3faea977aeeba1c837669413b88c95c27d08fb429f3dffb431a8fab361a9f");
            }
        }

        public static ItemStack QuartzArrowLeft
        {
            get
            {
                return GetHeadByTexture("Left - Quartz (Arrow)", "f2599bd986659b8ce2c4988525c94e19ddd39fad08a38284a197f1b70675acc");
            }
        }

        public static ItemStack QuartzArrowRight
        {
            get
            {
                return GetHeadByTexture("Right - Quartz (Arrow)", "c2f910c47da042e4aa28af6cc81cf48ac6caf37dab35f88db993accb9dfe516");
            }
        }

        #endregion

        #region Arrows - Black

        public static ItemStack[] BlackArrows
        {
            get
            {
                return new[] {
                    BlackArrowUp,
                    BlackArrowDown,
                    BlackArrowLeft,
                    BlackArrowRight
                };
            }
        }

        public static ItemStack BlackArrowUp
        {
            get
            {
                return GetHeadByTexture("Up - Black (Arrow)", "43974c29747174753bb429c98e776f7d5b01bb3c188e4ba2fd7881ac4e5b0ac");
            }
        }

        public static ItemStack BlackArrowDown
        {
            get
            {
                return GetHeadByTexture("Down - Black (Arrow)", "111e8763156e6d41e07a36f65e5cd42447d16c2a8bb78bdb4ba89a4e5d99c47");
            }
        }

        public static ItemStack BlackArrowLeft
        {
            get
            {
                return GetHeadByTexture("Left - Black (Arrow)", "6a6db6b6d12bb4dc7682a96b3b933e7a2951d357187918e8bedc65de9d796");
            }
        }

        public static ItemStack BlackArrowRight
        {
            get
            {
                return GetHeadByTexture("Right - Black (Arrow)", "6cb0cbe1c9d7a1b2d61df45c6d29a774cc564e2eb9a9b756fcf9e1334e371");
            }
        }

        #endregion

        #region Arrows - Iron

        public static ItemStack[] IronArrows
        {
            get
            {
                return new[] {
                    IronArrowUp,
                    IronArrowDown,
                    IronArrowLeft,
                    IronArrowRight
                };
            }
        }

        public static ItemStack IronArrowUp
        {
            get
            {
                return GetHeadByTexture("Up - Iron (Arrow)", "105a2cab8b68ea57e3af992a36e47c8ff9aa87cc8776281966f8c3cf31a38");
            }
        }

        public static ItemStack IronArrowDown
        {
            get
            {
                return GetHeadByTexture("Down - Iron (Arrow)", "c01586e39f6ffa63b4fb301b65ca7da8a92f7353aaab89d3886579125dfbaf9");
            }
        }

        public static ItemStack IronArrowLeft
        {
            get
            {
                return GetHeadByTexture("Left - Iron (Arrow)", "a185c97dbb8353de652698d24b64327b793a3f32a98be67b719fbedab35e");
            }
        }

        public static ItemStack IronArrowRight
        {
            get
            {
                return GetHeadByTexture("Right - Iron (Arrow)", "31c0ededd7115fc1b23d51ce966358b27195daf26ebb6e45a66c34c69c34091");
            }
        }

        #endregion

    }
}
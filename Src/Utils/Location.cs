﻿using System;
using CxServer.Worlds;
using CxServer.Entities;
using CxServer.BlocksAndItems.Blocks;
using Utils.Vector;

namespace CxServer.Utils
{
    /// <summary>
    /// Utility to store World, Position and Rotation.<br>
    /// Mainly for entities.
    /// </summary>
    public class Location
    {

        #region Constructor

        /// <summary>
        /// Create new instance of <see cref="CxServer.Utils.Location"/>.
        /// </summary>
        /// <param name="world">The World.</param>
        /// <param name="position">Position as [X, Y, Z].</param>
        /// <param name="rotation">Rotation as [Pitch, Yaw].</param>
        public Location(World world, Vector3d position, Vector2f rotation = default(Vector2f))
        {
            this.World = world;
            this.Position = position;
            this.Rotation = rotation;
        }

        /// <summary>
        /// Create new instance of <see cref="CxServer.Utils.Location"/>.<br>
        /// Copy data from entity.
        /// </summary>
        /// <param name="entity">Entity to copy from.</param>
        public Location(Entity entity) : this(entity.World, entity.Position, entity.Rotation)
        {
        }

        #endregion

        #region Variables

        /// <summary>
        /// World of this Location.
        /// </summary>
        public World World
        {
            get;
        }

        /// <summary>
        /// Position of this Location.
        /// </summary>
        public Vector3d Position
        {
            get;
        }

        /// <summary>
        /// Rotation of this Location.
        /// </summary>
        public Vector2f Rotation
        {
            get;
        }

        #endregion

        #region Properties

        /// <summary>
        /// X Coordinate.
        /// </summary>
        public double X
        {
            get
            {
                return Position.X;
            }
        }

        /// <summary>
        /// Y Coordinate.
        /// </summary>
        public double Y
        {
            get
            {
                return Position.Y;
            }
        }

        /// <summary>
        /// Z Coordinate.
        /// </summary>
        public double Z
        {
            get
            {
                return Position.Z;
            }
        }

        /// <summary>
        /// Pitch Rotation.
        /// </summary>
        public float Pitch
        {
            get
            {
                return Rotation.X;
            }
        }

        /// <summary>
        /// Yaw Rotation.
        /// </summary>
        public float Yaw
        {
            get
            {
                return Rotation.Y;
            }
        }

        #endregion

        #region Block

        /// <summary>
        /// Block X Coordinate.
        /// </summary>
        public int BlockX
        {
            get
            {
                return (int)Position.X + (Position.X < 0 ? -1 : 0);
            }
        }

        /// <summary>
        /// Block Y Coordinate.
        /// </summary>
        public int BlockY
        {
            get
            {
                return (int)Position.Y + (Position.Y < 0 ? -1 : 0);
            }
        }

        /// <summary>
        /// Block Z Coordinate.
        /// </summary>
        public int BlockZ
        {
            get
            {
                return (int)Position.Z + (Position.Z < 0 ? -1 : 0);
            }
        }

        /// <summary>
        /// Block on this position.
        /// </summary>
        public Block Block
        {
            get
            {
                return World.GetBlock(BlockX, BlockY, BlockZ);
            }
            set
            {
                World.SetBlock(BlockX, BlockY, BlockZ, value);
            }
        }

        /// <summary>
        /// ID of block on this position.
        /// </summary>
        public ushort BlockID
        {
            get
            {
                return World.GetBlockID(BlockX, BlockY, BlockZ);
            }
            set
            {
                World.SetBlock(BlockX, BlockY, BlockZ, value);
            }
        }

        /// <summary>
        /// Codename of block on this position.
        /// </summary>
        public string BlockCodename
        {
            get
            {
                return World.GetBlockCodename(BlockX, BlockY, BlockZ);
            }
            set
            {
                World.SetBlock(BlockX, BlockY, BlockZ, value);
            }
        }

        public TileEntity TileEntity
        {
            get
            {
                return World.GetTileEntity(BlockX, BlockY, BlockZ);
            }
            set
            {
                World.SetTileEntity(BlockX, BlockY, BlockZ, value);
            }
        }

        #endregion

        public override string ToString()
        {
            return string.Format("[{0}, {1}, {2}]", World.Name, Position, Rotation);
        }

        #region World Border

        public bool IsInsideWorldBorder
        {
            get
            {
                return World.IsInsideBorder(Position);
            }
        }

        public bool IsInsideWorldBorderFullBlock
        {
            get
            {
                return World.IsInsideBorderFullBlock(Position);
            }
        }

        #endregion
    }
}
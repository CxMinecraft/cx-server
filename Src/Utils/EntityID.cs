﻿using System;

namespace CxServer
{
    public struct EntityID
    {

        #region Constructors

        public EntityID(uint id)
        {
            this.ID = id;
        }

        #endregion

        #region Variables

        public uint ID
        {
            get;
        }

        #endregion

        #region operators

        public static EntityID operator +(EntityID eid0, EntityID eid1)
        {
            return (EntityID)(eid0.ID + eid1.ID);
        }

        public static EntityID operator -(EntityID eid0, EntityID eid1)
        {
            return (EntityID)(eid0.ID - eid1.ID);
        }

        public static EntityID operator +(EntityID eid, uint @int)
        {
            return (EntityID)(eid.ID + @int);
        }

        public static EntityID operator -(EntityID eid, uint @int)
        {
            return (EntityID)(eid.ID - @int);
        }

        public static EntityID operator +(uint @int, EntityID eid)
        {
            return (EntityID)(@int + eid.ID);
        }

        public static EntityID operator -(uint @int, EntityID eid)
        {
            return (EntityID)(@int - eid.ID);
        }

        public static implicit operator uint(EntityID eid)
        {
            return eid.ID;
        }

        public static implicit operator EntityID(uint eid)
        {
            return new EntityID(eid);
        }

        #endregion
    }
}
﻿using System;
using CxServer.Settings;

namespace CxServer.Utils
{
    public static class TimeSpanUtil
    {
        public static long GetServerTicks(this TimeSpan timespan)
        {
            return (long)(timespan.TotalSeconds * ServerSettings.TicksPerSecond);
        }

        public static TimeSpan FromServerTicks(long ticks)
        {
            return TimeSpan.FromMilliseconds(1000 / ServerSettings.TicksPerSecond * ticks);
        }
    }
}
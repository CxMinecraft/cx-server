﻿using System;
using CxServer.Commands;
using CxServer.Chat;
using CxServer.Utils;

namespace CxServer
{
    public class ConsoleCommandSender : ICommandSender
    {
        internal ConsoleCommandSender()
        {
        }

        #region ICommandSender implementation

        public void SendMessage(string message, MessageType type = MessageType.Chat)
        {
            Console.WriteLine(message);
        }

        public virtual void SendRawMessage(ChatMessage message, MessageType type = MessageType.Chat)
        {
            try
            {


                throw new NotImplementedException("Console Command Sender - Send raw message");
            }
            finally
            {
                //Console.ForegroundColor = ConsoleColor.White;
                //Console.BackgroundColor = ConsoleColor.Black;
                Console.ResetColor();
            }
        }
        // => SendMessage(type, message.OldFormat);

        /// <summary>
        /// Always returns true.
        /// </summary>
        public bool HasPermission(string permission)
        {
            return true;
        }

        #endregion

    }
}
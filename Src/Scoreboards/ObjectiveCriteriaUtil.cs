﻿using System;

namespace CxServer.Scoreboards
{
    public static class ObjectiveCriteriaUtil
    {
        public static string GetName(this ObjectiveCriteria criteria)
        {
            switch (criteria)
            {
                case ObjectiveCriteria.Dummy:
                    return "dummy";
                case ObjectiveCriteria.Trigger:
                    return "trigger";
                case ObjectiveCriteria.DeathCount:
                    return "deathCount";
                case ObjectiveCriteria.PlayerKillCount:
                    return "playerKillCount";
                case ObjectiveCriteria.TotalKillCount:
                    return "totalKillCount";
                case ObjectiveCriteria.Health:
                    return "health";
                case ObjectiveCriteria.Xp:
                    return "xp";
                case ObjectiveCriteria.Level:
                    return "level";
                case ObjectiveCriteria.Food:
                    return "food";
                case ObjectiveCriteria.Air:
                    return "air";
                case ObjectiveCriteria.Armor:
                    return "armor";
                default:
                    return null;
            }
        }
        public static bool IsReadOnly(this ObjectiveCriteria criteria)
        {
            switch (criteria)
            {
                case ObjectiveCriteria.Dummy:
                case ObjectiveCriteria.Trigger:
                case ObjectiveCriteria.DeathCount:
                case ObjectiveCriteria.PlayerKillCount:
                case ObjectiveCriteria.TotalKillCount:
                    return false;
                case ObjectiveCriteria.Health:
                case ObjectiveCriteria.Xp:
                case ObjectiveCriteria.Level:
                case ObjectiveCriteria.Food:
                case ObjectiveCriteria.Air:
                case ObjectiveCriteria.Armor:
                    return true;
                default:
                    return true;
            }
        }
    }
}
﻿using System;

namespace CxServer.Scoreboards
{
    public enum ObjectiveCriteria
    {
        Dummy,
        Trigger,
        DeathCount,
        PlayerKillCount,
        TotalKillCount,

        Health,
        Xp,
        Level,
        Food,
        Air,
        Armor
    }
}
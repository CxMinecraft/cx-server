﻿using System;

namespace CxServer.Scoreboards
{
    public enum DisplaySlot
    {
        List = 0,
        Sidebar = 1,
        BelowName = 2
    }
}
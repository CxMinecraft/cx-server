﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CxServer.Scoreboards
{
    public class Scoreboard
    {

        #region Constructors

        private Scoreboard(string name, string criteria = "dummy", DisplaySlot slot = DisplaySlot.Sidebar, string displayName = null, bool hearts = false)
        {
            this.Name = name;
            this._displayName = displayName ?? name;
            this.Criteria = criteria;
            this.Slot = slot;
            this._hearts = hearts;
        }

        #endregion

        #region Static

        private static readonly Dictionary<string, Scoreboard> _scoreboards = new Dictionary<string, Scoreboard>();

        public static Scoreboard GetScoreboard(string name)
        {
            Scoreboard scoreboard;
            if (_scoreboards.TryGetValue(name, out scoreboard))
                return scoreboard;
            else
                return null;
        }

        public static Scoreboard GetOrCreateScoreboard(string name, ObjectiveCriteria criteria, DisplaySlot slot = DisplaySlot.Sidebar, string displayName = null, bool hearts = false) => GetOrCreateScoreboard(name, criteria.GetName(), slot, displayName, hearts);
        //TODO achievement
        //TODO stats
        public static Scoreboard GetOrCreateScoreboard(string name, string criteria = "dummy", DisplaySlot slot = DisplaySlot.Sidebar, string displayName = null, bool hearts = false)
        {
            Scoreboard sb;
            if (_scoreboards.TryGetValue(name, out sb))
                return sb;

            sb = new Scoreboard(name, criteria, slot, displayName, hearts);
            _scoreboards.Add(name, sb);
            return sb;
        }

        #endregion

        #region Variables

        public string Name
        {
            get;
        }

        private string _displayName;

        public string DisplayName
        {
            get
            {
                return _displayName;
            }
            set
            {
                Update(value, _hearts);
            }
        }

        public DisplaySlot Slot
        {
            get;
        }

        public string Criteria
        {
            get;
        }

        private bool _hearts = false;

        public bool Hearts
        {
            get
            {
                return _hearts;
            }
            set
            {
                Update(_displayName, value);
            }
        }

        #endregion

        #region Update

        public void Update(string displayName, bool hearts)
        {
            _displayName = displayName;
            _hearts = hearts;

            throw new NotImplementedException("Scoreboard - update");
        }

        #endregion

        #region Score

        private Dictionary<string, int> _score = new Dictionary<string, int>();

        public bool HasAnyScore
        {
            get
            {
                return _score.Any();
            }
        }

        public bool HasScore(string name)
        {
            return _score.ContainsKey(name);
        }

        public int this[string name]
        {
            get
            {
                int value;
                if (_score.TryGetValue(name, out value))
                    return value;
                return 0;
            }
            set
            {
                throw new NotImplementedException("Scoreboard - this[string] - set");
            }
        }

        public void RemoveScore(string name)
        {
            throw new NotImplementedException("Scoreboard - remove score");
        }

        public void ClearAllScore()
        {
            throw new NotImplementedException("Scoreboard - clear all score");
        }

        #endregion
    }
}
﻿using System;

namespace CxServer
{
    public enum Dimension
    {
        Nether = -1,
        Overworld = 0,
        TheEnd = 1
    }
}
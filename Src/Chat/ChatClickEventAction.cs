﻿using System;

namespace CxServer.Chat
{
    public enum ChatClickEventAction
    {
        NONE = 0,
        OpenUrl,
        OpenFile,
        RunCommand,
        SuggestCommand
    }
}
﻿using System;

namespace CxServer.Chat
{
    public enum ChatHoverEventAction
    {
        NONE = 0,
        ShowText,
        ShowAchievement,
        ShowItem
    }
}
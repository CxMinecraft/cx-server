﻿using System;
using CxServer.Utils;
using Utils.Vector;

namespace CxServer.Collisions
{
    public static class AABB
    {
        public static bool IsInCollisionByBounds(Vector3f min0, Vector3f max0, Vector3f min1, Vector3f max1)
        {
            return (min0.X <= max1.X && max0.X >= min1.X) && (min0.Z <= max1.Z && max0.Z >= min1.Z) && (min0.Y <= max1.Y && max0.Y >= min1.Y);
        }

        public static bool IsInCollisionByCenter(Vector3f center0, Vector3f size0, Vector3f center1, Vector3f size1)
        {
            return IsInCollisionByBounds(center0 - (size0 / 2), center0 + (size0 / 2), center1 - (size1 / 2), center1 + (size1 / 2));
        }
    }
}
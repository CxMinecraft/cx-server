﻿using System;
using CxServer.Utils;
using CxServer.Worlds;
using CxServer.Inventories;
using System.IO;
using CxServer.Settings;
using System.Collections.Generic;
using CxServer.Entities.Living;
using Utils;
using Utils.Vector;

namespace CxServer
{
    public class OfflinePlayer
    {

        #region Constructors

        internal OfflinePlayer(UUID uuid)
        {
            this.UUID = uuid;

            string path = Path.Combine(ServerSettings.PlayersFolder, this.UUID.ToString(true));

            if (File.Exists(path))
            {
                //TODO load saved data

            }
            else
            {
                this.PlayedBefore = false;
                this.LastOnline = default(DateTime);

                this.MainInventory = null;
                this.AllInventories = null;

                this.LastWorld = null;
                this.LastPosition = default(Vector3f);
                this.LastRotation = default(Vector2f);
            }

        }

        #endregion

        /// <summary>
        /// Player's UUID
        /// </summary>
        public UUID UUID
        {
            get;
        }

        #region Online

        public bool IsOnline
        {
            get
            {
                return Server.GetOnlinePlayer(this.UUID) != null;
            }
        }

        public PlayerEntity OnlinePlayer
        {
            get
            {
                return Server.GetOnlinePlayer(this.UUID);
            }
        }

        #endregion

        public bool PlayedBefore
        {
            get;
        }

        public DateTime LastOnline
        {
            get;
        }

        public GameMode GameMode
        {
            get;
        }

        #region Inventory

        public PlayerInventory MainInventory
        {
            get;
        }

        public Dictionary<string, Inventory> AllInventories
        {
            get;
        }

        public ChestInventory EnderChest
        {
            get
            {
                Inventory ec;
                if (AllInventories.TryGetValue("ender_chest", out ec) && ec != null && ec is ChestInventory)
                    return (ChestInventory)ec;
                else
                    return null;
            }
        }

        #endregion

        #region Last Location

        public World LastWorld
        {
            get;
        }

        public Vector3f LastPosition
        {
            get;
        }

        public Vector2f LastRotation
        {
            get;
        }

        public Location LastLocation
        {
            get
            {
                return LastWorld == null ? null : new Location(LastWorld, LastPosition, LastRotation);
            }
        }

        #endregion

        #region Comparison

        public override bool Equals(object obj)
        {
            if (obj is OfflinePlayer)
                return OfflinePlayer.Equals(this, (OfflinePlayer)obj);
            else
                return false;
        }

        public override int GetHashCode()
        {
            throw new NotImplementedException("OfflinePlayer - GetHashCode");
            /*
			if(this.PlayedBefore && this.UUID != null)
				return this.UUID.GetHashCode();
			else
				return 0;
			*/
        }

        public static bool Equals(OfflinePlayer p1, OfflinePlayer p2)
        {
            throw new NotImplementedException("OfflinePlayer - Equals");
            /*
			if(p1 == null || p2 == null)
				return p1 == null && p2 == null;
			return p1.UUID == p2.UUID;
			*/
        }

        public static bool operator ==(OfflinePlayer p1, OfflinePlayer p2)
        {
            return OfflinePlayer.Equals(p1, p2);
        }

        public static bool operator !=(OfflinePlayer p1, OfflinePlayer p2)
        {
            return !OfflinePlayer.Equals(p1, p2);
        }

        #endregion

    }
}
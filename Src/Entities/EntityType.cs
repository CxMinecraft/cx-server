﻿using System;

namespace CxServer.Entities
{
    public enum EntityType
    {

        //Player
        Player = -1,

        //GlobalEntity
        GlobalEntity = -2,


        //Entities
        Creeper = 50,
        Skeleton = 51,
        Spider = 52,
        GiantZombie = 53,
        Zombie = 54,
        Slime = 55,
        Ghast = 56,
        ZombiePigman = 57,

        Enderman = 58,

        CaveSpider = 59,
        Silverfish = 60,
        Blaze = 61,
        MagmaCube = 62,
        EnderDragon = 63,
        Wither = 64,

        Bat = 65,

        Witch = 66,
        Endermite = 67,
        Guardian = 68,
        Shulker = 69,

        Pig = 90,
        Sheep = 91,
        Cow = 92,
        Chicken = 93,
        Squid = 94,

        Wolf = 95,

        Mooshroom = 96,
        Snowman = 97,
        Ocelot = 98,

        IronGolem = 99,

        Horse = 100,
        Rabbit = 101,

        PolarBear = 102,

        Villager = 120,



        //Objects
        Boat = 1,
        Item = 2,
        AreaEffectCloud = 3,

        Minecart = 10,

        TNT = 50,
        EnderCrystal = 51,

        Arrow = 60,
        Snowball = 61,
        Egg = 62,
        FireBall = 63,
        FireCharge = 64,
        Enderpearl = 65,
        WitherSkull = 66,
        ShulkerBullet = 67,

        FallingObjects = 70,
        ItemFrames = 71,

        EyeOfEnder = 72,
        Potion = 73,

        ExpBottle = 75,
        Firework = 76,

        LeashKnot = 77,
        ArmorStand = 78,

        FishingFloat = 90,

        SpectralArrow = 91,

        DragonFireball = 93
    }
}
﻿using System;
using CxServer.Utils;

namespace CxServer.Entities.Global
{
    public abstract class GlobalEntity : Entity
    {
        public GlobalEntity(Location location, GlobalEntityType globalType, float width = 0, float height = 0) : base(location, EntityType.GlobalEntity, width, height)
        {
            this.GlobalType = globalType;
        }

        public GlobalEntityType GlobalType
        {
            get;
        }
    }
}
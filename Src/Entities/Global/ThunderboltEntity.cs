﻿using System;
using CxServer.Utils;

namespace CxServer.Entities.Global
{
    public class ThunderboltEntity : GlobalEntity
    {
        public ThunderboltEntity(Location location) : base(location, GlobalEntityType.Thunderbolt, 2, 2)
        {
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CxServer.Entities
{
    public sealed class EntityType_old
    {
        private EntityType_old(string name, EntityHostileType hostileType, double width, double height) : this(name, hostileType, (float)width, (float)height)
        {

        }
        private EntityType_old(string name, EntityHostileType hostileType, float width, float height)
        {
            this.Name = name;

            this.HostileType = hostileType;

            this.Width = width;
            this.Height = height;
        }

        #region Name

        public string Name
        {
            get;
        }

        #endregion

        #region Hostile Type

        public EntityHostileType HostileType
        {
            get;
        }
        public enum EntityHostileType
        {
            /// <summary>
            /// Never attacks players<br>
            /// Used for animals
            /// </summary>
            Friendly,
            /// <summary>
            /// May attack players after specified situation
            /// </summary>
            Neutral,
            /// <summary>
            /// Always attacks players<br>
            /// Used for hostile enemies
            /// </summary>
            Hostile,
            /// <summary>
            /// Special entities
            /// </summary>
            Object,
            Projectile
        }

        #endregion

        #region Size

        public float Width
        {
            get;
        }
        public float Height
        {
            get;
        }

        #endregion

        #region Static

        private static readonly Dictionary<int, EntityType_old> _types = new Dictionary<int, EntityType_old>();

        public static bool RegisterEntityType(int id, EntityType_old type)
        {
            if (_types.ContainsKey(id))
                return false;
            _types.Add(id, type);
            return true;
        }

        public static EntityType_old GetByID(int id)
        {
            EntityType_old type;
            if (_types.TryGetValue(id, out type))
                return type;
            return null;
        }

        public static EntityType_old GetByName(string name)
        {
            return _types.Values.FirstOrDefault((et) => et.Name.Equals(name, StringComparison.InvariantCultureIgnoreCase));
        }

        public static List<EntityType_old> GetAllByHostileType(EntityHostileType type)
        {
            return _types.Values.Where((et) => et.HostileType == type).ToList();
        }
        public static List<EntityType_old> GetAllByHostileType(EntityHostileType type1, EntityHostileType type2)
        {
            return _types.Values.Where((et) => et.HostileType == type1 || et.HostileType == type2).ToList();
        }
        public static List<EntityType_old> GetAllByHostileType(EntityHostileType type1, EntityHostileType type2, EntityHostileType type3)
        {
            return _types.Values.Where((et) => et.HostileType == type1 || et.HostileType == type2 || et.HostileType == type3).ToList();
        }

        static EntityType_old()
        {
            //Player
            RegisterEntityType(-1, new EntityType_old("Player", EntityHostileType.Neutral, 0.6, 1.8));


            //Entities
            RegisterEntityType(50, new EntityType_old("Creeper", EntityHostileType.Hostile, 0.6, 1.7));
            RegisterEntityType(51, new EntityType_old("Skeleton", EntityHostileType.Hostile, 0.6, 1.99));
            RegisterEntityType(52, new EntityType_old("Spider", EntityHostileType.Hostile, 1.4, 0.9));
            RegisterEntityType(53, new EntityType_old("GiantZombie", EntityHostileType.Hostile, 0.6 * 6, 1.8 * 6));
            RegisterEntityType(54, new EntityType_old("Zombie", EntityHostileType.Hostile, 0.6, 1.8));
            RegisterEntityType(55, new EntityType_old("Slime", EntityHostileType.Hostile, 0.51000005, 1.51000005));
            RegisterEntityType(56, new EntityType_old("Ghast", EntityHostileType.Hostile, 4, 4));
            RegisterEntityType(57, new EntityType_old("ZombiePigman", EntityHostileType.Hostile, 0.6, 1.8));

            RegisterEntityType(58, new EntityType_old("Enderman", EntityHostileType.Neutral, 0.6, 2.9));

            RegisterEntityType(59, new EntityType_old("CaveSpider", EntityHostileType.Hostile, 0.7, 0.5));
            RegisterEntityType(60, new EntityType_old("Silverfish", EntityHostileType.Hostile, 0.4, 0.3));
            RegisterEntityType(61, new EntityType_old("Blaze", EntityHostileType.Hostile, 0.6, 1.8));
            RegisterEntityType(62, new EntityType_old("MagmaCube", EntityHostileType.Hostile, 0.51000005, 1.51000005));
            RegisterEntityType(63, new EntityType_old("EnderDragon", EntityHostileType.Hostile, 16, 8));
            RegisterEntityType(64, new EntityType_old("Wither", EntityHostileType.Hostile, 0.9, 3.5));

            RegisterEntityType(65, new EntityType_old("Bat", EntityHostileType.Friendly, 0.5, 0.9));

            RegisterEntityType(66, new EntityType_old("Witch", EntityHostileType.Hostile, 0.6, 1.8));
            RegisterEntityType(67, new EntityType_old("Endermite", EntityHostileType.Hostile, 0.4, 0.3));
            RegisterEntityType(68, new EntityType_old("Guardian", EntityHostileType.Hostile, 0.85, 0.85));
            RegisterEntityType(69, new EntityType_old("Shulker", EntityHostileType.Hostile, 1, 1));

            RegisterEntityType(90, new EntityType_old("Pig", EntityHostileType.Friendly, 0.9, 0.9));
            RegisterEntityType(91, new EntityType_old("Sheep", EntityHostileType.Friendly, 0.9, 1.3));
            RegisterEntityType(92, new EntityType_old("Cow", EntityHostileType.Friendly, 0.9, 1.4));
            RegisterEntityType(93, new EntityType_old("Chicken", EntityHostileType.Friendly, 0.4, 0.7));
            RegisterEntityType(94, new EntityType_old("Squid", EntityHostileType.Friendly, 0.8, 0.8));

            RegisterEntityType(95, new EntityType_old("Wolf", EntityHostileType.Neutral, 0.6, 0.85));

            RegisterEntityType(96, new EntityType_old("Mooshroom", EntityHostileType.Friendly, 0.9, 1.3));
            RegisterEntityType(97, new EntityType_old("Snowman", EntityHostileType.Friendly, 0.7, 1.9));
            RegisterEntityType(98, new EntityType_old("Ocelot", EntityHostileType.Friendly, 0.6, 0.8));

            RegisterEntityType(99, new EntityType_old("IronGolem", EntityHostileType.Neutral, 1.4, 2.7));

            RegisterEntityType(100, new EntityType_old("Horse", EntityHostileType.Friendly, 1.4, 1.6));
            RegisterEntityType(101, new EntityType_old("Rabbit", EntityHostileType.Friendly, 0.4, 0.5));

            RegisterEntityType(102, new EntityType_old("PolarBear", EntityHostileType.Neutral, 1.3, 1.4));

            RegisterEntityType(120, new EntityType_old("Villager", EntityHostileType.Friendly, 0.6, 1.95));



            //Objects
            RegisterEntityType(1, new EntityType_old("Boat", EntityHostileType.Object, 1.5, 0.6));
            RegisterEntityType(2, new EntityType_old("Item", EntityHostileType.Object, 0.25, 0.25));
            RegisterEntityType(3, new EntityType_old("AreaEffectCloud", EntityHostileType.Object, 0, 0));

            RegisterEntityType(10, new EntityType_old("Minecart", EntityHostileType.Object, 0.98, 0.7));

            RegisterEntityType(50, new EntityType_old("TNT", EntityHostileType.Object, 0.98, 0.98));
            RegisterEntityType(51, new EntityType_old("EnderCrystal", EntityHostileType.Object, 2, 2));

            RegisterEntityType(60, new EntityType_old("Arrow", EntityHostileType.Projectile, 0.5, 0.5));
            RegisterEntityType(61, new EntityType_old("Snowball", EntityHostileType.Projectile, 0.25, 0.25));
            RegisterEntityType(62, new EntityType_old("Egg", EntityHostileType.Projectile, 0.25, 0.25));
            RegisterEntityType(63, new EntityType_old("FireBall", EntityHostileType.Projectile, 1, 1));
            RegisterEntityType(64, new EntityType_old("FireCharge", EntityHostileType.Projectile, 0.3125, 0.3125));
            RegisterEntityType(65, new EntityType_old("Enderpearl", EntityHostileType.Projectile, 0.25, 0.25));
            RegisterEntityType(66, new EntityType_old("WitherSkull", EntityHostileType.Projectile, 0.3125, 0.3125));
            RegisterEntityType(67, new EntityType_old("ShulkerBullet", EntityHostileType.Projectile, 0.3125, 0.3125));

            RegisterEntityType(70, new EntityType_old("FallingObjects", EntityHostileType.Object, 0.98, 0.98));
            RegisterEntityType(71, new EntityType_old("ItemFrames", EntityHostileType.Object, 0, 0));

            RegisterEntityType(72, new EntityType_old("EyeOfEnder", EntityHostileType.Projectile, 0.25, 0.25));
            RegisterEntityType(73, new EntityType_old("Potion", EntityHostileType.Projectile, 0.25, 0.25));

            RegisterEntityType(75, new EntityType_old("ExpBottle", EntityHostileType.Projectile, 0.25, 0.25));
            RegisterEntityType(76, new EntityType_old("Firework", EntityHostileType.Projectile, 0.25, 0.25));

            RegisterEntityType(77, new EntityType_old("LeashKnot", EntityHostileType.Object, 0.5, 0.5));
            RegisterEntityType(78, new EntityType_old("ArmorStand", EntityHostileType.Object, 0.5, 2));

            RegisterEntityType(90, new EntityType_old("FishingFloat", EntityHostileType.Object, 0.25, 0.25));

            RegisterEntityType(91, new EntityType_old("SpectralArrow", EntityHostileType.Projectile, 0.5, 0.5));

            RegisterEntityType(93, new EntityType_old("DragonFireball", EntityHostileType.Object, 0.3125, 0.3125));
        }

        #endregion
    }
}
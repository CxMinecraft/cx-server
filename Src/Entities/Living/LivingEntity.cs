﻿using System;
using CxServer.Utils;
using CxServer.Inventories;

namespace CxServer.Entities.Living
{
    public abstract class LivingEntity : Entity
    {
        #region Constructor

        protected LivingEntity(Location location, EntityType type, Inventory inventory, int maxHealth = 20) : base(location, type)
        {
            MaxHealth = maxHealth;
            _health = _maxHealth;

            this.Inventory = inventory;
        }

        #endregion

        public float HeadPitch
        {
            get;
            set;
        }

        public override EntityMetadata Metadata
        {
            get
            {
                var metadata = base.Metadata;
                //metadata.Add(6, )//TODO active hand, 0x01 - is hand active, 0x02 - active hand (0 = main, 1 = offhand)
                metadata.Add(7, Health);
                //metadata.Add(8, varint);//TODO active potion color, 0 no effect
                //metadata.Add(9, bool);//TODO is ambient effect
                //metadata.Add(10, varint);//TODO number of arrows in entity
                return metadata;
                //2  - hurt animation + sound
                //3  - death animation + sound
                //29 - shield block sound
                //30 - shield break sound
                //33 - thorns sound (in addition to hurt animation and sound)
            }
        }

        #region Health

        private int _maxHealth = 1;

        public int MaxHealth
        {
            get
            {
                return _maxHealth;
            }
            set
            {
                _maxHealth = Math.Max(value, 1);
            }
        }

        protected float _health;

        public virtual float Health
        {
            get
            {
                return _health;
            }
            set
            {
                if (value <= 0)
                    _health = 0;
                else if (value > MaxHealth)
                    _health = _maxHealth;
                else
                    _health = value;
            }
        }

        public virtual bool IsDead()
        {
            return Health < 1;
        }

        #endregion

        public Inventory Inventory
        {
            get;
        }

    }
}
﻿using System;
using CxServer.Utils;
using CxServer.Settings;
using CxServer.Inventories;
using CxServer.Inventories.Utils;
using CxServer.Communication;
using CxServer.ResourcePacks;
using System.Collections.Generic;
using CxServer.Worlds;
using CxServer.Scoreboards;
using CxServer.Managers;
using CxServer.Commands;
using Utils.Vector;
using Utils;
using Utils.Extensions;

namespace CxServer.Entities.Living
{
    /// <summary>
    /// Player entity.
    /// </summary>
    public class PlayerEntity : LivingEntity, ICommandSender
    {

        #region Constructors

        public PlayerEntity(MinecraftClient client, OfflinePlayer offline) : base(offline.LastLocation, EntityType.Player, offline.MainInventory, 20)
        {
            this.Client = client;

            this.PlayerInventory = (PlayerInventory)base.Inventory;
        }

        #endregion

        public override EntityMetadata Metadata
        {
            get
            {
                var metadata = base.Metadata;

                return metadata;
                //9  - mark item use as finished (finished eating/drinking)
                //22 - enable reduced debug screen information
                //23 - disable reduced debug screen information
                //24 - set OP permission level to 0
                //25 - set OP permission level to 1
                //26 - set OP permission level to 2
                //27 - set OP permission level to 3
                //28 - set OP permission level to 4
                //35 - play the Totem of Undying
            }
        }

        #region Variables

        /// <summary>
        /// Client controlling this player entity.
        /// </summary>
        public MinecraftClient Client
        {
            get;
        }

        /// <summary>
        /// Player's current name.<br>
        /// This MAY NOT stay same for this player. DON'T identify player by this.
        /// </summary>
        public string Name
        {
            get
            {
                return Client.Username;
            }
        }

        private GameMode _gameMode = GameMode.Spectator;

        public GameMode GameMode
        {
            get
            {
                return _gameMode;
            }
            set
            {
                throw new NotImplementedException("Player - Set Gamemode");
            }
        }

        private bool _reducedDebugInfo = ServerSettings.ReducedDebugInfoClients;

        public bool ReducedDebugInfo
        {
            get
            {
                return _reducedDebugInfo;
            }
            set
            {
                _reducedDebugInfo = value;
                throw new NotImplementedException("Player - Set reduced Debug Info");
                //Entity Status Packet
                // 22 = Enable Reduced Debug
                // 23 = Disable Reduced Debug
            }
        }

        public bool CanFly
        {
            get
            {
                return false;//TODO Can Fly
            }
        }

        public bool IsFlying
        {
            get
            {
                return false;//TODO IsFlying
            }
        }

        public bool Invulnerable
        {
            get
            {
                return false;//TODO Invulnerable
            }
        }

        public Communication.Play.ToClient.PlayerAbilities.AbilitiesFlags Abilities
        {
            get
            {
                Communication.Play.ToClient.PlayerAbilities.AbilitiesFlags flags = 0;
                if (CanFly)
                    flags |= Communication.Play.ToClient.PlayerAbilities.AbilitiesFlags.AllowFlying;
                if (IsFlying)
                    flags |= Communication.Play.ToClient.PlayerAbilities.AbilitiesFlags.Flying;
                if (Invulnerable)
                    flags |= Communication.Play.ToClient.PlayerAbilities.AbilitiesFlags.Invulnerable;
                if (GameMode == GameMode.Creative)
                    flags |= Communication.Play.ToClient.PlayerAbilities.AbilitiesFlags.CreativeMode;
                return flags;
            }
        }

        public Vector3 CompassTarget
        {
            get
            {
                return CompassTargetPosition;
            }
            set
            {
                CompassTargetPosition = (Position)value;
            }
        }

        private Position _compassTargetPosition = default(Position);

        public Position CompassTargetPosition
        {
            get
            {
                return _compassTargetPosition;
            }
            set
            {
                _compassTargetPosition = value;

                SendPacket(new Communication.Play.ToClient.SpawnPosition(_compassTargetPosition));
            }
        }

        #endregion

        public override bool Spawn()
        {
            if (Spawned)
                return true;

            if (World == null)
            {
                World = WorldManager.MainWorld;
                Position = World.SpawnPositionVector;
                Rotation = new Vector2f(0, StaticRandom.NextInt(0, 360));
            }

            return World.Spawn(this);
        }

        public override bool Teleport(World world, Vector3d position, Vector2f rotation)
        {
            if (!Spawned)
                return false;

            if (world == this.World)
            {
                SendPacket(new Communication.Play.ToClient.PlayerPositionAndLook(Position, Rotation));
                //TODO teleport confirm
                return true;
            }
            else
            {
                throw new NotImplementedException("Player - Teleport");
            }
        }

        #region Inventory

        /// <summary>
        /// Inventory of this player.
        /// </summary>
        public PlayerInventory PlayerInventory
        {
            get;
            //TODO set
        }

        public ItemStack MainHandItem => PlayerInventory.MainHand;

        public ItemStack SecondHandItem => PlayerInventory.SecondHand;

        public int SelectedSlot => PlayerInventory.Hotbar.SelectedIndex;

        public ArmorInventory Armor => PlayerInventory.ArmorInventory;


        public Dictionary<string, Inventory> AllInventories
        {
            get;
        }

        public ChestInventory EnderChest
        {
            get
            {
                Inventory ec;
                if (AllInventories.TryGetValue("ender_chest", out ec) && ec != null && ec is ChestInventory)
                    return (ChestInventory)ec;
                else
                    return null;
            }
            set
            {
                AllInventories["ender_chest"] = value == null || value.Lines != 3 ? new ChestInventory(3) : value;
            }
        }

        #endregion

        #region Main Hand

        /// <summary>
        /// Player's current main hand.
        /// </summary>
        public MainHandEnum MainHand
        {
            get;
            internal set;
        }

        /// <summary>
        /// Enum used to identify player's main hand.
        /// </summary>
        public enum MainHandEnum
        {
            Left = 0,
            Right = 1
        }

        #endregion

        #region Resourcepack

        public void SetResourcePackByName(string name) => SetResourcePack(string.Format("{0}:{1}/{2}.zip", ServerSettings.ServerAddress, ServerSettings.ResourcePackPort, name));

        public void SetResourcePack(string address)
        {
            this.TargetResourcePackAddress = address;
            this.TargetResourcePackName = ResourcePackUtils.GetName(address);
            this.IsTargetResourcePackActive = false;

            this.SendPacket(new Communication.Play.ToClient.SetResourcePack(address));
        }

        //TODO OnPacketReceived += veryfi active resourcepack
        public string TargetResourcePackAddress
        {
            get;
            internal set;
        } = null;
        public string TargetResourcePackName
        {
            get;
            internal set;
        } = null;
        public bool IsTargetResourcePackActive
        {
            get;
            internal set;
        } = true;

        public const string DefaultResourcePack = null;

        #endregion

        public void SendPacket(Packet packet) => Client.SendPacket(packet);

        #region Boss Bar

        private readonly List<BossBar> _activeBossBars = new List<BossBar>();

        public void AddBossBar(BossBar bar)
        {
            throw new NotImplementedException("Player - Boss Bar - Add");
        }

        public void ClearBossBars()
        {
            throw new NotImplementedException("Player - Boss Bar - Clear");
        }

        public void RemoveBossBar(BossBar bar)
        {
            throw new NotImplementedException("Player - Boss Bar - Remove");
        }

        public int BossBarCount
        {
            get
            {
                return _activeBossBars.Count;
            }
        }

        public BossBar GetBossBar(int index) => _activeBossBars[index];

        #endregion

        #region Scoreboard

        private Scoreboard[] _activeScoreboard = new Scoreboard[Enum.GetValues(typeof(DisplaySlot)).Length];

        public void SetScoreboard(Scoreboard sb, DisplaySlot slot)
        {
            _activeScoreboard[(int)slot] = sb;

            SendPacket(new Communication.Play.ToClient.DisplayScoreboard(slot, sb));

            if (sb != null)
            {
                AddListeningScoreboards(sb);
            }
            //data send during AddListeningScoreboards
        }

        public void RemoveScoreboard(DisplaySlot slot) => SetScoreboard(null, slot);
        public void RemoveAllScoreboards()
        {
            foreach (var slot in Enum.GetValues(typeof(DisplaySlot)))
                RemoveScoreboard((DisplaySlot)slot);
        }

        public bool IsActiveScoreboard(Scoreboard sb)
        {
            return _activeScoreboard.Contains(sb);
        }

        internal List<Scoreboard> _listeningScoreboards = new List<Scoreboard>();

        public void ClearListeningScoreboards()
        {
            throw new NotImplementedException("Player - Scoreboard - Clear");
        }

        public void AddListeningScoreboards(Scoreboard sb)
        {
            if (_listeningScoreboards.Contains(sb))
                return;

            _listeningScoreboards.Add(sb);

            throw new NotImplementedException("Player - Scoreboard - Add - Send Data");
        }

        public void RemoveListeningScoreboards(Scoreboard sb)
        {
            if (!_listeningScoreboards.Contains(sb))
                return;

            _listeningScoreboards.Remove(sb);

            throw new NotImplementedException("Player - Scoreboard - Remove - Send Data");
        }

        public bool IsListeningScoreboard(Scoreboard sb)
        {
            return _listeningScoreboards.Contains(sb);
        }

        #endregion

        #region ICommandSender implementation

        public void SendMessage(string message, MessageType type = MessageType.Chat)
        {
            throw new NotImplementedException("Player - Send Message");
        }

        public void SendRawMessage(CxServer.Chat.ChatMessage message, MessageType type = MessageType.Chat)
        {
            throw new NotImplementedException("Player - Send Raw Message");
        }

        public bool HasPermission(string permission)
        {
            throw new NotImplementedException("Player - Permission");
        }

        #endregion

        #region Permission

        private readonly PermissionTrie _permissions_positive = new PermissionTrie();

        private readonly PermissionTrie _permissions_negative = new PermissionTrie();

        public List<string> PositivePermissions
        {
            get
            {
                return _permissions_positive.Keys;
            }
        }

        public List<string> NegativePermissions
        {
            get
            {
                return _permissions_negative.Keys;
            }
        }

        /// <summary>
        /// Give player permission.<br>
        /// permissions are separated by . char and may be prefixed with - char to negate the effect.
        /// </summary>
        /// <param name="perm">Permission.</param>
        public void GivePermission(string perm)
        {
            if (string.IsNullOrWhiteSpace(perm))
                return;
            perm = perm.Trim();

            PermissionTrie target;
            if (perm[0] == '-')
            {
                target = _permissions_negative;
                perm = perm.Substring(1);
            }
            else
                target = _permissions_positive;

            target[perm] = true;
        }

        /// <summary>
        /// Remove given permission.<br>
        /// permissions are separated by . char and may be prefixed with - char to negate the effect.
        /// </summary>
        /// <param name="perm">Permission.</param>
        public void RemovePermission(string perm)
        {
            if (string.IsNullOrWhiteSpace(perm))
                return;
            perm = perm.Trim();

            PermissionTrie target;
            if (perm[0] == '-')
            {
                target = _permissions_negative;
                perm = perm.Substring(1);
            }
            else
                target = _permissions_positive;

            target[perm] = false;
        }

        #endregion

        #region Events

        public static EventsManager.Event<MinecraftClient, Packet> PacketSend;

        public static EventsManager.Event<MinecraftClient, Packet> PacketReceived;

        /// <summary>
        /// Called when <see cref="CxServer.Communication.Play.ToClient.PluginMessage"/> packet is send.<br>
        /// MinecraftClient (1st parameter of delegate) is <b>ALWAYS</b> client of this player.<br>
        /// <br>
        /// Use <see cref="CxServer.Managers.EventsManager.ChannelMessageDelegate"/> delegate.
        /// </summary>
        public static EventsManager.Event<MinecraftClient, string, byte[]> ChannelMessageSend;
        /// <summary>
        /// Called when <see cref="CxServer.Communication.Play.ToServer.PluginMessage"/> packet is received.<br>
        /// MinecraftClient (1st parameter of delegate) is <b>ALWAYS</b> client of this player.<br>
        /// <br>
        /// Use <see cref="CxServer.Managers.EventsManager.ChannelMessageDelegate"/> delegate.
        /// </summary>
        public static EventsManager.Event<MinecraftClient, string, byte[]> ChannelMessageReceived;

        #endregion

    }
}
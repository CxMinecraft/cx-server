﻿using System;

namespace CxServer.Entities.Living.Utils
{
    public enum Animation
    {
        SwingArm = 0,
        TakeDamage = 1,
        LeaveBed = 2,
        EatFood = 3,
        CriticalEffect = 4,
        MagicCriticalEffect = 5
    }
}
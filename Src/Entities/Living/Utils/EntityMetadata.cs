﻿using System;
using System.Collections.Generic;
using CxServer.Communication;
using CxServer.Utils;
using Utils;
using Utils.Vector;

namespace CxServer
{
    public class EntityMetadata
    {
        public EntityMetadata()
        {
        }

        private class EntityMetadataValue
        {
            public EntityMetadataValue(ValueType type, Queue<byte> value)
            {
                this.Type = type;
                this.Value = value;
            }

            public ValueType Type
            {
                get;
            }

            public Queue<byte> Value
            {
                get;
            }
        }

        private enum ValueType
        {
            Byte = 0,
            VarInt = 1,
            Float = 2,
            String = 3,
            Chat = 4,
            Slot = 5,
            Boolean = 6,
            Rotation = 7,
            Position = 8,
            /// <summary>
            /// Boolean + Optional Position
            /// </summary>
            OptionalPosition = 9,
            Diraction = 10,
            /// <summary>
            /// Boolean + Optional UUID
            /// </summary>
            OptionalUUID = 11,
            /// <summary>
            /// VarInt<br>
            /// 0 for absent otherwise, id << 4 | data
            /// </summary>
            OptionalBlockID = 12
        }

        private Dictionary<byte, EntityMetadataValue> Data = new Dictionary<byte, EntityMetadataValue>();

        public void Clear()
        {
            Data.Clear();
        }

        public const byte DataEnd = 0xff;

        #region Add

        private void Add(byte id, ValueType type, Queue<byte> data)
        {
            if (id == DataEnd)
                return;

            var emv = new EntityMetadataValue(type, data);

            Data.Add(id, emv);
        }

        public void Add(byte id, sbyte value)
        {
            Queue<byte> data = new Queue<byte>();
            PacketEncoding.EncodeByte(data, value);
            Add(id, ValueType.Byte, data);
        }

        /// <summary>
        /// VarInt
        /// </summary>
        public void Add(byte id, int value)
        {
            Queue<byte> data = new Queue<byte>();
            PacketEncoding.EncodeVarInt(data, value);
            Add(id, ValueType.VarInt, data);
        }

        public void Add(byte id, float value)
        {
            Queue<byte> data = new Queue<byte>();
            PacketEncoding.EncodeFloat(data, value);
            Add(id, ValueType.Float, data);
        }

        public void Add(byte id, string value)
        {
            Queue<byte> data = new Queue<byte>();
            PacketEncoding.EncodeString(data, value);
            Add(id, ValueType.String, data);
        }

        public void Add(byte id, CxServer.Chat.ChatMessage value)
        {
            Queue<byte> data = new Queue<byte>();
            PacketEncoding.EncodeChat(data, value);
            Add(id, ValueType.Chat, data);
        }

        public void Add(byte id, ItemStack value)
        {
            Queue<byte> data = new Queue<byte>();
            PacketEncoding.EncodeItemStack(data, value);
            Add(id, ValueType.Slot, data);
        }

        public void Add(byte id, bool value)
        {
            Queue<byte> data = new Queue<byte>();
            PacketEncoding.EncodeBoolean(data, value);
            Add(id, ValueType.Boolean, data);
        }

        /// <summary>
        /// Rotation
        /// </summary>
        public void Add(byte id, Vector3f value)
        {
            Queue<byte> data = new Queue<byte>();
            PacketEncoding.EncodeFloat(data, value.X);
            PacketEncoding.EncodeFloat(data, value.Y);
            PacketEncoding.EncodeFloat(data, value.Z);
            Add(id, ValueType.Rotation, data);
        }

        public void Add(byte id, Position value)
        {
            Queue<byte> data = new Queue<byte>();
            PacketEncoding.EncodePosition(data, value);
            Add(id, ValueType.Position, data);
        }

        public void Add(byte id, Direction value)
        {
            Queue<byte> data = new Queue<byte>();
            PacketEncoding.EncodeVarInt(data, (int)value);
            Add(id, ValueType.Diraction, data);
        }

        #endregion

        #region Optional Add

        public void AddOptional(byte id, Position? value)
        {
            Queue<byte> data = new Queue<byte>();
            if (value == null)
                PacketEncoding.EncodeBoolean(data, false);
            else
            {
                PacketEncoding.EncodeBoolean(data, false);
                PacketEncoding.EncodePosition(data, value.Value);
            }
            Add(id, ValueType.OptionalPosition, data);
        }

        public void AddOptional(byte id, UUID? value)
        {
            Queue<byte> data = new Queue<byte>();
            if (value == null)
                PacketEncoding.EncodeBoolean(data, false);
            else
            {
                PacketEncoding.EncodeBoolean(data, false);
                PacketEncoding.EncodeUUID(data, value.Value);
            }
            Add(id, ValueType.OptionalUUID, data);
        }

        /// <summary>
        /// Block ID
        /// </summary>
        public void AddOptional(byte id, ushort? block_id, byte? block_data)
        {
            Queue<byte> data = new Queue<byte>();
            if (block_id == null || block_data == null)
                PacketEncoding.EncodeVarInt(data, 0);
            else
                PacketEncoding.EncodeVarInt(data, (((int)block_id.Value) << 4) | block_data.Value);
            Add(id, ValueType.OptionalBlockID, data);
        }

        #endregion

    }
}
﻿using System;
using CxServer.Utils;
using CxServer.Worlds;
using System.Collections.Generic;
using Utils;
using Utils.Vector;
using Utils.Extensions;

namespace CxServer.Entities
{
    /// <summary>
    /// Entity in world.
    /// </summary>
    public abstract class Entity
    {
        #region Constructor

        /// <summary>
        /// Create new instance of <see cref="CxServer.Entities.Entity"/>.
        /// </summary>
        /// <param name="location">Location of this entity.</param>
        /// <param name="typeID">Type ID. Negative ID will not send this entity to clients.</param>
        /// <param name="width">Width of collision box of this entity (2D XZ size).</param>
        /// <param name="height">Height of collision box of this entity.</param>
        protected Entity(Location location, EntityType type, float width = 0.6f, float height = 1.8f)
        {
            this.Type = type;
            this.Location = location;

            this.Width = width;
            this.Height = height;
        }

        #endregion

        /// <summary>
        /// Type ID used for Client &lt;-&gt; Server Communication.
        /// </summary>
        public EntityType Type
        {
            get;
        }

        public UUID UUID
        {
            get;
        }

        public virtual EntityMetadata Metadata
        {
            get
            {
                var metadata = new EntityMetadata();
                metadata.Add(0, new bool[] { OnFire, Crouched, Sprinting, EatingOrBlocking, Invisible, GlowingEffect, GlowingEffect, FlyingWithElytra }.ToByte());
                metadata.Add(1, TicksInAir);
                metadata.Add(2, CustomName);
                metadata.Add(3, CustomNameVisible);
                metadata.Add(4, Silent);
                metadata.Add(4, NoGravity);
                return metadata;
                //21 - play guardian sound effect from this entity
            }
        }

        //TODO event
        public string CustomName
        {
            get;
            set;
        }

        //TODO event
        public bool CustomNameVisible
        {
            get;
            set;
        }

        //TODO event
        public bool Silent
        {
            get;
            set;
        }

        //TODO event
        public bool NoGravity
        {
            get;
            set;
        }

        //TODO add event
        public bool OnFire
        {
            get;
            set;
        }

        //TODO add event
        public bool Crouched
        {
            get;
            set;
        }

        //TODO add event
        public bool Sprinting
        {
            get;
            set;
        }

        //TODO add event
        public bool EatingOrBlocking
        {
            get;
            set;
        }

        //TODO add event
        public bool Invisible
        {
            get;
            set;
        }

        //TODO add event
        public bool GlowingEffect
        {
            get;
            set;
        }

        //TODO add event
        public bool FlyingWithElytra
        {
            get;
            set;
        }

        //TODO event
        public int TicksInAir
        {
            get;
            set;
        }

        #region Size

        public float Width
        {
            get;
            protected set;
        }

        public float Height
        {
            get;
            protected set;
        }

        #endregion

        #region Position and rotation

        public Vector3d Velocity
        {
            get;
            internal set;
        }

        public double VX
        {
            get
            {
                return Velocity.X;
            }
        }

        public double VY
        {
            get
            {
                return Velocity.Y;
            }
        }

        public double VZ
        {
            get
            {
                return Velocity.Z;
            }
        }

        /// <summary>
        /// World in which is this entity.
        /// </summary>
        /// <value>The world.</value>
        public World World
        {
            get;
            internal set;
        }

        /// <summary>
        /// Entity Position.
        /// </summary>
        public Vector3d Position
        {
            get;
            internal set;
        }

        /// <summary>
        /// X Coordinate.
        /// </summary>
        public double X
        {
            get
            {
                return Position.X;
            }
        }

        /// <summary>
        /// Y Coordinate.
        /// </summary>
        public double Y
        {
            get
            {
                return Position.Y;
            }
        }

        /// <summary>
        /// Z Coordinate.
        /// </summary>
        public double Z
        {
            get
            {
                return Position.Z;
            }
        }

        /// <summary>
        /// Entity Rotation.
        /// </summary>
        public Vector2f Rotation
        {
            get;
            internal set;
        }

        /// <summary>
        /// Pitch Rotation.
        /// </summary>
        public float Pitch
        {
            get
            {
                return Rotation.X;
            }
        }

        /// <summary>
        /// Yaw Rotation.
        /// </summary>
        public float Yaw
        {
            get
            {
                return Rotation.Y;
            }
        }

        /// <summary>
        /// Location of this entity
        /// </summary>
        public Location Location
        {
            get
            {
                return new Location(this.World, this.Position, this.Rotation);
            }
            internal set
            {
                if (value == null)
                {
                    this.World = default(World);
                    this.Position = default(Vector3d);
                    this.Rotation = default(Vector2f);
                }
                else
                {
                    this.World = value.World;
                    this.Position = value.Position;
                    this.Rotation = value.Rotation;
                }
                this.Velocity = Vector3d.Zero;
            }
        }

        /// <summary>
        /// Teleport the entity to specified location.
        /// </summary>
        /// <param name="location">Target Location (world + position + rotation).</param>
        public virtual bool Teleport(Location location) => Teleport(location.World, location.Position, location.Rotation);

        /// <summary>
        /// Teleport the entity to specified specified world and position keeping rotation same.
        /// </summary>
        /// <param name="world">Target world.</param>
        /// <param name="position">Target position.</param>
        public virtual bool Teleport(World world, Vector3f position) => Teleport(world, position, this.Rotation);

        /// <summary>
        /// Teleport the entity to specified specified position.
        /// </summary>
        /// <param name="position">Target position.</param>
        public virtual bool Teleport(Vector3d position) => Teleport(this.World, position, this.Rotation);

        /// <summary>
        /// Teleport the entity to specified specified position and rotation keeping world same.
        /// </summary>
        /// <param name="position">Target position.</param>
        /// <param name="rotation">Target rotation.</param>
        public virtual bool Teleport(Vector3d position, Vector2f rotation) => Teleport(this.World, position, rotation);

        /// <summary>
        /// Teleport the entity to specified specified rotation keeping world and position same.
        /// </summary>
        /// <param name="rotation">Target rotation.</param>
        public virtual bool Teleport(Vector2f rotation) => Teleport(this.World, this.Position, rotation);

        /// <summary>
        /// Teleport the entity to specified specified world, position and rotation.
        /// </summary>
        /// <param name="world">Target world.</param>
        /// <param name="position">Target position.</param>
        /// <param name="rotation">Target rotation.</param>
        public virtual bool Teleport(World world, Vector3d position, Vector2f rotation)
        {
            if (!Spawned)
                return false;

            if (world == this.World)
            {

            }
            else
            {

            }

            throw new NotImplementedException("Entity - Teleport");
        }

        #endregion

        #region Nearby Entities

        /// <summary>
        /// Entities near this entity.
        /// </summary>
        /// <param name="maxDistance">Max search distance.</param>
        public List<Entity> NearbyEntities(float maxDistance) => World.NearbyEntities((Vector3)this.Position, maxDistance);

        /// <summary>
        /// Entities near this entity.<br>
        /// Ignores Y Coordinate.
        /// </summary>
        /// <param name="maxDistance">Max search distance.</param>
        public List<Entity> NearbyEntities2D(float maxDistance) => World.NearbyEntities((Vector2)this.Position.XZ, maxDistance);

        #endregion

        #region Update

        //TODO event
        internal virtual void Update()
        {
            throw new NotImplementedException("Update - Add behavior");
        }

        #endregion

        #region Spawning

        //TODO event
        public bool Spawned
        {
            get
            {
                return EntityID != 0;
            }
        }

        public EntityID EntityID
        {
            get;
            internal set;
        } = 0;

        public virtual bool Spawn()
        {
            if (Spawned)
                return true;

            if (World == null)
                return false;

            return World.Spawn(this);
        }

        #endregion

        #region Speed

        public double MovementSpeed
        {
            get;
            set;
        } = 0.699999988079071;

        public double FlyingSpeed
        {
            get;
            set;
        }
        // 250% of MovementSpeed

        #endregion

    }
}
﻿using System;
using CxServer.Utils;

namespace CxServer.Entities.Object
{
    public abstract class EntityObject : Entities.Entity
    {
        public EntityObject(Location location, EntityType type, int data, float width, float height) : base(location, type, width, height)
        {
            this.Data = data;
        }

        public int Data
        {
            get;
        }
    }
}
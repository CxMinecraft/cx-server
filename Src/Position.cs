﻿using System;
using CxServer.Utils;
using Utils.Vector;

namespace CxServer
{
    public struct Position
    {
        public int X
        {
            get;
        }

        public int Y
        {
            get;
        }

        public int Z
        {
            get;
        }

        public Position(int x, int y, int z)
        {
            if (x < -33554432 || x > 33554431)
                throw new ArgumentOutOfRangeException("x");
            if (z < -33554432 || z > 33554431)
                throw new ArgumentOutOfRangeException("z");
            if (y < -2048 || y > 2047)
                throw new ArgumentOutOfRangeException("y");
            this.X = x;
            this.Y = y;
            this.Z = z;
        }

        public Position(long value)
        {
            this.X = (int)(value >> 38);
            this.Y = (int)((value >> 26) & 0xFFF);
            this.Z = (int)(value << 38 >> 38);
        }

        public long ToLong()
        {
            return ((X & 0x3FFFFFFL) << 38) | ((Y & 0xFFFL) << 26) | (Z & 0x3FFFFFFL);
        }

        #region Operator

        public static implicit operator Vector3(Position position)
        {
            return new Vector3(position.X, position.Y, position.Z);
        }

        public static explicit operator Position(Vector3 vector)
        {
            return new Position(vector.X, vector.Y, vector.Z);
        }

        #endregion

    }
}
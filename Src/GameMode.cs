﻿using System;

namespace CxServer
{
    /// <summary>
    /// GameMode is used to specify which Game Mechanics will be applied on player.
    /// </summary>
    public enum GameMode
    {
        /// <summary>
        /// Survival GameMode.<br>
        /// Health, hunger and block breaking/placing are enabled.
        /// </summary>
        Survival = 0,
        /// <summary>
        /// Creative GameMode.<br>
        /// No health or hunger. Block breaking/placing is enabled.
        /// </summary>
        Creative = 1,
        /// <summary>
        /// Survival GameMode.<br>
        /// Health, hunger and are enabled but block breaking/placing is disabled.
        /// </summary>
        Adventure = 2,
        /// <summary>
        /// Spectator GameMode.<br>
        /// No collision, health or hunger but block breaking/placing is disabled.
        /// </summary>
        Spectator = 3,

        /// <summary>
        /// Survival GameMode with Hardcore flag.
        /// </summary>
        Survival_Hardcore = 8,
        /// <summary>
        /// Creative GameMode with Hardcore flag.
        /// </summary>
        Creative_Hardcore = 9,
        /// <summary>
        /// Adventure GameMode with Hardcore flag.
        /// </summary>
        Adventure_Hardcore = 10,
        /// <summary>
        /// Spectator GameMode with Hardcore flag.
        /// </summary>
        Spectator_Hardcore = 11,
    }
}
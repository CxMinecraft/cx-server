﻿using System;
using CxServer.BlocksAndItems.Blocks;
using CxServer.BlocksAndItems.Items;
using CxServer.NBT;

namespace CxServer
{
    public class ItemStack
    {

        #region Constructors - Normal

        public ItemStack(string item, sbyte amount = 1, short damage = 0, string nbt_json = null) : this(Managers.BlocksAndItemsManager.GetItem(item), amount, damage, nbt_json)
        {
        }

        public ItemStack(string item, sbyte amount, short damage, NBT_Compound nbt) : this(Managers.BlocksAndItemsManager.GetItem(item), amount, damage, nbt)
        {
        }

        public ItemStack(Block block, sbyte amount = 1, short damage = 0, string nbt_json = null) : this(block.Item, amount, damage, nbt_json)
        {
        }

        public ItemStack(Item item, sbyte amount = 1, short damage = 0, string nbt_json = null) : this(item, amount, damage, CompoundLoader.LoadFromJSON(nbt_json))
        {
        }

        public ItemStack(Block block, sbyte amount, short damage, NBT_Compound nbt) : this(block.Item, amount, damage, nbt)
        {
        }

        public ItemStack(Item item, sbyte amount, short damage, NBT_Compound nbt)
        {
            this.Item = item;
            this.Amount = amount;
            this.Damage = damage;

            this._nbt = nbt;
        }

        #endregion

        #region Constructors - ItemStack

        public ItemStack(ItemStack stack, string nbt_json) : this(stack.Item, stack.Amount, stack.Damage, stack.NBT + CompoundLoader.LoadFromJSON(nbt_json))
        {
        }

        public ItemStack(ItemStack stack, sbyte amount, string nbt_json) : this(stack.Item, amount, stack.Damage, stack.NBT + CompoundLoader.LoadFromJSON(nbt_json))
        {
        }

        public ItemStack(ItemStack stack, short damage, string nbt_json) : this(stack.Item, stack.Amount, damage, stack.NBT + CompoundLoader.LoadFromJSON(nbt_json))
        {
        }

        public ItemStack(ItemStack stack, sbyte amount, short damage, string nbt_json) : this(stack.Item, amount, damage, stack.NBT + CompoundLoader.LoadFromJSON(nbt_json))
        {
        }


        public ItemStack(ItemStack stack, NBT_Compound nbt = null) : this(stack.Item, stack.Amount, stack.Damage, stack.NBT + nbt)
        {
        }

        public ItemStack(ItemStack stack, sbyte amount, NBT_Compound nbt = null) : this(stack.Item, amount, stack.Damage, stack.NBT + nbt)
        {
        }

        public ItemStack(ItemStack stack, short damage, NBT_Compound nbt = null) : this(stack.Item, stack.Amount, damage, stack.NBT + nbt)
        {
        }

        public ItemStack(ItemStack stack, sbyte amount, short damage, NBT_Compound nbt = null) : this(stack.Item, amount, damage, stack.NBT + nbt)
        {
        }

        #endregion

        #region Variables

        public Item Item
        {
            get;
        }

        public sbyte Amount
        {
            get;
        }

        public short Damage
        {
            get;
        }

        private readonly NBT_Compound _nbt;

        public NBT_Compound NBT
        {
            get
            {
                return (NBT_Compound)_nbt.Clone();
            }
        }

        #endregion

    }
}
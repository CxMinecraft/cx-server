﻿using System;

namespace CxServer
{
    public enum InteractionType
    {
        /// <summary>
        /// Walk over 
        /// </summary>
        Physics,
        /// <summary>
        /// Left Mouse Button
        /// </summary>
        Break,
        /// <summary>
        /// Middle Mouse Button
        /// </summary>
        PickUp,
        /// <summary>
        /// Right Mouse Button
        /// </summary>
        Place
    }
}
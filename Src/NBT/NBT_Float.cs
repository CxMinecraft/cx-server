﻿using System;
using CxServer.Utils;
using Utils.Extensions;

namespace CxServer.NBT
{
    public class NBT_Float : NBT_Tag
    {

        #region Constructors

        public NBT_Float(string name, float value) : base(name, 5)
        {
            this.Value = value;
        }

        #endregion

        #region Variables

        public float Value
        {
            get;
        }

        #endregion

        public override void Write(System.IO.BinaryWriter writer, bool onlyValue = false)
        {
            base.Write(writer, onlyValue);

            writer.Write(BitConverter.GetBytes(Value).Reverse());
        }

        public override string ToString()
        {
            return string.Format("NBT_Float({0}): {1}", Name, Value);
        }

        public override NBT_Tag Clone()
        {
            return new NBT_Float(Name, Value);
        }
    }
}
﻿using System;
using CxServer.Utils;
using Utils.Extensions;

namespace CxServer.NBT
{
    public class NBT_Short : NBT_Tag
    {

        #region Constructors

        public NBT_Short(string name, short value) : base(name, 2)
        {
            this.Value = value;
        }

        #endregion

        #region Variables

        public short Value
        {
            get;
        }

        #endregion

        public override void Write(System.IO.BinaryWriter writer, bool onlyValue = false)
        {
            base.Write(writer, onlyValue);

            writer.Write(BitConverter.GetBytes(Value).Reverse());
        }

        public override string ToString()
        {
            return string.Format("NBT_Short({0}): {1}", Name, Value);
        }

        public override NBT_Tag Clone()
        {
            return new NBT_Short(Name, Value);
        }
    }
}
﻿using System;
using CxServer.Utils;
using Utils.Extensions;

namespace CxServer.NBT
{
    public class NBT_Double : NBT_Tag
    {

        #region Double

        public NBT_Double(string name, double value) : base(name, 6)
        {
            this.Value = value;
        }

        #endregion

        #region Variables

        public double Value
        {
            get;
        }

        #endregion

        public override void Write(System.IO.BinaryWriter writer, bool onlyValue = false)
        {
            base.Write(writer, onlyValue);

            writer.Write(BitConverter.GetBytes(Value).Reverse());
        }

        public override string ToString()
        {
            return string.Format("NBT_Double({0}): {1}", Name, Value);
        }

        public override NBT_Tag Clone()
        {
            return new NBT_Double(Name, Value);
        }
    }
}
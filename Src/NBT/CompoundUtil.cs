﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CxServer.NBT
{
    public static class CompoundUtil
    {
        public static void Add(this NBT_Compound nbt, NBT_Tag tag, bool ignoreDuplicity = true)
        {
            if (!ignoreDuplicity)
            {
                var n = nbt.FirstOrDefault((nb) => nb.Name == tag.Name);
                if (n != null)
                    nbt.Tags.Remove(n);
            }
            nbt.Tags.Add(tag);
        }

        public static void Add(this NBT_Compound nbt, string name, string value)
        {
            Add(nbt, new NBT_String(name, value));
        }

        #region Numbers

        public static void Add(this NBT_Compound nbt, string name, sbyte value)
        {
            Add(nbt, new NBT_Byte(name, value));
        }

        public static void Add(this NBT_Compound nbt, string name, short value)
        {
            Add(nbt, new NBT_Short(name, value));
        }

        public static void Add(this NBT_Compound nbt, string name, int value)
        {
            Add(nbt, new NBT_Int(name, value));
        }

        public static void Add(this NBT_Compound nbt, string name, long value)
        {
            Add(nbt, new NBT_Long(name, value));
        }

        public static void Add(this NBT_Compound nbt, string name, float value)
        {
            Add(nbt, new NBT_Float(name, value));
        }

        public static void Add(this NBT_Compound nbt, string name, double value)
        {
            Add(nbt, new NBT_Double(name, value));
        }

        #endregion

        #region Arrays - normal

        public static void Add(this NBT_Compound nbt, string name, sbyte[] value)
        {
            Add(nbt, new NBT_ByteArray(name, value));
        }

        public static void Add(this NBT_Compound nbt, string name, List<sbyte> value)
        {
            Add(nbt, new NBT_ByteArray(name, value));
        }

        public static void Add(this NBT_Compound nbt, string name, int[] value)
        {
            Add(nbt, new NBT_IntArray(name, value));
        }

        public static void Add(this NBT_Compound nbt, string name, List<int> value)
        {
            Add(nbt, new NBT_IntArray(name, value));
        }

        #endregion

        #region Arrays - to list

        public static void Add(this NBT_Compound nbt, string name, string[] value)
        {
            List<NBT_String> val = new List<NBT_String>();
            foreach (var v in value)
                val.Add(new NBT_String(null, v));
            Add(nbt, new NBT_List<NBT_String>(name, val));
        }

        public static void Add(this NBT_Compound nbt, string name, List<string> value)
        {
            List<NBT_String> val = new List<NBT_String>();
            foreach (var v in value)
                val.Add(new NBT_String(null, v));
            Add(nbt, new NBT_List<NBT_String>(name, val));
        }

        #endregion

        public static void Add<T>(this NBT_Compound nbt, string name, T[] value) where T : NBT_Tag
        {
            Add(nbt, new NBT_List<T>(name, value));
        }

        public static void Add<T>(this NBT_Compound nbt, string name, List<T> value) where T : NBT_Tag
        {
            Add(nbt, new NBT_List<T>(name, value));
        }
    }
}
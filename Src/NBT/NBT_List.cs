﻿using System;
using System.Collections.Generic;
using System.Linq;
using CxServer.Utils;
using Utils.Extensions;

namespace CxServer.NBT
{
    public class NBT_List<T> : NBT_Tag where T : NBT_Tag
    {

        #region Constructors

        public NBT_List(string name, T[] value) : base(name, 9)
        {
            this.Value = new List<T>(value);
        }

        public NBT_List(string name, List<T> value) : base(name, 9)
        {
            this.Value = new List<T>(value);
        }

        #endregion

        #region Variables

        private List<T> Value
        {
            get;
        }

        #endregion

        public override void Write(System.IO.BinaryWriter writer, bool onlyValue = false)
        {
            base.Write(writer, onlyValue);

            if (Value.Any())
                writer.Write(Value[0].ID);
            else
                writer.Write((byte)0);

            writer.Write(BitConverter.GetBytes(Value.Count).Reverse());

            foreach (var t in Value)
                if (t != null)
                    t.Write(writer, false);
        }

        public override string ToString()
        {
            return string.Format("NBT_List<{2}>({0}): {1}", Name, Value, typeof(T).FullName);
        }

        #region Cast

        public NBT_List<T2> Cast<T2>() where T2 : NBT_Tag
        {
            lock (Value)
            {
                T2[] arr = new T2[Value.Count];
                for (int i = 0; i < Value.Count; i++)
                    arr[i] = Value[i] as T2;
                return new NBT_List<T2>(Name, arr);
            }
        }

        #endregion

        public override NBT_Tag Clone()
        {
            return new NBT_List<T>(Name, Value);
        }
    }
}
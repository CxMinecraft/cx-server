﻿using System;
using CxServer.Utils;
using Utils.Extensions;

namespace CxServer.NBT
{
    public class NBT_Int : NBT_Tag
    {

        #region Constructors

        public NBT_Int(string name, int value) : base(name, 3)
        {
            this.Value = value;
        }

        #endregion

        #region Variables

        public int Value
        {
            get;
        }

        #endregion

        public override void Write(System.IO.BinaryWriter writer, bool onlyValue = false)
        {
            base.Write(writer, onlyValue);

            writer.Write(BitConverter.GetBytes(Value).Reverse());
        }

        public override string ToString()
        {
            return string.Format("NBT_Int({0}): {1}", Name, Value);
        }

        public override NBT_Tag Clone()
        {
            return new NBT_Int(Name, Value);
        }
    }
}
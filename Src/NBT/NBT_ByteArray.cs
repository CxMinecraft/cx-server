﻿using System;
using System.Collections.Generic;
using CxServer.Utils;
using Utils.Extensions;

namespace CxServer.NBT
{
    public class NBT_ByteArray : NBT_Tag
    {

        #region Constructors

        public NBT_ByteArray(string name, sbyte[] value) : base(name, 7)
        {
            this.Value = new List<sbyte>(value);
        }

        public NBT_ByteArray(string name, List<sbyte> value) : base(name, 7)
        {
            this.Value = new List<sbyte>(value);
        }

        #endregion

        #region Variables

        private List<sbyte> Value
        {
            get;
        }

        private sbyte[] _valueArray = new sbyte[0];
        private bool _valueUpdate = true;

        public sbyte[] ValueArray
        {
            get
            {
                if (_valueUpdate)
                    _valueArray = Value.ToArray();
                return _valueArray;
            }
        }

        public void Add(sbyte value)
        {
            Value.Add(value);
            _valueUpdate = true;
        }

        #endregion

        public override void Write(System.IO.BinaryWriter writer, bool onlyValue = false)
        {
            base.Write(writer, onlyValue);

            writer.Write(BitConverter.GetBytes(Value.Count).Reverse());

            foreach (var va in ValueArray)
                writer.Write(va);
        }

        public override string ToString()
        {
            return string.Format("NBT_ByteArray({0}): {1}", Name, Value);
        }

        public override NBT_Tag Clone()
        {
            return new NBT_ByteArray(Name, Value);
        }
    }
}
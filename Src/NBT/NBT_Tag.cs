﻿using System;
using System.Text;
using System.IO;
using CxServer.Utils;
using Utils.Extensions;

namespace CxServer.NBT
{
    public class NBT_Tag
    {

        #region Constructors

        public NBT_Tag(string name, byte id)
        {
            this.ID = id;

            if (string.IsNullOrEmpty(name))
            {
                this.Name = null;
                this.NameBytes = new byte[0];
                this.NameLength = 0;
            }
            else
            {
                this.Name = name;
                this.NameBytes = Encoding.UTF8.GetBytes(name);

                if (NameBytes.Length > ushort.MaxValue)
                    throw new ArgumentOutOfRangeException("name");

                this.NameLength = (ushort)NameBytes.Length;
            }
        }

        #endregion

        #region Variables

        public string Name
        {
            get;
        }

        public byte[] NameBytes
        {
            get;
        }

        public ushort NameLength
        {
            get;
        }

        public byte ID
        {
            get;
        }

        #endregion

        public virtual void Write(BinaryWriter writer, bool onlyValue = false)
        {
            if (onlyValue)
                return;
            writer.Write(ID);

            writer.Write(BitConverter.GetBytes(NameLength).Reverse());
            writer.Write(NameBytes);
        }

        public override string ToString()
        {
            return string.Format("NBT_Tag({0})", Name);
        }

        public virtual NBT_Tag Clone()
        {
            return new NBT_Tag(Name, ID);
        }
    }
}
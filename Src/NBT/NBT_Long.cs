﻿using System;
using CxServer.Utils;
using Utils.Extensions;

namespace CxServer.NBT
{
    public class NBT_Long : NBT_Tag
    {

        #region Constructors

        public NBT_Long(string name, long value) : base(name, 4)
        {
            this.Value = value;
        }

        #endregion

        #region Variables

        public long Value
        {
            get;
        }

        #endregion

        public override void Write(System.IO.BinaryWriter writer, bool onlyValue = false)
        {
            base.Write(writer, onlyValue);

            writer.Write(BitConverter.GetBytes(Value).Reverse());
        }

        public override string ToString()
        {
            return string.Format("NBT_Long({0}): {1}", Name, Value);
        }

        public override NBT_Tag Clone()
        {
            return new NBT_Long(Name, Value);
        }
    }
}
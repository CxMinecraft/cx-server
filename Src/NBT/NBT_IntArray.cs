﻿using System;
using System.Collections.Generic;
using CxServer.Utils;
using Utils.Extensions;

namespace CxServer.NBT
{
    public class NBT_IntArray : NBT_Tag
    {

        #region Constructors

        public NBT_IntArray(string name, int[] value) : base(name, 11)
        {
            this.Value = new List<int>(value);
        }

        public NBT_IntArray(string name, List<int> value) : base(name, 11)
        {
            this.Value = new List<int>(value);
        }

        #endregion

        #region Variables

        private List<int> Value
        {
            get;
        }

        private int[] _valueArray = new int[0];
        private bool _valueUpdate = true;

        public int[] ValueArray
        {
            get
            {
                if (_valueUpdate)
                    _valueArray = Value.ToArray();
                return _valueArray;
            }
        }

        public void Add(sbyte value)
        {
            Value.Add(value);
            _valueUpdate = true;
        }

        #endregion

        public override void Write(System.IO.BinaryWriter writer, bool onlyValue = false)
        {
            base.Write(writer, onlyValue);

            writer.Write(BitConverter.GetBytes(Value.Count).Reverse());

            foreach (var va in ValueArray)
                writer.Write(va);
        }

        public override string ToString()
        {
            return string.Format("NBT_IntArray({0}): {1}", Name, Value);
        }

        public override NBT_Tag Clone()
        {
            return new NBT_IntArray(Name, Value);
        }
    }
}
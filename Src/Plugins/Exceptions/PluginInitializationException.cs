﻿using System;

namespace CxServer.Plugins.Exceptions
{
    public class PluginInitializationException : Exception
    {
        public PluginInitializationException(Type type, Exception exception) : base(string.Format("Failed to load plugin of type '{0}' - Exception thrown: {1}", type.Name, exception.Message))
        {
            this.Type = type;
            this.Exception = exception;
        }

        public Type Type
        {
            get;
        }

        public Exception Exception
        {
            get;
        }
    }
}
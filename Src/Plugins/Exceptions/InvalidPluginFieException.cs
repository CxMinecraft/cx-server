﻿using System;

namespace CxServer
{
    public class InvalidPluginFieException : Exception
    {
        public InvalidPluginFieException(string path) : base(string.Format("Invalid plugin file name '{0}' loaded from '{1}'", System.IO.Path.GetFileName(path), System.IO.Path.GetDirectoryName(path)))
        {
            this.Path = path;
        }

        public string Path
        {
            get;
        }
    }
}
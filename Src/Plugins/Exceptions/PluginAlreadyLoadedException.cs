﻿using System;

namespace CxServer.Plugins.Exceptions
{
    public class PluginAlreadyLoadedException : Exception
    {
        public PluginAlreadyLoadedException(string codename, Version version) : base(string.Format("Failed to load plugin '{0}' version '{1}' - there is already plugin with this codename.", codename, version))
        {
            this.Codename = codename;
            this.Version = version;
        }

        public string Codename
        {
            get;
        }

        public Version Version
        {
            get;
        }
    }
}
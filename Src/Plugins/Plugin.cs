﻿using System;
using System.Collections.Generic;

namespace CxServer.Plugins
{
    public abstract class Plugin
    {
        protected Plugin(string codename, int version_major, int version_minor = 0, int version_build = 0) : this(codename, new Version(version_major, version_minor, version_build))
        {

        }

        protected Plugin(string codename, Version version)
        {
            if (version == null)
                throw new ArgumentNullException(nameof(version), "Plugin version cannot be null");

            this.Codename = codename;

            this.Version = version;
        }

        #region Dependencies

        /// <summary>
        /// Use this in plugin's constructor.<br>
        /// Register new dependency for this plugin.<br>
        /// It is not recommended to use this - use <see cref="AddDependencyMax"/> and specify maximum major version one higher then actual
        /// </summary>
        /// <param name="codename">Codename of plugin.</param>
        protected void AddDependency(string codename)
        {
            if (string.IsNullOrEmpty(codename))
                throw new ArgumentNullException(nameof(codename));

            _dependencies.Add(new Dependency(codename, null, null));
        }

        /// <summary>
        /// Use this in plugin's constructor.<br>
        /// Register new dependency for this plugin.<br>
        /// It is not recommended to use this - use <see cref="AddDependency(string, Version, Version)"/> and specify maximum major version one higher then actual
        /// </summary>
        /// <param name="codename">Codename of plugin.</param>
        /// <param name="minVersion">Minimum version (inclusive).</param>
        protected void AddDependencyMin(string codename, Version minVersion)
        {
            if (string.IsNullOrEmpty(codename))
                throw new ArgumentNullException(nameof(codename));

            if (minVersion == null)
                throw new ArgumentNullException(nameof(minVersion));

            _dependencies.Add(new Dependency(codename, minVersion, null));
        }

        /// <summary>
        /// Use this in plugin's constructor.<br>
        /// Register new dependency for this plugin.
        /// </summary>
        /// <param name="codename">Codename of plugin.</param>
        /// <param name="maxVersion">Maximum version (inclusive).</param>
        protected void AddDependencyMax(string codename, Version maxVersion)
        {
            if (string.IsNullOrEmpty(codename))
                throw new ArgumentNullException(nameof(codename));

            if (maxVersion == null)
                throw new ArgumentNullException(nameof(maxVersion));

            _dependencies.Add(new Dependency(codename, null, maxVersion));
        }

        /// <summary>
        /// Use this in plugin's constructor.<br>
        /// Register new dependency for this plugin.
        /// </summary>
        /// <param name="codename">Codename of plugin.</param>
        /// <param name="minVersion">Minimum version (inclusive).</param>
        /// <param name="maxVersion">Maximum version (inclusive).</param>
        protected void AddDependency(string codename, Version minVersion, Version maxVersion)
        {
            if (string.IsNullOrEmpty(codename))
                throw new ArgumentNullException(nameof(codename));

            if (minVersion == null)
                throw new ArgumentNullException(nameof(minVersion));
            if (maxVersion == null)
                throw new ArgumentNullException(nameof(maxVersion));

            _dependencies.Add(new Dependency(codename, minVersion, maxVersion));
        }

        /// <summary>
        /// Class to store dependency informations.
        /// </summary>
        private class Dependency
        {
            public Dependency(string codename, Version minVersion, Version maxVersion)
            {
                this.Codename = codename;
                this.VersionMin = minVersion;
                this.VersionMax = maxVersion;
            }

            public string Codename
            {
                get;
            }

            public Version VersionMin
            {
                get;
            }

            public Version VersionMax
            {
                get;
            }
        }

        private List<Dependency> _dependencies
        {
            get;
        } = new List<Dependency>();

        internal bool CheckDependency(List<Plugin> loadedPlugins)
        {
            foreach (var dependency in _dependencies)
                foreach (var plugin in loadedPlugins)
                    if (plugin != null
                       && string.Equals(plugin.Codename, dependency.Codename, StringComparison.InvariantCultureIgnoreCase)
                       && (dependency.VersionMin == null ? true : plugin.Version >= dependency.VersionMin)
                       && (dependency.VersionMax == null ? true : plugin.Version <= dependency.VersionMax))
                        return false;
            return true;
        }


        #endregion

        /// <summary>
        /// Turned on when this plugin switches stated from loades (instanced) to active (solved dependencies)
        /// </summary>
        /// <value><c>true</c> if active; otherwise, <c>false</c>.</value>
        public bool Active
        {
            get;
            internal set;
        }

        /// <summary>
        /// Unique codename of this plugin.<br>
        /// Lower-case without spaces is recommended (snake_case).<br>
        /// Case insensitive (lower-case still recommended).
        /// </summary>
        public string Codename
        {
            get;
        }

        /// <summary>
        /// Version of this plugin.
        /// </summary>
        public Version Version
        {
            get;
        }

        #region Initialization Steps

        /// <summary>
        /// Called right after dependency check.<br>
        /// Use this to load your configuration files.<br>
        /// Plugins on which you depend may not be activated before you.
        /// </summary>
        protected internal abstract void Activation();

        /// <summary>
        /// Called when all plugins are activated.<br>
        /// Use this for initialization of your own APIs.<br>
        /// Plugins may not be called in dependency order.
        /// </summary>
        protected internal abstract void PreInit();

        /// <summary>
        /// Use this with APIs.<br>
        /// Plugins may not be called in dependency order.
        /// </summary>
        protected internal abstract void Init();

        /// <summary>
        /// Use this to register events.<br>
        /// Plugins may not be called in dependency order.
        /// </summary>
        protected internal abstract void PostInit();

        #endregion
    }
}
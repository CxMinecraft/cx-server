﻿using System;
using System.Linq;
using System.Threading;
using System.Net.Sockets;
using System.Net;
using CxServer.Settings;
using System.IO;
using System.Collections.Generic;
using CxServer.Worlds;
using CxServer.Utils;
using System.Text.RegularExpressions;
using CxServer.Entities.Living;
using CxServer.Plugins;
using CxServer.ResourcePacks;
using System.Reflection;
using CxServer.Managers;
using CxServer.Commands;
using CxServer.Mojang;
using Utils;
using Utils.Extensions;

namespace CxServer
{
    /// <summary>
    /// Main class for Server.<br>
    /// Contains Entry Point (Main Function).
    /// </summary>
    public class Server
    {

        #region Players

        private static Thread _thread_clients;

        /// <summary>
        /// All connected players in <see cref="CxServer.Communication.Packet.PacketState.Play"/>.
        /// </summary>
        internal static readonly List<MinecraftClient> _clients = new List<MinecraftClient>();

        /// <summary>
        /// Current count of online players.
        /// </summary>
        public static int OnlinePlayersCount
        {
            get
            {
                lock (_clients)
                {
                    return _clients.Count;
                }
            }
        }

        /// <summary>
        /// Get OnlinePlayer by username.
        /// </summary>
        /// <param name="username">Username of player.</param>
        /// <param name="ignoreCase">Is this case sensitive?</param>
        public static PlayerEntity GetOnlinePlayer(string username, bool ignoreCase)
        {
            StringComparison comparasionType = ignoreCase ? StringComparison.InvariantCultureIgnoreCase : StringComparison.InvariantCulture;

            List<MinecraftClient> clients;
            lock (_clients)
            {
                clients = new List<MinecraftClient>(_clients);
            }
            foreach (var mc in clients)
                if (string.Equals(mc.Username, username, comparasionType))
                    return mc.Entity;
            return null;
        }

        /// <summary>
        /// Get OnlinePlayer by UUID.
        /// </summary>
        /// <param name="uuid">UUID of player.</param>
        public static PlayerEntity GetOnlinePlayer(UUID uuid)
        {
            List<MinecraftClient> clients;
            lock (_clients)
            {
                clients = new List<MinecraftClient>(_clients);
            }
            foreach (var mc in clients)
                if (mc.UUID == uuid)
                    return mc.Entity;
            return null;
        }

        /// <summary>
        /// Check if player is online by username.<br>
        /// Case sensitivity is determined by <see cref="ServerSettings.CaseSensitivePlayerNames"/>.
        /// </summary>
        /// <param name="username">Username of player.</param>
        public static bool IsPlayerOnline(string username) => IsPlayerOnline(username, ServerSettings.CaseSensitivePlayerNames);

        /// <summary>
        /// Check if player is online by username.
        /// </summary>
        /// <param name="username">Username of player.</param>
        /// <param name="caseSensitive">Is this case sensitive?</param>
        public static bool IsPlayerOnline(string username, bool caseSensitive)
        {
            StringComparison comparasionType = caseSensitive ? StringComparison.InvariantCulture : StringComparison.InvariantCultureIgnoreCase;

            List<MinecraftClient> clients;
            lock (_clients)
            {
                clients = new List<MinecraftClient>(_clients);
            }
            foreach (var mc in clients)
                if (string.Equals(mc.Username, username, comparasionType))
                    return true;
            return false;
        }

        /// <summary>
        /// Check if player is online by UUID.
        /// </summary>
        /// <param name="uuid">UUID of player.</param>
        public static bool IsPlayerOnline(UUID uuid)
        {
            List<MinecraftClient> clients;
            lock (_clients)
            {
                clients = new List<MinecraftClient>(_clients);
            }
            foreach (var mc in clients)
                if (mc.UUID == uuid)
                    return true;
            return false;
        }

        /// <summary>
        /// Online players.
        /// </summary>
        public static List<PlayerEntity> OnlinePlayers
        {
            get
            {
                var list = new List<PlayerEntity>();

                List<MinecraftClient> clients;
                lock (_clients)
                {
                    clients = new List<MinecraftClient>(_clients);
                }
                foreach (var mc in clients)
                    list.Add(mc.Entity);

                return list;
            }
        }

        /// <summary>
        /// Get player by UUID.<br>
        /// Always returns saved values (not up-to-date for online players >> check <see cref="CxServer.OfflinePlayer.IsOnline"/>).<br>
        /// Returns <see langref="null"/> for non-used UUIDs.
        /// </summary>
        /// <param name="uuid">UUID of player.</param>
        public static OfflinePlayer GetPlayer(UUID uuid)
        {
            return PlayedBefore(uuid) ? new OfflinePlayer(uuid) : null;
        }

        public static bool PlayedBefore(UUID uuid)
        {
            return File.Exists(Path.Combine(ServerSettings.PlayersFolder, uuid.ToString(true)));
        }

        public static bool PlayedBefore(string name)
        {
            return PlayedBefore(ServerSettings.OnlineMode ? MojangAPI.GetUUID(name) : UUID.GenerateUUID5(ServerSettings.CaseSensitivePlayerNames ? name : name.ToLowerInvariant()));
        }

        #endregion

        #region Versions

        public static string CxVersion
        {
            get;
        } = "0.0.1";

        public static string MinecraftVersion
        {
            get;
        } = "1.10";

        public static int ProtocolVersion
        {
            get;
        } = 210;

        #endregion

        /// <summary>
        /// Entry Point.
        /// </summary>
        /// <param name="args">The command-line arguments.</param>
        public static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.BackgroundColor = ConsoleColor.Black;
            Console.Clear();

            //Init logger and log main info
            ConsoleLogger.Log("Starting Cx Server version '{0}' for Minecraft version '{1}' with protocol version '{2}'", CxVersion, MinecraftVersion, ProtocolVersion);

            //Log HW data
            {
                Console.WriteLine("================================================================");
                Console.WriteLine("|                    Machine informations                      |");
                Console.WriteLine("================================================================");

                Console.WriteLine();

                Console.Write("Command line arguments: ");
                Console.WriteLine(Environment.CommandLine);

                Console.Write("Current directory: ");
                Console.WriteLine(Environment.CurrentDirectory);

                Console.WriteLine(Environment.Is64BitOperatingSystem ? "64-bit operating system" : "32-bit operating system");

                Console.WriteLine(Environment.Is64BitProcess ? "64-bit processor" : "32-bit processor");

                Console.Write("Machine name: ");
                Console.WriteLine(Environment.MachineName);

                Console.Write("Operating system: ");
                Console.WriteLine(Environment.OSVersion.ToString());

                Console.Write("Processor count: ");
                Console.WriteLine(Environment.ProcessorCount.ToString());

                Console.Write("User name: ");
                Console.WriteLine(Environment.UserName);

                Console.Write("Seconds from system startup: ");
                Console.WriteLine((Environment.TickCount / 1000f).ToString());

                Console.WriteLine();

                Console.WriteLine("================================================================");
                Console.WriteLine("|                 End of hardware informations                 |");
                Console.WriteLine("================================================================");
            }


            //Load default config
            ServerSettings.LoadConfig("server.yaml");

            bool standalone_resourcepack_server = false;

            #region Parse arguments

            //Log arguments
            Console.WriteLine("Command-line arguments: " + string.Join(",", args));

            //parse arguments
            foreach (string arg in args)
            {
                if (string.IsNullOrEmpty(arg))
                    continue;
                if (arg.StartsWith("--"))
                {
                    switch (arg)
                    {
                        //GUI

                        case "--gui":
                            {
                                Console.WriteLine("GUI is not supported (yet).");
                                break;
                            }

                        //Configs

                        case "--config=":
                            {
                                string cfg_path = arg.Substring("--config=".Length);
                                if (!File.Exists(cfg_path))
                                {
                                    Console.WriteLine("Cannot load config '{0}' - File Not Found");
                                    break;
                                }
                                ServerSettings.LoadConfig(File.ReadAllText(cfg_path));
                                break;
                            }

                        //Client Listeners

                        case "--port=":
                            {
                                ushort port;
                                if (ushort.TryParse(arg.Substring("--port=".Length), out port))
                                    ServerSettings.ServerPort = port;
                                break;
                            }
                        case "--address=":
                            {
                                IPAddress address;
                                if (IPAddress.TryParse(arg.Substring("--address=".Length), out address))
                                    ServerSettings.ServerAddress = address;
                                break;
                            }

                        //ResourcePack

                        case "--standalone_resource_pack_server":
                            {
                                standalone_resourcepack_server = true;
                                break;
                            }
                        case "--resource_pack=":
                            {
                                ServerSettings.ResourcePackEnabled = arg.Substring("--resource_pack=".Length)[0] == 't';
                                break;
                            }
                        case "--resource_pack_folder=":
                            {
                                ServerSettings.ResourcePackFolder = arg.Substring("--resource_pack_folder=".Length);
                                break;
                            }
                        case "--resource_pack_port=":
                            {
                                ushort port;
                                if (ushort.TryParse(arg.Substring("--resource_pack_port=".Length), out port))
                                    ServerSettings.ServerPort = port;
                                break;
                            }

                        //RCON

                        case "--rcon_port=":
                            {
                                ushort port;
                                if (ushort.TryParse(arg.Substring("--rcon_port=".Length), out port))
                                    ServerSettings.RConPort = port;
                                break;
                            }
                        case "--rcon_password=":
                            {
                                ServerSettings.RConPassword = arg.Substring("--rcon_password=".Length);
                                break;
                            }
                        case "--rcon=":
                            {
                                ServerSettings.RConEnabled = arg.Substring("--rcon=".Length)[0] == 't';
                                break;
                            }
                    }
                    //TODO long arguments
                }
                else if (arg.StartsWith("-"))
                {
                    switch (arg)
                    {
                        case "-srps":
                            {
                                standalone_resourcepack_server = true;
                                break;
                            }
                    }
                    //TODO short arguments
                }
                else
                {
                    PluginManager.LoadPlugin(arg);
                }
            }

            #endregion

            #region Player Data

            if (!Directory.Exists(ServerSettings.PlayersFolder))
            {
                Console.WriteLine("Player directory not found");
                Directory.CreateDirectory(ServerSettings.PlayersFolder);
            }

            #endregion

            #region ResourcePack Standalone

            if (standalone_resourcepack_server)
            {
                if (ServerSettings.ResourcePackPort == 0)
                {
                    Console.WriteLine("Cannot start Standalone ResourcePack Downloader - no port configured");
                    return;
                }

                Console.WriteLine("Starting Standalone ResourcePack Downloader");
                ResourcePackServer.StartStandalone();
                return;
            }

            #endregion

            #region Plugins

            if (!Directory.Exists(ServerSettings.PluginsFolder))
            {
                Console.WriteLine("No plugins directory found.");
                Directory.CreateDirectory(ServerSettings.PluginsFolder);
            }
            else
            {
                var files = Directory.GetFiles(ServerSettings.PluginsFolder, ServerSettings.PluginsRegex, SearchOption.TopDirectoryOnly);

                Console.WriteLine("Loading plugins from {0} files.", files.Length);

                foreach (var file in files)
                    PluginManager.LoadPluginFromFile(file);

                if (PluginManager._plugins.Any())
                {
                    PluginManager.CheckDependency();

                    PluginManager.DoLoadingPhase();

                    Console.WriteLine("Finished loading plugins. Total plugins: " + PluginManager._plugins.Count);
                }
            }

            PluginManager.IsPluginLoadingPhase = false;

            #endregion

            #region World Loading

            if (!Directory.Exists(ServerSettings.WorldsFolder))
            {
                Console.WriteLine("No worlds directory found.");
                Directory.CreateDirectory(ServerSettings.WorldsFolder);
            }
            else
            {
                foreach (var w in ServerSettings.Worlds)
                    WorldManager.RegisterWorld(new World(w));
                Console.WriteLine("Loaded {0} worlds.", WorldManager._worlds.Count);
            }

            #endregion

            #region ResourcePack Listener (HTTP)

            if (ServerSettings.ResourcePackEnabled)
            {
                if (ServerSettings.ResourcePackPort == 0)
                    Console.WriteLine("Cannot start ResourcePack Downloader - no port configured");
                else
                    ResourcePackServer.StartStandalone();
            }
            else
                Console.WriteLine("ResourcePack Downloader is disabled.");

            #endregion

            #region Remote Console Listener

            //init RCon listener
            //TODO RCON listener

            #endregion

            #region Client Listener

            //init client listener
            _thread_clients = new Thread(() =>
            {
                TcpListener listener = new TcpListener(ServerSettings.ServerAddress, ServerSettings.ServerPort);
                listener.Start();

                Console.WriteLine("Started listening for new players at address {0} and port {1}", ServerSettings.ServerAddress, ServerSettings.ServerPort);

                while (true)
                {
                    TcpClient client = listener.AcceptTcpClient();
                    MinecraftClient mcc = new MinecraftClient(client);
                    Console.WriteLine("New client {0}", client.Client.RemoteEndPoint);
                    mcc.Start();
                }
            });
            Thread.Sleep(1000);
            _thread_clients.Start();

            #endregion
        }

        /// <summary>
        /// Process command from console.
        /// </summary>
        /// <returns><c>true</c>, if successful, <c>false</c> otherwise.</returns>
        /// <param name="command">Command to process (including arguments).</param>
        public static bool ProcessConsoleCommand(string command) => ProcessCommand(ConsoleCommandSender, command);

        public static ConsoleCommandSender ConsoleCommandSender;

        public static bool ProcessCommand(ICommandSender sender, string command)
        {
            throw new NotImplementedException("Process Command");
        }

    }
}
﻿using System;
using System.Collections.Generic;
using CxServer.Worlds;
using CxServer.Entities;
using CxServer.BlocksAndItems.Items;

namespace CxServer.BlocksAndItems.Blocks
{
    public class Block : BlockOrItem
    {
        public Block(string codename, ushort id, BlockSolidity solidity = BlockSolidity.Solid, Type itemType = null) : base(id, codename)
        {
            this.Solidity = solidity;

            if (itemType == null)
                this.Item = new ItemBlock(this);
            else if (!itemType.IsSubclassOf(typeof(ItemBlock)))
                throw new ArgumentException("Item type is not ItemBlock", "itemType");
            else
                this.Item = (ItemBlock)Activator.CreateInstance(itemType, this);
        }

        #region Solidity

        public enum BlockSolidity
        {
            Air = 0,
            Transparent = 2,
            Solid = 1,
        }

        public BlockSolidity Solidity
        {
            get;
        }

        #endregion

        #region Items

        public ItemBlock Item
        {
            get;
        }

        public virtual List<ItemStack> GetDropItems(World world, int x, int y, int z, int fortune = 0, Entity breakerEntity = null)
        {
            return new List<ItemStack>();
        }

        public virtual ItemStack PickItem
        {
            get
            {
                return new ItemStack(Item);
            }
        }

        #endregion
    }
}
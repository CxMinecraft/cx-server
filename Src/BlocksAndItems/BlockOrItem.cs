﻿using System;

namespace CxServer.BlocksAndItems
{
    public class BlockOrItem
    {
        public BlockOrItem(ushort id, string codename)
        {
            this.ID = id;
            this.Codename = codename;
        }

        public ushort ID
        {
            get;
        }

        public string Codename
        {
            get;
        }
    }
}
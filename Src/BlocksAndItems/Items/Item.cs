﻿using System;

namespace CxServer.BlocksAndItems.Items
{
    public class Item : BlockOrItem
    {
        public Item(string codename, ushort id) : base(id, codename)
        {
        }
    }
}
﻿using System;
using CxServer.BlocksAndItems.Blocks;

namespace CxServer.BlocksAndItems.Items
{
    public class ItemBlock : Item
    {
        public ItemBlock(Block block) : base(block.Codename, block.ID)
        {
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using CxServer.Utils;
using System.Net;
using CxServer.Settings.MOTD;
using Utils;

namespace CxServer.Settings
{
    public static class ServerSettings
    {

        #region Remote Console

        /// <summary>
        /// Enable Remote Console using <a href="https://developer.valvesoftware.com/wiki/Source_RCON_Protocol">Source RCON Protocol</a>.
        /// </summary>
        public static bool RConEnabled
        {
            get;
            internal set;
        } = false;

        /// <summary>
        /// Remote Console port.
        /// </summary>
        public static ushort RConPort
        {
            get;
            internal set;
        } = 25567;

        /// <summary>
        /// Allow players (admins with permission) to login to Remote Console and use commands (requires additional permissions)
        /// </summary>
        public static bool RConAdminLogin
        {
            get;
            set;
        } = false;

        /// <summary>
        /// Remote Console password.
        /// </summary>
        public static string RConPassword
        {
            get;
            set;
        } = "SuperSecretPassword";

        #endregion

        #region Resource Pack Server

        /// <summary>
        /// Enable ResourcePack Sharing Server (HTTP).
        /// </summary>
        public static bool ResourcePackEnabled
        {
            get;
            internal set;
        } = true;

        /// <summary>
        /// ResourcePack Sharing Server port.
        /// </summary>
        public static ushort ResourcePackPort
        {
            get;
            internal set;
        } = 25566;

        /// <summary>
        /// ResourcePack Sharing Server folder.
        /// </summary>
        public static string ResourcePackFolder
        {
            get;
            internal set;
        } = "resourcepacks";

        /// <summary>
        /// ResourcePack Sharing Server Regex for validating filenames.
        /// </summary>
        public static string ResourcePackRegex
        {
            get;
            internal set;
        } = "^[a-zA-Z0-9_]+\\.zip$";

        /// <summary>
        /// ResourcePack Sharing Server Regex for validating filenames for ResourcePack List.
        /// </summary>
        public static string ResourcePackListRegex
        {
            get;
            internal set;
        } = "^[a-zA-Z0-9_]+\\.zip$";

        #endregion

        #region Client Listener

        /// <summary>
        /// Client Listener port.
        /// </summary>
        public static ushort ServerPort
        {
            get;
            internal set;
        } = 25565;

        /// <summary>
        /// Client Listener address (and other services).
        /// </summary>
        public static IPAddress ServerAddress
        {
            get;
            internal set;
        } = IPAddress.Any;

        /// <summary>
        /// Allow only Premium Minecraft players to join.
        /// </summary>
        public static bool OnlineMode
        {
            get;
            internal set;
        } = false;

        /// <summary>
        /// Maximum number of players on server.
        /// </summary>
        public static short MaxPlayers
        {
            get;
            set;
        } = 20;

        /// <summary>
        /// Maximum players from same IP address.<br>
        /// 0 = any
        /// </summary>
        public static byte MaxPlayersFromSameAddress
        {
            get;
            set;
        } = 2;

        #endregion

        #region Plugins

        /// <summary>
        /// Folder where are plugins stored.
        /// </summary>
        public static string PluginsFolder
        {
            get;
            internal set;
        } = "plugins";

        /// <summary>
        /// Regex for loading plugins (checking extension).
        /// </summary>
        public static string PluginsRegex
        {
            get;
            internal set;
        } = "*.dll|*.so$";

        #endregion

        #region Players

        /// <summary>
        /// Folder where are stored data of every Player.
        /// </summary>
        public static string PlayersFolder
        {
            get;
            internal set;
        } = "players";

        /// <summary>
        /// When enabled player with name <i>"Steve"</i> can connect when <i>"sTeVe"</i> is already connected.<br>
        /// When disabled player's <see cref="CxServer.Utils.UUID"/> is calculated from lower case variant (in <see cref="CxServer.Settings.ServerSettings.OnlineMode"/> off).<br>
        /// <b>DO NOT SWITCH THIS ON RUNNING <u>OFFLINE-MODE</u> SERVERS (player's inventories will have problems)</b>
        /// </summary>
        public static bool CaseSensitivePlayerNames
        {
            get;
            internal set;
        } = true;

        #endregion

        #region Other Settings

        /// <summary>
        /// Distance in chunks around every player where is world updated.<br>
        /// Should be same as Render distance on client.
        /// </summary>
        public static byte UpdateDistance
        {
            get;
            set;
        } = 10;

        /// <summary>
        /// Only check collisions for players. Mobs are controlled with AI.
        /// </summary>
        public static bool SimpleCollisions
        {
            get;
            internal set;
        } = true;

        /// <summary>
        /// Should clients show reduced debug info in Debug screen?<br>
        /// <see cref="CxServer.Entities.Living.PlayerEntity.ReducedDebugInfo"/>
        /// </summary>
        public static bool ReducedDebugInfoClients
        {
            get;
            set;
        } = false;

        public static string LogDirectory
        {
            get;
        } = "logs";

        public static int TicksPerSecond
        {
            get;
        } = 20;

        public static float TargetDelta
        {
            get
            {
                return 1f / TicksPerSecond;
            }
        }

        public static string LanguageDirectory
        {
            get;
        } = "langs";

        public static string FallbackLanguage
        {
            get;
            set;
        } = Translator.AmericanEnglish;

        #endregion

        #region Messages and Commands

        public static int MaximumMessageLength
        {
            get;
            set;
        } = 256;

        public static char CommandCharacter
        {
            get;
            set;
        } = '/';

        #endregion

        #region Client's Multiplayer Menu

        public static IMOTD MOTD
        {
            get;
            set;
        } = new SimpleMOTD();

        #endregion

        #region Worlds Settings

        /// <summary>
        /// Folder where are stored all Worlds
        /// </summary>
        public static string WorldsFolder
        {
            get;
            internal set;
        } = "worlds";

        /// <summary>
        /// Name of main world. Must be valid world name.
        /// </summary>
        public static string MainWorld
        {
            get;
            internal set;
        } = "world";

        /// <summary>
        /// Configuration of all (stored) worlds.
        /// </summary>
        public static List<WorldSettings> Worlds = new List<WorldSettings>();

        #endregion

        /// <summary>
        /// Load configuration. Call this before starting all services.
        /// </summary>
        /// <param name="content">Content of configuration</param>
        public static void LoadConfig(string content)
        {
            //throw new NotImplementedException();
            //TODO Load Server Config

            WorldSettings ws = new WorldSettings("Test") { Difficulty = Difficulty.Normal, Dimension = Dimension.Overworld, GeneratorName = "Flat", Seed = "Lipser" };
            Worlds.Add(ws);
            MainWorld = "Test";
        }

        public static bool SaveConfig(string path)
        {
            if (File.Exists(path))
            {
                File.Delete(path);
                if (File.Exists(path))
                    return false;
            }

            using (StreamWriter writer = new StreamWriter(File.Open(path, FileMode.CreateNew), System.Text.Encoding.UTF8))
            {
                writer.AutoFlush = false;

                //Remote Console
                {
                    writer.Write("RemoteConsole_Enabled=");
                    writer.WriteLine(RConEnabled ? "yes" : "no");

                    writer.Write("RemoteConsole_Port=");
                    writer.WriteLine(RConPort);

                    writer.Write("RemoteConsole_AdminLogin=");
                    writer.WriteLine(RConAdminLogin ? "yes" : "no");

                    writer.Write("RemoteConsole_Password=");
                    writer.WriteLine(RConPassword ?? StaticRandom.NextString(16));
                }

                writer.Flush();

                //Resource pack server
                {
                    writer.Write("ResourcePackServer_Enabled=");
                    writer.WriteLine(ResourcePackEnabled ? "yes" : "no");

                    writer.Write("ResourcePackServer_Port=");
                    writer.WriteLine(ResourcePackPort);

                    writer.Write("ResourcePackServer_Folder=");
                    writer.WriteLine(ResourcePackFolder ?? "resource_packs");

                    writer.Write("ResourcePackServer_Regex=");
                    writer.WriteLine(ResourcePackRegex ?? "^[a-zA-Z0-9_]+\\.zip$");
                }

                writer.Flush();

                //Client Listener
                {
                    writer.Write("Server_Port=");
                    writer.WriteLine(ServerPort);

                    writer.Write("Server_Address=");
                    string ip = ServerAddress.ToString();
                    if (ServerAddress == IPAddress.IPv6Any)
                        ip = "Any_IPv6";
                    else if (ServerAddress == IPAddress.Any)
                        ip = "Any_IPv4";
                    writer.WriteLine(ip);

                    writer.Write("Server_OnlineMode=");
                    writer.WriteLine(OnlineMode ? "yes" : "no");

                    writer.Write("Server_MaxPlayers=");
                    writer.WriteLine(MaxPlayers);

                    writer.Write("Server_MaxPlayers_SameAddress=");
                    writer.WriteLine(MaxPlayersFromSameAddress);
                }

                writer.Flush();

                //Plugins
                {
                    writer.Write("Plugins_Folder=");
                    writer.WriteLine(PluginsFolder ?? "plugins");

                    writer.Write("Plugins_Regex=");
                    writer.WriteLine(PluginsRegex ?? "*.dll|*.so$");
                }

                writer.Flush();

                //Players
                {
                    writer.Write("Players_Folder=");
                    writer.WriteLine(PlayersFolder ?? "players");

                    writer.Write("Players_CaseSensitive=");
                    writer.WriteLine(CaseSensitivePlayerNames ? "yes" : "no");
                }

                writer.Flush();

                //Client's Multiplayer Menu
                {
                    MOTD.Save(writer);
                }

                writer.Flush();

                //Other Settings
                {
                    writer.WriteLine("//Distance in chunks around players to update. Use same as Render Distance in your client.");
                    writer.Write("UpdateDistance=");
                    writer.WriteLine(UpdateDistance);

                    writer.WriteLine("//Check collision only for players?");
                    writer.Write("SimpleCollisions=");
                    writer.WriteLine(SimpleCollisions ? "yes" : "no");

                    writer.WriteLine("//Should clients show reduced debug info in Debug screen?");
                    writer.Write("ReducedDebugInfoClients=");
                    writer.WriteLine(ReducedDebugInfoClients ? "yes" : "no");
                }

                writer.Flush();

                //Worlds
                {
                    writer.WriteLine("//Folder to store worlds in.");
                    writer.Write("WorldsFolder=");
                    writer.WriteLine(WorldsFolder);

                    writer.WriteLine("//Name of main world. Must be valid world name.");
                    writer.Write("WorldMain=");
                    writer.WriteLine(MainWorld);

                    foreach (var ws in Worlds)
                        ws.Save(writer);
                }

                writer.Flush();
            }

            return true;
        }
    }
}
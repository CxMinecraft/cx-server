﻿using System;
using CxServer.Utils;
using System.IO;
using Utils;

namespace CxServer.Settings
{
    public sealed class WorldSettings
    {

        #region Constructor

        public WorldSettings(string name) : this(name, UUID.GenerateUUID3(name))
        {
        }

        internal WorldSettings(string name, UUID uuid)
        {
            this.Name = name;
            this.UUID = uuid;
        }

        #endregion

        public string Name { get; internal set; }

        public Difficulty Difficulty { get; internal set; } = Difficulty.Normal;

        /// <summary>
        /// Unique ID used for save validation
        /// </summary>
        public UUID UUID { get; internal set; }

        public int MaxBuildHeight { get; internal set; } = 256;

        public Dimension Dimension { get; internal set; } = Dimension.Overworld;

        #region Spawn

        /// <summary>
        /// Number of blocks where is spawn protected.<br>
        /// This is owerwritten by permission <b>minecraft.spawn.modify</b>
        /// </summary>
        public static int SpawnProtection
        {
            get;
            set;
        } = 2 * CxServer.Worlds.Chunk.Size;

        public Position SpawnPosition
        {
            get;
            set;
        } = new Position();

        #endregion

        #region Generator

        public string GeneratorName { get; internal set; } = "DEFAULT";

        public string GeneratorSettings { get; internal set; } = "";

        public string Seed { get; internal set; } = "";

        #endregion

        #region Parsing and Saving

        public void Save(StreamWriter writer)
        {
            string prefix = string.Format("World_{0}_", Name);

            //Generator
            {
                writer.Write(prefix);
                writer.Write("Generator_Name=");
                writer.WriteLine(GeneratorName ?? "DEFAULT");

                writer.Write(prefix);
                writer.Write("Generator_Settings=");
                writer.WriteLine(GeneratorSettings ?? "");

                writer.Write(prefix);
                writer.Write("Seed=");
                writer.WriteLine(Seed);
            }
            //TODO save world settings
        }

        public static WorldSettings Load(StreamReader reader)
        {
            throw new NotImplementedException("World Settings - Load");
        }

        #endregion
    }
}
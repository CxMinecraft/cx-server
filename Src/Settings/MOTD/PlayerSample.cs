﻿using System;
using CxServer.Utils;
using Utils;

namespace CxServer.Settings.MOTD
{
    public struct PlayerSample
    {
        public PlayerSample(string name, UUID uuid)
        {
            this.Name = name;
            this.UUID = uuid;
        }

        public string Name
        {
            get;
        }

        public UUID UUID
        {
            get;
        }

        public override string ToString()
        {
            return string.Format("{{ \"name\": \"{0}\", \"id\": \"{1}\" }}", Name, UUID.ToString(true));
        }
    }
}
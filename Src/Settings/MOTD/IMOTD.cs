﻿using System;
using System.Drawing;
using System.IO;

namespace CxServer.Settings.MOTD
{
    public interface IMOTD
    {
        //TODO change to Chat
        string Description
        {
            get;
        }

        Image FavIcon
        {
            get;
        }

        PlayerSample[] Sample
        {
            get;
        }

        int OnlinePlayers
        {
            get;
        }

        int MaxPlayers
        {
            get;
        }

        void Save(StreamWriter writer);
    }
}
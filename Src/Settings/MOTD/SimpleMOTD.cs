﻿using System;
using System.Drawing;
using System.IO;
using Utils;

namespace CxServer.Settings.MOTD
{
    public class SimpleMOTD : IMOTD
    {
        public SimpleMOTD(int onlinePlayers = 0, int maxPlayers = 20, string description = "C# Minecraft Server", Image favIcon = null, PlayerSample[] sample = null)
        {
            this.OnlinePlayers = onlinePlayers;
            this.MaxPlayers = maxPlayers;

            this.Description = description;
            this.FavIcon = favIcon;
            this.Sample = sample;
        }
        public SimpleMOTD(int onlinePlayers, int maxPlayers, string description, string favIconPath, PlayerSample[] sample = null)
        {
            this.OnlinePlayers = onlinePlayers;
            this.MaxPlayers = maxPlayers;

            this.Description = description;

            if (string.IsNullOrEmpty(favIconPath) || !File.Exists(favIconPath))
            {
                this.FavIcon = null;
                this.FavIconPath = null;
            }
            else
            {
                this.FavIcon = Image.FromFile(favIconPath);
                this.FavIconPath = favIconPath;
            }

            this.Sample = sample;
        }

        public string FavIconPath
        {
            get;
        }

        #region IMOTD implementation

        public string Description
        {
            get;
        }

        public Image FavIcon
        {
            get;
        }

        public PlayerSample[] Sample
        {
            get;
        }

        public int OnlinePlayers
        {
            get;
        }

        public int MaxPlayers
        {
            get;
        }

        public void Save(StreamWriter writer)
        {
            writer.WriteLine("MOTD_Type=Simple");

            writer.Write("MOTD_Description=");
            writer.WriteLine(Description.Replace("\n", "\\n"));

            writer.Write("MOTD_FavIconPath=");
            if (string.IsNullOrEmpty(FavIconPath))
                writer.WriteLine();
            else
                writer.WriteLine(FavIconPath);

            writer.Write("MOTD_Sample_Count=");
            if (Sample == null || Sample.Length == 0)
            {
                writer.WriteLine(0);
            }
            else
            {
                writer.WriteLine(Sample.Length);

                for (int i = 0; i < Sample.Length; i++)
                {
                    writer.Write("MOTD_Sample_");
                    writer.Write(i);
                    writer.Write('=');

                    var ps = Sample[i];

                    writer.Write(ps.UUID.ToString(true));
                    writer.Write(';');
                    writer.Write(ps.Name);
                    writer.WriteLine();
                }
            }
        }

        #endregion

        /// <summary>
        /// Create fake players from multi-line string.
        /// </summary>
        /// <param name="text">Multi-line text to use.</param>
        public static PlayerSample[] CreateFakeSample(string text)
        {
            if (string.IsNullOrEmpty(text))
                return new PlayerSample[0];

            string[] lines = text.Split('\n');
            PlayerSample[] sample = new PlayerSample[lines.Length];

            for (int i = 0; i < lines.Length; i++)
                sample[i] = new PlayerSample(lines[i], new UUID(0, i));

            return sample;
        }
    }
}
﻿using System;
using CxServer.Worlds.Generators;
using CxServer.Worlds;
using System.Collections.Generic;
using CxServer.Settings;
using System.Linq;
using CxServer.Utils;
using Utils;

namespace CxServer.Managers
{
    public static class WorldManager
    {

        #region World Generator

        public static WorldGenerator GetWorldGenerator(string name) => WorldGeneratorManager.GetGenerator(name);

        public static void RegisterWorldGenerator(string name, WorldGenerator generator) => WorldGeneratorManager.RegisterGenerator(name, generator);

        #endregion

        #region World

        public static bool RegisterWorld(World world)
        {
            if (WorldExists(world))
                return false;

            _worlds.Add(world);
            Console.WriteLine("Registred new world '{0}'", world.Name);
            return true;
        }

        public class WorldAlreadyRegistredException : Exception
        {
            public World World
            {
                get;
            }

            public WorldAlreadyRegistredException(World world) : base(string.Format("World '{0}'/'{1}' is already registred", world.Name, world.UUID))
            {
                this.World = world;
            }

            public WorldAlreadyRegistredException(World world, string message) : base(string.Format("World '{0}'/'{1}' is already registred: {2}", world.Name, world.UUID, message))
            {
                this.World = world;
            }
        }

        public class WorldNameAlreadyRegistredException : WorldAlreadyRegistredException
        {
            public WorldNameAlreadyRegistredException(World world) : base(world, "World with same name is already registred")
            {
            }
        }

        public class WorldUUIDAlreadyRegistredException : WorldAlreadyRegistredException
        {
            public WorldUUIDAlreadyRegistredException(World world) : base(world, "World with same UUID is already registred")
            {
            }
        }

        internal static readonly List<World> _worlds = new List<World>();

        public static World MainWorld
        {
            get
            {
                var world = GetWorld(ServerSettings.MainWorld);
                if (world != null)
                    return world;
                else
                    throw new NullReferenceException("No main world set or is invalid.");
            }
            set
            {
                if (_worlds.Contains(value))
                    ServerSettings.MainWorld = value.Name;
                else
                    throw new IndexOutOfRangeException("World is not registred.");
            }
        }

        public static bool WorldExists(World world)
        {
            return _worlds.Any(w => w.Name == world.Name || w.UUID == world.UUID);
        }

        public static World GetWorld(string name)
        {
            return _worlds.FirstOrDefault(w => w.Name == name);
        }

        public static World GetWorld(UUID uuid)
        {
            return _worlds.FirstOrDefault(w => w.UUID == uuid);
        }

        public static bool DestroyWorld(string name) => DestroyWorld(GetWorld(name));

        public static bool DestroyWorld(UUID uuid) => DestroyWorld(GetWorld(uuid));

        public static bool DestroyWorld(World world)
        {
            throw new NotImplementedException("World Manager - destroy / remove world");
        }

        public static World[] AllWorlds
        {
            get
            {
                return _worlds.ToArray();
            }
        }

        public static World[] GetAllWorlds(Dimension dimension)
        {
            return _worlds.Where(w => w.Dimension == dimension).ToArray();
        }

        public static World[] GetAllWorlds(Difficulty difficulty)
        {
            return _worlds.Where(w => w.Difficulty == difficulty).ToArray();
        }

        #endregion
    }
}
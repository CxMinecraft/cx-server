﻿using System;
using CxServer.Communication;
using System.Collections.Generic;
using CxServer.Entities.Living;

namespace CxServer.Managers
{
    public static class EventsManager
    {

        #region Event classes

        public sealed class Event
        {
            public enum Priority
            {
                Monitor = 0,
                Low = 1,
                Normal = 2,
                Hight = 3,
                Ultimate = 4
            }

            public class ActionParameterTypeMismatchException : Exception
            {
                public ActionParameterTypeMismatchException(Type parameterType, Type sourceType) : base(string.Format("Type '{0}' is not '{1}'", parameterType, sourceType))
                {
                    this.ParameterType = parameterType;
                    this.SourceType = sourceType;
                }

                public Type ParameterType
                {
                    get;
                }

                public Type SourceType
                {
                    get;
                }
            }

            public class ActionParameterCountMismatchException : Exception
            {
                public ActionParameterCountMismatchException(int parameterCount, int sourceCount) : base(string.Format("Wrong number of parameters. Expected {1} but got {0}.", parameterCount, sourceCount))
                {
                    this.ParameterCount = parameterCount;
                    this.SourceCount = sourceCount;
                }

                public int ParameterCount
                {
                    get;
                }

                public int SourceCount
                {
                    get;
                }
            }

            private List<Action>[] Handlers
            {
                get;
            } = new List<Action>[5] { new List<Action>(), new List<Action>(), new List<Action>(), new List<Action>(), new List<Action>() };

            public void Add(Action val, Event.Priority priority = Event.Priority.Normal)
            {
                var parameters = val.Method.GetParameters();
                if (parameters.Length != 0)
                    throw new Event.ActionParameterCountMismatchException(parameters.Length, 0);

                Handlers[(int)priority].Add(val);
            }

            public void Remove(Action val)
            {
                foreach (var list in Handlers)
                    list.Remove(val);
            }

            public void Call()
            {
                foreach (var list in Handlers)
                    foreach (var d in list)
                        d.DynamicInvoke();
            }
        }

        public sealed class Event<T>
        {
            private List<Action<T>>[] Handlers
            {
                get;
            } = new List<Action<T>>[5] { new List<Action<T>>(), new List<Action<T>>(), new List<Action<T>>(), new List<Action<T>>(), new List<Action<T>>() };

            public void Add(Action<T> val, Event.Priority priority = Event.Priority.Normal)
            {
                var parameters = val.Method.GetParameters();
                if (parameters.Length != 1)
                    throw new Event.ActionParameterCountMismatchException(parameters.Length, 1);
                if (!(parameters[0].ParameterType is T) || (parameters[0].ParameterType.IsValueType && !parameters[0].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[0].ParameterType, typeof(T));

                Handlers[(int)priority].Add(val);
            }

            public void Remove(Action<T> val)
            {
                foreach (var list in Handlers)
                    list.Remove(val);
            }

            public void Call(ref T arg)
            {
                foreach (var list in Handlers)
                    foreach (var d in list)
                        d.DynamicInvoke(arg);
            }
        }

        public sealed class Event<T0, T1>
        {
            private List<Action<T0, T1>>[] Handlers
            {
                get;
            } = new List<Action<T0, T1>>[5] { new List<Action<T0, T1>>(), new List<Action<T0, T1>>(), new List<Action<T0, T1>>(), new List<Action<T0, T1>>(), new List<Action<T0, T1>>() };

            public void Add(Action<T0, T1> val, Event.Priority priority = Event.Priority.Normal)
            {
                var parameters = val.Method.GetParameters();
                if (parameters.Length != 2)
                    throw new Event.ActionParameterCountMismatchException(parameters.Length, 2);
                if (!(parameters[0].ParameterType is T0) || (parameters[0].ParameterType.IsValueType && !parameters[0].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[0].ParameterType, typeof(T0));
                if (!(parameters[1].ParameterType is T1) || (parameters[1].ParameterType.IsValueType && !parameters[1].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[1].ParameterType, typeof(T1));

                Handlers[(int)priority].Add(val);
            }

            public void Remove(Action<T0, T1> val)
            {
                foreach (var list in Handlers)
                    list.Remove(val);
            }

            public void Call(ref T0 arg0, ref T1 arg1)
            {
                foreach (var list in Handlers)
                    foreach (var d in list)
                        d.DynamicInvoke(arg0, arg1);
            }
        }

        public sealed class Event<T0, T1, T2>
        {
            private List<Action<T0, T1, T2>>[] Handlers
            {
                get;
            } = new List<Action<T0, T1, T2>>[5] { new List<Action<T0, T1, T2>>(), new List<Action<T0, T1, T2>>(), new List<Action<T0, T1, T2>>(), new List<Action<T0, T1, T2>>(), new List<Action<T0, T1, T2>>() };

            public void Add(Action<T0, T1, T2> val, Event.Priority priority = Event.Priority.Normal)
            {
                var parameters = val.Method.GetParameters();
                if (parameters.Length != 3)
                    throw new Event.ActionParameterCountMismatchException(parameters.Length, 3);
                if (!(parameters[0].ParameterType is T0) || (parameters[0].ParameterType.IsValueType && !parameters[0].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[0].ParameterType, typeof(T0));
                if (!(parameters[1].ParameterType is T1) || (parameters[1].ParameterType.IsValueType && !parameters[1].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[1].ParameterType, typeof(T1));
                if (!(parameters[2].ParameterType is T2) || (parameters[2].ParameterType.IsValueType && !parameters[2].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[2].ParameterType, typeof(T2));

                Handlers[(int)priority].Add(val);
            }

            public void Remove(Action<T0, T1, T2> val)
            {
                foreach (var list in Handlers)
                    list.Remove(val);
            }

            public void Call(ref T0 arg0, ref T1 arg1, ref T2 arg2)
            {
                foreach (var list in Handlers)
                    foreach (var d in list)
                        d.DynamicInvoke(arg0, arg1, arg2);
            }
        }

        public sealed class Event<T0, T1, T2, T3>
        {
            private List<Action<T0, T1, T2, T3>>[] Handlers
            {
                get;
            } = new List<Action<T0, T1, T2, T3>>[5] { new List<Action<T0, T1, T2, T3>>(), new List<Action<T0, T1, T2, T3>>(), new List<Action<T0, T1, T2, T3>>(), new List<Action<T0, T1, T2, T3>>(), new List<Action<T0, T1, T2, T3>>() };

            public void Add(Action<T0, T1, T2, T3> val, Event.Priority priority = Event.Priority.Normal)
            {
                var parameters = val.Method.GetParameters();
                if (parameters.Length != 4)
                    throw new Event.ActionParameterCountMismatchException(parameters.Length, 4);
                if (!(parameters[0].ParameterType is T0) || (parameters[0].ParameterType.IsValueType && !parameters[0].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[0].ParameterType, typeof(T0));
                if (!(parameters[1].ParameterType is T1) || (parameters[1].ParameterType.IsValueType && !parameters[1].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[1].ParameterType, typeof(T1));
                if (!(parameters[2].ParameterType is T2) || (parameters[2].ParameterType.IsValueType && !parameters[2].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[2].ParameterType, typeof(T2));
                if (!(parameters[3].ParameterType is T3) || (parameters[3].ParameterType.IsValueType && !parameters[3].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[3].ParameterType, typeof(T3));

                Handlers[(int)priority].Add(val);
            }

            public void Remove(Action<T0, T1, T2, T3> val)
            {
                foreach (var list in Handlers)
                    list.Remove(val);
            }

            public void Call(ref T0 arg0, ref T1 arg1, ref T2 arg2, ref T3 arg3)
            {
                foreach (var list in Handlers)
                    foreach (var d in list)
                        d.DynamicInvoke(arg0, arg1, arg2, arg3);
            }
        }

        public sealed class Event<T0, T1, T2, T3, T4>
        {
            private List<Action<T0, T1, T2, T3, T4>>[] Handlers
            {
                get;
            } = new List<Action<T0, T1, T2, T3, T4>>[5] { new List<Action<T0, T1, T2, T3, T4>>(), new List<Action<T0, T1, T2, T3, T4>>(), new List<Action<T0, T1, T2, T3, T4>>(), new List<Action<T0, T1, T2, T3, T4>>(), new List<Action<T0, T1, T2, T3, T4>>() };

            public void Add(Action<T0, T1, T2, T3, T4> val, Event.Priority priority = Event.Priority.Normal)
            {
                var parameters = val.Method.GetParameters();
                if (parameters.Length != 5)
                    throw new Event.ActionParameterCountMismatchException(parameters.Length, 5);
                if (!(parameters[0].ParameterType is T0) || (parameters[0].ParameterType.IsValueType && !parameters[0].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[0].ParameterType, typeof(T0));
                if (!(parameters[1].ParameterType is T1) || (parameters[1].ParameterType.IsValueType && !parameters[1].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[1].ParameterType, typeof(T1));
                if (!(parameters[2].ParameterType is T2) || (parameters[2].ParameterType.IsValueType && !parameters[2].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[2].ParameterType, typeof(T2));
                if (!(parameters[3].ParameterType is T3) || (parameters[3].ParameterType.IsValueType && !parameters[3].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[3].ParameterType, typeof(T3));
                if (!(parameters[4].ParameterType is T4) || (parameters[4].ParameterType.IsValueType && !parameters[0].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[4].ParameterType, typeof(T4));

                Handlers[(int)priority].Add(val);
            }

            public void Remove(Action<T0, T1, T2, T3, T4> val)
            {
                foreach (var list in Handlers)
                    list.Remove(val);
            }

            public void Call(ref T0 arg0, ref T1 arg1, ref T2 arg2, ref T3 arg3, ref T4 arg4)
            {
                foreach (var list in Handlers)
                    foreach (var d in list)
                        d.DynamicInvoke(arg0, arg1, arg2, arg3, arg4);
            }
        }

        public sealed class Event<T0, T1, T2, T3, T4, T5>
        {
            private List<Action<T0, T1, T2, T3, T4, T5>>[] Handlers
            {
                get;
            } = new List<Action<T0, T1, T2, T3, T4, T5>>[5] { new List<Action<T0, T1, T2, T3, T4, T5>>(), new List<Action<T0, T1, T2, T3, T4, T5>>(), new List<Action<T0, T1, T2, T3, T4, T5>>(), new List<Action<T0, T1, T2, T3, T4, T5>>(), new List<Action<T0, T1, T2, T3, T4, T5>>() };

            public void Add(Action<T0, T1, T2, T3, T4, T5> val, Event.Priority priority = Event.Priority.Normal)
            {
                var parameters = val.Method.GetParameters();
                if (parameters.Length != 6)
                    throw new Event.ActionParameterCountMismatchException(parameters.Length, 6);
                if (!(parameters[0].ParameterType is T0) || (parameters[0].ParameterType.IsValueType && !parameters[0].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[0].ParameterType, typeof(T0));
                if (!(parameters[1].ParameterType is T1) || (parameters[1].ParameterType.IsValueType && !parameters[1].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[1].ParameterType, typeof(T1));
                if (!(parameters[2].ParameterType is T2) || (parameters[2].ParameterType.IsValueType && !parameters[2].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[2].ParameterType, typeof(T2));
                if (!(parameters[3].ParameterType is T3) || (parameters[3].ParameterType.IsValueType && !parameters[3].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[3].ParameterType, typeof(T3));
                if (!(parameters[4].ParameterType is T4) || (parameters[4].ParameterType.IsValueType && !parameters[0].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[4].ParameterType, typeof(T4));
                if (!(parameters[5].ParameterType is T5) || (parameters[5].ParameterType.IsValueType && !parameters[5].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[5].ParameterType, typeof(T5));

                Handlers[(int)priority].Add(val);
            }

            public void Remove(Action<T0, T1, T2, T3, T4, T5> val)
            {
                foreach (var list in Handlers)
                    list.Remove(val);
            }

            public void Call(ref T0 arg0, ref T1 arg1, ref T2 arg2, ref T3 arg3, ref T4 arg4, ref T5 arg5)
            {
                foreach (var list in Handlers)
                    foreach (var d in list)
                        d.DynamicInvoke(arg0, arg1, arg2, arg3, arg4, arg5);
            }
        }

        public sealed class Event<T0, T1, T2, T3, T4, T5, T6>
        {
            private List<Action<T0, T1, T2, T3, T4, T5, T6>>[] Handlers
            {
                get;
            } = new List<Action<T0, T1, T2, T3, T4, T5, T6>>[5] { new List<Action<T0, T1, T2, T3, T4, T5, T6>>(), new List<Action<T0, T1, T2, T3, T4, T5, T6>>(), new List<Action<T0, T1, T2, T3, T4, T5, T6>>(), new List<Action<T0, T1, T2, T3, T4, T5, T6>>(), new List<Action<T0, T1, T2, T3, T4, T5, T6>>() };

            public void Add(Action<T0, T1, T2, T3, T4, T5, T6> val, Event.Priority priority = Event.Priority.Normal)
            {
                var parameters = val.Method.GetParameters();
                if (parameters.Length != 7)
                    throw new Event.ActionParameterCountMismatchException(parameters.Length, 7);
                if (!(parameters[0].ParameterType is T0) || (parameters[0].ParameterType.IsValueType && !parameters[0].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[0].ParameterType, typeof(T0));
                if (!(parameters[1].ParameterType is T1) || (parameters[1].ParameterType.IsValueType && !parameters[1].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[1].ParameterType, typeof(T1));
                if (!(parameters[2].ParameterType is T2) || (parameters[2].ParameterType.IsValueType && !parameters[2].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[2].ParameterType, typeof(T2));
                if (!(parameters[3].ParameterType is T3) || (parameters[3].ParameterType.IsValueType && !parameters[3].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[3].ParameterType, typeof(T3));
                if (!(parameters[4].ParameterType is T4) || (parameters[4].ParameterType.IsValueType && !parameters[0].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[4].ParameterType, typeof(T4));
                if (!(parameters[5].ParameterType is T5) || (parameters[5].ParameterType.IsValueType && !parameters[5].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[5].ParameterType, typeof(T5));
                if (!(parameters[6].ParameterType is T6) || (parameters[6].ParameterType.IsValueType && !parameters[6].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[6].ParameterType, typeof(T6));

                Handlers[(int)priority].Add(val);
            }

            public void Remove(Action<T0, T1, T2, T3, T4, T5, T6> val)
            {
                foreach (var list in Handlers)
                    list.Remove(val);
            }

            public void Call(ref T0 arg0, ref T1 arg1, ref T2 arg2, ref T3 arg3, ref T4 arg4, ref T5 arg5, ref T6 arg6)
            {
                foreach (var list in Handlers)
                    foreach (var d in list)
                        d.DynamicInvoke(arg0, arg1, arg2, arg3, arg4, arg5, arg6);
            }
        }

        public sealed class Event<T0, T1, T2, T3, T4, T5, T6, T7>
        {
            private List<Action<T0, T1, T2, T3, T4, T5, T6, T7>>[] Handlers
            {
                get;
            } = new List<Action<T0, T1, T2, T3, T4, T5, T6, T7>>[5] { new List<Action<T0, T1, T2, T3, T4, T5, T6, T7>>(), new List<Action<T0, T1, T2, T3, T4, T5, T6, T7>>(), new List<Action<T0, T1, T2, T3, T4, T5, T6, T7>>(), new List<Action<T0, T1, T2, T3, T4, T5, T6, T7>>(), new List<Action<T0, T1, T2, T3, T4, T5, T6, T7>>() };

            public void Add(Action<T0, T1, T2, T3, T4, T5, T6, T7> val, Event.Priority priority = Event.Priority.Normal)
            {
                var parameters = val.Method.GetParameters();
                if (parameters.Length != 8)
                    throw new Event.ActionParameterCountMismatchException(parameters.Length, 8);
                if (!(parameters[0].ParameterType is T0) || (parameters[0].ParameterType.IsValueType && !parameters[0].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[0].ParameterType, typeof(T0));
                if (!(parameters[1].ParameterType is T1) || (parameters[1].ParameterType.IsValueType && !parameters[1].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[1].ParameterType, typeof(T1));
                if (!(parameters[2].ParameterType is T2) || (parameters[2].ParameterType.IsValueType && !parameters[2].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[2].ParameterType, typeof(T2));
                if (!(parameters[3].ParameterType is T3) || (parameters[3].ParameterType.IsValueType && !parameters[3].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[3].ParameterType, typeof(T3));
                if (!(parameters[4].ParameterType is T4) || (parameters[4].ParameterType.IsValueType && !parameters[0].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[4].ParameterType, typeof(T4));
                if (!(parameters[5].ParameterType is T5) || (parameters[5].ParameterType.IsValueType && !parameters[5].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[5].ParameterType, typeof(T5));
                if (!(parameters[6].ParameterType is T6) || (parameters[6].ParameterType.IsValueType && !parameters[6].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[6].ParameterType, typeof(T6));
                if (!(parameters[7].ParameterType is T7) || (parameters[7].ParameterType.IsValueType && !parameters[7].ParameterType.IsByRef))
                    throw new Event.ActionParameterTypeMismatchException(parameters[7].ParameterType, typeof(T7));

                Handlers[(int)priority].Add(val);
            }

            public void Remove(Action<T0, T1, T2, T3, T4, T5, T6, T7> val)
            {
                foreach (var list in Handlers)
                    list.Remove(val);
            }

            public void Call(ref T0 arg0, ref T1 arg1, ref T2 arg2, ref T3 arg3, ref T4 arg4, ref T5 arg5, ref T6 arg6, ref T7 arg7)
            {
                foreach (var list in Handlers)
                    foreach (var d in list)
                        d.DynamicInvoke(arg0, arg1, arg2, arg3, arg4, arg5, arg6, arg7);
            }
        }

        #endregion

        public static class Communication
        {

            #region Client connect/disconnect

            public delegate void ClientAction(MinecraftClient client);

            /// <summary>
            /// Called when client switches to <see cref="CxServer.Packet.PacketState.Play"/> mode.<br>
            /// <br>
            /// Use <see cref="CxServer.Managers.EventsManager.ClientAction"/> Action.
            /// </summary>
            public static Event<MinecraftClient> ClientConnected;
            /// <summary>
            /// Called when active (in <see cref="CxServer.Packet.PacketState.Play"/> mode) disconnects.<br>
            /// <br>
            /// Use <see cref="CxServer.Managers.EventsManager.ClientAction"/> Action.
            /// </summary>
            public static Event<MinecraftClient> ClientDisconnected;

            #endregion

            #region Packet send/received

            public delegate void PacketAction(MinecraftClient client, Packet packet);

            /// <summary>
            /// Do not use this if it is not 100% needed.<br>
            /// Use <see cref="CxServer.Entities.Living.PlayerEntity.PacketSend"/>.<br>
            /// <br>
            /// Use <see cref="CxServer.Managers.EventsManager.PacketAction"/> Action.
            /// </summary>
            public static Event<MinecraftClient, Packet> PacketSend;

            /// <summary>
            /// Do not use this if it is not 100% needed.<br>
            /// Use <see cref="CxServer.Entities.Living.PlayerEntity.PacketReceived"/>.<br>
            /// <br>
            /// Use <see cref="CxServer.Managers.EventsManager.PacketAction"/> Action.
            /// </summary>
            public static Event<MinecraftClient, Packet> PacketReceived;

            #endregion

            #region Channel Message

            public delegate void ChannelMessageAction(MinecraftClient client, string channel, byte[] data);

            /// <summary>
            /// Do not use this if it is not 100% needed.<br>
            /// Use <see cref="CxServer.Entities.Living.PlayerEntity.ChannelMessageSend"/>.<br>
            /// <br>
            /// Called when <see cref="CxServer.Communication.Play.ToClient.PluginMessage"/> packet is send.<br>
            /// Called for every player (active client).<br>
            /// <br>
            /// Use <see cref="CxServer.Managers.EventsManager.ChannelMessageAction"/> Action.
            /// </summary>
            public static Event<MinecraftClient, string, byte[]> ChannelMessageSend;
            /// <summary>
            /// Do not use this if it is not 100% needed.<br>
            /// Use <see cref="CxServer.Entities.Living.PlayerEntity.ChannelMessageReceived"/>.<br>
            /// <br>
            /// Called when received <see cref="CxServer.Communication.Play.ToServer.PluginMessage"/> packet.<br>
            /// <br>
            /// Use <see cref="CxServer.Managers.EventsManager.ChannelMessageAction"/> Action.
            /// </summary>
            public static Event<MinecraftClient, string, byte[]> ChannelMessageReceived;

            #endregion

            #region Chat

            //TODO Chat raw message received (before parsing it to commands/messages)
            //TODO Chat command received (starts with ServerSettings.CommandCharacter)
            //TODO Chat message received (does not start with ServerSettings.CommandCharacter)

            #endregion

            #region Join/Leave Server

            /// <summary>
            /// Called when player joins server.<br>
            /// <br>
            /// TODO Action
            /// </summary>
            public static Event<MinecraftClient> ServerJoined;
            /// <summary>
            /// Called when player leaves server (but it's entity still exists).<br>
            /// <br>
            /// TODO Action
            /// </summary>
            public static Event<MinecraftClient, PlayerEntity> ServerLeft;

            #endregion

            #region World (global) & Interaction

            //TODO Block placed
            //TODO Block placed by entity (player)

            //TODO Block breaked
            //TODO Block breaked by entity (player)

            //TODO Entity spawned (player entities are counted as entity too, can be cancelled)
            //TODO Entity despawned (if they live too long or (for player entities) if they disconnect, can be cancelled)
            //TODO Entity death (can be cancelled)
            //TODO Entity loaded/unloaded (from/to unloaded chunk, cannot be cancelled -> to cancel it you must move them out/to normal (loaded) chunk)

            //TODO Chunk Loaded/Unloaded (spawn area cannot be unloaded)
            //TODO Region Loaded/Unloaded (spawn area cannot be unloaded)
            //TODO World Loaded/Unloaded

            #endregion

            #region Bossbars

            //TODO

            #endregion

            #region Scoreboards

            //TODO

            #endregion

        }
    }
}
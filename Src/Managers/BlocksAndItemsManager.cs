﻿using System;
using System.Linq;
using System.Collections.Generic;
using CxServer.BlocksAndItems.Blocks;
using CxServer.BlocksAndItems.Items;

namespace CxServer.Managers
{
    public static class BlocksAndItemsManager
    {
        static BlocksAndItemsManager()
        {
        }

        public static void Register(Block block, ushort id)
        {
            Item item = block.Item;
            //TODO register block
            throw new NotImplementedException("Register block");
        }

        public static void Register(Item item, ushort id)
        {
            if (item == null)
                throw new ArgumentNullException("item");
            if (_items.ContainsKey(id))
                throw new IDAlreadyRegistredException(id);
            if (_items.Any((arg) => arg.Value.Codename == item.Codename))
                throw new CodenameAlreadyRegistredException(item.Codename);
            //TODO register item
            throw new NotImplementedException("Register item");
        }

        #region Items

        private static readonly Dictionary<ushort, Item> _items = new Dictionary<ushort, Item>();

        public static Item GetItem(string codename)
        {
            return _items.FirstOrDefault((kvp) => kvp.Value.Codename == codename).Value;
        }

        public static Item GetItem(ushort id)
        {
            Item item;
            if (_items.TryGetValue(id, out item))
                return item;
            else
                return null;
        }

        public static string GetItemCodename(ushort id)
        {
            Item item;
            if (_items.TryGetValue(id, out item))
                return item.Codename;
            else
                return null;
        }

        public static ushort GetItemID(string codename)
        {
            return _items.FirstOrDefault((kvp) => kvp.Value.Codename == codename).Key;
        }

        public static int GetItemID(Item item)
        {
            return _items.FirstOrDefault((kvp) => kvp.Value == item).Key;
        }

        #endregion

        #region Blocks

        private static readonly Dictionary<ushort, Block> _blocks = new Dictionary<ushort, Block>();

        /*
		public static void RegisterBlock(Block block, int id)
		{
			if(block == null)
				throw new ArgumentNullException("block");
			if(_blocks.ContainsKey(id))
				throw new IDAlreadyRegistredException(id);
			if(_blocks.ContainsValue(block) || _blocks.Any((kvp) => kvp.Value.Codename == block.Codename))
				throw new BlockAlreadyRegistredException(block);
			if(id <= 0)
				throw new ArgumentOutOfRangeException("id");
			_blocks.Add(id, block);
		}
		*/

        public static Block GetBlock(string codename)
        {
            return _blocks.FirstOrDefault((kvp) => kvp.Value.Codename == codename).Value;
        }

        public static Block GetBlock(ushort id)
        {
            Block block;
            if (_blocks.TryGetValue(id, out block))
                return block;
            else
                return null;
        }

        public static string GetBlockCodename(ushort id)
        {
            Block block;
            if (_blocks.TryGetValue(id, out block))
                return block.Codename;
            else
                return null;
        }

        public static ushort GetBlockID(string codename)
        {
            return _blocks.FirstOrDefault((kvp) => kvp.Value.Codename == codename).Key;
        }

        public static ushort GetBlockID(Block block)
        {
            return _blocks.FirstOrDefault((kvp) => kvp.Value == block).Key;
        }

        #endregion

        #region Exceptions

        public class CodenameAlreadyRegistredException : Exception
        {
            public string Codename
            {
                get;
            }

            public CodenameAlreadyRegistredException(string codename) : base(string.Format("Codename {0} is already registred", codename))
            {
                this.Codename = codename;
            }

            public CodenameAlreadyRegistredException(Block block) : this(block.Codename)
            {
            }

            public CodenameAlreadyRegistredException(Item item) : this(item.Codename)
            {
            }
        }

        public class IDAlreadyRegistredException : Exception
        {
            public ushort ID
            {
                get;
            }

            public IDAlreadyRegistredException(ushort id) : base(string.Format("ID {0} is already registred", id))
            {
                this.ID = id;
            }
        }

        #endregion
    }
}
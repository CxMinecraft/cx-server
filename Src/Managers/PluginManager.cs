﻿using System;
using System.Collections.Generic;
using CxServer.Plugins;
using System.Linq;
using System.Text.RegularExpressions;
using System.IO;
using System.Reflection;
using CxServer.Settings;
using CxServer.Plugins.Exceptions;

namespace CxServer.Managers
{
    public static class PluginManager
    {

        internal static readonly List<Plugin> _plugins = new List<Plugin>();

        public static bool IsPluginLoadingPhase
        {
            get;
            internal set;
        } = true;

        /// <summary>
        /// Check if is plugin loaded.
        /// </summary>
        /// <returns><c>true</c> if is plugin loaded, <c>false</c> otherwise.</returns>
        /// <param name="codename">Codename.</param>
        public static bool IsPluginLoaded(string codename)
        {
            return _plugins.Any(pl => string.Equals(pl.Codename, codename, StringComparison.InvariantCultureIgnoreCase));
        }

        public static Plugin GetPlugin(string codename) => _plugins.FirstOrDefault(pl => string.Equals(pl.Codename, codename, StringComparison.InvariantCultureIgnoreCase));

        public static string GetPluginVersion(string codename)
        {
            Plugin pl = GetPlugin(codename);
            return pl == null ? null : pl.Version.ToString();
        }


        /// <summary>
        /// Load plugin by name.
        /// </summary>
        /// <returns><c>true</c>, if plugin was loaded, <c>false</c> otherwise.</returns>
        /// <param name="name">Name of the plugin.</param>
        public static bool LoadPlugin(string name)
        {
            if (!Regex.IsMatch(name, "^[a-zA-Z0-9_]+$"))
                return false;

            string path = Path.Combine(ServerSettings.PluginsFolder, name + ".dll");
            if (File.Exists(path))
            {
                LoadPluginFromFile(path);
                return true;
            }

            path = Path.Combine(ServerSettings.PluginsFolder, name + ".so");
            if (File.Exists(path))
            {
                LoadPluginFromFile(path);
                return true;
            }

            return false;
        }

        internal static void LoadPluginFromFile(string path)
        {
            if (!path.EndsWith(".dll") && !path.EndsWith(".so"))
                throw new InvalidPluginFieException(path);

            if (!File.Exists(path))
                throw new FileNotFoundException("Plugin file does not exist.", path);

            var dll = Assembly.LoadFile(path);
            var pluginType = typeof(Plugin);

            List<Plugin> plugins = new List<Plugin>();

            foreach (var type in dll.GetExportedTypes())
            {
                if (type.IsNotPublic)
                    continue;
                if (!type.IsSubclassOf(pluginType))
                    continue;

                Plugin pl = null;
                try
                {
                    pl = (Plugin)Activator.CreateInstance(type, true);//constructor can be non-public
                }
                catch (Exception e)
                {
                    throw new PluginInitializationException(type, e);
                }

                lock (_plugins)
                {

                    //check if is loaded
                    if (_plugins.Any(p => string.Equals(p.Codename, pl.Codename, StringComparison.InvariantCultureIgnoreCase)))
                    {
                        throw new PluginAlreadyLoadedException(pl.Codename, pl.Version);
                    }
                }
                plugins.Add(pl);
                Console.WriteLine("Loaded plugin '{0}' version '{1}'.", pl.Codename, pl.Version);
            }

            Console.WriteLine("Loaded {0} plugins from '{1}'.", plugins.Count, path);

            if (!IsPluginLoadingPhase && plugins.Any())
            {
                Console.WriteLine("Plugins loaded out of standard loading phase. Faking the phase.");

                PluginManager.CheckDependency(plugins);

                DoLoadingPhase(plugins);
            }

            lock (_plugins)
            {
                _plugins.AddRange(plugins);
            }
        }

        internal static void CheckDependency(List<Plugin> plugins = null)
        {
            if (plugins == null)
            {
                lock (_plugins)
                {
                    plugins = _plugins;
                }
            }

            List<Plugin> totalPlugins = new List<Plugin>(plugins);
            lock (_plugins)
            {
                if (_plugins.Any())
                    totalPlugins.AddRange(_plugins);
            }

            List<Plugin> toRemove = new List<Plugin>();

            foreach (var pl in plugins)
                if (!pl.CheckDependency(totalPlugins))
                    toRemove.Add(pl);

            foreach (var plrm in toRemove)
                plugins.Remove(plrm);
        }

        internal static void DoLoadingPhase(List<Plugin> plugins = null)
        {
            if (plugins == null)
            {
                lock (_plugins)
                {
                    plugins = _plugins;
                }
            }

            Console.WriteLine("Activation... ");
            foreach (var pl in plugins)
                pl.Activation();
            Console.WriteLine("DONE");

            Console.WriteLine("Pre-init... ");
            foreach (var pl in plugins)
                pl.PreInit();
            Console.WriteLine("DONE");

            Console.WriteLine("Init... ");
            foreach (var pl in plugins)
                pl.Init();
            Console.WriteLine("DONE");

            Console.WriteLine("Post-init... ");
            foreach (var pl in plugins)
                pl.PostInit();
            Console.WriteLine("DONE");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using CxServer.Commands;
using CxServer.Utils;
using System.Linq;

namespace CxServer.Managers
{
    public static class CommandManager
    {
        private static List<Command> _commands = new List<Command>();

        /// <summary>
        /// Registers new command by name.
        /// </summary>
        /// <param name="name">Name of command.</param>
        public static Command RegisterCommand(string name)
        {
            name = name.ToLower();

            foreach (Command cmd in _commands)
                if (string.Equals(cmd.Name, name, StringComparison.InvariantCultureIgnoreCase))
                    throw new Command.CommandAlreadyRegistredException(name);

            Command command = new Command(name);
            _commands.Add(command);
            return command;
        }

        /// <summary>
        /// Gets the command (searching in command names and aliases).
        /// </summary>
        /// <param name="name">Name of command</param>
        public static Command GetCommand(string name)
        {
            name = name.ToLower();

            //First search names
            foreach (Command cmd in _commands)
                if (string.Equals(cmd.Name, name, StringComparison.InvariantCultureIgnoreCase))
                    return cmd;

            //Next aliases
            foreach (Command cmd in _commands)
                foreach (string a in cmd._aliases)
                    if (string.Equals(a, name, StringComparison.InvariantCultureIgnoreCase))
                        return cmd;

            //No command
            return null;
        }

        /// <summary>
        /// Removes the command.
        /// </summary>
        public static void RemoveCommand(Command command) => _commands.Remove(command);

        /// <summary>
        /// Gets the command exact (only searching in command names).
        /// </summary>
        /// <param name="name">Name of command</param>
        public static Command GetCommandExact(string name) => _commands.FirstOrDefault((comm) => string.Equals(comm.Name, name, StringComparison.InvariantCultureIgnoreCase));
    }
}
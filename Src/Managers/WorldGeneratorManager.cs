﻿using System;
using CxServer.Worlds.Generators;
using System.Collections.Generic;

namespace CxServer.Managers
{
    public static class WorldGeneratorManager
    {
        public static WorldGenerator GetGenerator(string name, string seed = null, string settings = null)
        {
            WorldGenerator generator;
            if (_generators.TryGetValue(name, out generator))
                return generator.Clone(seed, settings);
            else
                return null;
        }

        private static readonly Dictionary<string, WorldGenerator> _generators = new Dictionary<string, WorldGenerator>();

        public static void RegisterGenerator(string name, WorldGenerator generator)
        {
            _generators[name] = generator;
        }

        static WorldGeneratorManager()
        {
            RegisterGenerator("flat", new FlatWorldGenerator(null));
            //TODO register default generators
        }
    }
}
﻿using System;

namespace CxServer.Potions
{
    //TODO rework
    public enum PotionEffectType
    {
        /// <summary>
        /// Expands [FOV] Field of view
        /// Increases walking speed by 20%
        /// 
        /// Speed increases by 20% × level
        /// 
        /// At exceedingly high levels (~level +100), the player walks faster than chunks can load. If a “corrupt”[note 1] potion is used, speed is decreased.
        /// 
        /// Sprinting
        /// Riding a horse, pig, or minecart
        /// Walking on ice or packed ice
        /// </summary>
        Speed = 1,
        /// <summary>
        /// Contracts FOV
        /// Decreases walking speed by 15%
        ///
        /// Speed decreases by 15% × level
        /// 
        /// At levels 7 - 127, the player will be unable to move unless they jump while moving or use an ender pearl. At levels 128 or higher, the player will move extremely fast. If a "corrupt"[note 1] potion is used, speed is increased.
        /// 
        /// Walking on soul sand
        /// Swimming
        /// Witch attacks
        /// Riding a Boat on land
        /// Sneaking
        /// Eating
        /// Drinking milk or a potion
        /// Aiming a Bow
        /// </summary>
        Slowness = 2,
        /// <summary>
        /// Blocks break faster by 20% (arm swings faster).
        /// In 1.9,[upcoming] attack speed increases by 10%.
        /// 
        /// Digging speed increases by 20% × level.
        /// In 1.9,[upcoming] attack speed increases by 10% × level.
        /// 
        /// At levels 3 or higher, when the player mines something, the player's arm will not move. If a “corrupt”[note 1] potion is used, blocks are mined slower and the player's arm moves more slowly.
        /// 
        /// Efficiency on a Pickaxe, shovel or axe
        /// </summary>
        Haste = 3,
        /// <summary>
        /// Blocks break slower: speed is 30% of normal, time needed is multiplied by 3 1⁄3.
        /// In 1.9,[upcoming] attack speed decreases by 10%.
        /// 
        /// Block breaking speed is 9%/0.27%/0.081% of normal at level II/III/IV (time needed is multiplied by 11 1⁄9 / 370 10⁄27 / 1234 46⁄81). No extra effect at levels 5-127.
        /// In 1.9,[upcoming] attack speed decreases by 10% × level.
        /// 
        /// The effect does not affect block breaking in Creative mode, or blocks that are broken instantly. It has no effect on attack speed (pre-1.9) even though the hands move slower. Levels 128 to 254 won't move the hand at all.
        /// 
        /// Sneaking
        /// Being underwater
        /// Riding a horse, pig, or minecart
        /// </summary>
        MiningFatigue = 4,
        /// <summary>
        /// Increases damage dealt with melee attacks by 130%.
        /// In 1.9,[upcoming] increases damage dealt with melee attacks by 3 (Heart.svgHalf Heart.svg).
        ///
        /// Increases damage by (130 × level)% (Strength II is +260%, Strength III is +390%, etc.)
        /// In 1.9,[upcoming] adds (3 × level) damage. (Strength II is +6, Strength III is +9, etc.)
        /// 
        /// While a zombie villager is being cured, it will have the Strength I effect in Normal difficulty (or Strength II in Hard difficulty)
        /// 
        /// Any passive mob (except villagers and horses) can be killed when Strength is on level 5. If a “corrupt”[note 1] potion is used, melee attack damage dealt is decreased.
        ///
        /// If level is too high, player deals no damage.
        /// 
        /// Critical hits on mobs
        /// </summary>
        Strength = 5,
        /// <summary>
        /// Heals 4 (Heart.svgHeart.svg) to players. Undead mobs are dealt 6 (Heart.svgHeart.svgHeart.svg) damage.
        /// 
        /// Healing is 2 (Heart.svg) × 2level
        /// 
        /// Undead mobs (including wither) are harmed instead. If the effect is forced to last longer than 1 tick, it will heal entities every tick.
        /// </summary>
        InstantHealth = 6,
        /// <summary>
        /// Damages 6 (Heart.svgHeart.svgHeart.svg) to players. Undead mobs are healed by 6 (Heart.svgHeart.svgHeart.svg).
        /// 
        /// Damage is 3 (Heart.svgHalf Heart.svg) × 2level
        /// 
        /// Undead mobs (including Wither) are healed instead. Death is reported as “<player> was killed by magic”. If the effect is forced to last longer than 1 tick, it will damage entities every tick.
        /// </summary>
        InstantDamage = 7,
        /// <summary>
        /// Allows the player to jump higher by approximately ½ block.
        /// Reduces fall damage by 1 (Half Heart.svg).
        ///
        /// Increases jump height by about [level]/8+.46 per level. (Jump height is ([level]+4.2)^2/16, ignoring drag). Reduces fall damage by 1 (Half Heart.svg) each level.
        /// 
        /// If the level is 33, jumping will cause death upon landing. If the level is 128 - 250, the player will be unable to jump at all.
        /// If the level is 251-253, you will barely jump.
        /// If the level is 255, fall damage is disabled, though in 1.8.3 and up, the player will be shot up in the air when taking damage; fire is the best example. If a “corrupt”[note 1] potion is used, jump height is decreased; at most levels, the player is unable to jump.
        /// </summary>
        JumpBoost = 8,
        /// <summary>
        /// Wobbles and warps the screen
        /// 
        /// When the Nausea effect is canceled, the Nether portal animation would flash onto the screen.
        /// 
        /// Effect ends 2 seconds before it wears off.
        /// 
        /// Standing in a Nether portal.
        /// </summary>
        Nausea = 9,
        /// <summary>
        /// Regenerate 1 (Half Heart.svg) over time every 50 ticks/2.5 seconds
        /// 
        /// Regenerate 1 (Half Heart.svg) every 25/12/6/3/1 ticks (Heart.svg × 0.4/0.8/1.66/3.33/10 per second) at level 2/3/4/5/6. No extra effect at levels 7 and up.
        /// 
        /// When a villager opens a new tier of trade, it gets Regeneration I for 10 seconds
        /// 
        /// Does not affect undead mobs. Level 6 and up increases the effect drastically. Levels 96 and up may give less health than level 6.
        /// </summary>
        Regeneration = 10,
        /// <summary>
        /// Reduces all incoming damage by 20% except void damage
        /// 
        /// Level 5+ gives the player full immunity to all damage, with the exception of the void and the /kill command. If a "corrupt"[note 1] potion is used, damage taken is increased.
        /// 
        /// Reduces damage by 20% with each additional level
        /// </summary>
        Resistance = 11,
        /// <summary>
        /// Immunity to fire, lava and direct impact of blaze fireballs
        /// 
        /// Most Nether mobs (excludes chickens and skeletons) naturally are resistant to fire and lava without potion effects. However, nether mobs can still be damaged by the direct impact from a blaze fireball, while Fire Resistance makes the player and mobs immune to that.
        /// </summary>
        FireResistance = 12,
        /// <summary>
        /// Prevents the oxygen bar from decreasing
        /// Slightly increases visibility while underwater
        /// </summary>
        WaterBreathing = 13,
        /// <summary>
        /// Causes the entity model to disappear
        /// Mobs will not attack/sense the player until the player is much closer than normal.
        /// 
        /// Armor, held items, certain features of entities (sheep wool, spider eyes, etc.) and any particles emitted by mobs do not disappear with the entity.
        /// 
        /// Sneaking reduces the detection range to 80% of usual, and stacks with Invisibility.
        /// In 1.9,[upcoming] wearing the corresponding mob head reduces the detection range of skeletons, zombies, and creepers to half of usual, and stacks with Invisibility.
        /// </summary>
        Invisibility = 14,
        /// <summary>
        /// Creates thick black fog around the player
        /// Prevents sprinting and critical hits
        /// 
        /// The Sun and Moon can be seen as normal. Also, liquids do not fade into the fog perfectly. If combined with Night Vision, the screen will be covered by fog.
        /// </summary>
        Blindness = 15,
        /// <summary>
        /// Increases brightness to 15 (full) everywhere
        /// Increases ability to see underwater
        /// 
        /// Screen flashes for 10 seconds before the effect wears off. If used in the end everything will appear pink. If combined with Blindness, the screen will be covered by fog. If used in the Void, you will see nothing other than black.
        /// </summary>
        NightVision = 16,
        /// <summary>
        /// Causes food meter to deplete by 0.025 food exhaustion per tick (Hunger.svg per 16 seconds if target has no saturation)
        /// Hunger bar turns yellow-green (Poisoned Hunger.svg)
        ///
        /// Food exhaustion is 0.025 × level per tick
        /// 
        /// Rotten flesh (80% chance)
        /// Raw chicken (30% chance)
        /// Pufferfish
        /// 
        /// If used with the /effect command on level 127 on any difficulty besides peaceful, It will completely drain your hunger bar in less than 5 seconds. If a “corrupt”[note 1] potion is used, the Food Bar does not go down.
        /// </summary>
        Hunger = 17,
        /// <summary>
        /// Decreases damage dealt with melee attacks by 0.5 (Heart.svg × ¼)
        /// In 1.9,[upcoming] decreases damage dealt with melee attacks by 4 (Heart.svgHeart.svg)
        /// Zombie villagers can be cured of zombification using a golden apple if they have the weakness potion effect on them.
        /// 
        /// Decreases damage by 0.5 × level. In 1.9,[upcoming] decreases damage dealt with melee attacks by 4 × level.
        /// 
        /// If a "corrupt"[note 1] potion is used, melee attack damage dealt is increased.
        /// </summary>
        Weakness = 18,
        /// <summary>
        /// Does 1 (Half Heart.svg) damage every 25 ticks/1.25 seconds, but won't reduce health below 1 (Half Heart.svg).
        /// Hearts turn yellow-green (Poisoned Heart.svg)
        ///
        /// Does 1 (Half Heart.svg) damage every 12/6/3/1 ticks (Heart.svg × 0.8/1.66/3.33/10 per second) at level 2/3/4/5. No extra effect at levels 6 and up.
        /// 
        /// Attacked by a cave spider (excluding in Easy)
        /// Spider eye
        /// Poisonous potato (60% chance)
        /// Pufferfish
        /// 
        /// Does not affect undead mobs or spiders. Damage is suppressed while the player or mob is already at Half Heart.svg health, but can resume if healing occurs before the effect expires.
        /// </summary>
        Poison = 19,
        /// <summary>
        /// Does 1 (Half Heart.svg) damage every 40 ticks/2 seconds (unlike poison, the effect can kill players and other entities)
        /// Hearts turn black (Withered Heart.svg)
        /// 
        /// Does 1 (Half Heart.svg) damage every 12/6/3/1 ticks (Heart.svg × 0.8/1.66/3.33/10 per second) at level 2/3/4/5. No extra effect at levels 6 and up.
        /// 
        /// Attacked by a Wither (excluding in Easy)
        /// Attacked by a Wither skeleton
        /// 
        /// Death is reported as “<player> withered away”.
        /// </summary>
        Wither = 20,
        /// <summary>
        /// Adds 4 (Heart.svgHeart.svg) base health; extra health vanish when the effect ends
        /// 
        /// Adds 4 (Heart.svgHeart.svg) × level base health
        /// 
        /// Actual health may be above the base value after the effect ends and will not be capped, unless the entity is healed. Extra hearts of the mounted mob from the effect can be seen while riding a mounted mob. This effect can also be given with a strength of 0 to give 4 base health (Empty Heart.svgEmpty Heart.svg). If a "corrupt"[note 1] potion is used, hearts are removed.
        /// </summary>
        HealthBoost = 21,
        /// <summary>
        /// Adds 4 (Absorption Heart.svgAbsorption Heart.svg) absorption health that cannot be replenished by natural regeneration or other effects. Absorption health vanish when the effect ends
        /// 
        /// Adds 4 (Absorption Heart.svgAbsorption Heart.svg) × level absorption health
        /// 
        /// Extra hearts of the mounted mob from the effect can be seen while riding a mounted mob.
        ///
        /// If a "corrupt"[note 1] potion is used, hearts are given when the potion's effect ends.
        /// </summary>
        Absorption = 22,
        /// <summary>
        /// Extra hearts of the mounted mob from the effect can be seen while riding a mounted mob.
        /// 
        /// Replenishes 1 (Half Hunger.svg) × level per tick.
        /// 
        /// At level 20 it fills the hunger bar in a tick. If a "corrupt"[note 1] potion is used, hunger bar is decreased; at most levels, player will take starvation damage.
        /// </summary>
        Saturation = 23,
        /// <summary>
        /// Causes entities to glow with an outline that can be seen through opaque blocks.
        /// </summary>
        Glowing = 24,
        /// <summary>
        /// Causes the player to involuntarily rise upwards until the effect runs out.
        /// 
        /// The player floats up at a steady rate of approximately (level + 1) blocks per second.
        /// 
        /// Being hit with a Shulker's projectile.
        /// 
        /// Does not affect swimming or flying.
        /// 
        /// If a "corrupt"[note 1] potion is used, the player floats downwards instead. At level 255, the player cannot jump, fall down or climb stairs; otherwise, the rate of descent is about (255 − level) blocks per second (jumping a full block becomes impossible at level 250 without a Jump Boost effect). Fall damage in itself is not affected; however, taking fall damage causes the entity to bounce up in the air, taking fall damage each time.
        /// 
        /// Flying upwards
        /// Jumping on a slime block
        /// </summary>
        Levitation = 25,
        /// <summary>
        /// Increases chance of getting high-quality loot
        /// 
        /// Adds 1 × level to attribute generic.luck
        /// 
        /// Makes loot table entries with a high quality score more likely, and entries with negative quality less likely. Currently, only fishing uses quality.
        ///
        /// The new weight of each entry is floor( base_weight + quality * generic.luck).
        /// 
        /// Using a fishing rod enchanted with Luck of the Sea
        /// </summary>
        Luck = 26,
        /// <summary>
        /// Reduces chance of getting high-quality loot
        /// 
        /// Subtracts 1 × level from attribute generic.luck
        /// 
        /// Makes loot table entries with a high quality score less likely, and entries with negative quality more likely. Currently, only fishing uses quality.
        ///
        /// The new weight of each entry is floor( base_weight + quality * generic.luck).
        /// </summary>
        BadLuck = 27
    }
}
﻿using System;
using CxServer.Settings;
using CxServer.Utils;

namespace CxServer.Potions
{
    public class PotionEffect
    {

        #region Constructors

        public PotionEffect(PotionEffectType type, long durationTicks = -1, byte level = 0)
        {
            if (!Enum.IsDefined(typeof(PotionEffectType), type))
                throw new ArgumentOutOfRangeException("type");

            this.Type = type;
            this._durationInTicks = durationTicks;
            this.Level = level;
        }

        public PotionEffect(PotionEffectType type, TimeSpan duration, byte level = 0) : this(type, duration.GetServerTicks(), level)
        {
        }

        #endregion

        #region Variables

        public byte Level
        {
            get;
            set;
        }

        private PotionEffectType _type;

        public PotionEffectType Type
        {
            get
            {
                return _type;
            }
            set
            {
                if (Enum.IsDefined(typeof(PotionEffectType), value))
                    _type = value;
            }
        }

        private long _durationInTicks;

        public long DurationInTicks
        {
            get
            {
                return _durationInTicks == -1 ? long.MaxValue : _durationInTicks;
            }
            set
            {
                _durationInTicks = value <= 0 ? -1 : value;
            }
        }

        public TimeSpan Duration
        {
            get
            {
                return TimeSpanUtil.FromServerTicks(DurationInTicks);
            }
            set
            {
                _durationInTicks = value.GetServerTicks();
            }
        }

        #endregion

    }
}
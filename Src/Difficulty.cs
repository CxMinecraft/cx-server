﻿using System;

namespace CxServer
{
    /// <summary>
    /// How hard is the world.
    /// </summary>
    public enum Difficulty
    {
        /// <summary>
        /// Do dangerous entities.
        /// </summary>
        Peaceful = 0,
        /// <summary>
        /// Easy game for begginers.
        /// </summary>
        Easy = 1,
        /// <summary>
        /// Normal difficulty for most of players.
        /// </summary>
        Normal = 2,
        /// <summary>
        /// Hard game if you want challange.
        /// </summary>
        Hard = 3
    }
}
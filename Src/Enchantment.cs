﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace CxServer
{
    public sealed class Enchantment
    {
        public static readonly Enchantment Protection = new Enchantment(0, "protection");
        public static readonly Enchantment FireProtection = new Enchantment(1, "fire_protection");
        public static readonly Enchantment FeatherFalling = new Enchantment(2, " feather_falling");
        public static readonly Enchantment BlastProtection = new Enchantment(3, " blast_protection");
        public static readonly Enchantment ProjectileProtection = new Enchantment(4, "projectile_protection");
        public static readonly Enchantment Respiration = new Enchantment(5, "respiration");
        public static readonly Enchantment AquaAffinity = new Enchantment(6, "aqua_affinity");
        public static readonly Enchantment Thorns = new Enchantment(7, "thorns");
        public static readonly Enchantment DepthStrider = new Enchantment(8, "depth_strider");
        public static readonly Enchantment FrostWalker = new Enchantment(9, "frost_walker");
        public static readonly Enchantment Sharpness = new Enchantment(16, "sharpness");
        public static readonly Enchantment Smite = new Enchantment(17, "smite");
        public static readonly Enchantment BaneOfArthropods = new Enchantment(18, "bane_of_arthropods");
        public static readonly Enchantment Knockback = new Enchantment(19, "knockback");
        public static readonly Enchantment FireAspect = new Enchantment(20, "fire_aspect");
        public static readonly Enchantment Looting = new Enchantment(21, "looting");
        public static readonly Enchantment Efficiency = new Enchantment(32, "efficiency");
        public static readonly Enchantment SilkTouch = new Enchantment(33, "silk_touch");
        public static readonly Enchantment Unbreaking = new Enchantment(34, "unbreaking");
        public static readonly Enchantment Fortune = new Enchantment(35, "fortune");
        public static readonly Enchantment Power = new Enchantment(48, "power");
        public static readonly Enchantment Punch = new Enchantment(49, "punch");
        public static readonly Enchantment Flame = new Enchantment(50, "flame");
        public static readonly Enchantment Infinity = new Enchantment(51, "infinity");
        public static readonly Enchantment LuckOfSea = new Enchantment(61, "luck_of_the_sea");
        public static readonly Enchantment Lure = new Enchantment(62, "lure");
        public static readonly Enchantment Mending = new Enchantment(70, "mending");

        public static readonly List<Enchantment> enchantments = new List<Enchantment>();

        private Enchantment(int id, string name)
        {
            this.ID = id;
            this.Name = name;

            enchantments.Add(this);
        }

        public int ID
        {
            get;
            private set;
        }

        public string Name
        {
            get;
            private set;
        }

        public static implicit operator int(Enchantment enchantment)
        {
            return enchantment.ID;
        }

        public static implicit operator string(Enchantment enchantment)
        {
            return enchantment.Name;
        }

        public static implicit operator Enchantment(int id)
        {
            return ByID(id);
        }

        public static implicit operator Enchantment(string name)
        {
            return ByName(name);
        }

        public static Enchantment ByID(int id)
        {
            return enchantments.Where((Enchantment arg) => arg.ID == id).FirstOrDefault();
        }

        public static Enchantment ByName(string name)
        {
            return enchantments.Where((Enchantment arg) => arg.Name == name).FirstOrDefault();
        }
    }
}
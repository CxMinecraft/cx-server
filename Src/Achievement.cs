﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace CxServer
{
    public class Achievement
    {

        #region Constructors

        public Achievement(string name, Achievement prerequisites = null, bool super = false)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException("name");

            this.Name = name.Trim();
            this.Prerequisites = prerequisites;
            this.Super = super;

            _allAchievements.Add(name, this);
        }

        #endregion

        #region Variables

        public string Name
        {
            get;
        }

        public Achievement Prerequisites
        {
            get;
        }

        public bool Super
        {
            get;
        }

        #endregion

        #region Override + operators

        public override string ToString()
        {
            return this.Name;
        }

        public override bool Equals(object obj)
        {
            if (obj is Achievement)
                return Achievement.Equals(((Achievement)obj), this);
            else
                return false;
        }

        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }

        public static bool Equals(Achievement a1, Achievement a2)
        {
            return string.Equals(a1.Name, a2.Name) && a1.Prerequisites == a2.Prerequisites && a1.Super == a2.Super;
        }

        public static bool operator ==(Achievement a1, Achievement a2)
        {
            return Achievement.Equals(a1, a2);
        }

        public static bool operator !=(Achievement a1, Achievement a2)
        {
            return !Achievement.Equals(a1, a2);
        }

        public static explicit operator string(Achievement a)
        {
            return a.Name;
        }

        #endregion

        #region Static Achievements

        public static Achievement TakingInventory = new Achievement("openInventory");
        public static Achievement GettingWood = new Achievement("mineWood", TakingInventory);
        public static Achievement Benchmarking = new Achievement("buildWorkBench", GettingWood);
        public static Achievement TimeToFarm = new Achievement("buildHoe", Benchmarking);
        public static Achievement BakeBread = new Achievement("makeBread", TimeToFarm);
        public static Achievement TheLie = new Achievement("bakeCake", TimeToFarm);
        public static Achievement TimeToStrike = new Achievement("buildSword", Benchmarking);
        public static Achievement MonsterHunter = new Achievement("killEnemy", TimeToStrike);
        public static Achievement SniperDuel = new Achievement("snipeSkeleton", MonsterHunter, true);
        public static Achievement CowTipper = new Achievement("killCow", TimeToStrike);
        public static Achievement WhenPigsFly = new Achievement("flyPig", CowTipper, true);
        public static Achievement TimeToMine = new Achievement("buildPickaxe", Benchmarking);
        public static Achievement GettingAnUpgrade = new Achievement("buildBetterPickaxe", TimeToMine);
        public static Achievement HotTopic = new Achievement("buildFurnace", TimeToMine);
        public static Achievement DeliciousFish = new Achievement("cookFish", HotTopic);
        public static Achievement AcquiteHardware = new Achievement("acquireIron", HotTopic);
        public static Achievement OnARail = new Achievement("onARail", AcquiteHardware, true);
        public static Achievement Diamonds = new Achievement("diamonds", AcquiteHardware);
        public static Achievement Enchanter = new Achievement("enchantments", Diamonds);
        public static Achievement Overkill = new Achievement("overkill", Enchanter, true);
        public static Achievement Librarian = new Achievement("bookcase", Enchanter);
        public static Achievement WeNeedToGoDeeper = new Achievement("portal", AcquiteHardware);
        public static Achievement ReturnToSender = new Achievement("ghast", WeNeedToGoDeeper, true);
        public static Achievement IntoFire = new Achievement("blazerod", WeNeedToGoDeeper);
        public static Achievement LocalBrewery = new Achievement("potion", IntoFire);
        public static Achievement TheEnd_Enter = new Achievement("theEnd", IntoFire, true);
        public static Achievement TheEnd_Leave = new Achievement("theEnd2", TheEnd_Enter, true);
        public static Achievement TheBeginning_Spawn = new Achievement("spawnWither", TheEnd_Leave);
        public static Achievement TheBeginning_Kill = new Achievement("killWither", TheBeginning_Spawn);
        public static Achievement AdventuringTime = new Achievement("exploreAllBiomes", TheEnd_Enter, true);
        public static Achievement Repopulation = new Achievement("breedCow", CowTipper);
        public static Achievement DiamondsToYou = new Achievement("diamondsToYou", Diamonds);
        public static Achievement Beaconator = new Achievement("fullBeacon", TheBeginning_Kill, true);
        public static Achievement Overpowered = new Achievement("overpowered", GettingAnUpgrade, true);

        #endregion

        #region Static

        private static readonly Dictionary<string, Achievement> _allAchievements = new Dictionary<string, Achievement>();

        public static Achievement GetAchievement(string name)
        {
            Achievement achievement;
            if (_allAchievements.TryGetValue(name, out achievement))
                return achievement;
            else
                return null;
        }

        public static List<Achievement> AllAchievements
        {
            get
            {
                return new List<Achievement>(_allAchievements.Values);
            }
        }

        public static List<Achievement> AllSuperAchievements
        {
            get
            {
                return new List<Achievement>(_allAchievements.Values.Where(a => a.Super));
            }
        }

        #endregion
    }
}
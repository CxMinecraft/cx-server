﻿using System;
using CxServer.Utils;
using Utils.Configurations;
using Utils;

namespace CxServer.Mojang
{
    public static class MojangAPI
    {
        public static Profile GetProfile(UUID uuid)
        {
            string url = "https://sessionserver.mojang.com/session/minecraft/profile/" + uuid.ToString(false);
            string response = HttpRequest.RequestGET(url);
            var profile = new Profile(response);
            if (profile.Name == null || profile.UUID == UUID.Empty)
                return null;
            else
                return profile;
        }

        public static UUID GetUUID(string name)
        {
            string url = "https://api.mojang.com/users/profiles/minecraft/" + name;
            string response = HttpRequest.RequestGET(url);
            Configuration cfg = JSON.Load(response);
            if (cfg == null)
                return UUID.Empty;
            if (!cfg.Contains("id") || !cfg.Contains("name"))
                return UUID.Empty;
            if (!string.Equals(cfg.GetString("name"), name, StringComparison.InvariantCultureIgnoreCase))
                return UUID.Empty;
            return new UUID(cfg.GetString("name"));
        }

        public static UUID GetUUID(string name, DateTime time) => GetUUID(name, (long)(time.Subtract(new DateTime(1970, 1, 1))).TotalSeconds);

        public static UUID GetUUID(string name, long time)
        {
            string url = string.Format("https://api.mojang.com/users/profiles/minecraft/{0}?at={1}", name, time);
            string response = HttpRequest.RequestGET(url);
            Configuration cfg = JSON.Load(response);
            if (cfg == null)
                return UUID.Empty;
            if (!cfg.Contains("id") || !cfg.Contains("name"))
                return UUID.Empty;
            if (!string.Equals(cfg.GetString("name"), name, StringComparison.InvariantCultureIgnoreCase))
                return UUID.Empty;
            return new UUID(cfg.GetString("name"));
        }
    }
}
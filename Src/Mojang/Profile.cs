﻿using System;
using CxServer.Utils;
using Utils.Configurations;
using System.Text;
using Utils;

namespace CxServer.Mojang
{
    public class Profile
    {

        #region Constructor

        public Profile(string json) : this(JSON.Load(json))
        {
        }

        public Profile(Configuration json)
        {
            if (json == null)
                return;

            UUID = json.Contains("id") ? new UUID(json.GetString("id")) : UUID.Empty;
            Name = json.Contains("name") ? json.GetString("name") : null;

            foreach (var properties in json.GetConfigurationList("properties", false))
            {
                if (!properties.Contains("name"))
                    continue;
                if (properties.GetString("name") != "textures")
                    continue;
                if (!properties.Contains("value"))
                    continue;

                string value = Encoding.UTF8.GetString(Convert.FromBase64String(properties.GetString("value")));

                Configuration cfg = JSON.Load(value);
                if (!cfg.Contains("textures"))
                    continue;

                Configuration textures = cfg.GetConfiguration("textures");

                if (textures.Contains("SKIN") && textures.Contains("SKIN.url"))
                {
                    SkinUrl = textures.GetString("SKIN.url");

                    SkinSlim = textures.Contains("SKIN.metadata.model") && textures.GetString("SKIN.metadata.model") == "slim";
                }

                if (textures.Contains("CAPE") && textures.Contains("CAPE.url"))
                {
                    CapeUrl = textures.GetString("CAPE.url");
                }
            }
        }

        #endregion

        #region Variables

        public UUID UUID
        {
            get;
        } = UUID.Empty;

        public string Name
        {
            get;
        } = null;

        private string _skinUrl = null;
        //Null if no found
        public string SkinUrl
        {
            get
            {
                return _skinUrl;
            }
            set
            {
                if (value == null)
                    _skinUrl = null;
                else
                {
                    int index = value.IndexOf('/');
                    if (index == -1)
                        _skinUrl = value;
                    else if (index == value.Length - 1)
                        SkinUrl = value.Substring(0, value.Length - 1);
                    else
                        _skinUrl = value.Substring(index + 1);
                }
            }
        }

        public bool SkinSlim
        {
            get;
            set;
        } = false;

        private string _capeUrl = null;
        //Null if no found
        public string CapeUrl
        {
            get
            {
                return _capeUrl;
            }
            set
            {
                if (value == null)
                    _capeUrl = null;
                else
                {
                    int index = value.IndexOf('/');
                    if (index == -1)
                        _capeUrl = value;
                    else if (index == value.Length - 1)
                        CapeUrl = value.Substring(0, value.Length - 1);
                    else
                        _capeUrl = value.Substring(index + 1);
                }
            }
        }

        #endregion

    }
}
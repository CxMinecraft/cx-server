﻿using System;
using System.Security.Cryptography;

namespace CxServer.Mojang
{
    /// <summary>
    /// Utility for Client&lt;-&gt;Server Authentification.
    /// </summary>
    public static class UserHash
    {
        /// <summary>
        /// Create hash of player's/client's username. Use this for Client&lt;-&gt;Server Authentification.
        /// </summary>
        /// <param name="username">Client's username.</param>
        public static string HashUsername(string username)
        {
            var sha1 = SHA1.Create();
            byte[] hash = sha1.ComputeHash(System.Text.Encoding.UTF8.GetBytes(username));
            bool negative = (hash[0] & 0x80) == 0x80;
            if (negative) // check for negative hashes
                hash = TwosCompliment(hash);
            // Create the string and trim away the zeroes
            string digest = GetHexString(hash).TrimStart('0');
            if (negative)
                digest = "-" + digest;
            return digest;
        }

        private static string GetHexString(byte[] p)
        {
            string result = string.Empty;
            for (int i = 0; i < p.Length; i++)
                result += p[i].ToString("x2"); // Converts to hex string
            return result;
        }

        private static byte[] TwosCompliment(byte[] p) // little endian
        {
            int i;
            bool carry = true;
            for (i = p.Length - 1; i >= 0; i--)
            {
                p[i] = (byte)~p[i];
                if (carry)
                {
                    carry = p[i] == 0xFF;
                    p[i]++;
                }
            }
            return p;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using Utils.Configurations;
using System.IO;
using CxServer.Settings;
using System.Text;

namespace CxServer
{
    public static class Translator
    {

        #region All types

        public static string Afrikaans = "af_ZA";
        public static string Arabic = "ar_SA";
        public static string Asturian = "ast_ES";
        public static string Azerbaijani = "az_AZ";
        public static string Bulgarian = "bg_BG";
        public static string Catalan = "ca_ES";
        public static string Czech = "cs_CZ";
        public static string Welsh = "cy_GB";
        public static string Danish = "da_DK";
        public static string German = "de_DE";
        public static string Greek = "el_GR";
        public static string AustralianEnglish = "en_AU";
        public static string CanadianEnglish = "en_CA";
        public static string BritishEnglish = "en_GB";
        public static string PirateEnglist = "en_PT";
        public static string AmericanEnglish = "en_US";
        public static string Esperanto = "eo_UY";
        public static string ArgentinianSpanish = "es_AR";
        public static string Spanish = "es_ES";
        public static string MexicanSpanish = "es_MX";
        public static string UruguayanSpanish = "es_UY";
        public static string VenezuelanSpanish = "es_VE";
        public static string Estonian = "et_EE";
        public static string Basque = "eu_ES";
        public static string Persian = "fa_IR";
        public static string Finnish = "fi_FI";
        public static string French = "fr_FR";
        public static string CanadianFrench = "fr_CA";
        public static string Irish = "ga_IE";
        public static string Galician = "gl_ES";
        public static string Manx = "gv_IM";
        public static string Hebrew = "he_IL";
        public static string Hindi = "hi_IN";
        public static string Croatian = "hr_HR";
        public static string Hungarian = "hu_HU";
        public static string Armenian = "hy_AM";
        public static string Indonesian = "id_ID";
        public static string Icelandic = "is_IS";
        public static string Italian = "it_IT";
        public static string Japanese = "ja_JP";
        public static string Georgian = "ka_GE";
        public static string Korean = "ko_KR";
        public static string Cornwall = "kw_GB";
        public static string Latin = "la_VA";
        public static string Luxembourgish = "lb_LU";
        public static string Lithuanian = "lt_LT";
        public static string Latvian = "lv_LV";
        public static string Maori = "mi_NZ";
        public static string Malay = "ms_MY";
        public static string Maltese = "mt_MT";
        public static string LowGerman = "nds_DE";
        public static string Dutch = "nl_NL";
        public static string NorwegianNyorsk = "nn_NO";
        public static string Norwegian = "no_NO";
        public static string Occitan = "oc_FR";
        public static string Polish = "pl_PL";
        public static string BrazilianPortuguese = "pt_BR";
        public static string Portuguese = "pt_PT";
        public static string Quenya = "qya_AA";
        public static string Romanian = "ro_RO";
        public static string Russian = "ru_RU";
        public static string Slovak = "sk_SK";
        public static string Slovenian = "sl_SI";
        public static string NorthernSami = "sme";
        public static string Serbian = "sr_SP";
        public static string Swedish = "sb_SE";
        public static string Thai = "th_TH";
        public static string Filipino = "tl_PH";
        public static string Klingon = "tlh_AA";
        public static string Turkish = "tr_TR";
        public static string Ukrainian = "uk_UA";
        public static string Valencian = "ca-val_ES";
        public static string Vietnamese = "vi_VN";
        public static string ChineseSimplified = "zh_CN";
        public static string ChineseTraditional = "zh_TW";

        #endregion

        //TODO unload function
        //TODO check player translation >> unload
        //TODO add permanent not-unloadable language

        //TODO add server setting to load all availible languages to memory (and keep them -> no unload)

        private static readonly Dictionary<string, Configuration> _groups = new Dictionary<string, Configuration>();

        private static bool Load(string group, string lang)
        {
            string gl = GetDictionaryKey(group, lang);

            string path = Path.Combine(ServerSettings.LanguageDirectory, lang, group + ".lang");

            if (!File.Exists(path))
                return false;

            if (_groups.ContainsKey(gl))
                _groups[gl] = _groups[gl] + YAML.Load(File.ReadAllText(path, Encoding.UTF8));
            else
                _groups[gl] = YAML.Load(File.ReadAllText(path, Encoding.UTF8));
            return true;
        }

        private static string GetDictionaryKey(string group, string lang) => string.Format("{0}.{1}", lang.ToLower(), group.ToLower());

        public static string Translate(string group, string key, string lang = "en_us")
        {
            string gl = GetDictionaryKey(group, lang);

            if (!_groups.ContainsKey(gl) && !Load(group, lang))
            {
                if (lang == AmericanEnglish)
                    return key;
                else
                    Translate(group, key, AmericanEnglish);
            }
            Configuration g = _groups[gl];
            if (g == null)
                return key;
            return g.GetString(key, key);
        }
    }
}
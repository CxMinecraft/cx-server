﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Entities.Living;

namespace CxServer.Communication.Play.ToServer
{
    public class HeldItemChange : Packet
    {

        #region Variables

        public short Slot
        {
            get;
            set;
        }

        #endregion

        public HeldItemChange(short slot) : base(0x17, PacketState.Play, PacketSide.ToServer)
        {
            this.Slot = slot;
        }

        public override string ToString()
        {
            return string.Format("[HeldItemChange: Slot={0}]", Slot);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeShort(queue, Slot);
        }
    }
}
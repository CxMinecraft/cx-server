﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Entities.Living;

namespace CxServer.Communication.Play.ToServer
{
    public class ResourcePackStatus : Packet
    {

        public enum ResultEnum
        {
            SuccessfullyLoaded = 0,
            Declined = 1,
            FailedDownload = 2,
            Accepted = 3
        }

        #region Variables

        public ResultEnum Result
        {
            get;
            set;
        }

        #endregion

        public ResourcePackStatus(ResultEnum result) : base(0x16, PacketState.Play, PacketSide.ToServer)
        {
            this.Result = result;
        }

        public override string ToString()
        {
            return string.Format("[ResourcePackStatus: Result={0}]", Result);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeVarInt(queue, (int)Result);
        }
    }
}
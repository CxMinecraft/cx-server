﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Entities.Living;

namespace CxServer.Communication.Play.ToServer
{
    public class ClientSettings : Packet
    {
        public enum ChatModeType
        {
            Full = 0,
            CommandsOnly = 1,
            Hidden = 2
        }

        #region Variables

        public string Locale
        {
            get;
            set;
        }

        public sbyte ViewDistance
        {
            get;
            set;
        }

        public ChatModeType ChatMode
        {
            get;
            set;
        }

        public bool ChatColors
        {
            get;
            set;
        }

        public SkinParts DisplayedSkinParts
        {
            get;
            set;
        }

        public PlayerEntity.MainHandEnum MainHand
        {
            get;
            set;
        }

        #endregion

        public ClientSettings(string locale, sbyte viewDistance, ChatModeType chatMode, bool chatColors, SkinParts displayedSkinParts, PlayerEntity.MainHandEnum mainHand) : base(0x04, PacketState.Play, PacketSide.ToServer)
        {
            this.Locale = locale;
            this.ViewDistance = viewDistance;
            this.ChatMode = chatMode;
            this.ChatColors = chatColors;
            this.DisplayedSkinParts = displayedSkinParts;
            this.MainHand = mainHand;
        }

        public void Apply(PlayerEntity player)
        {
            throw new NotImplementedException("Client Settings - Apply to player");
        }

        public override string ToString()
        {
            return string.Format("[ClientSettings: Locale={0}, ViewDistance={1}, ChatMode={2}, ChatColors={3}, DisplayedSkinParts={4}, MainHand={5}]", Locale, ViewDistance, ChatMode, ChatColors, DisplayedSkinParts, MainHand);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeString(queue, Locale);
            PacketEncoding.EncodeByte(queue, ViewDistance);
            PacketEncoding.EncodeVarInt(queue, (int)ChatMode);
            PacketEncoding.EncodeBoolean(queue, ChatColors);
            PacketEncoding.EncodeUnsignedByte(queue, (byte)DisplayedSkinParts);
            PacketEncoding.EncodeVarInt(queue, (int)MainHand);
        }
    }
}
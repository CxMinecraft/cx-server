﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Entities.Living;

namespace CxServer.Communication.Play.ToServer
{
    public class ChatMessage : Packet
    {

        #region Variables

        public string Message
        {
            get;
            set;
        }

        #endregion

        /// <summary>
        /// Maximum length 256
        /// </summary>
        public ChatMessage(string message) : base(0x02, PacketState.Play, PacketSide.ToServer)
        {
            this.Message = message;
        }

        public override string ToString()
        {
            return string.Format("[ChatMessage: Message={0}]", Message);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeString(queue, Message);
        }
    }
}
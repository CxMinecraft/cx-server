﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Entities.Living;
using CxServer.Utils;
using Utils;

namespace CxServer.Communication.Play.ToServer
{
    /// <summary>
    /// Teleports the player to the given entity. The player must be in spectator mode.
    /// </summary>
    public class Spectate : Packet
    {

        #region Variables

        public UUID UUID
        {
            get;
            set;
        }

        public LivingEntity Entity
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        #endregion

        public Spectate(UUID uuid) : base(0x1B, PacketState.Play, PacketSide.ToServer)
        {
            this.UUID = uuid;
        }

        public override string ToString()
        {
            return string.Format("[Spectate: UUID={0}]", UUID);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeUUID(queue, UUID);
        }
    }
}
﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Entities.Living;

namespace CxServer.Communication.Play.ToServer
{
    public class KeepAlive : Packet
    {

        #region Variables

        public int AliveID
        {
            get;
            set;
        }

        #endregion

        public KeepAlive(int aliveID) : base(0x0B, PacketState.Play, PacketSide.ToServer)
        {
            this.AliveID = aliveID;
        }

        public override string ToString()
        {
            return string.Format("[KeepAlive: AliveID={0}]", AliveID);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeVarInt(queue, AliveID);
        }
    }
}
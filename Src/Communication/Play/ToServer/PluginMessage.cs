﻿using System;
using System.IO;
using System.Collections.Generic;

namespace CxServer.Communication.Play.ToServer
{
    public class PluginMessage : Packet
    {
        public string Channel
        {
            get;
            set;
        }

        public byte[] Data
        {
            get;
            set;
        }

        public string DataString
        {
            get
            {
                return PacketEncoding.DecodeString(new Queue<byte>(Data));
            }
            set
            {
                var buffer = new Queue<byte>();
                PacketEncoding.EncodeString(buffer, value);
                Data = buffer.ToArray();
            }
        }

        public PluginMessage(string channel, byte[] data) : base(0x09, PacketState.Play, PacketSide.ToServer)
        {
            this.Channel = channel;
            this.Data = data;
        }

        public PluginMessage(string channel, string dataString) : base(0x09, PacketState.Play, PacketSide.ToServer)
        {
            this.Channel = channel;
            this.DataString = dataString;
        }

        public override string ToString()
        {
            return string.Format("[PluginMessage: Channel={0}, Data={1}]", Channel, Data);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeString(queue, Channel);
            PacketEncoding.EncodeByteArrayWithoutLength(queue, Data);
        }
    }
}
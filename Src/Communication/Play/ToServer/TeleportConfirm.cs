﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Entities.Living;

namespace CxServer.Communication.Play.ToServer
{
    public class TeleportConfirm : Packet
    {

        #region Variables

        public int TeleportID
        {
            get;
            set;
        }

        #endregion

        public TeleportConfirm(int teleportID) : base(0x00, PacketState.Play, PacketSide.ToServer)
        {
            this.TeleportID = teleportID;
        }

        public override string ToString()
        {
            return string.Format("[TeleportConfirm: TeleportID={0}]", TeleportID);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeVarInt(queue, TeleportID);
        }
    }
}
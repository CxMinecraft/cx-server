﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Entities.Living;

namespace CxServer.Communication.Play.ToServer
{
    public class ClientStatus : Packet
    {

        public enum ActionEnum
        {
            PerformRespawn = 0,
            RequestStats = 1,
            OpenInventory = 2
        }

        #region Variables

        public ActionEnum Action
        {
            get;
            set;
        }

        #endregion

        public ClientStatus(ActionEnum action) : base(0x03, PacketState.Play, PacketSide.ToServer)
        {
            this.Action = action;
        }

        public override string ToString()
        {
            return string.Format("[ClientStatus: Action={0}]", Action);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeVarInt(queue, (int)Action);
        }
    }
}
﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Entities.Living;

namespace CxServer.Communication.Play.ToServer
{
    public class UpdateSign : Packet
    {

        #region Variables

        public Position Position
        {
            get;
            set;
        }

        public string[] Lines
        {
            get;
        }

        #endregion

        public UpdateSign(Position position, string[] lines) : base(0x19, PacketState.Play, PacketSide.ToServer)
        {
            this.Position = position;

            if (lines.Length != 4)
                throw new ArgumentOutOfRangeException("lines");

            this.Lines = lines;
        }

        public UpdateSign(Position position, string line1, string line2, string line3, string line4) : this(position, new[] { line1, line2, line3, line4 })
        {
        }

        public override string ToString()
        {
            return string.Format("[UpdateSign: Position={0}, Lines={1}]", Position, "\"" + string.Join("\", \"", Lines) + "\"");
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodePosition(queue, Position);

            for (int i = 0; i < 4; i++)
                PacketEncoding.EncodeString(queue, Lines[i]);
        }
    }
}
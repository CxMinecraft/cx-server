﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Entities.Living;

namespace CxServer.Communication.Play.ToServer
{
    public class ArmSwing : Packet
    {

        #region Variables

        public bool MainHand
        {
            get;
            set;
        }

        #endregion

        public ArmSwing(bool mainHand = true) : base(0x1A, PacketState.Play, PacketSide.ToServer)
        {
            this.MainHand = mainHand;
        }

        public override string ToString()
        {
            return string.Format("[ArmSwing: MainHand={0}]", MainHand);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeVarInt(queue, MainHand ? 0 : 1);
        }
    }
}
﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Entities.Living;

namespace CxServer.Communication.Play.ToServer
{
    public class TabComplete : Packet
    {

        #region Variables

        /// <summary>
        /// All text behind the cursor<br>
        /// (e.g. to the left of the cursor in left-to-right languages like English)
        /// </summary>
        public string Text
        {
            get;
            set;
        }

        /// <summary>
        /// If true, the server will parse Text as a command even if it doesn't start with a '/'.<br>
        /// Used in the command block GUI.
        /// </summary>
        public bool AssumeCommand
        {
            get;
            set;
        }

        /// <summary>
        /// Optional<br>
        /// The position of the block being looked at.
        /// </summary>
        public Position? LookedAtBlock
        {
            get;
            set;
        }

        #endregion

        public TabComplete(string text, bool asumeCommand) : base(0x01, PacketState.Play, PacketSide.ToServer)
        {
            this.Text = text;
            this.AssumeCommand = AssumeCommand;
            this.LookedAtBlock = null;
        }

        public TabComplete(string text, bool asumeCommand, Position lookedAtBlock) : base(0x01, PacketState.Play, PacketSide.ToServer)
        {
            this.Text = text;
            this.AssumeCommand = AssumeCommand;
            this.LookedAtBlock = lookedAtBlock;
        }

        public override string ToString()
        {
            return string.Format("[TabComplete: Text={0}, AssumeCommand={1}, LookedAtBlock={2}]", Text, AssumeCommand, LookedAtBlock);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeString(queue, Text);
            PacketEncoding.EncodeBoolean(queue, AssumeCommand);

            PacketEncoding.EncodeBoolean(queue, LookedAtBlock != null);
            if (LookedAtBlock != null)
                PacketEncoding.EncodePosition(queue, LookedAtBlock.Value);
        }
    }
}
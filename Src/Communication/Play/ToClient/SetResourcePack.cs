﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Chat;
using CxServer.ResourcePacks;

namespace CxServer.Communication.Play.ToClient
{
    public class SetResourcePack : Packet
    {
        public string Address
        {
            get;
            set;
        }

        public string Name
        {
            get;
            set;
        }

        public string Hash
        {
            get;
            set;
        }

        public SetResourcePack(string address) : base(0x32, PacketState.Play, PacketSide.ToClient)
        {
            this.Address = address;
            this.Name = ResourcePackUtils.GetName(this.Address);
            this.Hash = ResourcePackUtils.GetHash(address) ?? this.Name;
        }

        public override string ToString()
        {
            return string.Format("[SetResourcePack: Address={0}, Hash={1}]", Address, Hash);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeString(queue, Address);
            PacketEncoding.EncodeString(queue, Hash);
        }
    }
}
﻿using System;
using System.IO;
using System.Collections.Generic;

namespace CxServer.Communication.Play.ToClient
{
    public class Statistics : Packet
    {
        public IDictionary<string, int> Stats
        {
            get;
        }

        public Statistics(IDictionary<string, int> stats) : base(0x07, PacketState.Play, PacketSide.ToClient)
        {
            this.Stats = stats ?? new Dictionary<string, int>();
        }

        public override string ToString()
        {
            return string.Format("[Statistics: Count={0}]", Stats.Count);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeVarInt(queue, Stats.Count);
            foreach (var kvp in Stats)
            {
                PacketEncoding.EncodeString(queue, kvp.Key);
                PacketEncoding.EncodeVarInt(queue, kvp.Value);
            }
        }
    }
}
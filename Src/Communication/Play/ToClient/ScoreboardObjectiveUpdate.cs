﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Chat;
using CxServer.Scoreboards;

namespace CxServer.Communication.Play.ToClient
{
    public class ScoreboardObjectiveUpdate : ScoreboardObjective
    {
        public ScoreboardObjectiveUpdate(string scoreboardName, string displayName, bool hearts = false) : base(scoreboardName, 2)
        {
            this.DisplayName = displayName;
            this.Hearts = hearts;
        }

        public ScoreboardObjectiveUpdate(Scoreboard scoreboard) : this(scoreboard.Name, scoreboard.DisplayName, scoreboard.Hearts)
        {
        }

        public string DisplayName
        {
            get;
            set;
        }

        public bool Hearts
        {
            get;
            set;
        }

        public override string ToString()
        {
            return string.Format("[ScoreboardObjectiveUpdate: ScoreboardName={0}, DisplayName={1}, Hearts={2}]", ScoreboardName, DisplayName, Hearts);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            base.ToBytes(queue);
            PacketEncoding.EncodeString(queue, DisplayName);
            PacketEncoding.EncodeString(queue, Hearts ? "hearts" : "integer");
        }
    }
}
﻿using System;
using System.IO;
using System.Collections.Generic;

namespace CxServer.Communication.Play.ToClient
{
    public class PluginMessage : Packet
    {
        public string Channel
        {
            get;
            set;
        }

        public byte[] Data
        {
            get;
            set;
        }

        public PluginMessage(string channel, byte[] data) : base(0x18, PacketState.Play, PacketSide.ToClient)
        {
            this.Channel = channel;
            this.Data = data;
        }

        public override string ToString()
        {
            return string.Format("[PluginMessage: Channel={0}, Data={1}]", Channel, Data);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeString(queue, Channel);
            PacketEncoding.EncodeByteArrayWithoutLength(queue, Data);
        }
    }
}
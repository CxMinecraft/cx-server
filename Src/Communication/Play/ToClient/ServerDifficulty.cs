﻿using System;
using System.IO;
using System.Collections.Generic;

namespace CxServer.Communication.Play.ToClient
{
    public class ServerDifficulty : Packet
    {
        public Difficulty Difficulty
        {
            get;
            set;
        }

        public ServerDifficulty(Difficulty difficulty) : base(0x0D, PacketState.Play, PacketSide.ToClient)
        {
            this.Difficulty = difficulty;
        }

        public override string ToString()
        {
            return string.Format("[ServerDifficulty: Difficulty={0}]", Difficulty);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeUnsignedByte(queue, (byte)Difficulty);
        }
    }
}
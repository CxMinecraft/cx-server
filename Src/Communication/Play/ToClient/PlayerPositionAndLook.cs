﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Chat;
using CxServer.Utils;
using Utils.Vector;

namespace CxServer.Communication.Play.ToClient
{
    public class PlayerPositionAndLook : Packet
    {

        #region Variables

        public double X
        {
            get;
            set;
        }

        public double Y
        {
            get;
            set;
        }

        public double Z
        {
            get;
            set;
        }

        public Vector3d Position
        {
            get
            {
                return new Vector3d(X, Y, Z);
            }
            set
            {
                X = value.X;
                Y = value.Y;
                Z = value.Z;
            }
        }

        public float Pitch
        {
            get;
            set;
        }

        public float Yaw
        {
            get;
            set;
        }

        public Vector2f Rotation
        {
            get
            {
                return new Vector2f(Pitch, Yaw);
            }
            set
            {
                Pitch = value.X;
                Yaw = value.Y;
            }
        }

        public RelativeFlags Flags
        {
            get;
            set;
        }

        public int TeleportID
        {
            get;
            set;
        }

        #endregion

        [Flags]
        public enum RelativeFlags
        {
            X = 0x01,
            Y = 0x02,
            Z = 0x04,
            Position = X | Y | Z,
            Pitch = 0x08,
            Yaw = 0x10,
            Rotation = Pitch | Yaw
        }

        #region Utility functions

        public Vector3d UpdatePosition(Vector3d position)
        {
            double x = Flags.HasFlag(RelativeFlags.X) ? position.X + Position.X : Position.X;
            double y = Flags.HasFlag(RelativeFlags.Y) ? position.Y + Position.Y : Position.Y;
            double z = Flags.HasFlag(RelativeFlags.Z) ? position.Z + Position.Z : Position.Z;
            return new Vector3d(x, y, z);
        }

        public Vector2d UpdateRotation(Vector2d position)
        {
            double x = Flags.HasFlag(RelativeFlags.Pitch) ? position.X + Rotation.X : Rotation.X;
            double y = Flags.HasFlag(RelativeFlags.Yaw) ? position.Y + Rotation.Y : Rotation.Y;
            return new Vector2d(x, y);
        }

        #endregion

        #region Constructors

        public PlayerPositionAndLook(Vector3d position, bool relativePosition, Vector2f rotation, bool relativeRotation) : this(position.X, position.Y, position.Z, relativePosition, rotation.X, rotation.Y, relativeRotation)
        {
        }

        public PlayerPositionAndLook(Vector3d position, Vector2f rotation, RelativeFlags flags) : this(position.X, position.Y, position.Z, rotation.X, rotation.Y, flags)
        {
        }

        public PlayerPositionAndLook(Vector3d position, Vector2f rotation) : this(position.X, position.Y, position.Z, rotation.X, rotation.Y)
        {
        }

        public PlayerPositionAndLook(double x, double y, double z, float pitch, float yaw) : this(x, y, z, pitch, yaw, (RelativeFlags)0)
        {
        }

        public PlayerPositionAndLook(double x, double y, double z, bool relativePosition, float pitch, float yaw, bool relativeRotation) : this(x, y, z, pitch, yaw, (relativePosition ? RelativeFlags.Position : 0) | (relativeRotation ? RelativeFlags.Rotation : 0))
        {
        }

        public PlayerPositionAndLook(double x, double y, double z, float pitch, float yaw, RelativeFlags flags) : this(Environment.TickCount, x, y, z, pitch, yaw, flags)
        {
        }

        public PlayerPositionAndLook(int teleportID, Vector3d position, bool relativePosition, Vector2f rotation, bool relativeRotation) : this(teleportID, position.X, position.Y, position.Z, relativePosition, rotation.X, rotation.Y, relativeRotation)
        {
        }

        public PlayerPositionAndLook(int teleportID, Vector3d position, Vector2f rotation, RelativeFlags flags) : this(teleportID, position.X, position.Y, position.Z, rotation.X, rotation.Y, flags)
        {
        }

        public PlayerPositionAndLook(int teleportID, Vector3d position, Vector2f rotation) : this(teleportID, position.X, position.Y, position.Z, rotation.X, rotation.Y)
        {
        }

        public PlayerPositionAndLook(int teleportID, double x, double y, double z, float pitch, float yaw) : this(teleportID, x, y, z, pitch, yaw, (RelativeFlags)0)
        {
        }

        public PlayerPositionAndLook(int teleportID, double x, double y, double z, bool relativePosition, float pitch, float yaw, bool relativeRotation) : this(teleportID, x, y, z, pitch, yaw, (relativePosition ? RelativeFlags.Position : 0) | (relativeRotation ? RelativeFlags.Rotation : 0))
        {
        }

        public PlayerPositionAndLook(int teleportID, double x, double y, double z, float pitch, float yaw, RelativeFlags flags) : base(0x2E, PacketState.Play, PacketSide.ToClient)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;

            this.Pitch = pitch;
            this.Yaw = yaw;

            this.Flags = flags;

            this.TeleportID = teleportID;
        }

        #endregion

        public override string ToString()
        {
            return string.Format("[PlayerPositionAndLook: Position={0}, Rotation={1}, Flags={2}, TeleportID={3}]", Position, Rotation, Flags, TeleportID);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeDouble(queue, X);
            PacketEncoding.EncodeDouble(queue, Y);
            PacketEncoding.EncodeDouble(queue, Z);

            PacketEncoding.EncodeFloat(queue, Yaw);
            PacketEncoding.EncodeFloat(queue, Pitch);

            PacketEncoding.EncodeUnsignedByte(queue, (byte)Flags);

            PacketEncoding.EncodeVarInt(queue, TeleportID);
        }
    }
}
﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Entities.Global;
using CxServer.Utils;
using Utils.Vector;

namespace CxServer.Communication.Play.ToClient
{
    public class SpawnGlobalEntity : Packet
    {

        #region Variables

        public EntityID EntityID
        {
            get;
            set;
        }

        public GlobalEntityType Type
        {
            get;
            set;
        }

        public Vector3d Position
        {
            get;
            set;
        }

        #endregion

        public SpawnGlobalEntity(EntityID entityID, GlobalEntityType type, Vector3d position) : base(0x02, PacketState.Play, PacketSide.ToClient)
        {
            this.EntityID = entityID;
            this.Type = type;
            this.Position = position;
        }

        public SpawnGlobalEntity(GlobalEntity entity) : this(entity.EntityID, entity.GlobalType, entity.Position)
        {
        }

        public override string ToString()
        {
            return string.Format("[SpawnGlobalEntity: EntityID={0}, Type={1}, Position={2}]", EntityID, Type, Position);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeEntityID(queue, EntityID);
            PacketEncoding.EncodeUnsignedByte(queue, (byte)(int)Type);

            PacketEncoding.EncodeDouble(queue, Position.X);
            PacketEncoding.EncodeDouble(queue, Position.Y);
            PacketEncoding.EncodeDouble(queue, Position.Z);
        }
    }
}
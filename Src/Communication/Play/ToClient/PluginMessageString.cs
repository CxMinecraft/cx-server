﻿using System;
using System.IO;
using System.Collections.Generic;

namespace CxServer.Communication.Play.ToClient
{
    public class PluginMessageString : Packet
    {
        public string Channel
        {
            get;
            set;
        }

        public string Message
        {
            get;
            set;
        }

        public PluginMessageString(string channel, string message) : base(0x18, PacketState.Play, PacketSide.ToClient)
        {
            this.Channel = channel;
        }

        public override string ToString()
        {
            return string.Format("[PluginMessage: Channel={0}, DataMessage={1}]", Channel, Message);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeString(queue, Channel);
            PacketEncoding.EncodeString(queue, Message);
        }
    }
}
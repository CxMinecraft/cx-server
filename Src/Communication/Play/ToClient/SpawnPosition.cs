﻿using System;
using System.IO;
using System.Collections.Generic;

namespace CxServer.Communication.Play.ToClient
{
    public class SpawnPosition : Packet
    {
        public Position Position
        {
            get;
            set;
        }

        public SpawnPosition(Position position) : base(0x43, PacketState.Play, PacketSide.ToClient)
        {
            this.Position = position;
        }

        public override string ToString()
        {
            return string.Format("[SpawnPosition: Position={0}]", Position);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeLong(queue, Position.ToLong());
        }
    }
}
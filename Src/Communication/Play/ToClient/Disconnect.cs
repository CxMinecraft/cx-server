﻿using System;
using System.IO;
using System.Collections.Generic;

namespace CxServer.Communication.Play.ToClient
{
    public class Disconnect : Packet
    {
        public CxServer.Chat.ChatMessage Chat
        {
            get;
            set;
        }

        public Disconnect(CxServer.Chat.ChatMessage chat) : base(0x1A, PacketState.Play, PacketSide.ToClient)
        {
            this.Chat = chat;
        }

        public Disconnect(string reasonText) : this(new CxServer.Chat.ChatMessagePlain(reasonText))
        {
        }

        public override string ToString()
        {
            return string.Format("[Disconnect: Chat={0}]", Chat);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeChat(queue, Chat);
        }
    }
}
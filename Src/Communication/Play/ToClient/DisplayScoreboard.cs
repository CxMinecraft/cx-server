﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Chat;
using CxServer.Scoreboards;

namespace CxServer.Communication.Play.ToClient
{
    public class DisplayScoreboard : Packet
    {
        public DisplaySlot Slot
        {
            get;
            set;
        }

        public string ScoreboardName
        {
            get;
            set;
        }

        public Scoreboard Scoreboard
        {
            get
            {
                return Scoreboard.GetScoreboard(ScoreboardName);
            }
            set
            {
                ScoreboardName = value == null ? null : value.Name;
            }
        }

        public DisplayScoreboard(DisplaySlot slot, string scoreboardName) : base(0x38, PacketState.Play, PacketSide.ToClient)
        {
            this.Slot = slot;
            this.ScoreboardName = scoreboardName;
        }

        public DisplayScoreboard(DisplaySlot slot, Scoreboard scoreboard) : this(slot, scoreboard.Name)
        {
        }

        public override string ToString()
        {
            return string.Format("[DisplayScoreboard: Slot={0}, ScoreboardName={1}]", Slot, ScoreboardName);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeByte(queue, (sbyte)Slot);
            PacketEncoding.EncodeString(queue, ScoreboardName);
        }
    }
}
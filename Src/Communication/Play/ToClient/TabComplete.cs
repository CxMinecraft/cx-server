﻿using System;
using System.IO;
using System.Collections.Generic;

namespace CxServer.Communication.Play.ToClient
{
    public class TabComplete : Packet
    {
        public List<string> Matches
        {
            get;
        }

        public TabComplete(List<string> matches) : base(0x0E, PacketState.Play, PacketSide.ToClient)
        {
            this.Matches = matches ?? new List<string>();
        }

        public TabComplete(string[] matches) : this(matches == null ? new List<string>() : new List<string>(matches))
        {
        }

        public override string ToString()
        {
            return string.Format("[TabComplete: Count={0}, Matches={1}]", Matches.Count, "\"" + string.Join("\", \"", Matches) + "\"");
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeVarInt(queue, Matches.Count);
            foreach (var s in Matches)
                PacketEncoding.EncodeString(queue, s);
        }
    }
}
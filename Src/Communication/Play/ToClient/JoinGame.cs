﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Entities.Living;

namespace CxServer.Communication.Play.ToClient
{
    public class JoinGame : Packet
    {
        public EntityID EntityID
        {
            get;
            set;
        }

        public GameMode GameMode
        {
            get;
            set;
        }

        public Dimension Dimension
        {
            get;
            set;
        }

        public Difficulty Difficulty
        {
            get;
            set;
        }

        public byte MaxPlayers
        {
            get;
            set;
        }

        public string LevelType
        {
            get;
            set;
        }

        public bool RedurecDebugInfo
        {
            get;
            set;
        }

        public JoinGame(EntityID entityID, GameMode gamemode, Dimension dimension, Difficulty difficulty, byte maxPlayers, string levelType, bool reducedDebugInfo = false) : base(0x23, PacketState.Play, PacketSide.ToClient)
        {
            this.EntityID = entityID;
            this.GameMode = gamemode;
            this.Dimension = dimension;
            this.Difficulty = difficulty;
            this.MaxPlayers = maxPlayers;
            this.LevelType = levelType;
            this.RedurecDebugInfo = reducedDebugInfo;
        }

        public JoinGame(PlayerEntity player, byte maxPlayers) : base(0x23, PacketState.Play, PacketSide.ToClient)
        {
            this.EntityID = player.EntityID;
            this.GameMode = player.GameMode;
            this.Dimension = player.World.Dimension;
            this.Difficulty = player.World.Difficulty;
            this.MaxPlayers = maxPlayers;
            this.LevelType = player.World.Generator.TypeName;
            this.RedurecDebugInfo = player.ReducedDebugInfo;
        }

        public override string ToString()
        {
            return string.Format("[JoinGame: EntityID={0}, GameMode={1}, Dimension={2}, Difficulty={3}, MaxPlayers={4}, LevelType={5}, RedurecDebugInfo={6}]", EntityID, GameMode, Dimension, Difficulty, MaxPlayers, LevelType, RedurecDebugInfo);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeUnsignedInt(queue, EntityID);
            PacketEncoding.EncodeUnsignedByte(queue, (byte)GameMode);
            PacketEncoding.EncodeInt(queue, (int)Dimension);
            PacketEncoding.EncodeUnsignedByte(queue, (byte)Difficulty);
            PacketEncoding.EncodeUnsignedByte(queue, MaxPlayers);
            PacketEncoding.EncodeString(queue, LevelType);
            PacketEncoding.EncodeBoolean(queue, RedurecDebugInfo);
        }
    }
}
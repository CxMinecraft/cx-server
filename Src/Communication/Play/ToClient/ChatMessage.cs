﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Commands;

namespace CxServer.Communication.Play.ToClient
{
    public class ChatMessage : Packet
    {
        public CxServer.Chat.ChatMessage Message
        {
            get;
            set;
        }

        public MessageType Type
        {
            get;
            set;
        }

        public ChatMessage(CxServer.Chat.ChatMessage message, MessageType type) : base(0x0F, PacketState.Play, PacketSide.ToClient)
        {
            this.Message = message;
            this.Type = type;
        }

        public override string ToString()
        {
            return string.Format("[ChatMessage: Message={0}, Type={1}]", Message, Type);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeString(queue, Message == null ? "" : Message.ToJSON());
            PacketEncoding.EncodeByte(queue, (sbyte)Type);
        }
    }
}
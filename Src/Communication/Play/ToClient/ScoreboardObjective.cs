﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Chat;
using CxServer.Scoreboards;

namespace CxServer.Communication.Play.ToClient
{
    public abstract class ScoreboardObjective : Packet
    {
        public string ScoreboardName
        {
            get;
            set;
        }

        public Scoreboard Scoreboard
        {
            get
            {
                return Scoreboard.GetScoreboard(ScoreboardName);
            }
            set
            {
                ScoreboardName = value == null ? null : value.Name;
            }
        }

        public byte Mode
        {
            get;
            set;
        }

        public ScoreboardObjective(string scoreboardName, byte mode) : base(0x38, PacketState.Play, PacketSide.ToClient)
        {
            this.ScoreboardName = scoreboardName;
            this.Mode = mode;
        }

        public override string ToString()
        {
            return string.Format("[ScoreboardObjective: ScoreboardName={0}, Mode={1}]", ScoreboardName, Mode);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeString(queue, ScoreboardName);
            PacketEncoding.EncodeUnsignedByte(queue, Mode);
        }
    }
}
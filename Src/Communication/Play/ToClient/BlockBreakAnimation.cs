﻿using System;
using System.IO;
using System.Collections.Generic;

namespace CxServer.Communication.Play.ToClient
{
    /// <summary>
    /// 0–9 are the displayable destroy stages and each other number means that there is no animation on this coordinate.<br>
    /// <br>
    /// Block break animations can still be applied on air; the animation will remain visible although there is no block being broken. However, if this is applied to a transparent block, odd graphical effects may happen, including water losing its transparency. (An effect similar to this can be seen in normal gameplay when breaking ice blocks)<br>
    /// <br>
    /// If you need to display several break animations at the same time you have to give each of them a unique Entity ID. 
    /// </summary>
    public class BlockBreakAnimation : Packet
    {

        #region Variables

        public EntityID EntityID
        {
            get;
            set;
        }

        public Position Position
        {
            get;
            set;
        }

        /// <summary>
        /// 0–9 to set it, any other value to remove it 
        /// </summary>
        public sbyte DestroyStage
        {
            get;
            set;
        }

        #endregion

        public BlockBreakAnimation(EntityID entityID, Position position, sbyte destroyStage) : base(0x08, PacketState.Play, PacketSide.ToClient)
        {
            this.EntityID = entityID;
            this.Position = position;
            this.DestroyStage = destroyStage;
        }

        public override string ToString()
        {
            return string.Format("[BlockBreakAnimation: EntityID={0}, Position={1}, DestroyStage={2}]", EntityID, Position, DestroyStage);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeEntityID(queue, EntityID);
            PacketEncoding.EncodePosition(queue, Position);
            PacketEncoding.EncodeByte(queue, DestroyStage);
        }
    }
}
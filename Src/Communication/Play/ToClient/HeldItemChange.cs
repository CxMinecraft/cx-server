﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Entities.Living;

namespace CxServer.Communication.Play.ToClient
{
    public class HeldItemChange : Packet
    {

        #region Variables

        public byte Slot
        {
            get;
            set;
        }

        #endregion

        public HeldItemChange(byte slot) : base(0x37, PacketState.Play, PacketSide.ToClient)
        {
            this.Slot = slot;
        }

        public override string ToString()
        {
            return string.Format("[HeldItemChange: Slot={0}]", Slot);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeUnsignedByte(queue, Slot);
        }
    }
}
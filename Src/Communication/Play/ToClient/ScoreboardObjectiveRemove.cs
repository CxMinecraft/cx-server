﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Chat;
using CxServer.Scoreboards;

namespace CxServer.Communication.Play.ToClient
{
    public class ScoreboardObjectiveRemove : ScoreboardObjective
    {
        public ScoreboardObjectiveRemove(string scoreboardName) : base(scoreboardName, 1)
        {
        }

        public override string ToString()
        {
            return string.Format("[ScoreboardObjectiveRemove: ScoreboardName={0}]", ScoreboardName);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using CxServer.Utils;
using CxServer.NBT;
using System.Linq;
using CxServer.Worlds;
using Utils.Extensions;

namespace CxServer.Communication.Play.ToClient
{
    public class ChunkData : Packet
    {

        #region Variables

        public int ChunkX
        {
            get;
            set;
        }

        public int ChunkY
        {
            get;
            set;
        }

        /// <summary>
        /// Requires all chunks present (will send air if this is enabled).
        /// </summary>
        public bool IncludeBiomes
        {
            get;
            set;
        }

        public Biome[,] Biomes
        {
            get;
        }

        public Worlds.ChunkData[] Data
        {
            get;
            set;
        }

        public List<NBT_Compound> BlockEntities
        {
            get;
            set;
        }

        /// <summary>
        /// Only for Overworld
        /// </summary>
        public bool IncludeSkyLight
        {
            get;
            set;
        }

        #endregion

        public ChunkData(int chunkX, int chunkY, bool includeBiomes, Worlds.ChunkData[] data, bool includeSkyLight = false, Biome[,] biomes = null, List<NBT_Compound> blockEntities = null) : base(0x20, PacketState.Play, PacketSide.ToClient)
        {
            this.ChunkX = chunkX;
            this.ChunkY = chunkY;

            this.IncludeSkyLight = includeSkyLight;

            this.IncludeBiomes = includeBiomes;

            if (biomes == null)
                this.Biomes = new Biome[Chunk.Size, Chunk.Size];
            else if (biomes.GetLength(0) != Chunk.Size || biomes.GetLength(1) != Chunk.Size)
                throw new ArgumentOutOfRangeException("biomes");
            else
                this.Biomes = biomes;

            this.Data = data;

            this.BlockEntities = blockEntities;
        }

        public override string ToString()
        {
            return string.Format("[ChunkData: ChunkX={0}, ChunkY={1}, IncludeBiomes={2}, Data.Length={3}, BlockEntities.Count={4}]", ChunkX, ChunkY, IncludeBiomes, Data.Length, BlockEntities.Count);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeInt(queue, ChunkX);
            PacketEncoding.EncodeInt(queue, ChunkY);

            PacketEncoding.EncodeBoolean(queue, IncludeBiomes);

            //Data
            PacketEncoding.EncodeVarInt(queue, Data.Select(d => d != null).ToArray().PadLeft(32, false).ToInt());

            List<byte> data = new List<byte>();

            for (int i = 0; i < this.Data.Length; i++)
                if (this.Data[i] != null)
                    data.AddRange(this.Data[i].ToBytes(IncludeSkyLight));
            if (IncludeBiomes)
            {
                for (int x = 0; x < Chunk.Size; x++)
                    for (int y = 0; y < Chunk.Size; y++)
                        data.Add(Biomes[x, y] == null ? (byte)0 : Biomes[x, y].ID);
            }

            PacketEncoding.EncodeByteArray(queue, data);

            //TileEntities
            //TODO add tile entities
            PacketEncoding.EncodeVarInt(queue, 0);
        }
    }
}
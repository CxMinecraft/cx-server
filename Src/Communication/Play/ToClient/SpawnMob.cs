﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Utils;
using CxServer.Entities;
using CxServer.Entities.Object;
using CxServer.Entities.Living;
using Utils;
using Utils.Vector;

namespace CxServer.Communication.Play.ToClient
{
    public class SpawnMob : Packet
    {

        #region Variables

        public EntityID EntityID
        {
            get;
            set;
        }

        public UUID UUID
        {
            get;
            set;
        }

        public EntityType Type
        {
            get;
            set;
        }

        public Vector3d Position
        {
            get;
            set;
        }

        public Vector2f Rotation
        {
            get;
            set;
        }

        public float HeadPitch
        {
            get;
            set;
        }

        public Vector3d Velocity
        {
            get;
            set;
        }

        public EntityMetadata Metadata
        {
            get;
            set;
        }

        #endregion

        public SpawnMob(EntityID entityID, UUID uuid, EntityType type, Vector3d position, Vector2f rotation, float headPitch, Vector3d velocity, EntityMetadata metadata) : base(0x03, PacketState.Play, PacketSide.ToClient)
        {
            this.EntityID = entityID;
            this.UUID = uuid;
            this.Type = type;
            this.Position = position;
            this.Rotation = rotation;
            this.HeadPitch = headPitch;
            this.Velocity = velocity;
            this.Metadata = metadata;
        }

        public SpawnMob(LivingEntity entity) : this(entity.EntityID, entity.UUID, entity.Type, entity.Position, entity.Rotation, entity.HeadPitch, entity.Velocity, entity.Metadata)
        {

        }

        public override string ToString()
        {
            return string.Format("[SpawnMob: EntityID={0}, UUID={1}, Type={2}, Position={3}, Rotation={4}, HeadPitch={5}, Velocity={6}, Metadata={7}]", EntityID, UUID, Type, Position, Rotation, HeadPitch, Velocity, Metadata);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeUnsignedVarInt(queue, EntityID);
            PacketEncoding.EncodeUUID(queue, UUID);

            PacketEncoding.EncodeUnsignedByte(queue, (byte)(((int)Type) % 256));

            PacketEncoding.EncodeDouble(queue, Position.X);
            PacketEncoding.EncodeDouble(queue, Position.Y);
            PacketEncoding.EncodeDouble(queue, Position.Z);

            PacketEncoding.EncodeAngleFloat(queue, Rotation.X);//pitch
            PacketEncoding.EncodeAngleFloat(queue, Rotation.Y);//yaw

            PacketEncoding.EncodeFloat(queue, HeadPitch);

            PacketEncoding.EncodeVelocity(queue, Velocity.X);
            PacketEncoding.EncodeVelocity(queue, Velocity.Y);
            PacketEncoding.EncodeVelocity(queue, Velocity.Z);

            PacketEncoding.EncodeMetadata(queue, Metadata);
        }
    }
}
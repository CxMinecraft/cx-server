﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Entities.Living;

namespace CxServer.Communication.Play.ToClient
{
    public class PlayerAbilities : Packet
    {
        public AbilitiesFlags Flags
        {
            get;
            set;
        }

        public float FlyingSpeed
        {
            get;
            set;
        }

        public float FOVModifier
        {
            get;
            set;
        }

        public PlayerAbilities(PlayerEntity player) : this(player.Abilities, (float)player.FlyingSpeed, (float)player.MovementSpeed)
        {
        }

        public PlayerAbilities(PlayerEntity player, float fovModifier) : this(player.Abilities, (float)player.FlyingSpeed, fovModifier)
        {
        }

        public PlayerAbilities(AbilitiesFlags flags, float flyingSpeed, float fovModifier) : base(0x2B, PacketState.Play, PacketSide.ToClient)
        {
            this.Flags = flags;
            this.FlyingSpeed = flyingSpeed;
            this.FOVModifier = fovModifier;
        }

        public bool HasFlag(AbilitiesFlags flag)
        {
            return Flags.HasFlag(flag);
        }

        public override string ToString()
        {
            return string.Format("[PlayerAbilities: Flags={0}, FlyingSpeed={1}, FOVModifier={2}]", Flags, FlyingSpeed, FOVModifier);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeUnsignedByte(queue, (byte)Flags);
            PacketEncoding.EncodeFloat(queue, FlyingSpeed);
            PacketEncoding.EncodeFloat(queue, FOVModifier);
        }

        [Flags]
        public enum AbilitiesFlags
        {
            Invulnerable = 0x01,
            Flying = 0x02,
            AllowFlying = 0x04,
            CreativeMode = 0x08
        }
    }
}
﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Utils;
using Utils.Vector;

namespace CxServer.Communication.Play.ToClient
{
    public class SpawnExperienceOrb : Packet
    {

        #region Variables

        public EntityID EntityID
        {
            get;
            set;
        }

        public Vector3d Position
        {
            get;
            set;
        }

        public short Amount
        {
            get;
            set;
        }

        #endregion

        public SpawnExperienceOrb(EntityID entityID, Vector3d position, short amount) : base(0x01, PacketState.Play, PacketSide.ToClient)
        {
            this.EntityID = entityID;
            this.Position = position;
            this.Amount = amount;
        }

        public override string ToString()
        {
            return string.Format("[SpawnExperienceOrb: EntityID={0}, Position={1}, Amount={2}]", EntityID, Position, Amount);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeEntityID(queue, EntityID);

            PacketEncoding.EncodeDouble(queue, Position.X);
            PacketEncoding.EncodeDouble(queue, Position.Y);
            PacketEncoding.EncodeDouble(queue, Position.Z);

            PacketEncoding.EncodeShort(queue, Amount);
        }
    }
}
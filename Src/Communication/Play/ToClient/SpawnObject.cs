﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Utils;
using CxServer.Entities;
using CxServer.Entities.Object;
using Utils;
using Utils.Vector;

namespace CxServer.Communication.Play.ToClient
{
	public class SpawnObject : Packet
	{

		#region Variables

		public EntityID EntityID {
			get;
			set;
		}

		public UUID UUID {
			get;
			set;
		}

		public EntityType Type {
			get;
			set;
		}

		public Vector3d Position {
			get;
			set;
		}

		public Vector2f Rotation {
			get;
			set;
		}

		public int Data {
			get;
			set;
		}

		public Vector3d Velocity {
			get;
			set;
		}

		#endregion

		public SpawnObject (EntityID entityID, UUID uuid, EntityType type, Vector3d position, Vector2f rotation, int data, Vector3d velocity) : base (0x00, PacketState.Play, PacketSide.ToClient)
		{
			this.EntityID = entityID;
			this.UUID = uuid;
			this.Type = type;
			this.Position = position;
			this.Rotation = rotation;
			this.Data = data;
			this.Velocity = velocity;
		}

		public SpawnObject (EntityObject entity) : this (entity.EntityID, entity.UUID, entity.Type, entity.Position, entity.Rotation, entity.Data, entity.Velocity)
		{

		}

		public override string ToString ()
		{
			return string.Format ("[SpawnObject: EntityID={0}, UUID={1}, Type={2}, Position={3}, Rotation={4}, Data={5}, Velocity={6}]", EntityID, UUID, Type, Position, Rotation, Data, Velocity);
		}

		public override void ToBytes (Queue<byte> queue)
		{
			PacketEncoding.EncodeUnsignedVarInt (queue, EntityID);
			PacketEncoding.EncodeUUID (queue, UUID);

			PacketEncoding.EncodeUnsignedByte (queue, (byte)(((int)Type) % 256));

			PacketEncoding.EncodeDouble (queue, Position.X);
			PacketEncoding.EncodeDouble (queue, Position.Y);
			PacketEncoding.EncodeDouble (queue, Position.Z);

			PacketEncoding.EncodeAngleFloat (queue, Rotation.X);//pitch
			PacketEncoding.EncodeAngleFloat (queue, Rotation.Y);//yaw

			PacketEncoding.EncodeInt (queue, Data);

			PacketEncoding.EncodeVelocity (queue, Velocity.X);
			PacketEncoding.EncodeVelocity (queue, Velocity.Y);
			PacketEncoding.EncodeVelocity (queue, Velocity.Z);
		}
	}
}
﻿using System;
using System.Collections.Generic;

namespace CxServer.Communication.Handshake.ToServer
{
    public class Handshake : Packet
    {
        public int ProtocolVersion
        {
            get;
        }

        public string ServerAddress
        {
            get;
        }

        public ushort ServerPort
        {
            get;
        }

        public Handshake.NextState Next
        {
            get;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CxServer.Communication.Handshake.ToServer.Handshake"/> class.
        /// </summary>
        /// <param name="protocolVersion">VarInt</param>
        /// <param name="serverAddress">String</param>
        /// <param name="serverPort">Unsigned Short</param>
        /// <param name="nextState">VarInt</param>
        public Handshake(int protocolVersion, string serverAddress, ushort serverPort, NextState nextState) : base(0x00, PacketState.HandShaking, PacketSide.ToServer)
        {
            this.ProtocolVersion = protocolVersion;
            this.ServerAddress = serverAddress;
            this.ServerPort = serverPort;
            this.Next = nextState;
        }

        public enum NextState
        {
            Status = 1,
            Login = 2
        }

        public override string ToString()
        {
            return string.Format("[Handshake: ProtocolVersion={0}, ServerAddress={1}, ServerPort={2}, Next={3}]", ProtocolVersion, ServerAddress, ServerPort, Next);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeVarInt(queue, ProtocolVersion);
            PacketEncoding.EncodeString(queue, ServerAddress);
            PacketEncoding.EncodeUnsignedShort(queue, ServerPort);
            PacketEncoding.EncodeVarInt(queue, (int)Next);
        }
    }
}
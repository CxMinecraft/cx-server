﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Utils;

namespace CxServer.Communication.Login.ToClient
{
    public class SetCompression : Packet
    {
        public int MaximumSize
        {
            get;
        }

        public SetCompression(int maximumSize) : base(0x03, PacketState.Login, PacketSide.ToClient)
        {
            this.MaximumSize = maximumSize;
        }

        public override string ToString()
        {
            return string.Format("[SetCompression: MaximumSize={0}]", MaximumSize);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeVarInt(queue, MaximumSize < 0 ? -1 : MaximumSize);
        }
    }
}
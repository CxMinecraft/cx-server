﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Utils;
using Utils;

namespace CxServer.Communication.Login.ToClient
{
    public class LoginSuccess : Packet
    {
        public UUID UUID
        {
            get;
        }
        public string Username
        {
            get;
        }

        public LoginSuccess(UUID uuid, string username) : base(0x02, PacketState.Login, PacketSide.ToClient)
        {
            this.UUID = uuid;
            this.Username = username;
        }

        public override string ToString()
        {
            return string.Format("[LoginSuccess: UUID={0}, Username={1}]", UUID, Username);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeString(queue, UUID.ToString(true));
            PacketEncoding.EncodeString(queue, Username);
        }
    }
}
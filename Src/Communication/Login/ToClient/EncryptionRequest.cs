﻿using System;
using System.IO;
using System.Collections.Generic;

namespace CxServer.Communication.Login.ToClient
{
    public class EncryptionRequest : Packet
    {
        public string ServerID
        {
            get;
        }
        public byte[] PublicKey
        {
            get;
        }
        public byte[] VerifyToken
        {
            get;
        }

        public EncryptionRequest(string serverID, byte[] publicKey, byte[] verifyToken) : base(0x01, PacketState.Login, PacketSide.ToClient)
        {
            this.ServerID = serverID;
            this.PublicKey = publicKey;
            this.VerifyToken = verifyToken;
        }

        public override string ToString()
        {
            return string.Format("[EncryptionRequest: ServerID={0}, PublicKey={1}, VerifyToken={2}]", ServerID, PublicKey, VerifyToken);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeString(queue, ServerID);

            PacketEncoding.EncodeByteArray(queue, PublicKey);
            PacketEncoding.EncodeByteArray(queue, VerifyToken);
        }
    }
}
﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Chat;

namespace CxServer.Communication.Login.ToClient
{
    public class Disconnect : Packet
    {
        public ChatMessage Reason
        {
            get;
        }

        public Disconnect(ChatMessage reason) : base(0x00, PacketState.Login, PacketSide.ToClient)
        {
            this.Reason = reason;
        }

        public Disconnect(string reasonText) : this(new ChatMessagePlain(reasonText))
        {
        }

        public override string ToString()
        {
            return string.Format("[Disconnect: Reason={0}]", Reason);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeString(queue, Reason.ToJSON());
        }
    }
}
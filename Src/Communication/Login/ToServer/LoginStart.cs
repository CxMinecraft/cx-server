﻿using System;
using System.IO;
using System.Collections.Generic;

namespace CxServer.Communication.Login.ToServer
{
	public class LoginStart : Packet
	{
		public string Username
		{
			get;
		}

		public LoginStart(string username) : base(0x00, PacketState.Login, PacketSide.ToServer)
		{
			this.Username = username;
		}

		public override string ToString()
		{
			return string.Format("[LoginStart: Username={0}]", Username);
		}

		public override void ToBytes(Queue<byte> queue)
		{
			PacketEncoding.EncodeString(queue, Username);
		}
	}
}
﻿using System;
using System.IO;
using System.Collections.Generic;

namespace CxServer.Communication.Login.ToServer
{
	public class EncryptionResponse : Packet
	{
		public byte[] SharedKey
		{
			get;
		}
		public byte[] VerifyToken
		{
			get;
		}

		public EncryptionResponse(byte[] sharedKey, byte[] verifyToken) : base(0x01, PacketState.Login, PacketSide.ToServer)
		{
			this.SharedKey = sharedKey;
			this.VerifyToken = verifyToken;
		}

		public override string ToString()
		{
			return string.Format("[EncryptionResponse: SharedKey={0}, VerifyToken={1}]", SharedKey, VerifyToken);
		}

		public override void ToBytes(Queue<byte> queue)
		{
			PacketEncoding.EncodeVarInt(queue, SharedKey.Length);
			foreach(byte b in SharedKey)
				queue.Enqueue(b);

			PacketEncoding.EncodeVarInt(queue, VerifyToken.Length);
			foreach(byte b in VerifyToken)
				queue.Enqueue(b);
		}
	}
}
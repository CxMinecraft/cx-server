﻿using System;
using System.Collections.Generic;
using CxServer.Utils;
using CxServer.Chat;
using Utils;

namespace CxServer.Communication
{
    public static class PacketEncoding
    {
        #region VarInt

        public static int? DecodeVarInt(Queue<byte> buffer)
        {
            var i = VarInt.DecodeInt(buffer);
            if (i == null)
                return null;
            else
                return (int)i.Value;
        }

        public static void EncodeVarInt(Queue<byte> buffer, int value)
        {
            EncodeByteArrayWithoutLength(buffer, VarInt.EncodeInt((uint)value));
        }

        #endregion

        #region VarInt

        public static uint? DecodeUnsignedVarInt(Queue<byte> buffer)
        {
            var i = VarInt.DecodeInt(buffer);
            if (i == null)
                return null;
            else
                return (uint)i.Value;
        }

        public static void EncodeUnsignedVarInt(Queue<byte> buffer, uint value)
        {
            EncodeByteArrayWithoutLength(buffer, VarInt.EncodeInt(value));
        }

        #endregion

        #region VarLong

        public static long? DecodeVarLong(Queue<byte> buffer)
        {
            var l = VarInt.DecodeLong(buffer);
            if (l == null)
                return null;
            else
                return (long)l.Value;
        }

        public static void EncodeVarLong(Queue<byte> buffer, long value)
        {
            EncodeByteArrayWithoutLength(buffer, VarInt.EncodeLong((ulong)value));
        }

        #endregion

        #region VarLong

        public static ulong? DecodeUnsignedVarLong(Queue<byte> buffer)
        {
            var l = VarInt.DecodeLong(buffer);
            if (l == null)
                return null;
            else
                return (ulong)l.Value;
        }

        public static void EncodeUnsignedVarLong(Queue<byte> buffer, ulong value)
        {
            EncodeByteArrayWithoutLength(buffer, VarInt.EncodeLong(value));
        }

        #endregion

        #region String

        public static string DecodeString(Queue<byte> buffer)
        {
            int? length = DecodeVarInt(buffer);
            if (length == null || length.Value <= 0)
                return null;
            return System.Text.Encoding.UTF8.GetString(DecodeByteArray(buffer, length.Value));
        }

        public static void EncodeString(Queue<byte> buffer, string value)
        {
            byte[] bytes = value == null ? new byte[0] : System.Text.Encoding.UTF8.GetBytes(value);
            EncodeVarInt(buffer, bytes.Length);
            EncodeByteArrayWithoutLength(buffer, bytes);
        }

        #endregion

        #region Byte Array

        public static byte[] DecodeByteArray(Queue<byte> buffer)
        {
            return DecodeByteArray(buffer, DecodeVarInt(buffer).Value);
        }

        public static byte[] DecodeByteArray(Queue<byte> buffer, int length)
        {
            byte[] bytes = new byte[length];
            for (int i = 0; i < length; i++)
                bytes[i] = buffer.Dequeue();
            return bytes;
        }

        public static void EncodeByteArray(Queue<byte> buffer, byte[] values)
        {
            PacketEncoding.EncodeVarInt(buffer, values.Length);
            foreach (var b in values)
                buffer.Enqueue(b);
        }

        public static void EncodeByteArrayWithoutLength(Queue<byte> buffer, byte[] values)
        {
            foreach (var b in values)
                buffer.Enqueue(b);
        }

        public static void EncodeByteArray(Queue<byte> buffer, List<byte> values)
        {
            PacketEncoding.EncodeVarInt(buffer, values.Count);
            foreach (var b in values)
                buffer.Enqueue(b);
        }

        public static void EncodeByteArrayWithoutLength(Queue<byte> buffer, List<byte> values)
        {
            foreach (var b in values)
                buffer.Enqueue(b);
        }

        #endregion

        #region Boolean

        public static bool DecodeBoolean(Queue<byte> buffer)
        {
            return buffer.Dequeue() != 0x00;
        }

        public static void EncodeBoolean(Queue<byte> buffer, bool value)
        {
            buffer.Enqueue((byte)(value ? 0x01 : 0x00));
        }

        #endregion

        #region Byte

        public static sbyte DecodeByte(Queue<byte> buffer)
        {
            return (sbyte)(((int)buffer.Dequeue()) - 128);
        }

        public static void EncodeByte(Queue<byte> buffer, sbyte value)
        {
            buffer.Enqueue(BitConverter.GetBytes(value)[0]);
        }

        #endregion

        #region Unsigned Byte

        public static byte DecodeUnsignedByte(Queue<byte> buffer)
        {
            return buffer.Dequeue();
        }

        public static void EncodeUnsignedByte(Queue<byte> buffer, byte value)
        {
            buffer.Enqueue(value);
        }

        #endregion

        #region Short

        public static short DecodeShort(Queue<byte> buffer)
        {
            byte[] bytes = DecodeByteArray(buffer, 2);
            Array.Reverse(bytes);
            return BitConverter.ToInt16(bytes, 0);
        }

        public static void EncodeShort(Queue<byte> buffer, short value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            Array.Reverse(bytes);
            EncodeByteArrayWithoutLength(buffer, bytes);
        }

        #endregion

        #region Unsigned Short

        public static ushort DecodeUnsignedShort(Queue<byte> buffer)
        {
            byte[] bytes = DecodeByteArray(buffer, 2);
            Array.Reverse(bytes);
            return BitConverter.ToUInt16(bytes, 0);
        }

        public static void EncodeUnsignedShort(Queue<byte> buffer, ushort value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            Array.Reverse(bytes);
            EncodeByteArrayWithoutLength(buffer, bytes);
        }

        #endregion

        #region Int

        public static int DecodeInt(Queue<byte> buffer)
        {
            byte[] bytes = DecodeByteArray(buffer, 4);
            Array.Reverse(bytes);
            return BitConverter.ToInt32(bytes, 0);
        }

        public static void EncodeInt(Queue<byte> buffer, int value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            Array.Reverse(bytes);
            EncodeByteArrayWithoutLength(buffer, bytes);
        }

        #endregion

        #region Int

        public static uint DecodeUnsignedInt(Queue<byte> buffer)
        {
            byte[] bytes = DecodeByteArray(buffer, 4);
            Array.Reverse(bytes);
            return BitConverter.ToUInt32(bytes, 0);
        }

        public static void EncodeUnsignedInt(Queue<byte> buffer, uint value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            Array.Reverse(bytes);
            EncodeByteArrayWithoutLength(buffer, bytes);
        }

        #endregion

        #region Long

        public static long DecodeLong(Queue<byte> buffer)
        {
            byte[] bytes = DecodeByteArray(buffer, 8);
            Array.Reverse(bytes);
            return BitConverter.ToInt64(bytes, 0);
        }

        public static void EncodeLong(Queue<byte> buffer, long value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            Array.Reverse(bytes);
            EncodeByteArrayWithoutLength(buffer, bytes);
        }

        #endregion

        #region Float

        public static float DecodeFloat(Queue<byte> buffer)
        {
            byte[] bytes = DecodeByteArray(buffer, 4);
            Array.Reverse(bytes);
            return BitConverter.ToSingle(bytes, 0);
        }

        public static void EncodeFloat(Queue<byte> buffer, float value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            Array.Reverse(bytes);
            EncodeByteArrayWithoutLength(buffer, bytes);
        }

        #endregion

        #region Double

        public static double DecodeDouble(Queue<byte> buffer)
        {
            byte[] bytes = DecodeByteArray(buffer, 8);
            Array.Reverse(bytes);
            return BitConverter.ToDouble(bytes, 0);
        }

        public static void EncodeDouble(Queue<byte> buffer, double value)
        {
            byte[] bytes = BitConverter.GetBytes(value);
            Array.Reverse(bytes);
            EncodeByteArrayWithoutLength(buffer, bytes);
        }

        #endregion

        #region UUID

        public static UUID DecodeUUID(Queue<byte> buffer)
        {
            return new UUID(DecodeByteArray(buffer, 16));
        }

        public static void EncodeUUID(Queue<byte> buffer, UUID value)
        {
            EncodeByteArrayWithoutLength(buffer, value.ByteArray);
        }

        #endregion

        #region Angle

        /// <summary>
        /// 0 - 255
        /// </summary>
        public static byte DecodeAngleByte(Queue<byte> buffer)
        {
            return buffer.Dequeue();
        }

        /// <summary>
        /// 0.0 - 1.0
        /// </summary>
        public static float DecodeAngleFloat(Queue<byte> buffer)
        {
            return buffer.Dequeue() / 256f;
        }

        public static void EncodeAngleFloat(Queue<byte> buffer, float value)
        {
            value %= 1;
            buffer.Enqueue((byte)(value * 256));
        }

        #endregion

        #region String

        public static ChatMessage DecodeChat(Queue<byte> buffer)
        {
            return ChatMessage.FromJSON(DecodeString(buffer));
        }

        public static void EncodeChat(Queue<byte> buffer, ChatMessage value) => EncodeString(buffer, value.ToJSON());

        #endregion

        #region Position

        public static Position DecodePosition(Queue<byte> buffer) => new Position(DecodeLong(buffer));

        public static void EncodePosition(Queue<byte> buffer, Position position) => EncodeLong(buffer, position.ToLong());

        #endregion

        #region Velocity

        public static double DecodeVelocity(Queue<byte> buffer) => DecodeShort(buffer) / 8000d;

        public static void EncodeVelocity(Queue<byte> buffer, double velocity) => EncodeShort(buffer, (short)(velocity * 8000));

        #endregion

        #region Entity ID

        public static EntityID DecodeEntityID(Queue<byte> buffer)
        {
            var val = DecodeUnsignedVarInt(buffer);
            if (val == null)
                return 0;
            else
                return val.Value;
        }

        public static void EncodeEntityID(Queue<byte> buffer, EntityID id) => EncodeUnsignedVarInt(buffer, id.ID);

        #endregion

        #region Metadata

        public static EntityMetadata DecodeMetadata(Queue<byte> buffer)
        {
            throw new NotImplementedException();
        }

        public static void EncodeMetadata(Queue<byte> buffer, EntityMetadata metadata)
        {
            throw new NotImplementedException();
        }

        #endregion

        #region ItemStack

        public static ItemStack DecodeItemStack(Queue<byte> buffer)
        {
            throw new NotImplementedException();
        }

        public static void EncodeItemStack(Queue<byte> buffer, ItemStack item)
        {
            throw new NotImplementedException();
        }

        #endregion

    }
}
﻿using System;

namespace CxServer.Communication
{
    /// <summary>
    /// Messages used in client - server communication.<br>
    /// Maximum 16 chars.
    /// </summary>
    public static class MessageChannel
    {

        #region Reserved

        public const string Channel_Register = "REGISTER";
        public const string Channel_Unregister = "UNREGISTER";

        #endregion

        #region Internal

        public const string Minecraft_Brand = "MC|Brand";

        public const string Minecraft_CommandBlock = "MC|AdvCmd";
        public const string Minecraft_CommandBlockMinecart = "MC|AutoCmd";

        public const string Minecraft_BookEdit = "MC|BEdit";
        public const string Minecraft_BookOpen = "MC|BOpen";
        public const string Minecraft_BookSign = "MC|BSign";

        public const string Minecraft_Beacon = "MC|Beacon";

        public const string Minecraft_DebugPath = "MC|DebugPath";

        public const string Minecraft_ItemName = "MC|ItemName";
        public const string Minecraft_PickItem = "MC|PickItem";

        public const string Minecraft_StopSound = "MC|StopSound";

        public const string Minecraft_Struct = "MC|Struct";

        public const string Minecraft_TradeList = "MC|TrList";
        public const string Minecraft_TradeSell = "MC|TrSel";

        #endregion

        #region World Downloader

        public const string WorldDownloaderInit = "WDL|INIT";
        public const string WorldDownloaderControl = "WDL|CONTROL";
        public const string WorldDownloaderRequest = "WDL|REQUEST";

        #endregion

        #region WorldEdit - Client User Interface

        public const string WorldEditCUI = "WECUI";

        #endregion

        #region BungeeCord

        public const string BungeeCord = "BungeeCord";

        #endregion

        #region Forge Mod Loader

        public const string ForgeModLoader_HandShake = "FML|HS";

        #endregion

    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using Utils;
using Utils.Extensions;

namespace CxServer.Communication
{
    public abstract class Packet
    {
        public int ID
        {
            get;
        }

        public PacketState State
        {
            get;
        }

        public PacketSide Side
        {
            get;
        }

        protected Packet(int id, PacketState state, PacketSide side)
        {
            this.ID = id;
            this.State = state;
            this.Side = side;
        }

        public abstract void ToBytes(Queue<byte> queue);

        #region Enums

        public enum PacketState
        {
            HandShaking,
            Play,
            Status,
            Login
        }

        public enum PacketSide
        {
            ToClient = 0,
            ToServer = 1
        }

        #endregion

        #region Static

        /// <summary>
        /// Register parsers
        /// </summary>
        static Packet()
        {
            //Handshake
            {
                //To Server
                {
                    Packet.RegisterParser(0x00, PacketState.HandShaking, PacketSide.ToServer, (id, buffer) =>
                        new Communication.Handshake.ToServer.Handshake(
                        PacketEncoding.DecodeVarInt(buffer).Value,
                        PacketEncoding.DecodeString(buffer),
                        PacketEncoding.DecodeUnsignedShort(buffer),
                        (CxServer.Communication.Handshake.ToServer.Handshake.NextState)PacketEncoding.DecodeVarInt(buffer).Value
                    )
                    );
                }
            }
            //Status
            {
                //To Client
                {
                    Packet.RegisterParser(0x00, PacketState.Status, PacketSide.ToClient, (id, buffer) =>
                        new Communication.Status.ToClient.Response(
                        PacketEncoding.DecodeString(buffer)
                    )
                    );
                    Packet.RegisterParser(0x01, PacketState.Status, PacketSide.ToClient, (id, buffer) =>
                        new Communication.Status.ToClient.Pong(
                        PacketEncoding.DecodeLong(buffer)
                    )
                    );
                }
                //To Server
                {
                    Packet.RegisterParser(0x00, PacketState.Status, PacketSide.ToServer, (id, buffer) =>
                        new Communication.Status.ToServer.Request(
                    )
                    );
                    Packet.RegisterParser(0x01, PacketState.Status, PacketSide.ToServer, (id, buffer) =>
                        new Communication.Status.ToServer.Ping(
                        PacketEncoding.DecodeLong(buffer)
                    )
                    );
                }
            }
            //Login
            {
                //To Client
                {
                    Packet.RegisterParser(0x00, PacketState.Login, PacketSide.ToClient, (id, buffer) =>
                        new Communication.Login.ToClient.Disconnect(
                        PacketEncoding.DecodeString(buffer)
                    )
                    );
                    Packet.RegisterParser(0x01, PacketState.Login, PacketSide.ToClient, (id, buffer) =>
                        new Communication.Login.ToClient.EncryptionRequest(
                        PacketEncoding.DecodeString(buffer),
                        PacketEncoding.DecodeByteArray(buffer),
                        PacketEncoding.DecodeByteArray(buffer)
                    )
                    );
                    Packet.RegisterParser(0x02, PacketState.Login, PacketSide.ToClient, (id, buffer) =>
                        new Communication.Login.ToClient.LoginSuccess(
                        PacketEncoding.DecodeUUID(buffer),
                        PacketEncoding.DecodeString(buffer)
                    )
                    );
                }
                //To Server
                {
                    Packet.RegisterParser(0x00, PacketState.Login, PacketSide.ToServer, (id, buffer) =>
                        new Communication.Login.ToServer.LoginStart(
                        PacketEncoding.DecodeString(buffer)
                    )
                    );
                    Packet.RegisterParser(0x01, PacketState.Login, PacketSide.ToServer, (id, buffer) =>
                        new Communication.Login.ToServer.EncryptionResponse(
                        PacketEncoding.DecodeByteArray(buffer),
                        PacketEncoding.DecodeByteArray(buffer)
                    )
                    );
                }
            }
        }

        public static Packet ParsePacket(int id, PacketState state, PacketSide side, Queue<byte> buffer)
        {
            ParserDelegate parser;
            if (!_parsers.TryGetValue(new Tuple<int, PacketState, PacketSide>(id, state, side), out parser) || parser == null)
                return null;
            return parser(id, buffer);
        }

        public delegate Packet ParserDelegate(int id, Queue<byte> buffer);

        private static readonly Dictionary<Tuple<int, PacketState, PacketSide>, ParserDelegate> _parsers = new Dictionary<Tuple<int, PacketState, PacketSide>, ParserDelegate>();

        public static bool RegisterParser(int id, PacketState state, PacketSide side, ParserDelegate parser)
        {
            if (id < 0)
            {
                Console.Error.WriteLine("Failed to register parser - invalid ID '{0}'", id);
                return false;
            }
            if (!Enum.IsDefined(typeof(PacketState), state))
            {
                Console.Error.WriteLine("Failed to register parser - invalid packet state '{0}'", state);
                return false;
            }
            if (!Enum.IsDefined(typeof(PacketSide), side))
            {
                Console.Error.WriteLine("Failed to register parser - invalid packet side '{0}'", state);
                return false;
            }

            Tuple<int, PacketState, PacketSide> tuple = new Tuple<int, PacketState, PacketSide>(id, state, side);

            if (_parsers.ContainsKey(tuple))
            {
                Console.Error.WriteLine("Failed to register parser - ID is already registred ({0})", id);
                return true;
            }

            _parsers.Add(tuple, parser);
            return true;
        }

        #endregion
    }
}
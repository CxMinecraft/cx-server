﻿using System;
using System.IO;
using System.Collections.Generic;

namespace CxServer.Communication.Status.ToServer
{
    public class Ping : Packet
    {
        public long PingID
        {
            get;
        }

        public Ping(long id) : base(0x01, PacketState.Status, PacketSide.ToServer)
        {
            this.PingID = id;
        }

        public override string ToString()
        {
            return string.Format("[Ping: PingID={0}]", PingID);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeLong(queue, PingID);
        }
    }
}
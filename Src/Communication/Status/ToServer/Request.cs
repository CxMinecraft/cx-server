﻿using System;
using System.IO;
using System.Collections.Generic;

namespace CxServer.Communication.Status.ToServer
{
    public class Request : Packet
    {
        public Request() : base(0x00, PacketState.Status, PacketSide.ToServer)
        {
        }

        public override string ToString()
        {
            return "[Request]";
        }

        public override void ToBytes(Queue<byte> queue)
        {
        }
    }
}
﻿using System;
using System.IO;
using System.Collections.Generic;

namespace CxServer.Communication.Status.ToClient
{
    public class Pong : Packet
    {
        public long PingID
        {
            get;
        }

        public Pong(long id) : base(0x01, PacketState.Status, PacketSide.ToClient)
        {
            this.PingID = id;
        }

        public override string ToString()
        {
            return string.Format("[Pong: PingID={0}]", PingID);
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeLong(queue, PingID);
        }
    }
}
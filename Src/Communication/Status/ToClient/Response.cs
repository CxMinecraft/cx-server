﻿using System;
using System.IO;
using System.Collections.Generic;
using CxServer.Utils;
using CxServer.Settings.MOTD;
using Utils.Configurations;
using Utils.Extensions;

namespace CxServer.Communication.Status.ToClient
{
    public class Response : Packet
    {

        #region Variables

        private readonly string _response;

        public string ResponseJSON
        {
            get
            {
                if (ResponseConfiguration == null)
                    return _response;
                else
                    return ResponseConfiguration.SaveAsJSON();
            }
        }

        public Configuration ResponseConfiguration
        {
            get;
        }

        #endregion

        #region Constructors

        public Response(string response) : base(0x00, PacketState.Status, PacketSide.ToClient)
        {
            this._response = response;
            this.ResponseConfiguration = null;
        }

        public Response(IMOTD motd) : this(Server.MinecraftVersion, Server.ProtocolVersion, motd)
        {
        }

        public Response(int playersMax, int playersOnline, PlayerSample[] playersSample, string description, System.Drawing.Image iconPNG) : this(Server.MinecraftVersion, Server.ProtocolVersion, playersMax, playersOnline, playersSample, description, iconPNG)
        {
        }

        public Response(string versionName, int versionProtocol, IMOTD motd) : this(versionName, versionProtocol, motd.MaxPlayers, motd.OnlinePlayers, motd.Sample, motd.Description, motd.FavIcon)
        {
        }

        public Response(string versionName, int versionProtocol, int playersMax, int playersOnline, PlayerSample[] playersSample, string description, System.Drawing.Image iconPNG) : base(0x00, PacketState.Status, PacketSide.ToClient)
        {
            this._response = null;
            Configuration cfg = new Configuration();
            cfg.Set("version.name", versionName);
            cfg.Set("version.protocol", versionProtocol);

            cfg.Set("players.max", playersMax);
            cfg.Set("players.online", playersOnline);
            //TODO sample
            if (playersSample != null)
            {
                List<Configuration> cfg_list = new List<Configuration>(playersSample.Length);
                for (int i = 0; i < playersSample.Length; i++)
                {
                    Configuration c = new Configuration();
                    PlayerSample ps = playersSample[i];
                    c.Set("name", ps.Name);
                    c.Set("id", ps.UUID.ToString(true));
                    cfg_list.Add(c);
                }
                cfg.Set("players.sample", cfg_list);
            }

            //TODO change to Chat
            cfg.Set("description.text", description);

            if (iconPNG != null)
                cfg.Set("favicon", ImageUtils.EncodeBase64WithPrefix(iconPNG));

            this.ResponseConfiguration = cfg;
        }

        #endregion

        public override string ToString()
        {
            return string.Format("[Status: Response={0}]", ResponseJSON.Replace("\n", ""));
        }

        public override void ToBytes(Queue<byte> queue)
        {
            PacketEncoding.EncodeString(queue, ResponseJSON);
        }
    }
}
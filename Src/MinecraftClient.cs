﻿using System;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Collections.Generic;
using CxServer.Communication;
using CxServer.Settings;
using System.Linq;
using CxServer.Utils;
using CxServer.Entities.Living;
using Utils;
using Utils.Extensions;

namespace CxServer
{
    /// <summary>
    /// Connection to Minecraft client.
    /// </summary>
    public sealed class MinecraftClient
    {
        /// <summary>
        /// Player's username.<br>
        /// Only use in <see cref="CxServer.Communication.Packet.PacketState.Play"/>.
        /// </summary>
        public string Username
        {
            get;
            private set;
        }

        /// <summary>
        /// Player's unique ID.<br>
        /// Only use in <see cref="CxServer.Communication.Packet.PacketState.Play"/>.
        /// </summary>
        public UUID UUID
        {
            get;
            private set;
        }

        private TcpClient TcpClient
        {
            get;
        }

        /// <summary>
        /// Player's entity controlled by this client.<br>
        /// Only use in <see cref="CxServer.Communication.Packet.PacketState.Play"/>.
        /// </summary>
        public PlayerEntity Entity
        {
            get;
            private set;
        }

        public string ClientBrand
        {
            get;
            private set;
        }

        #region Compression

        public bool CompressionEnabled
        {
            get;
            set;
        }

        public int CompressionThreshold
        {
            get;
            set;
        }

        #endregion

        #region Constructors

        internal MinecraftClient(TcpClient client)
        {
            this.TcpClient = client;
        }

        #endregion

        #region Communication

        private Thread thread = null;

        /// <summary>
        /// Is communication compressed.<br>
        /// Only use in <see cref="CxServer.Communication.Packet.PacketState.Play"/>.
        /// </summary>
        public bool CompressedCommunication = false;

        private Packet ParseNextPacket(BinaryReader reader)
        {
            int packetLength = VarInt.ReadInt(reader);

            int packetID = VarInt.ReadInt(reader);

            Queue<byte> buffer = new Queue<byte>();
            while (packetLength-- > 0)
                buffer.Enqueue(reader.ReadByte());

            return Packet.ParsePacket(packetID, CurrentState, Packet.PacketSide.ToServer, buffer);
        }

        /// <summary>
        /// Send packet to client.
        /// </summary>
        /// <param name="packet">The packet.</param>
        public void SendPacket(Packet packet)
        {
            Console.WriteLine("{0};{1}", _writer == null, packet == null);
            Console.WriteLine("SendPacket");
            if (_writer == null || packet == null)
                return;

            Console.WriteLine("OUT: {0}", packet);

            Queue<byte> queue = new Queue<byte>();
            packet.ToBytes(queue);

            byte[] id_bytes = VarInt.WriteIntToByteArray(packet.ID);
            int length = id_bytes.Length + queue.Count;

            if (CompressionEnabled)
            {
                int uncompressedLength = queue.Count + id_bytes.Length;

                if (uncompressedLength >= CompressionThreshold)//compress
                {
                    var compress_id = CompressionUtils.Compress(id_bytes);
                    var compress_content = CompressionUtils.Compress(queue.ToArray());



                    //total length
                    VarInt.WriteIntToWriter((compress_id.Length + compress_content.Length + VarInt.CountIntBytes(uncompressedLength)), _writer);
                    //uncompressed length
                    VarInt.WriteIntToWriter(uncompressedLength, _writer);
                    //packet id
                    _writer.Write(compress_id);
                    //packet content
                    _writer.Write(compress_content);
                }
                else//do not compress
                {
                    //packet length
                    VarInt.WriteIntToWriter(length + 1, _writer);
                    //compressed data length
                    VarInt.WriteIntToWriter(0, _writer);
                    //packet id
                    _writer.Write(id_bytes);
                    //packet content
                    while (queue.Any())
                        _writer.Write(queue.Dequeue());
                    _writer.Flush();
                }
            }
            else
            {
                //packet length
                VarInt.WriteIntToWriter(length, _writer);
                //packet id
                _writer.Write(id_bytes);
                //packet content
                while (queue.Any())
                    _writer.Write(queue.Dequeue());
                _writer.Flush();
            }
        }

        private BinaryWriter _writer = null;

        /// <summary>
        /// Player's current state.
        /// </summary>
        public Packet.PacketState CurrentState = Packet.PacketState.HandShaking;

        #endregion

        /// <summary>
        /// Start on new thread.
        /// </summary>
        public void Start()
        {
            if (thread != null)
                return;

            thread = new Thread(() =>
            {
                try
                {
                    var stream = TcpClient.GetStream();
                    BinaryReader reader = new BinaryReader(stream);
                    _writer = new BinaryWriter(stream);

                    while (TcpClient.Connected)
                    {
                        Packet packet = ParseNextPacket(reader);

                        Console.WriteLine("NULL: {0}; {1}; {2}", packet == null, CurrentState, packet);

                        if (packet == null)
                            continue;

                        switch (CurrentState)
                        {
                            case Packet.PacketState.HandShaking:
                            default:
                                if (packet is Communication.Handshake.ToServer.Handshake)
                                {
                                    var handshake = (Communication.Handshake.ToServer.Handshake)packet;
                                    switch (handshake.Next)
                                    {
                                        case Communication.Handshake.ToServer.Handshake.NextState.Status:
                                            CurrentState = Packet.PacketState.Status;
                                            break;
                                        case Communication.Handshake.ToServer.Handshake.NextState.Login:
                                            CurrentState = Packet.PacketState.Login;
                                            break;
                                        default:
                                            TcpClient.Close();
                                            break;
                                    }
                                }
                                break;
                            case Packet.PacketState.Status:
                                if (packet is Communication.Status.ToServer.Request)
                                {
                                    //TODO player samples
                                    //SendPacket(new Communication.Status.ToClient.Response("1.10.2", 210, ServerSettings.MaxPlayers, Server.OnlinePlayersCount, null, ServerSettings.MOTD.Description, ServerSettings.MOTD.FavIcon));
                                    //SendPacket(new Communication.Status.ToClient.Response("1.10.2", 210, 10, 2, null, "MOTD", null));
                                    SendPacket(new Communication.Status.ToClient.Response("1.10.2", 210, ServerSettings.MOTD));
                                }
                                else if (packet is Communication.Status.ToServer.Ping)
                                {
                                    var ping = (Communication.Status.ToServer.Ping)packet;
                                    SendPacket(new Communication.Status.ToClient.Pong(ping.PingID));
                                    TcpClient.Close();
                                    break;
                                }
                                break;
                            case Packet.PacketState.Login:
                                if (packet is Communication.Login.ToServer.LoginStart)
                                {
                                    var loginStart = (Communication.Login.ToServer.LoginStart)packet;
                                    Username = loginStart.Username;

                                    if (Server.IsPlayerOnline(Username))
                                    {
                                        SendPacket(new Communication.Login.ToClient.Disconnect("Username already used."));
                                        TcpClient.Close();
                                        break;
                                    }
                                    //Get/Create UUID
                                    if (ServerSettings.OnlineMode)
                                    {
                                        throw new NotImplementedException("Online mode is not supporter (yet).");
                                    }
                                    else
                                    {
                                        UUID = UUID.GenerateUUID5(ServerSettings.CaseSensitivePlayerNames ? Username : Username.ToLowerInvariant());
                                        SendPacket(new Communication.Login.ToClient.LoginSuccess(UUID, Username));
                                    }
                                    //Switch from connecting to play
                                    CurrentState = Packet.PacketState.Play;
                                    //Add player to online players
                                    lock (Server._clients)
                                    {
                                        StringComparison comparasionType = ServerSettings.CaseSensitivePlayerNames ? StringComparison.InvariantCulture : StringComparison.InvariantCultureIgnoreCase;
                                        foreach (var c in Server._clients)
                                        {
                                            if (string.Equals(c.Username, Username, comparasionType))
                                            {
                                                SendPacket(new Communication.Play.ToClient.Disconnect("Username already used. Someone was faster."));
                                                TcpClient.Close();
                                                break;
                                            }
                                            if (c.UUID == UUID)
                                            {
                                                SendPacket(new Communication.Play.ToClient.Disconnect("Your UUID is already used."));
                                                TcpClient.Close();
                                                break;
                                            }
                                        }
                                        Server._clients.Add(this);
                                    }
                                    //Create player entity
                                    OfflinePlayer offline = Server.GetPlayer(UUID);
                                    this.Entity = new PlayerEntity(this, offline);
                                    if (!this.Entity.Spawn())
                                    {
                                        SendPacket(new Communication.Play.ToClient.Disconnect("Cannot spawn your entity."));
                                        TcpClient.Close();
                                        break;
                                    }
                                    //join packet
                                    SendPacket(new Communication.Play.ToClient.JoinGame(this.Entity, ServerSettings.MaxPlayers > byte.MaxValue ? byte.MaxValue : (byte)ServerSettings.MaxPlayers));
                                    //server brand
                                    SendPacket(new Communication.Play.ToClient.PluginMessageString(MessageChannel.Minecraft_Brand, "Cx"));
                                    //compass target
                                    this.Entity.CompassTarget = this.Entity.World.SpawnPosition;
                                    //playerAbilities
                                    SendPacket(new Communication.Play.ToClient.PlayerAbilities(this.Entity));
                                    //teleport
                                    this.Entity.Teleport(this.Entity.World.SpawnPositionVectorF);
                                    //DONE
                                }
                                break;
                            case Packet.PacketState.Play:
                                if (packet is Communication.Play.ToServer.PluginMessage)
                                {
                                    var pluginMessage = (Communication.Play.ToServer.PluginMessage)packet;

                                    if (pluginMessage.Channel == MessageChannel.Minecraft_Brand)
                                    {
                                        this.ClientBrand = pluginMessage.DataString;
                                    }

                                    //TODO call event and process in event
                                }
                                else if (packet is Communication.Play.ToServer.ClientSettings)
                                {
                                    var clientSettings = (Communication.Play.ToServer.ClientSettings)packet;

                                    clientSettings.Apply(this.Entity);
                                }
                                break;
                        }
                        /*
						 * 
						lock(_clients)
						{
							_clients.Add(mcc);
						}
						 */
                    }
                }
                catch (Exception e)
                {
                    TcpClient.Close();
                    Console.Error.WriteLine(e);
                }
            });
            thread.Start();
        }
    }
}
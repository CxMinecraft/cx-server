﻿using System;
using CxServer.BlocksAndItems.Blocks;
using CxServer.Utils;
using Utils.Vector;

namespace CxServer.Worlds
{
    public class Region
    {
        public const int SizeInChunks = 32;
        public const int SizeInBlocks = SizeInChunks * Chunk.Size;

        public int HeightInChunks;
        public int HeightInBlocks;

        private Chunk[,,] _chunks;

        public Region(int heightInChunks = 16)
        {
            if (heightInChunks <= 0)
                throw new ArgumentOutOfRangeException("heightInChunks");

            if (heightInChunks > 16)
                throw new ArgumentOutOfRangeException("heightInChunks", "Height maximum is 16 chunks / 256 blocks");

            this.HeightInChunks = heightInChunks;
            this.HeightInBlocks = heightInChunks * Chunk.Size;

            this._chunks = new Chunk[SizeInChunks, HeightInChunks, SizeInChunks];
        }

        public Chunk GetChunk(int localX, int localY, int localZ)
        {
            return this._chunks[localX, localY, localZ];
        }

        public Chunk GetChunkByBlock(int localBlockX, int localBlockY, int localBlockZ)
        {
            return this._chunks[localBlockX / Chunk.Size, localBlockY / Chunk.Size, localBlockZ / Chunk.Size];
        }

        #region Block ID

        public Block GetBlock(int localX, int localY, int localZ)
        {
            Chunk chunk = GetChunkByBlock(localX, localY, localZ);
            if (chunk == null)
                return null;
            return chunk.GetBlock(localX % Chunk.Size, localY % Chunk.Size, localZ % Chunk.Size);
        }

        public bool SetBlock(int localX, int localY, int localZ, Block b)
        {
            Chunk chunk = GetChunkByBlock(localX, localY, localZ);
            if (chunk == null)
                return false;
            chunk.SetBlock(localX % Chunk.Size, localY % Chunk.Size, localZ % Chunk.Size, b);
            return true;
        }

        #endregion

        #region Block Meta

        public byte GetBlockMeta(int localX, int localY, int localZ)
        {
            Chunk chunk = GetChunkByBlock(localX, localY, localZ);
            if (chunk == null)
                return 0;
            return chunk.GetBlockMeta(localX % Chunk.Size, localY % Chunk.Size, localZ % Chunk.Size);
        }

        public bool SetBlockMeta(int localX, int localY, int localZ, byte id)
        {
            Chunk chunk = GetChunkByBlock(localX, localY, localZ);
            if (chunk == null)
                return false;
            chunk.SetBlockMeta(localX % Chunk.Size, localY % Chunk.Size, localZ % Chunk.Size, id);
            return true;
        }

        #endregion

        #region Biome

        private readonly byte[,] _biomes = new byte[SizeInBlocks, SizeInBlocks];

        public byte GetBiomeID(int localX, int localZ)
        {
            return _biomes[localX, localZ];
        }

        public void SetBiome(int localX, int localZ, byte biomeID)
        {
            _biomes[localX, localZ] = biomeID;
        }

        #endregion

        #region For Packets

        public ChunkData[] GetChunkData(int localChunkX, int localChunkZ)
        {
            ChunkData[] data = new ChunkData[this.HeightInChunks];

            for (int y = 0; y < this.HeightInChunks; y++)
            {
                Chunk chunk = GetChunk(localChunkX, y, localChunkZ);
                if (chunk != null)
                    data[y] = chunk.ChunkData;
            }

            return data;
        }

        public byte[,] GetBiomes(int localChunkX, int localChunkZ)
        {
            byte[,] bytes = new byte[Chunk.Size, Chunk.Size];

            int lx = localChunkX * Chunk.Size;
            int lz = localChunkZ * Chunk.Size;

            for (int x = 0; x < Chunk.Size; x++)
                for (int z = 0; z < Chunk.Size; z++)
                    bytes[x, z] = _biomes[lx + x, lz + z];

            return bytes;
        }

        #endregion

        #region TileEntity

        public TileEntity GetTileEntity(Vector3 v) => GetTileEntity(v.X, v.Y, v.Z);

        public TileEntity GetTileEntity(int x, int y, int z)
        {
            throw new NotImplementedException("Region - Get Tile Entity");
        }

        public bool SetTileEntity(Vector3 v, TileEntity te) => SetTileEntity(v.X, v.Y, v.Z, te);

        public bool SetTileEntity(int x, int y, int z, TileEntity te)
        {
            throw new NotImplementedException("Region - Set Tile Entity");
        }

        #endregion

    }
}
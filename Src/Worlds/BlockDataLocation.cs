﻿using System;
using CxServer.BlocksAndItems.Blocks;
using CxServer.Utils;
using Utils.Vector;

namespace CxServer.Worlds
{
    public class BlockDataLocation
    {

        #region Constructor

        public BlockDataLocation(BlockData data, Location location) : this(data.Block, data.Meta, location, data.Biome, data.Tile)
        {
        }

        public BlockDataLocation(Block block, byte meta, Location location, Biome biome = null, TileEntity tile = null) : this(block, meta, location == null ? null : location.World, location == null ? Vector3f.Zero : location.Position, biome, tile)
        {
        }

        public BlockDataLocation(Block block, byte meta, World world, Vector3d position, Biome biome = null, TileEntity tile = null)
        {
            this.Block = block;
            this.Meta = meta;
            this.Biome = biome;
            this.Tile = tile;

            this.World = world;
            this.Position = position;
        }

        #endregion

        #region Variables

        public Block Block
        {
            get;
            private set;
        }

        public byte Meta
        {
            get;
            private set;
        }

        public Biome Biome
        {
            get;
            private set;
        }

        public TileEntity Tile
        {
            get;
            private set;
        }

        public BlockData Data
        {
            get
            {
                return new BlockData(Block, Meta, Biome, Tile);
            }
        }

        public Location Location
        {
            get
            {
                return new Location(World, Position);
            }
        }

        public World World
        {
            get;
            private set;
        }

        public Vector3d Position
        {
            get;
            private set;
        }

        #endregion

        public override bool Equals(object obj)
        {
            if (obj is BlockData)
            {
                var bd = (BlockData)obj;
                return bd.Block == this.Block && bd.Meta == this.Meta && bd.Tile == this.Tile;
            }
            else
                return false;
        }

        public override int GetHashCode()
        {
            if (Tile == null)
                return ((Block.GetHashCode() << 1) + Meta + Location.GetHashCode()) * 2;
            else
                return ((Block.GetHashCode() << 1) + Meta + Location.GetHashCode());
        }

    }
}
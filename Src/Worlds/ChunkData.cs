﻿using System;
using System.Collections.Generic;
using CxServer.Communication;
using CxServer.BlocksAndItems.Blocks;

namespace CxServer.Worlds
{
    public class ChunkData
    {

        public class FlexibleStorage
        {
            public FlexibleStorage(int bitsPerBlock, int size = Chunk.TotalBlocks) : this(bitsPerBlock, new long[roundToNearest(size * bitsPerBlock, 64) / 64])
            {

            }

            public FlexibleStorage(int bitsPerBlock, long[] data)
            {
                if (bitsPerBlock < 1 || bitsPerBlock > 32)
                {
                    throw new ArgumentOutOfRangeException("bitsPerBlock", "Outside of accepted range.");
                }

                this.BitsPerBlock = bitsPerBlock;
                this.Data = data;

                this.Size = this.Data.Length * 64 / this.BitsPerBlock;
                this._maxEntryValue = (1L << this.BitsPerBlock) - 1;
            }

            public long[] Data
            {
                get;
            }

            public int BitsPerBlock
            {
                get;
            }

            public int Size
            {
                get;
            }

            private readonly long _maxEntryValue;

            private static int roundToNearest(int value, int roundTo)
            {
                if (roundTo == 0)
                {
                    return 0;
                }
                else if (value == 0)
                {
                    return roundTo;
                }
                else
                {
                    if (value < 0)
                    {
                        roundTo *= -1;
                    }

                    int remainder = value % roundTo;
                    return remainder != 0 ? value + roundTo - remainder : value;
                }
            }

            public int this[int index]
            {
                get
                {
                    if (index < 0 || index > this.Size - 1)
                        throw new IndexOutOfRangeException();

                    int bitIndex = index * this.BitsPerBlock;
                    int startIndex = bitIndex / 64;
                    int endIndex = ((index + 1) * this.BitsPerBlock - 1) / 64;
                    int startBitSubIndex = bitIndex % 64;
                    if (startIndex == endIndex)
                        return (int)(this.Data[startIndex] >> startBitSubIndex & this._maxEntryValue);
                    else
                    {
                        int endBitSubIndex = 64 - startBitSubIndex;
                        return (int)((this.Data[startIndex] >> startBitSubIndex | this.Data[endIndex] << endBitSubIndex) & this._maxEntryValue);
                    }
                }
                set
                {
                    if (index < 0 || index > this.Size - 1)
                        throw new ArgumentOutOfRangeException();

                    if (value < 0 || value > this._maxEntryValue)
                        throw new ArgumentOutOfRangeException("value", "Value cannot be outside of accepted range.");

                    int bitIndex = index * this.BitsPerBlock;
                    int startIndex = bitIndex / 64;
                    int endIndex = ((index + 1) * this.BitsPerBlock - 1) / 64;
                    int startBitSubIndex = bitIndex % 64;
                    this.Data[startIndex] = this.Data[startIndex] & ~(this._maxEntryValue << startBitSubIndex) | ((long)value & this._maxEntryValue) << startBitSubIndex;
                    if (startIndex != endIndex)
                    {
                        int endBitSubIndex = 64 - startBitSubIndex;
                        this.Data[endIndex] = this.Data[endIndex] >> endBitSubIndex << endBitSubIndex | ((long)value & this._maxEntryValue) >> endBitSubIndex;
                    }
                }
            }
        }

        public ChunkData(Block[,,] block, byte[,,] block_meta)//TODO blockLight + sky light
        {
            List<int> states = new List<int>();
            FlexibleStorage storage = new FlexibleStorage(BitsPerBlock);

            for (int y = 0; y < Chunk.Size; y++)
            {
                for (int z = 0; z < Chunk.Size; z++)
                {
                    for (int x = 0; x < Chunk.Size; x++)
                    {
                        int s = GetBlockState(block[x, y, z].ID, block_meta[x, y, z]);
                        int id = BitsPerBlock <= 8 ? states.IndexOf(s) : s;
                        if (id == -1)
                        {
                            states.Add(s);

                            if (states.Count > 1 << BitsPerBlock)
                            {
                                BitsPerBlock++;
                            }

                            List<int> oldStates = states;
                            if (BitsPerBlock > 8)
                            {
                                oldStates = new List<int>(states);
                                states.Clear();
                                BitsPerBlock = 13;
                            }

                            FlexibleStorage oldStorage = storage;
                            storage = new FlexibleStorage(BitsPerBlock, storage.Size);
                            for (int i = 0; i < storage.Size; i++)
                            {
                                storage[i] = BitsPerBlock <= 8 ? oldStorage[i] : oldStates[i];
                            }

                            id = BitsPerBlock <= 8 ? states.IndexOf(s) : s;
                        }

                        storage[Index(x, y, z)] = id;
                    }
                }
            }

            this.Palette = states;
            this.Data = storage;
        }

        public List<int> Palette
        {
            get;
        }

        public FlexibleStorage Data
        {
            get;
        }

        private static int Index(int x, int y, int z)
        {
            return y << 8 | z << 4 | x;
        }

        private static int GetBlockState(ushort id, byte meta)
        {
            return (((int)id) << 4) | (meta & 0xF);
        }

        public byte BitsPerBlock
        {
            get;
            private set;
        } = 4;

        public Queue<byte> ToBytes(bool skyLight)
        {
            Queue<byte> queue = new Queue<byte>();

            PacketEncoding.EncodeUnsignedByte(queue, BitsPerBlock);

            //palette
            int paletteLength = Palette.Count;
            PacketEncoding.EncodeVarInt(queue, paletteLength);
            for (int i = 0; i < paletteLength; i++)
                PacketEncoding.EncodeVarInt(queue, Palette[i]);

            //data array
            PacketEncoding.EncodeVarInt(queue, Data.Size);
            for (int i = 0; i < Data.Size; i++)
                PacketEncoding.EncodeLong(queue, Data.Data[i]);

            //block light
            //TODO use real block light
            for (int i = 0; i < Chunk.TotalBlocks / 2; i++)
                PacketEncoding.EncodeUnsignedByte(queue, byte.MaxValue);

            //sky light
            //TODO use real sky light
            if (skyLight)
                for (int i = 0; i < Chunk.TotalBlocks / 2; i++)
                    PacketEncoding.EncodeUnsignedByte(queue, byte.MaxValue);


            return queue;
        }
    }
}
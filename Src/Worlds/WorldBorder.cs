﻿using System;
using CxServer.Utils;
using CxServer.Worlds;
using Utils.Vector;

namespace CxServer
{
    public sealed class WorldBorder
    {

        #region Constructors

        public WorldBorder(double centerX = 0, double centerY = 0, double diameter = 60000000, int teleportBoundary = 29999984, int warningTimeSeconds = 15, int warningBlocks = 5)
        {
            this._center = new Vector2d(centerX, centerY);
            this._diameterTarget = this._diameterSource = diameter;

            this._teleportBoundary = teleportBoundary;

            this._warningTimeSeconds = warningTimeSeconds;
            this._warningBlocks = warningBlocks;
        }

        public WorldBorder(Vector2d center, double diameter = 60000000, int teleportBoundary = 29999984, int warningTimeSeconds = 15, int warningBlocks = 5) : this(center.X, center.Y, diameter, teleportBoundary, warningTimeSeconds, warningBlocks)
        {
        }

        public WorldBorder(double centerX, double centerY, double diameter, int teleportBoundary, TimeSpan warningTime, int warningBlocks = 5) : this(centerX, centerY, diameter, teleportBoundary, (int)warningTime.TotalSeconds, warningBlocks)
        {
        }

        public WorldBorder(Vector2d center, double diameter, int teleportBoundary, TimeSpan warningTime, int warningBlocks = 5) : this(center.X, center.Y, diameter, teleportBoundary, (int)warningTime.TotalSeconds, warningBlocks)
        {
        }

        #endregion

        #region Center + Size

        private Vector2d _center;

        public Vector2d Center
        {
            get
            {
                return _center;
            }
            set
            {
                throw new NotImplementedException("WorldBorder - Center - Set");
            }
        }

        public double CenterX
        {
            get
            {
                return _center.X;
            }
        }

        public double CenterY
        {
            get
            {
                return _center.Y;
            }
        }

        public bool IsResizing
        {
            get
            {
                return DateTime.Now >= DiameterResizeEnd;
            }
        }

        private double _diameterSource;
        private double _diameterTarget;

        public DateTime DiameterResizeStart
        {
            get;
            private set;
        }

        public DateTime DiameterResizeEnd
        {
            get;
            private set;
        }

        public TimeSpan DiameterResizeDuration
        {
            get
            {
                return DiameterResizeEnd - DiameterResizeStart;
            }
        }

        public double CurrentDiameter
        {
            get
            {
                if (IsResizing)
                {
                    double difference = _diameterTarget - _diameterSource;
                    TimeSpan passed = DateTime.Now - DiameterResizeStart;
                    double percentage = passed.TotalMilliseconds / DiameterResizeDuration.TotalMilliseconds;
                    return _diameterSource + (difference * percentage);
                }
                else
                    return _diameterTarget;
            }
            set
            {
                Resize(value);
            }
        }

        public double TargetDiameter
        {
            get
            {
                return _diameterTarget;
            }
            set
            {
                if (IsResizing)
                    Resize(value, DiameterResizeEnd - DateTime.Now);
                else
                    Resize(value);
            }
        }

        public double SourceDiameter
        {
            get
            {
                if (IsResizing)
                    return _diameterSource;
                else
                    return _diameterTarget;
            }
        }

        #endregion

        #region Resize Function

        public void Resize(double targetDiameter, int moveDurationSeconds = 0) => Resize(TargetDiameter, new TimeSpan(0, 0, moveDurationSeconds), CurrentDiameter);

        public void Resize(double targetDiameter, TimeSpan moveDuration) => Resize(targetDiameter, moveDuration, CurrentDiameter);

        public void Resize(double targetDiameter, int moveDurationSeconds, double sourceDiameter) => Resize(targetDiameter, new TimeSpan(0, 0, moveDurationSeconds), sourceDiameter);

        public void Resize(double targetDiameter, TimeSpan moveDuration, double sourceDiameter)
        {
            this._diameterSource = sourceDiameter;
            this._diameterTarget = targetDiameter;

            this.DiameterResizeStart = DateTime.Now;
            this.DiameterResizeEnd = DateTime.Now + moveDuration;

            //TODO send packet to all players in associated world
            throw new NotImplementedException("World Border - Resize - send packet to all players in world");
        }

        #endregion

        private int _teleportBoundary;

        public int TeleportBoundary
        {
            get
            {
                return _teleportBoundary;
            }
            set
            {
                throw new NotImplementedException("WorldBorder - Teleport Boundary - Set");
            }
        }

        #region Warnings

        private int _warningTimeSeconds;

        public int WarningTimeSeconds
        {
            get
            {
                return _warningTimeSeconds;
            }
            set
            {
                throw new NotImplementedException("WorldBorder - Warning Time Seconds - Set");
            }
        }

        public TimeSpan WarningTime
        {
            get
            {
                return new TimeSpan(0, 0, _warningTimeSeconds);
            }
            set
            {
                WarningTimeSeconds = (int)value.TotalSeconds;
            }
        }

        private int _warningBlocks;

        public int WarningBlocks
        {
            get
            {
                return _warningBlocks;
            }
            set
            {
                throw new NotImplementedException("WorldBorder - Warning Blocks - Set");
            }
        }

        #endregion

        #region World

        internal World World = null;

        #endregion

        #region Inside

        public bool IsInside(Vector3 position) => IsInside(position.XZ);

        public bool IsInside(Vector3f position) => IsInside(position.XZ);

        public bool IsInside(Vector3d position) => IsInside(position.XZ);

        public bool IsInside(Vector2 position) => IsInside((Vector2d)position);

        public bool IsInside(Vector2f position) => IsInside((Vector2d)position);

        public bool IsInside(Vector2d position)
        {
            //return Math.Abs(position.X - CenterX) < (CurrentDiameter / 2) && Math.Abs(position.Y - CenterY) < (CurrentDiameter / 2);
            return Math.Max(Math.Abs(position.X - CenterX), Math.Abs(position.Y - CenterY)) < (CurrentDiameter / 2);
        }

        public bool IsInsideFullBlock(Vector3 position) => IsInsideFullBlock(position.XZ);

        public bool IsInsideFullBlock(Vector3f position) => IsInsideFullBlock(position.XZ);

        public bool IsInsideFullBlock(Vector3d position) => IsInsideFullBlock(position.XZ);

        public bool IsInsideFullBlock(Vector2 position) => IsInsideFullBlock((Vector2d)position);

        public bool IsInsideFullBlock(Vector2f position) => IsInsideFullBlock((Vector2d)position);

        public bool IsInsideFullBlock(Vector2d position)
        {
            //return Math.Ceiling(Math.Abs(position.X - CenterX)) < (CurrentDiameter / 2) && Math.Ceiling(Math.Abs(position.Y - CenterY)) < (CurrentDiameter / 2);
            return Math.Ceiling(Math.Max(Math.Abs(position.X - CenterX), Math.Abs(position.Y - CenterY))) < (CurrentDiameter / 2);
        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using CxServer.Settings;
using CxServer.Worlds.Generators;
using CxServer.Utils;
using CxServer.Entities;
using CxServer.Entities.Living;
using System.Linq;
using CxServer.BlocksAndItems.Blocks;
using CxServer.Communication;
using CxServer.Managers;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using Utils;
using Utils.Vector;

namespace CxServer.Worlds
{
    public sealed class World
    {

        #region EntityID

        public static EntityID LastEntityID
        {
            get;
            private set;
        }

        public static EntityID NextEntityID
        {
            get
            {
                if (LastEntityID == uint.MaxValue)
                    return 0;
                return ++LastEntityID;
            }
        }

        #endregion

        #region Constructors

        public World(string name) : this(name, WorldGeneratorManager.GetGenerator("default"))
        {
        }

        public World(string name, WorldGenerator generator, Dimension dimension = Dimension.Overworld, Difficulty difficulty = Difficulty.Normal, int heightInChunks = 16, Position spawnPosition = new Position()) : this(name, generator, dimension, difficulty, heightInChunks, UUID.GenerateUUID3(name), spawnPosition)
        {
        }

        private World(string name, WorldGenerator generator, Dimension dimension, Difficulty difficulty, int heightInChunks, UUID uuid, Position spawnPosition)
        {
            this.Name = name;

            this.Generator = generator.Clone();

            this.Dimension = dimension;
            this._difficulty = difficulty;

            if (heightInChunks > 16)
                throw new ArgumentOutOfRangeException(nameof(heightInChunks), "Height maximum is 16 chunks / 256 blocks");

            this.HeightInChunks = heightInChunks;
            this.HeightInBlocks = this.HeightInChunks * Chunk.Size;

            this.UUID = uuid;

            this._spawnPosition = spawnPosition;

            //TODO load spawn area
        }

        public World(WorldSettings settings) : this(settings.Name, WorldGeneratorManager.GetGenerator(settings.GeneratorName, settings.Seed, settings.GeneratorSettings), settings.Dimension, settings.Difficulty, settings.MaxBuildHeight / Chunk.Size + (settings.MaxBuildHeight % Chunk.Size == 0 ? 0 : 1), settings.UUID, settings.SpawnPosition)
        {
        }

        #endregion

        #region Save & Load

        public void SaveAll()
        {
            throw new NotImplementedException("World - Save All");
        }

        #endregion

        #region Identification

        public UUID UUID
        {
            get;
        }

        public string Name
        {
            get;
        }

        #endregion

        #region Settings

        private Position _spawnPosition;

        public Position SpawnPosition
        {
            get
            {
                return _spawnPosition;
            }
            set
            {
                _spawnPosition = value;
                throw new NotImplementedException("Change compass target for all players");
            }
        }

        public Vector3 SpawnPositionVector
        {
            get
            {
                return (Vector3)SpawnPosition;
            }
            set
            {
                SpawnPosition = (Position)value;
            }
        }

        public Vector3f SpawnPositionVectorF
        {
            get
            {
                return ((Vector3f)(Vector3)SpawnPosition) + Vector3f.Half;
            }
            set
            {
                SpawnPosition = (Position)(Vector3)value;
            }
        }

        public Dimension Dimension
        {
            get;
        }

        private Difficulty _difficulty;

        public Difficulty Difficulty
        {
            get
            {
                return _difficulty;
            }
            set
            {
                if (Enum.IsDefined(typeof(Difficulty), value))
                {
                    _difficulty = value;
                    SendPacketToAllPlayers(new Communication.Play.ToClient.ServerDifficulty(value));
                }
            }
        }

        public int HeightInChunks
        {
            get;
        }

        public int HeightInBlocks
        {
            get;
        }

        public WorldGenerator Generator;

        #endregion

        #region WorldBorder

        private WorldBorder _worldBorder = null;

        public WorldBorder WorldBorder
        {
            get
            {
                return _worldBorder ?? (_worldBorder = new WorldBorder());//TODO set this world
            }
            set
            {
                if (_worldBorder != null)
                    _worldBorder.World = null;

                _worldBorder = value ?? new WorldBorder();
                _worldBorder.World = this;
            }
        }

        public bool IsInsideBorder(Vector3 position) => IsInsideBorder(position.XZ);

        public bool IsInsideBorder(Vector3f position) => IsInsideBorder(position.XZ);

        public bool IsInsideBorder(Vector3d position) => IsInsideBorder(position.XZ);

        public bool IsInsideBorder(Vector2 position) => IsInsideBorder((Vector2d)position);

        public bool IsInsideBorder(Vector2f position) => IsInsideBorder((Vector2d)position);

        public bool IsInsideBorder(Vector2d position) => WorldBorder.IsInside(position);

        public bool IsInsideBorderFullBlock(Vector3 position) => IsInsideBorderFullBlock(position.XZ);

        public bool IsInsideBorderFullBlock(Vector3f position) => IsInsideBorderFullBlock(position.XZ);

        public bool IsInsideBorderFullBlock(Vector3d position) => IsInsideBorderFullBlock(position.XZ);

        public bool IsInsideBorderFullBlock(Vector2 position) => IsInsideBorderFullBlock((Vector2d)position);

        public bool IsInsideBorderFullBlock(Vector2f position) => IsInsideBorderFullBlock((Vector2d)position);

        public bool IsInsideBorderFullBlock(Vector2d position) => WorldBorder.IsInsideFullBlock(position);

        #endregion

        #region Regions

        private Dictionary<Vector2, Region> _regions = new Dictionary<Vector2, Region>();

        public bool IsRegionLoaded(int regionX, int regionZ) => _regions.ContainsKey(new Vector2(regionX, regionZ));

        public Region GetRegionFromBlockCoordinates(Vector3 v) => GetRegionFromBlockCoordinates(v.X, v.Z);

        public Region GetRegionFromBlockCoordinates(Vector2 v) => GetRegionFromBlockCoordinates(v.X, v.Y);

        public Region GetRegionFromBlockCoordinates(int x, int z) => GetRegion(x < 0 ? x / Region.SizeInBlocks - 1 : x / Region.SizeInBlocks, z < 0 ? z / Region.SizeInBlocks - 1 : z / Region.SizeInBlocks);

        public Region GetRegion(int regionX, int regionZ)
        {
            Region region;
            return _regions.TryGetValue(new Vector2(regionX, regionZ), out region) ? region : null;
        }

        public Region GetOrCreateRegionFromBlockCoordinates(Vector3 v) => GetOrCreateRegionFromBlockCoordinates(v.X, v.Z);

        public Region GetOrCreateRegionFromBlockCoordinates(Vector2 v) => GetOrCreateRegionFromBlockCoordinates(v.X, v.Y);

        public Region GetOrCreateRegionFromBlockCoordinates(int x, int z) => GetOrCreateRegion(x < 0 ? x / Region.SizeInBlocks - 1 : x / Region.SizeInBlocks, z < 0 ? z / Region.SizeInBlocks - 1 : z / Region.SizeInBlocks);

        public Region GetOrCreateRegion(int regionX, int regionZ)
        {
            Region region;
            //Use Loaded
            if (_regions.TryGetValue(new Vector2(regionX, regionZ), out region))
                return region;

            //Load
            if (LoadRegion(regionX, regionZ))
                return GetRegion(regionX, regionZ);

            //Generate
            int x1 = regionX < 0 ? regionX * Region.SizeInBlocks + 1 : regionX * Region.SizeInBlocks;
            int z1 = regionZ < 0 ? regionZ * Region.SizeInBlocks + 1 : regionZ * Region.SizeInBlocks;
            Generator.GenerateRegion(regionX, regionZ, region = new Region(HeightInChunks), x1, z1, x1 + Region.SizeInBlocks, z1 + Region.SizeInBlocks);
            return region;
        }

        public bool LoadRegionFromBlockCoordinates(Vector3 v) => LoadRegionFromBlockCoordinates(v.X, v.Z);

        public bool LoadRegionFromBlockCoordinates(Vector2 v) => LoadRegionFromBlockCoordinates(v.X, v.Y);

        public bool LoadRegionFromBlockCoordinates(int x, int z) => LoadRegion(x < 0 ? x / Region.SizeInBlocks - 1 : x / Region.SizeInBlocks, z < 0 ? z / Region.SizeInBlocks - 1 : z / Region.SizeInBlocks);

        public bool LoadRegion(int regionX, int regionZ)
        {
            return false;//TODO load region
                         //throw new NotImplementedException();
        }

        public bool UnloadRegionFromBlockCoordinates(Vector3 v) => UnloadRegionFromBlockCoordinates(v.X, v.Z);

        public bool UnloadRegionFromBlockCoordinates(Vector2 v) => UnloadRegionFromBlockCoordinates(v.X, v.Y);

        public bool UnloadRegionFromBlockCoordinates(int x, int z) => UnloadRegion(x < 0 ? x / Region.SizeInBlocks - 1 : x / Region.SizeInBlocks, z < 0 ? z / Region.SizeInBlocks - 1 : z / Region.SizeInBlocks);

        public bool UnloadRegion(int regionXx, int regionZ)
        {
            return false;//TODO unload region
                         //throw new NotImplementedException();
        }

        public bool SaveRegionFromBlockCoordinates(Vector3 v) => SaveRegionFromBlockCoordinates(v.X, v.Z);

        public bool SaveRegionFromBlockCoordinates(Vector2 v) => SaveRegionFromBlockCoordinates(v.X, v.Y);

        public bool SaveRegionFromBlockCoordinates(int x, int z) => SaveRegion(x < 0 ? x / Region.SizeInBlocks - 1 : x / Region.SizeInBlocks, z < 0 ? z / Region.SizeInBlocks - 1 : z / Region.SizeInBlocks);

        public bool SaveRegion(int regionX, int regionZ)
        {
            return false;//TODO load region
                         //throw new NotImplementedException();
        }

        #endregion

        #region Block

        public ushort GetBlockID(Vector3 v) => GetBlockID(v.X, v.Y, v.Z);

        public ushort GetBlockID(int x, int y, int z)
        {
            Block b = GetBlock(x, y, z);
            return b == null ? (ushort)0 : b.ID;
        }

        public string GetBlockCodename(Vector3 v) => GetBlockCodename(v.X, v.Y, v.Z);

        public string GetBlockCodename(int x, int y, int z) => BlocksAndItemsManager.GetBlockCodename(GetBlockID(x, y, z));

        public Block GetBlock(Vector3 v) => GetBlock(v.X, v.Y, v.Z);

        public Block GetBlock(int x, int y, int z)
        {
            Region region = GetRegionFromBlockCoordinates(x, z);
            if (region == null)
                return null;
            return region.GetBlock(x % Region.SizeInBlocks, y, z % Region.SizeInBlocks);
        }

        public bool SetBlock(Vector3 v, Block block) => SetBlock(v.X, v.Y, v.Z, block);

        public bool SetBlock(int x, int y, int z, Block block)
        {
            Region region = GetRegionFromBlockCoordinates(x, z);
            if (region == null)
                return false;
            return region.SetBlock(x % Region.SizeInBlocks, y, z % Region.SizeInBlocks, block);
        }

        public bool SetBlock(Vector3 v, string codename) => SetBlock(v.X, v.Y, v.Z, codename);

        public bool SetBlock(int x, int y, int z, string codename) => SetBlock(x, y, z, BlocksAndItemsManager.GetBlockID(codename));

        public bool SetBlock(Vector3 v, ushort id) => SetBlock(v.X, v.Y, v.Z, id);

        public bool SetBlock(int x, int y, int z, ushort id) => SetBlock(x, y, z, BlocksAndItemsManager.GetBlock(id));

        #endregion

        #region Block Meta

        public byte GetBlockMeta(Vector3 v) => GetBlockMeta(v.X, v.Y, v.Z);

        public byte GetBlockMeta(int x, int y, int z)
        {
            Region region = GetRegionFromBlockCoordinates(x, z);
            if (region == null)
                return 0;
            return region.GetBlockMeta(x % Region.SizeInBlocks, y, z % Region.SizeInBlocks);
        }

        public bool SetBlockMeta(Vector3 v, byte id) => SetBlockMeta(v.X, v.Y, v.Z, id);

        public bool SetBlockMeta(int x, int y, int z, byte id)
        {
            Region region = GetRegionFromBlockCoordinates(x, z);
            if (region == null)
                return false;
            return region.SetBlockMeta(x % Region.SizeInBlocks, y, z % Region.SizeInBlocks, id);
        }

        #endregion

        #region Biomes

        public Biome GetBiome(Vector2 v) => GetBiome(v.X, v.Y);

        public Biome GetBiome(Vector3 v) => GetBiome(v.X, v.Z);

        public Biome GetBiome(int x, int z) => Biome.ByID(GetBiomeID(x, z));

        public byte GetBiomeID(Vector2 v) => GetBiomeID(v.X, v.Y);

        public byte GetBiomeID(Vector3 v) => GetBiomeID(v.X, v.Z);

        public byte GetBiomeID(int x, int z)
        {
            Region region = GetRegionFromBlockCoordinates(x, z);
            if (region == null)
                return Biome.DefaultID;
            return region.GetBiomeID(x % Region.SizeInBlocks, z % Region.SizeInBlocks);
        }

        public bool SetBiome(Vector3 v, Biome biome) => SetBiome(v.X, v.Z, biome);

        public bool SetBiome(Vector2 v, Biome biome) => SetBiome(v.X, v.Y, biome);

        public bool SetBiome(int x, int z, Biome biome) => SetBiome(x, z, biome.ID);

        public bool SetBiome(Vector3 v, byte biomeID) => SetBiome(v.X, v.Z, biomeID);

        public bool SetBiome(Vector2 v, byte biomeID) => SetBiome(v.X, v.Y, biomeID);

        public bool SetBiome(int x, int z, byte biomeID)
        {
            Region region = GetRegionFromBlockCoordinates(x, z);
            if (region == null)
                return false;
            region.SetBiome(x % Region.SizeInBlocks, z % Region.SizeInBlocks, biomeID);
            return true;
        }

        #endregion

        #region TileEntity

        public TileEntity GetTileEntity(Vector3 v) => GetTileEntity(v.X, v.Y, v.Z);

        public TileEntity GetTileEntity(int x, int y, int z)
        {
            Region region = GetRegionFromBlockCoordinates(x, z);
            if (region == null)
                return null;
            return region.GetTileEntity(x % Region.SizeInBlocks, y % Region.SizeInBlocks, z % Region.SizeInBlocks);
        }

        public bool SetTileEntity(Vector3 v, TileEntity te) => SetTileEntity(v.X, v.Y, v.Z, te);

        public bool SetTileEntity(int x, int y, int z, TileEntity te)
        {
            Region region = GetRegionFromBlockCoordinates(x, z);
            if (region == null)
                return false;
            region.SetTileEntity(x % Region.SizeInBlocks, y % Region.SizeInBlocks, z % Region.SizeInBlocks, te);
            return true;
        }

        #endregion

        #region BlockData

        //TODO BlockData

        #endregion

        #region BlockDataLocation

        //TODO BlockDataLocation

        #endregion

        #region Entities

        private readonly List<Entity> _entities = new List<Entity>();

        /// <summary>
        /// Players NOT included!
        /// </summary>
        public IReadOnlyCollection<Entity> Entities
        {
            get
            {
                return _entities.AsReadOnly();
            }
        }

        /// <summary>
        /// Players included.
        /// </summary>
        public IReadOnlyCollection<Entity> AllEntities
        {
            get
            {
                var l = new List<Entity>(_entities);
                l.AddRange(Players);
                return l.AsReadOnly();
                //TODO keep in memory
            }
        }

        public IList<Entity> NearbyEntities(Entity entity, float maxDistance, bool includePlayers = false)
        {
            if (entity.World != this)
                throw new WorldNotMatchException(entity.World, this);

            return NearbyEntities((Vector3)entity.Position, maxDistance, includePlayers);
        }

        public IList<Entity> NearbyEntities(Vector3 point, float maxDistance, bool includePlayers = false)
        {
            List<Entity> entities = new List<Entity>();

            foreach (var ent in _entities)
                if (ent.Position.DistanceTo(point) <= maxDistance)
                    entities.Add(ent);

            if (includePlayers)
                foreach (var pla in Players)
                    if (pla.Position.DistanceTo(point) <= maxDistance)
                        entities.Add(pla);

            return entities;
        }

        public IList<Entity> NearbyEntities(Vector2 point, float maxDistance, bool includePlayers = false)
        {
            List<Entity> entities = new List<Entity>();

            foreach (var ent in _entities)
                if (ent.Position.XZ.DistanceTo(point) <= maxDistance)
                    entities.Add(ent);

            if (includePlayers)
                foreach (var pla in Players)
                    if (pla.Position.XZ.DistanceTo(point) <= maxDistance)
                        entities.Add(pla);

            return entities;
        }

        internal bool Spawn(Entity entity)
        {
            if (entity.Spawned)
                return false;

            EntityID eid = World.NextEntityID;
            if (eid == 0)
                return false;

            //TODO check for player => NPC

            entity.EntityID = eid;
            _entities.Add(entity);
            return true;
        }

        #endregion

        #region Item

        public EntityItem DropItem(ItemStack item, Vector3f position)
        {
            throw new NotImplementedException();
        }

        private static Random _itemRandom = new Random();

        public EntityItem DropItemNaturally(ItemStack item, Vector3 block_position)
        {
            Vector3f diff = new Vector3f(_itemRandom.Next(0, 16) / 16f, _itemRandom.Next(2, 14) / 16f, _itemRandom.Next(0, 16) / 16f);
            var ei = DropItem(item, block_position + diff);
            //TODO add force out of center
            return ei;
        }
        public EntityItem DropItemInCenterOfBlock(ItemStack item, Vector3 block_position) => DropItem(item, block_position + Vector3f.Half);

        #endregion

        #region Players

        public IEnumerable<PlayerEntity> Players
        {
            get
            {
                return Server.OnlinePlayers.Where(p => p.World == this);
            }
        }

        public IEnumerable<PlayerEntity> NearbyPlayers(Entity entity, float maxDistance)
        {
            if (entity.World != this)
                throw new WorldNotMatchException(entity.World, this);

            return NearbyPlayers((Vector3)entity.Position, maxDistance);
        }

        public IEnumerable<PlayerEntity> NearbyPlayers(Vector3 point, float maxDistance)
        {
            return Players.Where((p) => p.Position.DistanceTo(point) <= maxDistance);
        }

        public IEnumerable<PlayerEntity> NearbyPlayers(Vector2 point, float maxDistance)
        {
            return Players.Where((p) => p.Position.XZ.DistanceTo(point) <= maxDistance);
        }

        #endregion

        #region Exceptions

        public class WorldNotMatchException : Exception
        {
            public WorldNotMatchException(World world1, World world2) : base(string.Format("World '{0}' is not same as World '{1}'", world1.Name, world2.Name))
            {
                this.World1 = world1;
                this.World2 = world2;
            }

            public World World1
            {
                get;
            }

            public World World2
            {
                get;
            }
        }

        #endregion

        #region Explosion

        public void CreateExplosion(Entity entity, float power = 3.7f)
        {
            if (entity.World != this)
                throw new WorldNotMatchException(entity.World, this);

            CreateExplosion(entity.Position, power);
        }

        public void CreateExplosion(Entity entity, float power, float maxDamage)
        {
            if (entity.World != this)
                throw new WorldNotMatchException(entity.World, this);

            CreateExplosion(entity.Position, power, maxDamage);
        }

        public void CreateExplosion(Vector3d position, float power = 3.7f) => CreateExplosion(position, power, power * 2);

        public void CreateExplosion(Vector3d position, float power, float maxDamage)
        {
            power = Math.Abs(power);

            //TODO explosion
            throw new NotImplementedException("Create explosion");
        }

        #endregion

        internal void Update()
        {
            foreach (var entity in _entities)
                entity.Update();

            //TODO load/unload chunks/regions
        }

        #region Packets

        public void SendPacketToAllPlayers(Packet packet)
        {
            foreach (PlayerEntity player in Players)
                player.SendPacket(packet);
        }

        public void SendPacketToAllPlayersWithPermission(Packet packet, string permission)
        {
            foreach (PlayerEntity player in Players)
                if (player.HasPermission(permission))
                    player.SendPacket(packet);
        }

        public void SendPacketToAllPlayersWithAnyPermission(Packet packet, params string[] permission)
        {
            foreach (PlayerEntity player in Players)
                if (player.HasAnyPermission(permission))
                    player.SendPacket(packet);
        }

        public void SendPacketToAllPlayersWithAllPermissions(Packet packet, params string[] permission)
        {
            foreach (PlayerEntity player in Players)
                if (player.HasAllPermission(permission))
                    player.SendPacket(packet);
        }

        #endregion

    }
}
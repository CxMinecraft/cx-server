﻿using System;
using CxServer.BlocksAndItems.Blocks;

namespace CxServer.Worlds
{
    public class BlockData
    {

        #region Constructor

        public BlockData(Block block, byte meta, Biome biome = null, TileEntity tile = null)
        {
            this.Block = block;
            this.Meta = meta;
            this.Biome = biome;
            this.Tile = tile;
        }

        #endregion

        #region Variables

        public Block Block
        {
            get;
            private set;
        }

        public byte Meta
        {
            get;
            private set;
        }

        public Biome Biome
        {
            get;
            private set;
        }

        public TileEntity Tile
        {
            get;
            private set;
        }

        #endregion

        public override string ToString()
        {
            return string.Format("[BlockData: Block={0}, Meta={1}, Tile={2}]", Block, Meta, Tile);
        }

        public override bool Equals(object obj)
        {
            if (obj is BlockData)
            {
                var bd = (BlockData)obj;
                return bd.Block == this.Block && bd.Meta == this.Meta && bd.Tile == this.Tile;
            }
            else
                return false;
        }

        public override int GetHashCode()
        {
            if (Tile == null)
                return ((Block.GetHashCode() << 1) + Meta) * 2;
            else
                return ((Block.GetHashCode() << 1) + Meta);
        }

        #region Operator

        public static BlockDataLocation operator +(BlockData data, CxServer.Utils.Location loc)
        {
            return new BlockDataLocation(data, loc);
        }

        public static explicit operator BlockData(BlockDataLocation bdl)
        {
            return bdl.Data;
        }

        #endregion

    }
}
﻿using System;
using CxServer.BlocksAndItems.Blocks;

namespace CxServer.Worlds
{
    public class Chunk
    {
        public const int Size = 16;
        public const int TotalBlocks = Size * Size * Size;

        public Chunk()
        {
        }

        #region Block ID

        private readonly Block[,,] _blocks_id = new Block[Chunk.Size, Chunk.Size, Chunk.Size];

        public Block GetBlock(int x, int y, int z)
        {
            modified = true;
            return _blocks_id[x, y, z];
        }

        public void SetBlock(int x, int y, int z, Block b)
        {
            _blocks_id[x, y, z] = b;
        }

        #endregion

        #region BlockMeta

        private readonly byte[,,] _blocks_meta = new byte[Chunk.Size, Chunk.Size, Chunk.Size];

        public byte GetBlockMeta(int x, int y, int z)
        {
            modified = true;
            return _blocks_meta[x, y, z];
        }

        public void SetBlockMeta(int x, int y, int z, byte id)
        {
            _blocks_meta[x, y, z] = id;
        }

        #endregion

        private bool modified = true;

        private ChunkData _chunkData = null;

        public ChunkData ChunkData
        {
            get
            {
                if (modified)
                {
                    _chunkData = new ChunkData(_blocks_id, _blocks_meta);
                    modified = false;
                }
                return _chunkData;
            }
        }
    }
}
﻿using System;
using CxServer.BlocksAndItems.Blocks;
using System.Collections.Generic;
using System.Text;
using CxServer.Managers;

namespace CxServer.Worlds.Generators
{
    public class FlatWorldGenerator : WorldGenerator
    {

        #region Constructors

        public FlatWorldGenerator(string seed, params MapLayer[] layers)
        {
            this.Seed = seed;

            if (layers == null || layers.Length == 0)
                throw new ArgumentNullException("layers");

            List<MapLayer> l = new List<MapLayer>();

            for (int i = 0; i < layers.Length; i++)
                if (layers[i].Height != 0)
                    l.Add(layers[i]);

            this.Layers = l.ToArray();

            ushort spawnHeight = 0;
            foreach (var ml in Layers)
                spawnHeight += ml.Height;
            this.SpawnHeight = spawnHeight;

            if (this.SpawnHeight == 0)
                throw new ArgumentOutOfRangeException("layers");
        }

        public FlatWorldGenerator(string seed, string settings)
        {
            this.Seed = seed;
        }

        #endregion

        #region Layers

        public MapLayer[] Layers
        {
            get;
        }

        public struct MapLayer
        {
            public Block Block
            {
                get;
            }

            public byte Height
            {
                get;
            }

            public MapLayer(Block block, byte height = 1)
            {
                this.Block = block;
                this.Height = height;
            }

            public MapLayer(ushort blockID, byte height = 1) : this(BlocksAndItemsManager.GetBlock(blockID), height)
            {
            }

            public override string ToString()
            {
                return string.Format("[MapLayer: BlockID={0}, Height={1}]", Block, Height);
            }

            public override int GetHashCode()
            {
                return Height + (Block.ID << 8);
            }

            public override bool Equals(object obj)
            {
                if (obj is MapLayer)
                {
                    var layer = (MapLayer)obj;
                    return this.Block == layer.Block && this.Height == layer.Height;
                }
                else
                    return false;
            }
        }

        #endregion

        #region WorldGenerator implementation

        public override void GenerateRegion(int rX, int rZ, Region region, int x1, int z1, int x2, int z2)
        {
            int y = 0;
            for (int i = 0; i < Layers.Length; i++)
            {
                var layer = Layers[i];
                Block b = layer.Block;
                int height = layer.Height;
                for (int y0 = 0; y0 < height; y0++)
                {
                    for (int x = x1; x < x2; x++)
                        for (int z = z1; z < z2; z++)
                            region.SetBlock(x, y, z, b);
                    y++;
                }
            }
        }

        public override WorldGenerator Clone()
        {
            return new FlatWorldGenerator(Seed, GeneratorSettings);
        }

        public override WorldGenerator Clone(string seed, string settings)
        {
            return new FlatWorldGenerator(seed, settings);
        }

        public override string GeneratorSettings
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < Layers.Length; i++)
                {
                    var ml = Layers[i];

                    sb.Append(ml.Height);
                    sb.Append('x');
                    sb.Append(ml.Block.ID);
                    sb.Append(';');
                }
                return sb.ToString();
            }
        }

        public override string Seed
        {
            get;
        }

        public override string TypeName
        {
            get
            {
                return "flat";
            }
        }

        public override ushort SpawnHeight
        {
            get;
        }

        #endregion
    }
}


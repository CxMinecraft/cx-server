﻿using System;

namespace CxServer.Worlds.Generators
{
    public abstract class WorldGenerator
    {
        public abstract void GenerateRegion(int rX, int rZ, Region region, int x1, int z1, int x2, int z2);

        public abstract WorldGenerator Clone();

        public abstract WorldGenerator Clone(string seed, string settings);

        public abstract string GeneratorSettings
        {
            get;
        }

        public abstract string Seed
        {
            get;
        }

        public abstract string TypeName
        {
            get;
        }

        public abstract ushort SpawnHeight
        {
            get;
        }
    }
}
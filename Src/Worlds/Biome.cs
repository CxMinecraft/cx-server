﻿using System;

namespace CxServer.Worlds
{
    public class Biome
    {
        //TODO change to struct/enum?

        /// <summary>
        /// Use this ID if instance is null (and you need placeholder ID).<br>
        /// This is ID of Void Biome.
        /// </summary>
        public const byte DefaultID = 127;

        public static Biome ByID(byte id)
        {
            throw new NotImplementedException();
        }

        public static Biome ByName(string name)
        {
            throw new NotImplementedException();
        }

        #region Constructors

        public Biome(byte id, string name)
        {
            this.ID = id;
            this.Name = name;

            //TODO autoregister
        }

        #endregion

        #region Variables

        public byte ID
        {
            get;
        }

        public string Name
        {
            get;
        }

        #endregion

    }
}
﻿using System;
using CxServer.Utils;
using CxServer.Chat;
using CxServer.Entities.Living;
using System.Collections.Generic;
using Utils;

namespace CxServer
{
    public class BossBar
    {

        #region UUIDs

        private static ulong lastUUID = 1;

        public static UUID LastUUID
        {
            get
            {
                return new UUID(lastUUID);
            }
        }
        public static UUID NextUUID
        {
            get
            {
                return new UUID(++lastUUID);
            }
        }

        #endregion

        #region Constructors

        public BossBar(ChatMessage title, float health, BossBarColor color = BossBarColor.Pink, BossBarDivision division = BossBarDivision.NoDivision, BossBarFlags flags = BossBarFlags.NONE) : this(NextUUID, title, health, color, division, flags)
        {
        }

        public BossBar(UUID uuid, ChatMessage title, float health, BossBarColor color, BossBarDivision division, BossBarFlags flags)
        {
            this.UUID = uuid;
            this._title = title;
            this._health = Math.Min(Math.Max(health, 0f), 1f);
            this._color = color;
            this._division = division;
            this._flags = flags;
        }

        #endregion

        #region Variables

        public UUID UUID
        {
            get;
        }

        private ChatMessage _title;
        public ChatMessage Title
        {
            get
            {
                return _title;
            }
            set
            {
                UpdateTitle(value);
            }
        }

        private float _health;
        public float Health
        {
            get
            {
                return _health;
            }
            set
            {
                UpdateHealth(value);
            }
        }

        private BossBarColor _color;
        public BossBarColor Color
        {
            get
            {
                return _color;
            }
            set
            {
                UpdateStyle(value, _division);
            }
        }

        private BossBarDivision _division;
        public BossBarDivision Division
        {
            get
            {
                return _division;
            }
            set
            {
                UpdateStyle(_color, value);
            }
        }

        private BossBarFlags _flags;
        public BossBarFlags Flags
        {
            get
            {
                return _flags;
            }
            set
            {
                UpdateFlags(value);
            }
        }

        #endregion

        #region Functions

        public void UpdateHealth(float health)
        {
            this._health = Math.Min(Math.Max(health, 0f), 1f);
            //TODO update
        }

        public void UpdateTitle(ChatMessage title)
        {
            this._title = title;
            //TODO update
        }

        public void UpdateStyle(BossBarColor color, BossBarDivision division)
        {
            this._color = Enum.IsDefined(typeof(BossBarColor), color) ? color : BossBarColor.Yellow;
            this._division = division;
            //TODO update
        }

        public void UpdateFlags(BossBarFlags flags)
        {
            this._flags = flags;
            //TODO update
        }

        #endregion

        #region Update

        private readonly List<PlayerEntity> _players = new List<PlayerEntity>();

        internal void AddPlayer(PlayerEntity player)
        {
            _players.Add(player);
            //TODO send packet
        }

        internal void RemovePlayer(PlayerEntity player)
        {
            //TODO
        }

        #endregion

        #region Utils

        public enum BossBarColor
        {
            Pink = 0,
            Blue = 1,
            Red = 2,
            Green = 3,
            Yellow = 4,
            Purple = 5,
            White = 6
        }

        public enum BossBarDivision
        {
            NoDivision = 0,
            Notches6 = 1,
            Notches10 = 2,
            Notches12 = 3,
            Notches20 = 4
        }

        [Flags]
        public enum BossBarFlags
        {
            NONE = 0x0,
            DarkenSky = 0x1,
            PlayEndMusic = 0x2
        }

        #endregion

        public override string ToString()
        {
            return string.Format("[BossBar: UUID={0}, Title={1}, Health={2}, Color={3}, Division={4}, Flags={5}]", UUID, Title, Health, Color, Division, Flags);
        }

        public override int GetHashCode()
        {
            return UUID.GetHashCode();
        }

        public override bool Equals(object obj)
        {
            if (obj is BossBar)
            {
                BossBar bb = (BossBar)obj;
                return this.UUID == bb.UUID;
            }
            else
                return false;
        }

    }
}
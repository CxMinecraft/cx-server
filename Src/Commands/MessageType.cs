﻿using System;

namespace CxServer.Commands
{
    public enum MessageType
    {
        /// <summary>
        /// Chat box.
        /// Works only when chat enabled.
        /// </summary>
        Chat = 0,
        /// <summary>
        /// Chat box.
        /// Works when chat enabled or in commands only mode.
        /// </summary>
        Command = 1,
        /// <summary>
        /// Old style formatting recommended.
        /// </summary>
        AboveHotbar = 2
    }
}
﻿using System;

namespace CxServer.Commands
{
    public interface ICommandSender
    {
        void SendMessage(string message, MessageType type = MessageType.Chat);

        void SendRawMessage(Chat.ChatMessage message, MessageType type = MessageType.Chat);

        bool HasPermission(string permission);
    }
}
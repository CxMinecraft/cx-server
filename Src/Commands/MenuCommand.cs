﻿using System;
using CxServer.Managers;
using System.Text.RegularExpressions;
using CxServer.Commands;
using System.Collections.Generic;

namespace CxServer.Commands
{
    public class MenuCommand
    {
        public MenuCommand(Command command)
        {
            this.Command = command;
        }

        public Command Command
        {
            get;
        }

        public delegate void MenuItemDelegate(ICommandSender sender, string command, Dictionary<string, string> variable_parameters);

        public void AddSubmenu<T>(string format, MenuItemDelegate toExecute) where T : ICommandSender
        {
            //add <player> [distance=3] [world]
            Parameter[] parameters = Parameter.Parse(format);
            if (parameters == null)
                throw new NullReferenceException("Failed to parse parameters");

            //TODO check if is reachable ?

            _items.Add(new MenuItem(typeof(T), parameters, toExecute));
        }

        /// <summary>
        /// Parse parameters
        /// </summary>
        /// <param name="format">Command format (without command name and names of parts must fit regex [a-zA-Z0-9_]+).</param>
        /// <param name="toExecute">Delegate to execute when this submenu is used</param>
        public void AddSubmenu(string format, MenuItemDelegate toExecute)
        {
            //add <player> [distance=3] [world]
            Parameter[] parameters = Parameter.Parse(format);
            if (parameters == null)
                throw new NullReferenceException("Failed to parse parameters");

            //TODO check if is reachable ?

            _items.Add(new MenuItem(parameters, toExecute));
        }

        private List<MenuItem> _items
        {
            get;
        } = new List<MenuItem>();

        /// <summary>
        /// Use this for <see cref="CxServer.Commands.Command.Execute"/>
        /// </summary>
        /// <param name="sender">Sender of this command.</param>
        /// <param name="command">Command (name should be same as in constructor). <b>IGNORED</b>/</param>
        /// <param name="args">Arguments of command.</param>
        public void ProcessCommand(ICommandSender sender, Command command, string[] args)
        {
            throw new NotImplementedException();
        }

        #region MenuItem

        private class MenuItem
        {
            public MenuItem(Parameter[] parameters, MenuItemDelegate @delegate) : this(null, parameters, @delegate)
            {
            }

            public MenuItem(Type sender_type, Parameter[] parameters, MenuItemDelegate @delegate)
            {
                this.SenderType = sender_type;
                this.Parameters = parameters;
                this.Delegate = @delegate;
            }

            public Type SenderType
            {
                get;
            }

            public Parameter[] Parameters
            {
                get;
            }

            public MenuItemDelegate Delegate
            {
                get;
            }
        }

        #endregion

        #region Parameters

        private class Parameter
        {
            public Parameter(string name, ParameterType type = ParameterType.Static, string defaultValue = null)
            {
                this.Name = name;
                this.Type = type;
                this.DefaultValue = defaultValue;
            }

            public string Name
            {
                get;
            }

            public ParameterType Type
            {
                get;
            }

            public string DefaultValue
            {
                get;
            }

            public bool HasDefaultValue
            {
                get
                {
                    return Type == ParameterType.Optional && string.IsNullOrEmpty(DefaultValue);
                }
            }

            public override string ToString()
            {
                switch (Type)
                {
                    default:
                        return Name;
                    case ParameterType.Requied:
                        return string.Format("<{0}>", Name);
                    case ParameterType.Optional:
                        return string.Format(HasDefaultValue ? "[{0}={1}]" : "[{0}]", Name, DefaultValue);
                }
            }

            /// <summary>
            /// Parse parameters
            /// </summary>
            /// <param name="format">Command format (without command name and names of parts must fit regex [a-zA-Z0-9_]+).</param>
            public static Parameter[] Parse(string format)
            {
                string[] raw = format.Split(' ');
                Parameter[] parameters = new Parameter[raw.Length];

                for (int i = 0; i < raw.Length; i++)
                {
                    parameters[i] = ParseSingleParameter(raw[i]);
                    if (parameters[i] == null)
                        return null;//TODO throw exception with parameter index
                }

                return parameters;
            }

            public static readonly string Format = "^[a-zA-Z0-9_]+$";

            public static Parameter ParseSingleParameter(string parameter)
            {
                if (string.IsNullOrEmpty(parameter))
                    return null;
                if (parameter[0] == '<')
                {
                    if (parameter[parameter.Length - 1] == '>')
                    {
                        parameter = parameter.Substring(1, parameter.Length - 2);
                        if (Regex.IsMatch(parameter, Format))
                            return new Parameter(parameter, ParameterType.Requied);
                        else
                            return null;
                    }
                    else
                        return null;
                }
                if (parameter[0] == '[')
                {
                    if (parameter[parameter.Length - 1] == ']')
                    {
                        //TODO optional
                        parameter = parameter.Substring(1, parameter.Length - 2);

                        int optional_index = parameter.IndexOf('=');
                        if (optional_index != -1)//no optional part
                        {
                            if (Regex.IsMatch(parameter, Format))
                                return new Parameter(parameter, ParameterType.Optional);
                            else
                                return null;
                        }
                        else//has optional part
                        {
                            string val = optional_index == parameter.Length - 1 ? null : parameter.Substring(optional_index + 1);
                            parameter = parameter.Substring(0, optional_index);
                            if (Regex.IsMatch(parameter, Format))
                                return new Parameter(parameter, ParameterType.Optional, val);
                            else
                                return null;
                        }
                    }
                    else
                        return null;
                }

                if (Regex.IsMatch(parameter, Format))
                    return new Parameter(parameter, ParameterType.Static);
                else
                    return null;
            }
        }

        private enum ParameterType
        {
            Static,
            /// <summary>
            /// Using <<br>
            /// Can have default value
            /// </summary>
            Requied,
            /// <summary>
            /// Using [<br>
            /// Can have default value (after =)
            /// </summary>
            Optional
        }

        #endregion

    }
}
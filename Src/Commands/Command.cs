﻿using System;
using System.Collections.Generic;
using CxServer.Managers;

namespace CxServer.Commands
{
    public class Command
    {

        #region Constructors

        internal Command(string name)
        {
            this.Name = name;
        }

        #endregion

        #region Variables

        public string Name
        {
            get;
            private set;
        }

        internal List<string> _aliases = new List<string>();

        public string[] Aliases
        {
            get
            {
                return _aliases.ToArray();
            }
        }

        #endregion

        public void AddAlias(string alias)
        {
            alias = alias.ToLowerInvariant();

            if (CommandManager.GetCommand(alias) != null)
                throw new CommandAlreadyRegistredException(alias);

            _aliases.Add(alias);
        }

        #region Events

        public EventsManager.Event<ICommandSender, Command, string[]> Execute;

        public delegate void CommandEvent(ICommandSender sender, Command command, string[] args);

        #endregion

        #region Exceptions

        public class CommandAlreadyRegistredException : Exception
        {
            public CommandAlreadyRegistredException(string command)
            {
                this.Command = command;
            }

            public string Command
            {
                get;
                private set;
            }

            public override string Message
            {
                get
                {
                    return string.Format("Command '{0}' is already registred.", Command);
                }
            }
        }

        #endregion

    }
}
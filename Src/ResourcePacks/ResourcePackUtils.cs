﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using System.IO;
using CxServer.Settings;
using System.Collections.Generic;
using CxServer.Entities.Living;
using CxServer.Utils;
using Utils.Extensions;

namespace CxServer.ResourcePacks
{
    public static class ResourcePackUtils
    {

        #region List

        /// <summary>
        /// All ResourcePacks availible (includes extension)
        /// </summary>
        public static string[] AllResourcePacks
        {
            get
            {
                List<string> list = new List<string>();
                foreach (string f in Directory.GetFiles(ServerSettings.ResourcePackFolder, "*.zip", SearchOption.TopDirectoryOnly))
                {
                    string fs = f.Substring(ServerSettings.ResourcePackFolder.Length + 1);
                    if (Regex.IsMatch(fs, ServerSettings.ResourcePackRegex) && Regex.IsMatch(fs, ServerSettings.ResourcePackListRegex))
                        list.Add(fs);
                }

                return list.ToArray();
            }
        }

        /// <summary>
        /// Get total download count
        /// </summary>
        /// <param name="name">Name of resource pack (can be with extension)</param>
        public static int GetDownloadCount(string name)
        {
            int count = 0;

            if (!name.EndsWith(".zip"))
                name = name + ".zip";

            name = Path.Combine(ServerSettings.ResourcePackFolder, name + ".downloads");

            if (File.Exists(name) && !int.TryParse(File.ReadAllText(name), out count))
                count = 0;

            return count;
        }

        #endregion

        public static string GetName(string address)
        {
            int rpi = address.LastIndexOf('/');
            if (rpi != -1)
                address = address.Substring(rpi + 1);
            if (address.EndsWith(".zip"))
                address = address.Substring(0, address.Length - ".zip".Length);
            return address;
        }

        public static bool IsLocalAddress(string address)
        {
            return address.StartsWith(string.Format("{0}:{1}", ServerSettings.ServerAddress, ServerSettings.ResourcePackPort));
        }

        public static string GetHash(string address)
        {
            if (IsLocalAddress(address))
            {
                string name = GetName(address);
                string path = Path.Combine(ServerSettings.ResourcePackFolder, name + ".zip");
                if (!File.Exists(path))
                    return null;
                return File.ReadAllBytes(path).CalculateSHA1HashToString();
            }
            else
                throw new NotImplementedException("Remote resource pack hash");
        }

        #region Present

        public static bool IsResourcePackPresentByName(string name)
        {
            return File.Exists(Path.Combine(ServerSettings.ResourcePackFolder, name + ".zip"));
        }

        public static bool IsResourcePackPresentByAddress(string address)
        {
            if (IsLocalAddress(address))
                return IsResourcePackPresentByName(GetName(address));
            else
                throw new NotImplementedException("Remote resourcepack present");
        }

        #endregion


        //TODO remote resource-packs
        //TODO local cache

    }
}
﻿using System;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;
using CxServer.Settings;
using System.Collections.Generic;

namespace CxServer.ResourcePacks
{
    /// <summary>
    /// Server for downloading <a href="http://minecraft.gamepedia.com/Resource_pack">ResourcePacks</a>
    /// </summary>
    public class ResourcePackServer
    {
        /// <summary>
        /// The client.
        /// </summary>
        public TcpClient Client;

        /// <summary>
        /// Initializes a new instance of the <see cref="CxServer.ResourcePacks.ResourcePackServer"/> class.
        /// </summary>
        /// <param name="client">Client.</param>
        public ResourcePackServer(TcpClient client)
        {
            this.Client = client;
        }

        private Thread _thread;

        /// <summary>
        /// Start this instance on it's own thread.
        /// </summary>
        public void Start()
        {
            _thread = new Thread(() =>
            {
                try
                {
                    var stream = Client.GetStream();
                    var reader = new StreamReader(stream);
                    string line = reader.ReadLine();

                    if (string.IsNullOrEmpty(line) || !line.StartsWith("GET "))
                    {
                        SendInvalid(stream);
                        return;
                    }
                    line = line.Substring("GET ".Length);
                    if (string.IsNullOrEmpty(line))
                    {
                        SendInvalid(stream);
                        return;
                    }

                    int lindex = line.LastIndexOf(' ');
                    if (lindex == -1)
                    {
                        SendInvalid(stream);
                        return;
                    }
                    line = line.Substring(0, lindex);

                    if (line[0] == '/')
                        line = line.Substring(1);
                    if (string.IsNullOrEmpty(line))
                    {
                        SendResourcePackList(stream);
                        return;
                    }


                    if (!Regex.IsMatch(line, ServerSettings.ResourcePackRegex))
                    {
                        SendInvalid(stream);
                        return;
                    }

                    string path = Path.Combine(ServerSettings.ResourcePackFolder, line);

                    if (File.Exists(path))
                    {
                        using (var fs = File.Open(path, FileMode.Open))
                        {
                            SendResourcePack(stream, line, fs);
                            fs.Close();
                        }
                    }
                    else
                        SendNotFound(stream);

                }
                catch
                {
                    if (Client.Connected)
                        Client.Close();
                }
                finally
                {
                    if (Client.Connected)
                        Client.Close();
                }
            });
            _thread.Start();
        }

        #region Outputs

        private static readonly byte[] bytesInvalid = Encoding.UTF8.GetBytes("<!DOCTYPE html><html><head><meta charset=\"UTF-8\"><title>Invalid Request</title></head><body>Sorry but your HTTP Request is not valid</body></html>");

        private static void SendInvalid(Stream stream)
        {
            using (var writer = new StreamWriter(stream))
            {
                writer.WriteLine("HTTP/1.0 403 Invalid Request");
                writer.WriteLine("Date: " + DateTime.Now.ToString("R"));
                writer.WriteLine("Server: CxMinecraftServer - ResourcePack Downloader");
                writer.WriteLine("Connection: Close");
                writer.WriteLine("Content-Type: text/html; charset=utf-8");
                writer.WriteLine("Content-Length: " + bytesInvalid.Length);
                writer.WriteLine();

                writer.Flush();

                BinaryWriter bw = new BinaryWriter(writer.BaseStream);
                bw.Write(bytesInvalid);
                bw.Flush();
            }
        }

        private static readonly byte[] bytesNotFound = Encoding.UTF8.GetBytes("<!DOCTYPE html><html><head><meta charset=\"UTF-8\"><title>File Not Found</title></head><body>Sorry but we were unable to find your file</body></html>");

        private static void SendNotFound(Stream stream)
        {
            using (var writer = new StreamWriter(stream))
            {
                writer.WriteLine("HTTP/1.0 404 File Not Found");
                writer.WriteLine("Date: " + DateTime.Now.ToString("R"));
                writer.WriteLine("Server: CxMinecraftServer - ResourcePack Downloader");
                writer.WriteLine("Connection: Close");
                writer.WriteLine("Content-Type: text/html; charset=utf-8");
                writer.WriteLine("Content-Length: " + bytesNotFound.Length);
                writer.WriteLine();

                writer.Flush();

                BinaryWriter bw = new BinaryWriter(writer.BaseStream);
                bw.Write(bytesNotFound);
                bw.Flush();
            }
        }

        private static readonly byte[] bytesResourcePackListStart = Encoding.UTF8.GetBytes("<!DOCTYPE html><html><head><meta charset=\"UTF-8\"><title>ResourcePacks List</title></head><body>List of all availible Resource Packs<ul>");
        private static readonly byte[] bytesResourcePackListEnd = Encoding.UTF8.GetBytes("</ul></body></html>");
        private static readonly string stringResourcePackListItem = "<li><a href=\"{0}\">{1}</a> ({2})</li>";

        private static void SendResourcePackList(Stream stream)
        {
            string[] rps = ResourcePackUtils.AllResourcePacks;
            byte[][] rpb = new byte[rps.Length][];

            int length = bytesResourcePackListStart.Length + bytesResourcePackListEnd.Length;

            for (int i = 0; i < rps.Length; i++)
            {
                var bts = Encoding.UTF8.GetBytes(string.Format(stringResourcePackListItem, rps[i], Path.GetFileNameWithoutExtension(rps[i]), ResourcePackUtils.GetDownloadCount(rps[i])));
                length += bts.Length;
                rpb[i] = bts;
            }

            using (var writer = new StreamWriter(stream))
            {
                writer.WriteLine("HTTP/1.0 200 OK");
                writer.WriteLine("Date: " + DateTime.Now.ToString("R"));
                writer.WriteLine("Server: CxMinecraftServer - ResourcePack Downloader");
                writer.WriteLine("Connection: Close");
                writer.WriteLine("Content-Type: text/html; charset=utf-8");
                writer.WriteLine("Content-Length: " + length);
                writer.WriteLine();

                writer.Flush();

                BinaryWriter bw = new BinaryWriter(writer.BaseStream);
                bw.Write(bytesResourcePackListStart);
                foreach (byte[] rp in rpb)
                    bw.Write(rp);
                bw.Write(bytesResourcePackListEnd);
                bw.Flush();
            }
        }

        private static void SendResourcePack(Stream stream, string name, FileStream rp_fs)
        {
            using (var writer = new StreamWriter(stream))
            {
                writer.WriteLine("HTTP/1.0 200 OK");
                writer.WriteLine("Date: " + DateTime.Now.ToString("R"));
                writer.WriteLine("Server: CxMinecraftServer - ResourcePack Downloader");
                writer.WriteLine("Connection: Close");
                writer.WriteLine("Content-Type: application/zip");
                writer.WriteLine("Content-Length: " + rp_fs.Length);
                writer.WriteLine();

                writer.Flush();

                rp_fs.CopyTo(writer.BaseStream);
            }

            string path = Path.Combine(ServerSettings.ResourcePackFolder, name + ".downloads");



            File.WriteAllText(path, (ResourcePackUtils.GetDownloadCount(name) + 1).ToString());
        }

        #endregion

        #region Standalone

        /// <summary>
        /// Starts standalone server.
        /// </summary>
        public static void StartStandalone()
        {
            if (!Directory.Exists(ServerSettings.ResourcePackFolder))
                Directory.CreateDirectory(ServerSettings.ResourcePackFolder);

            Thread thread_resourcepacks = new Thread(() =>
            {
                TcpListener listener = new TcpListener(ServerSettings.ServerAddress, ServerSettings.ResourcePackPort);
                listener.Start();

                Console.WriteLine("ResourcePack Downloader hosted at address {0} and port {1}", ServerSettings.ServerAddress, ServerSettings.ResourcePackPort);

                while (true)
                {
                    TcpClient client = listener.AcceptTcpClient();
                    new ResourcePackServer(client).Start();
                }
            });
            thread_resourcepacks.Start();
        }

        #endregion
    }
}